<?php

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    /**************************/
    /****** PANEL ROUTES ******/
    /**************************/
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    /**************************/
    /***** COUNTRY ROUTES *****/
    /**************************/
    Route::get('country/states/{country}', 'ClientsController@getStates');
    Route::get('state/cities/{state}', 'ClientsController@getCities');

    /**************************/
    /****** TASKS ROUTES ******/
    /**************************/
    Route::get('tasks', 'TasksController@index')->name('tasks');

    Route::get('tasks/create', 'TasksController@create')->name('taskCreate');
    Route::post('tasks/store', 'TasksController@store')->name('taskStore');

    Route::get('tasks/edit/{task}', 'TasksController@edit')->name('taskEdit');
    Route::post('tasks/update/{task}', 'TasksController@update')->name('taskUpdate');

    Route::post('tasks/delete/{task}', 'TasksController@delete')->name('taskDelete');

    /**************************/
    /****** MAILS ROUTES ******/
    /**************************/
    Route::post('mails/send', 'MailsController@quickSend')->name('quickSendMail');

    /**************************/
    /**** SCHEDULES ROUTES ****/
    /**************************/
    Route::get('schedules', 'SchedulesController@index')->name('schedules');

    Route::get('schedules/create', 'SchedulesController@create')->name('scheduleCreate');
    Route::post('schedules/store', 'SchedulesController@store')->name('scheduleStore');

    Route::get('schedules/view/{schedule}', 'SchedulesController@view')->name('scheduleView');
    Route::post('schedules/update/{schedule}', 'SchedulesController@update')->name('scheduleUpdate');
    Route::post('schedules/delete/{schedule}', 'SchedulesController@delete')->name('scheduleDelete');

    /*************************/
    /**** MESSAGES ROUTES ****/
    /*************************/
    Route::get('mails/reponse/{mail}', 'MailsController@formMail')->name('responseMail');
    Route::get('clients/send/{client}', 'ClientsController@formClientMail')->name('responseClientMail');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer']], function () {
    /**************************/
    /****** EVENTS ROUTES *****/
    /**************************/
    Route::get('events', 'EventsController@index')->name('events');// NO ESTA EN USO

    Route::get('events/search', 'EventsController@search')->name('eventSearch');// NO ESTA EN USO
    Route::post('events/delete/{event}', 'EventsController@delete')->name('eventDelete'); // NO ESTA EN USO
    /**************************/
    /***** WAYS ROUTES *****/
    /**************************/
    Route::get('acquisition/ways', 'ContactWaysController@index')->name('contactWays');
    Route::get('acquisition/ways/search', 'ContactWaysController@search')->name('contactWaysSearch');

    Route::get('acquisition/ways/create', 'ContactWaysController@create')->name('contactWaysCreate');
    Route::post('acquisition/ways/store', 'ContactWaysController@store')->name('contactWaysStore');

    Route::get('acquisition/ways/edit/{way}', 'ContactWaysController@edit')->name('contactWaysEdit');
    Route::post('acquisition/ways/update/{way}', 'ContactWaysController@update')->name('contactWaysUpdate');

    Route::post('acquisition/ways/delete/{way}', 'ContactWaysController@delete')->name('contactWaysDelete');

    /*******************************/
    /***** MAIN SOURCES ROUTES *****/
    /*******************************/
    Route::get('acquisition/sources/main', 'AcquisitionsMainSourcesController@index')->name('acquisitionMainSources');
    Route::get('acquisition/sources/main/search', 'AcquisitionsMainSourcesController@search')->name('acquisitionMainSourcesSearch');

    Route::get('acquisition/sources/main/create', 'AcquisitionsMainSourcesController@create')->name('acquisitionMainSourcesCreate');
    Route::post('acquisition/sources/main/store', 'AcquisitionsMainSourcesController@store')->name('acquisitionMainSourcesStore');

    Route::get('acquisition/sources/main/edit/{mainSource}', 'AcquisitionsMainSourcesController@edit')->name('acquisitionMainSourcesEdit');
    Route::post('acquisition/sources/main/update/{mainSource}', 'AcquisitionsMainSourcesController@update')->name('acquisitionMainSourcesUpdate');

    Route::post('acquisition/sources/main/delete/{mainSource}', 'AcquisitionsMainSourcesController@delete')->name('acquisitionMainSourcesDelete');

    /************************************/
    /***** SECONDARY SOURCES ROUTES *****/
    /************************************/
    Route::get('acquisition/sources/secondary', 'AcquisitionsSecondarySourcesController@index')->name('acquisitionSecondarySources');
    Route::get('acquisition/sources/secondary/search', 'AcquisitionsSecondarySourcesController@search')->name('acquisitionSecondarySourcesSearch');

    Route::get('acquisition/sources/secondary/create', 'AcquisitionsSecondarySourcesController@create')->name('acquisitionSecondarySourcesCreate');
    Route::post('acquisition/sources/secondary/store', 'AcquisitionsSecondarySourcesController@store')->name('acquisitionSecondarySourcesStore');

    Route::get('acquisition/sources/secondary/edit/{source}', 'AcquisitionsSecondarySourcesController@edit')->name('acquisitionSecondarySourcesEdit');
    Route::post('acquisition/sources/secondary/update/{source}', 'AcquisitionsSecondarySourcesController@update')->name('acquisitionSecondarySourcesUpdate');

    Route::post('acquisition/sources/secondary/delete/{source}', 'AcquisitionsSecondarySourcesController@delete')->name('acquisitionSecondarySourcesDelete');

    /**************************/
    /***** MEDIUMS ROUTES *****/
    /**************************/
    Route::get('acquisition/mediums', 'ContactMediumsController@index')->name('contactMediums');
    Route::get('acquisition/mediums/search', 'ContactMediumsController@search')->name('contactMediumsSearch');

    Route::get('acquisition/mediums/create', 'ContactMediumsController@create')->name('contactMediumsCreate');
    Route::post('acquisition/mediums/store', 'ContactMediumsController@store')->name('contactMediumsStore');

    Route::get('acquisition/mediums/edit/{medium}', 'ContactMediumsController@edit')->name('contactMediumsEdit');
    Route::post('acquisition/mediums/update/{medium}', 'ContactMediumsController@update')->name('contactMediumsUpdate');

    Route::post('acquisition/mediums/delete/{medium}', 'ContactMediumsController@delete')->name('contactMediumsDelete');

    /*******************************/
    /***** MAIN MEDIUMS ROUTES *****/
    /*******************************/
    Route::get('acquisition/mediums/main', 'AcquisitionsMainMediumsController@index')->name('acquisitionMainMediums');
    Route::get('acquisition/mediums/main/search', 'AcquisitionsMainMediumsController@search')->name('acquisitionMainMediumsSearch');

    Route::get('acquisition/mediums/main/create', 'AcquisitionsMainMediumsController@create')->name('acquisitionMainMediumsCreate');
    Route::post('acquisition/mediums/main/store', 'AcquisitionsMainMediumsController@store')->name('acquisitionMainMediumsStore');

    Route::get('acquisition/mediums/main/edit/{mainSource}', 'AcquisitionsMainMediumsController@edit')->name('acquisitionMainMediumsEdit');
    Route::post('acquisition/mediums/main/update/{mainSource}', 'AcquisitionsMainMediumsController@update')->name('acquisitionMainMediumsUpdate');

    Route::post('acquisition/mediums/main/delete/{mainSource}', 'AcquisitionsMainMediumsController@delete')->name('acquisitionMainMediumsDelete');

    /************************************/
    /***** SECONDARY MEDIUMS ROUTES *****/
    /************************************/
    Route::get('acquisition/mediums/secondary', 'AcquisitionsSecondaryMediumsController@index')->name('acquisitionSecondaryMediums');
    Route::get('acquisition/mediums/secondary/search', 'AcquisitionsSecondaryMediumsController@search')->name('acquisitionSecondaryMediumsSearch');

    Route::get('acquisition/mediums/secondary/create', 'AcquisitionsSecondaryMediumsController@create')->name('acquisitionSecondaryMediumsCreate');
    Route::post('acquisition/mediums/secondary/store', 'AcquisitionsSecondaryMediumsController@store')->name('acquisitionSecondaryMediumsStore');

    Route::get('acquisition/mediums/secondary/edit/{source}', 'AcquisitionsSecondaryMediumsController@edit')->name('acquisitionSecondaryMediumsEdit');
    Route::post('acquisition/mediums/secondary/update/{source}', 'AcquisitionsSecondaryMediumsController@update')->name('acquisitionSecondaryMediumsUpdate');

    Route::post('acquisition/mediums/secondary/delete/{source}', 'AcquisitionsSecondaryMediumsController@delete')->name('acquisitionSecondaryMediumsDelete');

    /***************************/
    /****** ASSIST USERS *******/
    /***************************/
    Route::get('users/assist/{user}', 'UsersController@assist')->name('assist');
    Route::post('users/assist/update/{user}', 'UsersController@assistUpdate')->name('assistUpdate');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|Referror']], function () {
    /**************************/
    /**** FOLLOW UPS ROUTES ***/
    /**************************/
    Route::get('followups', 'FollowUpsController@index')->name('followups');
    Route::get('followups/search', 'FollowUpsController@search')->name('followupSearch');

    Route::get('followups/view/{followup}', 'FollowUpsController@view')->name('followupView');
    /**************************/
    /**** FOLLOW UPS ROUTES ***/
    /**************************/
    Route::post('followups/update/{followup}', 'FollowUpsController@update')->name('followupUpdate');
    Route::post('followups/delete/{followup}', 'FollowUpsController@delete')->name('followupDelete');

    Route::post('followups/open/{followup}/{route}/{client}', 'FollowUpsController@open')->name('followupStatusOpen');
    Route::post('followups/process/{followup}/{route}/{client}', 'FollowUpsController@process')->name('followupStatusProcess');
    Route::post('followups/close/{followup}/{route}/{client}', 'FollowUpsController@close')->name('followupStatusClose');

    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/
    Route::get('clients/mails/{client}', 'ClientsController@formMail')->name('clientMail');
    Route::post('clients/sends/{client}', 'ClientsController@sendMail')->name('clientSend');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Sales Agent']], function () {
    /**************************/
    /**** FOLLOW UPS ROUTES ***/
    /**************************/
    Route::get('followups/share/{agent}/{followup}', 'FollowUpsController@share')->name('followupShare');
    Route::post('followups/share/store/{followup}', 'FollowUpsController@shareStore')->name('followupShareStore');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager']], function () {

    /**************************/
    /* USERS ROUTES ***/
    /**************************/
    Route::get('users', 'UsersController@index')->name('users');
    Route::get('users/search', 'UsersController@search')->name('userSearch');

    Route::get('users/create', 'UsersController@create')->name('userCreate');
    Route::post('users/store', 'UsersController@store')->name('userStore');

    Route::get('users/view/{user}', 'UsersController@view')->name('userView');
    Route::post('users/update/{user}', 'UsersController@update')->name('userUpdate');

    Route::post('users/delete/{user}', 'UsersController@delete')->name('userDelete');

    Route::post('users/active/{user}', 'UsersController@active')->name('userActive');
    Route::post('users/deactivate/{user}', 'UsersController@deactivate')->name('userDeactivate');

    /**************************/
    /**** MARKETING ROUTES ****/
    /**************************/
    Route::get('marketing', 'MarketingInformationController@index')->name('marketing');

    /***************************/
    /**** RE-ASSIGN CLIENTS ****/
    /***************************/
    Route::get('clients/reassign/{client}', 'ClientsController@reassign')->name('reassign');
    Route::post('clients/reassign/update/{client}', 'ClientsController@reassignUpdate')->name('reassignUpdate');

});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant|Personal Assistant|Referror']], function () {
    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/
    Route::get('clients', 'ClientsController@index')->name('clients');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Referror|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/
    Route::get('clients/view/{client}', 'ClientsController@view')->name('clientView');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/
    Route::post('clients/update/{client}', 'ClientsController@update')->name('clientUpdate');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant|Personal Assistant|Referror']], function () {
    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/
    Route::get('clients/search', 'ClientsController@search')->name('clientSearch');

    Route::post('clients/verify/{email}/{name}', 'ClientsController@verifyEmail')->name('verifyEmail');

    Route::get('clients/create/step_one', 'ClientsController@create')->name('clientCreate');
    Route::post('clients/create/step_one', 'ClientsController@saveDataStepOne')->name('saveDataStepOne');

    Route::get('clients/create/step_two', 'ClientsController@createStepTwo')->name('createStepTwo');
    Route::post('clients/create/step_two', 'ClientsController@saveDataStepTwo')->name('saveDataStepTwo');

    Route::get('clients/create/step_three', 'ClientsController@createStepThree')->name('createStepThree');
    Route::post('clients/create/step_three', 'ClientsController@saveDataStepThree')->name('saveDataStepThree');

    Route::get('clients/create/step_four', 'ClientsController@createStepFour')->name('createStepFour');
    Route::post('clients/store', 'ClientsController@store')->name('clientStore');


    /********************************/
    /***** CREATE SELECT ROUTES *****/
    /********************************/
    Route::get('agency/agents/{agency}', 'ClientsController@getAgents');
    Route::get('sources/main/{source}', 'ClientsController@getMainSources');
    Route::get('mediums/main/{source}', 'ClientsController@getMainMediums');
    Route::get('sources/secondary/{mainSource}/{idSource}', 'ClientsController@getSecondarySources');
    Route::get('mediums/secondary/{mainSource}/{idSource}', 'ClientsController@getSecondaryMediums');

    /********************************/
    /****** EDIT SELECT ROUTES ******/
    /********************************/
    Route::get('agencies/agency', 'ClientsController@getAgencies');
    Route::get('sources', 'ClientsController@getSources');

    /***************************/
    /****** GLOBAL ROUTES ******/
    /***************************/
    Route::get('source/create/{source}', 'GlobalController@create')->name('getForm');
    Route::get('source/sub/create/{source}/{sourceId}', 'GlobalController@subCreate')->name('getSubForm');
    Route::post('source/store/{source}', 'GlobalController@store')->name('sourceStore');
    Route::post('source/sub/store/{source}', 'GlobalController@subStore')->name('sourceSubStore');

    /**************************/
    /***** AGENCIES ROUTES ****/
    /**************************/
    Route::get('agencies/create', 'AgenciesController@create')->name('agencyCreate');
    Route::post('agencies/store', 'AgenciesController@store')->name('agencyStore');

    /**************************/
    /****** AGENTS ROUTES *****/
    /**************************/
    Route::get('agents/create/{agency}', 'AgentsController@create')->name('agentCreate');
    Route::post('agents/store', 'AgentsController@store')->name('agentStore');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Sales Agent']], function () {
    /**************************/
    /***** CLIENTS ROUTES *****/
    /**************************/

    Route::post('clients/delete/{client}', 'ClientsController@delete')->name('clientDelete');

    Route::get('clients/export', 'ClientsController@export')->name('clientsExport');
    Route::get('clients/import/form', 'ClientsController@formImport')->name('clientsFormImport');
    Route::post('clients/import', 'ClientsController@import')->name('clientsImport');

});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer']], function () {
    /*****************************/
    /***** PROPERTIES ROUTES *****/
    /*****************************/

    Route::get('properties/export', 'PropertiesController@export')->name('propertiesExport');
    Route::get('properties/import/form', 'PropertiesController@formImport')->name('propertiesFormImport');
    Route::post('properties/import', 'PropertiesController@import')->name('propertiesImport');
    Route::get('properties/images/download/{property}', 'PropertiesController@imagesDownload')->name('propertiesImagesDownload');

});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Sales Agent']], function () {
    /**************************/
    /***** AGENCIES ROUTES ****/
    /**************************/
    Route::get('agencies', 'AgenciesController@index')->name('agencies');
    Route::get('agencies/search', 'AgenciesController@search')->name('agencySearch');

    Route::get('agencies/view/{agency}', 'AgenciesController@view')->name('agencyView');
    Route::post('agencies/update/{agency}', 'AgenciesController@update')->name('agencyUpdate');

    Route::post('agencies/delete/{agency}', 'AgenciesController@delete')->name('agencyDelete');

    /**************************/
    /****** AGENTS ROUTES *****/
    /**************************/
    Route::get('agents', 'AgentsController@index')->name('agents');
    Route::get('agents/search', 'AgentsController@search')->name('agentSearch');
    Route::get('agents/create', 'AgentsController@singleCreate')->name('agentSingleCreate');

    Route::get('agents/view/{agent}', 'AgentsController@view')->name('agentView');
    Route::post('agents/update/{agent}', 'AgentsController@update')->name('agentUpdate');

    Route::post('agents/delete/{agent}', 'AgentsController@delete')->name('agentDelete');

});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /***** BINNACLE ROUTES ****/
    /**************************/
    Route::get('visits', 'VisitsController@index')->name('visits');
    Route::get('visits/search', 'VisitsController@search')->name('visitSearch');

    Route::get('visits/create', 'VisitsController@create')->name('visitCreate');
    Route::post('visits/store', 'VisitsController@store')->name('visitStore');

    Route::get('visits/view/{visit}', 'VisitsController@view')->name('visitView');
    Route::post('visits/update/{visit}', 'VisitsController@update')->name('visitUpdate');

    Route::post('visits/delete/{visit}', 'VisitsController@delete')->name('visitDelete');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant']], function () {
    /**************************/
    /**** REFERRORS ROUTES ****/
    /**************************/
    Route::get('referrors', 'ReferrorsController@index')->name('referrors');
    Route::get('referrors/search', 'ReferrorsController@search')->name('referrorSearch');

    Route::get('referrors/view/{referror}', 'ReferrorsController@view')->name('referrorView');
    Route::post('referrors/update/{referror}', 'ReferrorsController@update')->name('referrorUpdate');
    Route::post('referrors/delete/{referror}', 'ReferrorsController@delete')->name('referrorDelete');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /**** REFERRORS ROUTES ****/
    /**************************/
    Route::get('referrors/create', 'ReferrorsController@create')->name('referrorCreate');
    Route::post('referrors/store', 'ReferrorsController@store')->name('referrorStore');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|General Assistant']], function () {
    /**************************/
    /** SALES OFFICES ROUTES **/
    /**************************/
    Route::get('offices', 'SalesOfficesController@index')->name('offices');
    Route::get('offices/search', 'SalesOfficesController@search')->name('officeSearch');

    Route::get('offices/view/{office}', 'SalesOfficesController@view')->name('officeView');
    Route::post('offices/update/{office}', 'SalesOfficesController@update')->name('officeUpdate');

    Route::post('offices/delete/{office}', 'SalesOfficesController@delete')->name('officeDelete');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|General Assistant']], function () {
    /**************************/
    /** SALES OFFICES ROUTES **/
    /**************************/
    Route::get('offices/create', 'SalesOfficesController@create')->name('officeCreate');
    Route::post('offices/store', 'SalesOfficesController@store')->name('officeStore');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Sales Agent']], function () {
    /**************************/
    /****** AGENTS ROUTES *****/
    /**************************/
    Route::post('agents/store/single', 'AgentsController@singleStore')->name('agentSingleStore');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /**** REMINDERS ROUTES ****/
    /**************************/
    Route::get('reminders/{type}', 'RemindersController@index')->name('reminders');

    Route::get('reminders/create/{type}/{client}', 'RemindersController@create')->name('reminderCreate');
    Route::post('reminders/store/{client}', 'RemindersController@store')->name('reminderStore');

    Route::get('reminders/edit/{type}/{client}/{reminder}', 'RemindersController@edit')->name('reminderEdit');
    Route::post('reminders/update/{type}/{reminder}', 'RemindersController@update')->name('reminderUpdate');

    Route::post('reminders/delete/{reminder}', 'RemindersController@delete')->name('reminderDelete');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|Sales Agent|General Assistant|Personal Assistant']], function () {
    /**************************/
    /**** COMMENTS ROUTES *****/
    /**************************/
    Route::post('comments/update/{comment}', 'CommentsController@update')->name('commentUpdate');
    Route::post('comments/delete/{comment}', 'CommentsController@delete')->name('commentDelete');

});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'role:Administrator|Developer|Broker|Office Manager|General Assistant']], function () {
    /***************************/
    /**** PROPERTIES ROUTES ****/
    /***************************/
    Route::get('properties', 'PropertiesController@index')->name('properties');
    Route::get('properties/search', 'PropertiesController@search')->name('propertiesSearch');

    Route::get('properties/create', 'PropertiesController@create')->name('propertyCreate');
    Route::post('properties/store', 'PropertiesController@store')->name('propertyStore');
    Route::get('properties/view/{property}', 'PropertiesController@view')->name('propertyView');
    Route::post('properties/update/{property}', 'PropertiesController@update')->name('propertyUpdate');

    Route::post('properties/delete/{property}', 'PropertiesController@delete')->name('propertyDelete');
    Route::post('properties/file/delete/{property}', 'PropertiesController@deleteFile')->name('propertyDeleteFile');

    /*************************************/
    /**** PROPERTIES AMENITIES ROUTES ****/
    /*************************************/
    Route::get('properties/amenities', 'PropertiesAmenitiesController@index')->name('propertiesAmenities');
    Route::get('properties/amenities/search', 'PropertiesAmenitiesController@search')->name('propertiesAmenitiesSearch');

    Route::get('properties/amenities/create', 'PropertiesAmenitiesController@create')->name('propertiesAmenityCreate');
    Route::post('properties/amenities/store', 'PropertiesAmenitiesController@store')->name('propertiesAmenityStore');
    Route::get('properties/amenities/view/{amenity}', 'PropertiesAmenitiesController@view')->name('propertiesAmenityView');
    Route::post('properties/amenities/update/{amenity}', 'PropertiesAmenitiesController@update')->name('propertiesAmenityUpdate');

    Route::post('properties/amenities/delete/{amenity}', 'PropertiesAmenitiesController@delete')->name('propertiesAmenityDelete');

    /****************************************/
    /**** PROPERTIES DEVELOPMENTS ROUTES ****/
    /****************************************/
    Route::get('properties/developments', 'PropertiesDevelopmentsController@index')->name('propertiesDevelopments');
    Route::get('properties/developments/search', 'PropertiesDevelopmentsController@search')->name('propertiesDevelopmentsSearch');

    Route::get('properties/developments/create', 'PropertiesDevelopmentsController@create')->name('propertiesDevelopmentCreate');
    Route::post('properties/developments/store', 'PropertiesDevelopmentsController@store')->name('propertiesDevelopmentStore');
    Route::get('properties/developments/view/{development}', 'PropertiesDevelopmentsController@view')->name('propertiesDevelopmentView');
    Route::post('properties/developments/update/{development}', 'PropertiesDevelopmentsController@update')->name('propertiesDevelopmentUpdate');

    Route::post('properties/developments/delete/{development}', 'PropertiesDevelopmentsController@delete')->name('propertiesDevelopmentDelete');

    /*************************************/
    /**** PROPERTIES LOCATIONS ROUTES ****/
    /*************************************/
    Route::get('properties/locations', 'PropertiesLocationsController@index')->name('propertiesLocations');
    Route::get('properties/locations/search', 'PropertiesLocationsController@search')->name('propertiesLocationsSearch');

    Route::get('properties/locations/create', 'PropertiesLocationsController@create')->name('propertiesLocationCreate');
    Route::post('properties/locations/store', 'PropertiesLocationsController@store')->name('propertiesLocationStore');
    Route::get('properties/locations/view/{location}', 'PropertiesLocationsController@view')->name('propertiesLocationView');
    Route::post('properties/locations/update/{location}', 'PropertiesLocationsController@update')->name('propertiesLocationUpdate');

    Route::post('properties/locations/delete/{location}', 'PropertiesLocationsController@delete')->name('propertiesLocationDelete');

    /*********************************/
    /**** PROPERTIES TYPES ROUTES ****/
    /*********************************/
    Route::get('properties/types', 'PropertiesTypesController@index')->name('propertiesTypes');
    Route::get('properties/types/search', 'PropertiesTypesController@search')->name('propertiesTypesSearch');

    Route::get('properties/types/create', 'PropertiesTypesController@create')->name('propertiesTypeCreate');
    Route::post('properties/types/store', 'PropertiesTypesController@store')->name('propertiesTypeStore');
    Route::get('properties/types/view/{type}', 'PropertiesTypesController@view')->name('propertiesTypeView');
    Route::post('properties/types/update/{type}', 'PropertiesTypesController@update')->name('propertiesTypeUpdate');

    Route::post('properties/types/delete/{type}', 'PropertiesTypesController@delete')->name('propertiesTypeDelete');

    /**********************************/
    /**** PROPERTIES STYLES ROUTES ****/
    /**********************************/
    Route::get('properties/styles', 'PropertiesStylesController@index')->name('propertiesStyles');
    Route::get('properties/styles/search', 'PropertiesStylesController@search')->name('propertiesStylesSearch');

    Route::get('properties/styles/create', 'PropertiesStylesController@create')->name('propertiesStyleCreate');
    Route::post('properties/styles/store', 'PropertiesStylesController@store')->name('propertiesStyleStore');
    Route::get('properties/styles/view/{style}', 'PropertiesStylesController@view')->name('propertiesStyleView');
    Route::post('properties/styles/update/{style}', 'PropertiesStylesController@update')->name('propertiesStyleUpdate');

    Route::post('properties/styles/delete/{style}', 'PropertiesStylesController@delete')->name('propertiesStyleDelete');
});