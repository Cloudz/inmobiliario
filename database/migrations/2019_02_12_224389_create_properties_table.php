<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->text('baths')->nullable();
            $table->text('beds')->nullable();
            $table->text('parking')->nullable();
            $table->text('m2')->nullable();
            $table->text('ft2')->nullable();
            $table->text('type')->nullable();
            $table->integer('type_id')->index()->unsigned();
            $table->integer('development_id')->index()->unsigned();
            $table->integer('location_id')->index()->unsigned();
            $table->integer('style_id')->index()->unsigned();
            $table->text('files')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
