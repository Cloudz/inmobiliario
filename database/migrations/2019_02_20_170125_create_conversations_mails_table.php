<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->text('subject')->nullable();
            $table->text('sender')->nullable();
            $table->text('htmlBody')->nullable();
            $table->text('textBody')->nullable();
            $table->text('recipient')->nullable();
            $table->text('attachments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations_mails');
    }
}
