$(function () {
    /************************/
    /** show flash message **/
    /************************/
    $(document).on('click', ".delete-trigger", function (e) {
        e.preventDefault();
        var url   = $(this).prop('href');
        var modal = $("#delete_confirmation");
        modal.find('form').prop('action', url);

        $('[data-remodal-id=modal_confirmation]').remodal().open();
        $('[data-remodal-id=modal_confirmation]').html(modal.html());
        return false;
    });
    $(document).on('click', '.delete_confirmation_cancel', function () {
        $('[data-remodal-id=modal_confirmation]').remodal().close();
        $('[data-remodal-id=modal_verify_data]').remodal().close();
        $('[data-remodal-id=modal_form]').remodal().close();
        $('[data-remodal-id=modal_create]').remodal().close();
    });
    $('.btn-accept-register').on('click', function(){
        $('[data-remodal-id=modal_verify_data]').remodal().close();
    });
    $(document).on('click', ".show-information", function (e) {
        e.preventDefault();
        var title   = $(this).data('title');
        var content = $(this).data('content');

        $('[data-remodal-id=modal_information]').remodal().open();
        $('[data-remodal-id=modal_information]').html('<div class="col-md-12"><div class="box box-primary" style="text-align: center; border:0"><div class=" box-body"><h3>'+title+'</h3></div><br><div class="box-footer">'+content+'</div></div></div></div>');
        return false;
    });
    $(document).on('click', ".edit-trigger", function (e) {
        e.preventDefault();

        var route   = $(this).data('route');
        var csrf    = $("input[name=_token]").val();
        var comment = $(this).data('comment');
        var source  = $(this).data('source');
        var url     = $(this).data('url');

        $('[data-remodal-id=modal_form]').remodal().open();
        $('[data-remodal-id=modal_form]').html('<div class="col-md-12"><div class="box box-primary" style="text-align: center; border:0"><div class=" box-body"><h3>Editar Comentario</h3></div><br><div class="box-footer"><form id="form_update" action="'+route+'" method="post" class="ajax-form"><input type="hidden" name="_token" value="'+csrf+'"><input type="text" name="f_comment" class="form-control" style="margin-bottom: 30px" value="'+comment+'"><input type="hidden" name="f_url" value="'+url+'"><input type="hidden" name="f_source" value="'+source+'"><button type="button" class="btn delete_btn_modal delete_confirmation_cancel" data-remodal-action="cancel"> <i class="fa fa-times"></i>&nbsp;&nbsp;Cancelar</button><button type="submit" class="btn btn-info" style="margin-left:20px;padding:10px 20px;"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Enviar Información</button></form></div></div></div>');
        return false;
    });
    $(document).on('click', ".create-trigger", function (e) {
        e.preventDefault();

        var route  = $(this).data('route');
        var source = $(this).data('main-source');
        var url = route + "/source/create/" + source;

        $('[data-remodal-id=modal_create]').remodal().open();
        $.get( url, function( data ) {
            $('[data-remodal-id=modal_create]').html(data);
        });
        return false;
    });
    $(document).on('click', ".create-trigger-sub", function (e) {
        e.preventDefault();

        var route    = $(this).data('route');
        var source   = $(this).data('main-source');
        var sourceId = $(this).data('main-source-id');
        var url = route + "/source/sub/create/" + source + "/" + sourceId;

        $('[data-remodal-id=modal_create]').remodal().open();
        $.get( url, function( data ) {
            $('[data-remodal-id=modal_create]').html(data);
        });
        return false;
    });
    $(document).on('submit', '.delete_popup_form', function (e) {
        e.preventDefault();
        var form = $(this);
        var action = form.prop('action');
        var req = form.serialize();
        $.post(action, req, function (res) {
            if (res.success) {
                $('[data-remodal-id=modal_confirmation]').remodal().close();
                location.reload();
            }
        }, 'json');
    });
    /************************/
    /** flash message      **/
    /************************/
    if ($(".flash-msg").length > 0) {
        var _session_msg = $(".flash-msg");
        var _session_txt = _session_msg.html();

        $.bootstrapGrowl(_session_txt, {
            ele: 'body',
            type: 'null',
            offset: {from: 'top', amount: 40},
            align: 'right',
            width: 'auto',
            delay: 4500, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: false,
            stackup_spacing: 5
        });
    }
    /************************/
    /** ajax forms 2.0     **/
    /************************/
    var proccessing_form = false;
    $(document).on("submit", ".ajax-form", function (e) {
        e.preventDefault();
        var form = $(this);
        var action = $(this).attr("action");
        var redirect = $(this).data("redirect");
        var formData = new FormData(form[0]);

        if(!proccessing_form) {
            proccessing_form = true;
            $.ajax({
                url: action,
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: formData,
                success: function(res){
                    if(res.success){
                        if(res.redirect){
                            document.location = res.redirect;
                        }
                    }else if(res.errors){
                        var errElem = $('<ul></ul>');

                        for(var name in res.errors){
                            var errors = res.errors[name];
                            //errElem.append($('<li>' + name + ': ' + res.errors[name][0] + '</li>'));
                            errElem.append($('<li>' + res.errors[name][0] + '</li>'));
                        }
                        var errorTemplate = '<div class="alert alert-danger"><ul>' + errElem.html() + '</ul></div>';
                        $.bootstrapGrowl(errorTemplate, {
                            ele: 'body',
                            type: 'null', 
                            offset: {from: 'top', amount: 40},
                            align: 'right',
                            width: 'auto',
                            delay: 3000, 
                            allow_dismiss: true,
                            stackup_spacing: 5
                        });
                    }
                    proccessing_form = false;
                },
                error: function (data) {
                    $.bootstrapGrowl('Ha ocurrido un error, revise su conexion e intentelo nuevamente', {
                        ele: 'body',
                        type: 'info', // (null, 'info', 'danger', 'success')
                        offset: {from: 'top', amount: 40},
                        align: 'right',
                        width: 'auto',
                        delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                        allow_dismiss: false,
                        stackup_spacing: 5
                    });
                    proccessing_form = false;
                },
            });
        }

        return false;
    });

    function verifyEmail(trigger){
        $(trigger).on("click", function (e) {
            e.preventDefault();
            arrayVerifyEmail = [];
            arrayVerifyEmail = [{"main_email":$('#main_email').val(), "name":$('#name').val()}];
            var sendVal = JSON.stringify(arrayVerifyEmail);
            $url = '/admin/clients/verify/'+arrayVerifyEmail[0].main_email+'/'+arrayVerifyEmail[0].name;
            if(!proccessing_form) {
                proccessing_form = true;
                $.ajax({
                    url: $url,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: { valVerify: sendVal },
                    success: function(res){
                        var clientsData = [];
                        var htmlClient = [];
                        $(res.clients).each(function(index, value){
                            nameClient = value.name;
                            lastNameClient = value.last_name;
                            agentClient = value.agent;
                            existClient = value.exist;
                            dateClient  = value.created_at;
                            clientsData.push({
                                name:nameClient,
                                last_name:lastNameClient,
                                agent:agentClient,
                                exist:existClient,
                                date:dateClient
                            });
                            htmlClient.push("<tr role='row' class='"+clientsData[index].exist+"'><td style='width: 10%'>"+clientsData[index].name+"</td><td style='width: 10%'>"+clientsData[index].last_name+"</td><td style='width: 10%'>"+clientsData[index].agent+"</td><td style='width: 10%'>"+clientsData[index].date+"</td></tr>");
                        });
                        $('[data-remodal-id=modal_verify_data]').remodal().open();
                        $('[data-remodal-id=modal_verify_data]').find('.count_registers').html(res.clients.length);
                        $('[data-remodal-id=modal_verify_data]').find('.list_registers').html(htmlClient);
                        if(res.success){
                            $('.success-registers').show();
                            $('.error-registers').hide();
                            $('.next_verify').removeClass('bgLinkNext');
                            $('.next_verify').attr('disabled', false);
                        }else{
                            $('.error-registers').show();
                            $('.success-registers').hide();
                            $('.next_verify').addClass('bgLinkNext');
                            $('.next_verify').attr('disabled', true);
                        }
                        proccessing_form = false;
                    },
                    error: function (data) {
                        $.bootstrapGrowl('Ha ocurrido un error, revise su conexion e intentelo nuevamente', {
                            ele: 'body',
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 40},
                            align: 'right',
                            width: 'auto',
                            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: false,
                            stackup_spacing: 5
                        });
                        proccessing_form = false;
                    },
                });
            }

            return false;
        });
    }

    verifyEmail('.verify_client');

    /************************/
    /****  Tiny MCE      ****/
    /************************/
    function tinyMCE($selector){
        tinymce.init({
            selector: $selector,
            height: 250,
            menubar: false,
            branding: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });
    }

    tinyMCE('#f_message');

    /************************/
    /**   Bootstrap Tabs   **/
    /************************/
    if($(".tabs-menu-wrapper").length > 0){

        $(".tabs-menu-wrapper").each(function(){
            var tabs_wrapper = $(this);
            
            tabs_wrapper.find(".tabs-menu a").click(function (event) {
                var tab_index = $(this).parent().index();
                event.preventDefault();
                $(this).parent().siblings().removeClass("current");
                $(this).parent().addClass("current");
                tabs_wrapper.find(".tab-content").hide();
                tabs_wrapper.find('.tab-content').eq(tab_index).fadeIn();
                $('input').each(function(){
                    if($(this).is(':visible')){
                        if ($(this).attr('data-focusable')) {
                            $(this).attr('required', 'required');
                        }
                    }else{
                        if ($(this).attr('data-focusable')) {
                            $(this).removeAttr('required');
                        }
                    }
                })
            });

            var current_tab = tabs_wrapper.find(".tabs-menu li.current");
            if(current_tab){
                tabs_wrapper.find(".tab-content").hide();
                current_tab.find('a').trigger('click');
            }
        });
    }else{
        $(".tabs-menu a").click(function (event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
            $(document).on('nested:fieldRemoved', function (e) {
                $('[required]', e.field).removeAttr('required');
            });
        });
        var current_tab = $(".tabs-menu li.current");
        if(current_tab){
            $(".tab-content").css("display", "none");
            current_tab.find('a').trigger('click');
        }
    }

    /**********************************/
    /*** MAKE DATES DATETIMEPICKERS ***/
    /**********************************/

    $('#first_contact').datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
        showClose: true,
        maxDate: moment()
    });

    $('#date_start, #date_reminded, #date_visit').datetimepicker({
        locale: 'es',
        showClose: true
    });

    $('#start_at').datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD HH:mm:ss',
        showClose: true
    });

    $('#end_at').datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD HH:mm:ss',
        showClose: true
    });


    /**********************************/
    /*** SLIDER INTEREST ***/
    /**********************************/

    $('#slider').slider({
        value:$("#level_interest").val(),
        min:1,
        max:4,
        step:1,
        slide:function(event, ui){
            var status;
            switch(ui.value){
                case 1:
                status = $("#level_interest").data('interest-one');
                break;
                case 2:
                status = $("#level_interest").data('interest-two');
                break;
                case 3:
                status = $("#level_interest").data('interest-three');
                break;
                case 4:
                status = $("#level_interest").data('interest-four');
                break;
            }
            $("#levelInterestLabel").text(status);
            $("#level_interest").val(ui.value);
        }
    });
    $("#level_interest").val( $("#slider").slider("value") );
    switch($("#level_interest").val()){
        case '1':
            $("#levelInterestLabel").text($(this).data('interest-one'));
        break;
        case '2':
            $("#levelInterestLabel").text($(this).data('interest-two'));
        break;
        case '3':
            $("#levelInterestLabel").text($(this).data('interest-three'));
        break;
        case '4':
            $("#levelInterestLabel").text($(this).data('interest-four'));
        break;
    }

    /**********************************/
    /*** FUNCTIONS ADMINLTE ***/
    /**********************************/
    // Make the dashboard widgets sortable Using jquery UI
    $('.connectedSortable').sortable({
      placeholder         : 'sort-highlight',
      connectWith         : '.connectedSortable',
      handle              : '.box-header, .nav-tabs',
      forcePlaceholderSize: true,
      zIndex              : 999999
    });
    $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

    // jQuery UI sortable for the todo list
    $('.todo-list').sortable({
      placeholder         : 'sort-highlight',
      handle              : '.handle',
      forcePlaceholderSize: true,
      zIndex              : 999999
    });

    // SLIMSCROLL FOR CHAT WIDGET
    function tinyScroll( $item, $height ){
        $($item).slimScroll({
          height: $height
        });
    }

    //tinyScroll('#chat-box', '250px');
    tinyScroll('.container_list', '300px');
    tinyScroll('.list-scroll', '500px');

    /**********************************/
    /*** CHANGE LABELS CALENDAR ***/
    /**********************************/

    $('.fc-listWeek-button').html('Semanal');
    $('.fc-month-button').html('Mensual');
    $('.fc-listYear-button').html('Anual');

    /**********************************/
    /*** FUNCTIONS EDIT ***/
    /**********************************/

    $('#btn_edit_form').on('click', function(e){
        e.preventDefault();
        $('#form_update select, #form_update input, #form_update checkbox').prop("disabled", false);
        $('#form_update select, #form_update input, #form_update textarea').attr("readonly", false);
        $('.slide_edit').show();
    });

    $('#btn_edit_form_client').on('click', function(e){
        e.preventDefault();
        $('#form_update select, #form_update input, #form_update checkbox').prop("disabled", false);
        $('#form_update select, #form_update input, #form_update textarea').attr("readonly", false);
        $('.container-edit-marketing').show();
        if(!$('.client_complete').val()){
            $('.marketing').attr('required', 'required');
        }
        $('#client_edit').val(1);
    });

    $('#form_create_step_one').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });

    /**********************************/
    /*** SELECT DYNAMIC ***/
    /**********************************/
    function fastSelect(container, msgHolder, msgResults){
        $(container).fastselect({
            placeholder: msgHolder,
            noResultsText: msgResults,
        });
    }

    fastSelect('#assign_roles', 'Asignar Agente', 'No Hay Agentes');
    fastSelect('#assign_agents', 'Asignar Agente', 'No Hay Agentes');
    fastSelect('#assist_agents', 'Asignar Agente', 'No Hay Agentes');
    fastSelect('#assign_amenities', 'Asignar Amenidades', 'No Hay Amenidades');

    /**********************************/
    /*** CHECK IF SEND EMAIL ***/
    /**********************************/
    $('#send_information').change(function() {
        if($(this).is(":checked")) {
            $('.f_message').show();
        }else{
            $('.f_message').hide();
        }
    });

    /************************/
    /****   ShowImage    ****/
    /************************/
    function readURL(input, preview_img, preview_img_edit) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(preview_img).css('display', 'block');
                $(preview_img).html('<img class="hidde_img_dyn" src="' + e.target.result + '" width="100%" height="300">');
                $(preview_img).css("background-image", "url(" + e.target.result + ")");
                $(preview_img_edit).css("display", "none");
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_src").change(function () {
        readURL(this, '#preview_img', '#preview_img_edit');
    });

    $("#img_src_main").change(function () {
        readURL(this, '#preview_img', '#preview_img_edit');
    });

    $("#img_src_one").change(function () {
        readURL(this, '#preview_img_one', '#preview_img_edit_one');
    });

    $("#img_src_two").change(function () {
        readURL(this, '#preview_img_two', '#preview_img_edit_two');
    });

    /************************/
    /****  Messages Box  ****/
    /************************/
    $('.container-body-msg a').each(function(){
        $(this).attr('target', '_blank')
    });

    $('.show-conversation').on('click', function(e){
        e.preventDefault();
        $(this).closest('.container-title-msg').siblings('.container-conversation').slideToggle();
    });

    $('.show-body-msg').on('click', function(e){
        e.preventDefault();
        $(this).closest('.container-subject-msg').nextAll('.container-body-msg:first').slideToggle();
    });

    $('.reply-msg').on('click', function(e){
        e.preventDefault();
        $('#email-send-mail').val($(this).data('email'));
    });

    /*********************************/
    /****  LOADING IN AJAX CALLS  ****/
    /*********************************/
    var $loading = $('#loader').hide();
    $(document).ajaxStart(function() {
        $loading.css('opacity', 1);
        $loading.fadeIn();
    }).ajaxStop(function() {
        $loading.fadeOut();
        $loading.css('opacity', 0);
    });
    
});







