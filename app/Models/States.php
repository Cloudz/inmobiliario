<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $table = "states";

    public $timestamps = false;

    protected $fillable = [
        'name',
        'country_id',
    ];

    public function cities(){
        return $this->hasMany('App\Models\Cities', 'state_id');
    }

    public function country(){
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function clients(){
        return $this->hasMany('App\Models\Clients', 'id');
    }
}
