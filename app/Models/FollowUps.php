<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class FollowUps extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "follow_ups";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'folio',
        'status',
        'type',
        'client_id',
        'user_id',
    ];

    protected $fillable = [
        'folio',
        'status',
        'type',
        'client_id',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function client(){
        return $this->belongsTo('App\Models\Clients', 'client_id');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'followups_collaborators', 'followup_id', 'user_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
