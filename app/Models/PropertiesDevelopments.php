<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesDevelopments extends Model
{
    use Sortable;

    protected $table = "properties_developments";

    public $sortable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];
}
