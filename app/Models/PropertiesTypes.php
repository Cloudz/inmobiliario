<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesTypes extends Model
{

    use Sortable;

    protected $table = "properties_types";

    public $sortable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];

}