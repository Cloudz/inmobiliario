<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesStyles extends Model
{
    use Sortable;

    protected $table = "properties_styles";

    public $sortable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];
}
