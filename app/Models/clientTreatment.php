<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class clientTreatment extends Model
{
    protected $table = "clients_treatments";

    protected $fillable = [
        'name',
    ];
}
