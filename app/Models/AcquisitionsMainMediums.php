<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class AcquisitionsMainMediums extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "acquisitions_main_mediums";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'description',
        'model',
        'is_multi',
        'source_id',
        'contact_way_id',
        'secondary_id',
    ];

    protected $fillable = [
        'name',
        'description',
        'model',
        'is_multi',
        'source_id',
        'contact_way_id',
        'secondary_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }
}
