<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = "countries";

    public $timestamps = false;

    protected $fillable = [
        'sortname',
        'name',
        'phonecode',
    ];

    public function states(){
        return $this->hasMany('App\Models\States', 'country_id');
    }

    public function clients(){
        return $this->hasMany('App\Models\Clients', 'id');
    }
}
