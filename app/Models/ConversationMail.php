<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ConversationMail extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "conversations_mails";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'subject',
        'sender',
        'htmlBody',
        'textBody',
        'recipient',
    ];

    protected $fillable = [
        'subject',
        'sender',
        'htmlBody',
        'textBody',
        'recipient',
    ];
}
