<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class AcquisitionsSecondarySources extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "acquisitions_secondary_sources";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'description',
        'model',
    ];

    protected $fillable = [
        'name',
        'description',
        'model',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }
}
