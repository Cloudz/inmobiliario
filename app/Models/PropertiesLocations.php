<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesLocations extends Model
{
    use Sortable;

    protected $table = "properties_locations";

    public $sortable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];
}
