<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reminders extends Model
{
    use SoftDeletes;

    protected $table = "reminders";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'subject', 
        'description', 
        'started_on',
        'type',
        'interval',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }
}
