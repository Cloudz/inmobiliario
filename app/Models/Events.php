<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Events extends Model
{
    use SoftDeletes;

    protected $table = "events";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'description', 
        'eventable_id', 
        'eventable_type',
    ];

    public function eventable(){
        return $this->morphTo();
    }
}
