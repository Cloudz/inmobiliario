<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interests extends Model
{
    use SoftDeletes;

    protected $table = "interests";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
    ];
}
