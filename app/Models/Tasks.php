<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tasks extends Model
{
    use SoftDeletes;

    protected $table = "tasks";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'subject', 
        'description', 
        'started_on',
        'reminded_on',
        'accomplished',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }
}
