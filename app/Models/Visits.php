<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Visits extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "visits";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'visit_date',
        'client_id',
        'agent_id',
        'sale_office_id',
    ];

    protected $fillable = [
        'visit_date',
        'client_id',
        'agent_id',
        'sale_office_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function office(){
        return $this->belongsTo('App\Models\Sources\SalesOffices', 'sale_office_id');
    }

    public function client(){
        return $this->belongsTo('App\Models\Clients', 'client_id');
    }

    public function adviser(){
        return $this->belongsTo('App\Models\User', 'agent_id');
    }
}
