<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketingInformation extends Model
{
    use SoftDeletes;

    protected $table = "marketing_information";
    protected $dates = ['deleted_at'];

    protected $guarded = [
        'contact_way_id',
        'main_source_id',
        'secondary_source_id',
        'interest_id',
        'level_interest_id',
        'client_id',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function referrors(){
        return $this->belongsToMany('App\Models\Sources\Referrors', 'marketings_referrors', 'marketings_id', 'referrors_id');
    }

    public function mediums_referrors(){
        return $this->belongsToMany('App\Models\Sources\Referrors', 'marketings_mediums_referrors', 'marketings_id', 'referrors_id');
    }

    public function getContactWay(){
        return $this->belongsTo('App\Models\ContactWays', 'contact_way_id');
    }

    public function getContactMedium(){
        return $this->belongsTo('App\Models\ContactMediums', 'contact_medium_id');
    }

    public function getInterest(){
        return $this->belongsTo('App\Models\Interests', 'interest_id');
    }

    public function getClient(){
        return $this->belongsTo('App\Models\Clients', 'client_id');
    }
}
