<?php 

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole{
    
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}