<?php

namespace App\Models\Sources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Agencies extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "agencies";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'website',
        'main_office_phone',
        'secondary_office_phone',
        'address',
        'country_id',
        'user_id',
    ];

    protected $fillable = [
        'name',
        'website',
        'main_office_phone', 
        'secondary_office_phone', 
        'address', 
        'country_id', 
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function source(){
        return $this->morphMany('App\Models\ContactWays', 'sourceable');
    }

    public function countryName(){
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function agents(){
        return $this->belongsTo('App\Models\Sources\Agents', 'id');
    }

    public function hasAttribute($attr){
        return array_key_exists($attr, $this->attributes);
    }

}
