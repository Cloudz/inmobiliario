<?php

namespace App\Models\Sources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class SalesOffices extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "sales_offices";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'main_phone',
        'secondary_phone',
        'fax',
        'active',
        'country_id',
        'state_id',
        'city_id',
        'user_id',
        'street',
        'number_int',
        'number_ext',
        'cp',
    ];

    protected $fillable = [
        'name',
        'main_phone',
        'secondary_phone',
        'fax',
        'active',
        'country_id',
        'state_id',
        'city_id',
        'user_id',
        'street',
        'number_int',
        'number_ext',
        'cp',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function countryName(){
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function stateName(){
        return $this->belongsTo('App\Models\States', 'state_id');
    }

    public function cityName(){
        return $this->belongsTo('App\Models\Cities', 'city_id');
    }

    public function visits(){
        return $this->hasMany('App\Models\Visits', 'sale_office_id');
    }

    public function hasAttribute($attr){
        return array_key_exists($attr, $this->attributes);
    }
}
