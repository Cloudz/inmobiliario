<?php

namespace App\Models\Sources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Referrors extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "referrors";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'last_name',
        'main_email',
        'secondary_email',
        'main_mobile',
        'secondary_mobile',
        'home_phone',
        'office_phone',
        'country_id',
        'address',
        'occupation',
        'gender_id',
        'user_id',
    ];

    protected $fillable = [
        'name',
        'last_name', 
        'main_email', 
        'secondary_email', 
        'main_mobile', 
        'secondary_mobile', 
        'home_phone', 
        'office_phone', 
        'country_id', 
        'address', 
        'occupation',
        'gender_id',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function source(){
        return $this->morphMany('App\Models\ContactWays', 'sourceable');
    }

    public function countryName(){
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function marketingInformation(){
        return $this->belongsToMany('App\Models\MarketingInformation', 'marketings_referrors', 'referrors_id', 'marketings_id');
    }

    public function hasAttribute($attr){
        return array_key_exists($attr, $this->attributes);
    }
}
