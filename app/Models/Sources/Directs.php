<?php

namespace App\Models\Sources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Directs extends Model
{
    use SoftDeletes;

    protected $table = "directs";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function hasAttribute($attr){
        return array_key_exists($attr, $this->attributes);
    }
}
