<?php

namespace App\Models\Sources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Agents extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "agents";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'email',
        'mobile',
        'agency_id',
        'is_broker',
        'user_id',
    ];

    protected $fillable = [
        'name',
        'email',
        'mobile',
        'agency_id',
        'is_broker',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function agencyName(){
        return $this->belongsTo('App\Models\Sources\Agencies', 'agency_id');
    }

    public function hasAttribute($attr){
        return array_key_exists($attr, $this->attributes);
    }

}
