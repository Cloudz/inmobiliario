<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesImages extends Model
{
    use Sortable;

    protected $table = "properties_images";

    public $sortable = [
        'url',
    ];

    protected $fillable = [
        'url',
    ];
}
