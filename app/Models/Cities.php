<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table = "cities";

    public $timestamps = false;

    protected $fillable = [
        'name',
        'state_id',
    ];

    public function states(){
        return $this->belongsTo('App\Models\States', 'state_id');
    }

    public function clients(){
        return $this->hasMany('App\Models\Clients', 'id');
    }
}
