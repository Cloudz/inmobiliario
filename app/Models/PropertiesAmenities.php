<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PropertiesAmenities extends Model
{
    use Sortable;

    protected $table = "properties_amenities";

    public $sortable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];
}
