<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Clients extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "clients";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'last_name',
        'main_email',
        'secondary_email',
        'main_mobile',
        'secondary_mobile',
        'home_phone',
        'office_phone',
        'country_id',
        'state_id',
        'city_id',
        'street',
        'number_int',
        'number_ext',
        'cp',
        'gender_id',
        'user_id',
        'first_contact',
        'treatment_id'
    ];

    protected $fillable = [
        'name',
        'last_name', 
        'main_email', 
        'secondary_email', 
        'main_mobile', 
        'secondary_mobile', 
        'home_phone', 
        'office_phone', 
        'country_id',
        'state_id',
        'city_id',
        'street',
        'number_int',
        'number_ext',
        'cp', 
        'gender_id', 
        'user_id', 
        'first_contact',
        'status',
        'treatment_id'
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function countryName(){
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function stateName(){
        return $this->belongsTo('App\Models\States', 'state_id');
    }

    public function cityName(){
        return $this->belongsTo('App\Models\Cities', 'city_id');
    }

    public function marketingInformation(){
        return $this->hasOne('App\Models\MarketingInformation', 'client_id', 'id');
    }
}
