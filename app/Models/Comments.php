<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use SoftDeletes;

    protected $table = "comments";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'comments', 
        'commentable_id',
        'commentable_type',
        'user_id',
    ];

    public function commentable(){
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
