<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ContactWays extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "contact_ways";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'description',
        'model',
        'main_source_id',
        'is_multi',
        'user_id',
    ];

    protected $fillable = [
        'name',
        'description',
        'model',
        'main_source_id',
        'is_multi',
        'user_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public static function getModels($path){
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            $extension = pathinfo($result, PATHINFO_EXTENSION);
            if ($result === '.' or $result === '..') continue;
            $filename = $result;
            if ($extension == 'php') {
                if (is_dir($filename)) {
                    $out = array_merge($out, getModels($filename));
                } else {
                    $out[] = substr($filename, 0, -4);
                }
            }else{
                $out = [];
            }
        }
        return $out;
    }
}
