<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }
    use Sortable;

    protected $table = "users";

    public $sortable = [
        'name',
        'last_name',
        'email',
        'password',
        'is_active'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    public function role(){
        return $this->belongsToMany('App\Models\Role');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'agents_assisted', 'user_id', 'agent_id');
    }

    public function followups(){
        return $this->belongsToMany('App\Models\FollowUps', 'followups_collaborators', 'user_id', 'followup_id');
    }
}
