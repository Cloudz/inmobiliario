<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Properties extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "properties";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'name',
        'description',
        'file',
        'address',
        'baths',
        'beds',
        'parking',
        'm2',
        'ft2',
        'type',
        'type_id',
        'development_id',
        'location_id',
        'style_id',
    ];

    protected $fillable = [
        'name',
        'description',
        'file',
        'address',
        'baths',
        'beds',
        'parking',
        'm2',
        'ft2',
        'type',
        'type_id',
        'development_id',
        'location_id',
        'style_id',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function amenities(){
        return $this->belongsToMany('App\Models\PropertiesAmenities', 'amenity_property', 'amenity_id', 'property_id');
    }

    public function getType(){
        return $this->belongsTo('App\Models\PropertiesTypes', 'type_id');
    }

    public function getDevelopment(){
        return $this->belongsTo('App\Models\PropertiesDevelopments', 'development_id');
    }

    public function getLocation(){
        return $this->belongsTo('App\Models\PropertiesLocations', 'location_id');
    }

    public function getStyle(){
        return $this->belongsTo('App\Models\PropertiesStyles', 'style_id');
    }
}
