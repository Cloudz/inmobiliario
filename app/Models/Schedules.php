<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Schedules extends Model
{
    use SoftDeletes;

    use Sortable;

    protected $table = "schedules";
    protected $dates = ['deleted_at'];

    public $sortable = [
        'start_at',
        'end_at',
    ];

    protected $fillable = [
        'agent_id',
        'office_id',
        'start_at',
        'end_at',
    ];

    public function event(){
        return $this->morphMany('App\Models\Events', 'eventable');
    }

    public function comment(){
        return $this->morphMany('App\Models\Comments', 'commentable');
    }

    public function agent(){
        return $this->hasOne('App\Models\Sources\Agents', 'id', 'agent_id');
    }

    public function office(){
        return $this->hasOne('App\Models\Sources\SalesOffices', 'id',  'office_id');
    }

}
