<?php
namespace App\Helpers;

use Auth;

class UsersHelpers
{
    public static function getUser(){

        return Auth::user();

    }

    public static function getUserID(){

        return Auth::user()->id;

    }

    public static function getUsers(){
        $users = [];
        foreach (self::getUser()->users as $userID){
            $users[] = $userID->id;
        }

        return $users;
    }
}