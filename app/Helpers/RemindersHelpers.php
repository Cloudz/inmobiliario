<?php
namespace App\Helpers;

use App\Models\Reminders;
use DateTime;
use DateTimeZone;

class RemindersHelpers
{
    public static function getReminders($type){
        $tz = 'America/Mexico_City';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp( $timestamp );
        $date = $dt->format('Y-m-d H:i:s');
        $reminders = Reminders::where('type', $type)->get();

        return $reminders;
    }
}