<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\{
    Clients
};

class ClientsExport implements FromView
{
    public function view(): View
    {
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $clients = Clients::sortable()->orderBy('created_at', 'desc')->get();
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $clients = Clients::sortable()->where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->get();
        }else{
            $clients = Clients::sortable()->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->get();
        }
        return view( 'exports.clients', compact('clients') );
    }
}
