<?php

namespace App\Exports;

use Session;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\{
    Properties
};

class PropertiesExport implements FromView
{
    public function view(): View
    {
        $properties = Properties::all();
        return view( 'exports.properties', compact('properties') );
    }
}
