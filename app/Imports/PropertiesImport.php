<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use App\Models\{
    Properties, PropertiesAmenities, PropertiesDevelopments, PropertiesLocations, PropertiesStyles, PropertiesTypes
};

class PropertiesImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows){
        $getRows = $rows->toArray();
        foreach (array_slice($getRows,1) as $row) {
            $type           = PropertiesTypes::where('name', $row[9])->first();
            $development    = PropertiesDevelopments::where('name', $row[10])->first();
            $location       = PropertiesLocations::where('name', $row[11])->first();
            $style          = PropertiesStyles::where('name', $row[12])->first();
            $amenities      = [];
            $amenitiesNames = array_filter(explode(PHP_EOL, $row[13]));
            foreach ($amenitiesNames as $amenityName){
                $amenities[] = PropertiesAmenities::where('name', $amenityName)->first()->id;
            }

            $propertyVerify = Properties::where('name', $row[0])->first();

            if(isset($propertyVerify)){
                continue;
            }else {
                $property = new Properties();

                $property->name           = $row[0];
                $property->description    = $row[1];
                $property->address        = $row[2];
                $property->baths          = $row[3];
                $property->beds           = $row[4];
                $property->parking        = $row[5];
                $property->m2             = $row[6];
                $property->ft2            = $row[7];
                $property->type           = $row[8];
                $property->type_id        = $type->id;
                $property->development_id = $development->id;
                $property->location_id    = $location->id;
                $property->style_id       = $style->id;

                if($property->save()){
                    $property->amenities()->sync($amenities);
                }

            }

        }
    }
}
