<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use App\Models\{
    AcquisitionsMainMediums, AcquisitionsMainSources, AcquisitionsSecondaryMediums, AcquisitionsSecondarySources, Cities, Clients, ContactMediums, ContactWays, Countries, FollowUps, Interests, MarketingInformation, States, User
};

class ClientsImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows){
        $getRows = $rows->toArray();
        foreach (array_slice($getRows,1) as $row){
            $path_model          = 'App\Models\Sources\\';
            $country             = Countries::where('name', $row[9])->first();
            $state               = States::where('name', $row[10])->first();
            $city                = Cities::where('name', $row[11])->first();
            $user                = User::where('name', $row[17])->first();
            $contactWay          = ContactWays::where('name', $row[19])->first();
            $contactMedium       = ContactMediums::where('name', $row[22])->first();
            $mainModelNameSource = $path_model.$contactWay->model;
            $mainSource          = $mainModelNameSource::where('name', $row[20])->first();
            $secondarySource     = AcquisitionsSecondarySources::where('name', $row[21])->first();
            $mainModelNameMedium = $path_model.$contactMedium->model;
            $mainMedium          = $mainModelNameMedium::where('name', $row[23])->first();
            $secondaryMedium     = AcquisitionsSecondaryMediums::where('name', $row[24])->first();
            $interest            = Interests::where('name', $row[25])->first();

            $emailVerify = Clients::where('main_email', $row[3])->first();

            if($row[16] == 'Hombre') {
                $gender = 0;
            }elseif($row[16] == 'Mujer') {
                $gender = 1;
            }else{
                $gender = 2;
            }

            if(!$city){
                $cityID = NULL;
            }else{
                $cityID = $city->id;
            }

            if(!isset($mainMedium)){
                $mainMedium = NULL;
            }else{
                $mainMedium = $mainMedium->id;
            }

            if(!isset($secondarySource)){
                $secondarySourceID = NULL;
            }else{
                $secondarySourceID = $secondarySource->id;
            }

            if(!isset($secondaryMedium)){
                $secondaryMediumID = NULL;
            }else{
                $secondaryMediumID = $secondaryMedium->id;
            }

            if(isset($emailVerify)){
                continue;
            }else{
                $client = new Clients;

                $client->name = $row[0];
                $client->last_name = $row[1];
                $client->client_key = $row[2];
                $client->main_email = $row[3];
                $client->secondary_email = $row[4];
                $client->main_mobile = $row[5];
                $client->secondary_mobile = $row[6];
                $client->home_phone = $row[7];
                $client->office_phone = $row[8];
                $client->country_id = $country->id;
                $client->state_id = $state->id;
                $client->city_id = $cityID;
                $client->street = $row[12];
                $client->number_int = $row[13];
                $client->number_ext = $row[14];
                $client->zipcode = $row[15];
                $client->gender_id = $gender;
                $client->user_id = $user->id;
                $client->first_contact = $row[18];
                $client->status = 0;

                if($client->save()){
                    $followup = new FollowUps;

                    $followup->folio     = 'FOLIO-'.$client->id;
                    $followup->status    = 1;
                    $followup->type      = 'General';
                    $followup->client_id = $client->id;
                    if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                        $followup->user_id = $client->user_id;
                    }else{
                        $followup->user_id = \UsersHelpers::getUserID();
                    }

                    $followup->save();

                    $marketingInfo = new MarketingInformation;

                    $marketingInfo->contact_way_id    = $contactWay->id;
                    $marketingInfo->contact_medium_id = $contactMedium->id;

                    if ($contactWay->is_multi != 1) {
                        $marketingInfo->main_source_id  = $contactWay->main_source_id;
                    }else{
                        $marketingInfo->main_source_id  = 0;
                    }
                    if ($contactMedium->is_multi != 1) {
                        $marketingInfo->main_medium_source_id = $contactMedium->main_source_id;
                    }else{
                        $marketingInfo->main_medium_source_id = 0;
                    }

                    $marketingInfo->secondary_source_id        = $secondarySourceID;
                    $marketingInfo->secondary_medium_source_id = $secondaryMediumID;
                    $marketingInfo->interest_id                = $interest->id;
                    $marketingInfo->level_interest_id          = $row[26];
                    $marketingInfo->client_id                  = $client->id;

                    if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                        $marketingInfo->user_id = $client->user_id;
                    }else{
                        $marketingInfo->user_id = \UsersHelpers::getUserID();
                    }

                    if( $marketingInfo->save() ){
                        if ($contactWay->is_multi == 1 && isset($mainSource)) {
                            $mainLowerSource = strtolower($mainSource->model);
                            $marketingInfo->$mainLowerSource()->sync($contactWay->main_source_id);
                        }
                        if ($contactMedium->is_multi == 1 && isset($mainMedium)) {
                            $mainLowerMedium = 'mediums_'.strtolower($mainMedium->model);
                            $marketingInfo->$mainLowerMedium()->sync($contactMedium->main_source_id);
                        }
                    }
                }

            }
        }

    }
}
