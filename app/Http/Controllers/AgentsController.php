<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Countries, User
};

use App\Models\Sources\{
    Agencies, Agents
};

class AgentsController extends Controller
{
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker'])) {
            $agents = Agents::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $agents = Agents::sortable()->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate(50);
        }
        return view( "agents.index", compact('agents') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker'])) {
            $agents = Agents::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('email', "like", "%".$search."%")
                    ->orWhere('mobile', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $agents = Agents::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('email', "like", "%".$search."%")
                    ->orWhere('mobile', "like", "%".$search."%");
            })->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( 'agents.index', compact('agents', 'search') );
    }

    public function create( Agencies $agency ){
        $countries = Countries::all();
        return view ('agents.create', compact('countries', 'agency') );
    }

    public function singleCreate(){
        $agencies  = Agencies::all();
        $countries = Countries::all();
        return view ('agents.single-create', compact('countries', 'agencies') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $agent = new Agents;

        $rules = [];
        $messages = [];

        $rules['name']  = 'required|min:3';
        $rules['email'] = 'required|email|unique:agents';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['email.required'] = 'El email es obligatorio';
        $messages['email.email']    = 'El email tiene formato incorrecto';
        $messages['email.unique']   = 'El email ya se encuentra registrado';

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            if ($request->input( "is_broker" ) == 'on') {
                $is_broker = 1;
            }else{
                $is_broker = 0;
            }

            if (!empty($request->input( "mobile" ))) {
                $agent->mobile = $request->input( "mobile" );
            }

            $agent->name      = ucfirst($request->input( "name" ));
            $agent->email     = $request->input( "email" );
            $agent->mobile    = $request->input( "mobile" );
            $agent->agency_id = $request->input( "agency_id" );
            $agent->is_broker = $is_broker;
            $agent->user_id   = \UsersHelpers::getUserID();

            if( $agent->save() ){
                $agent->event()->create(['description' => 'Agente Agregado']);
                if (!empty($request->input( "comments" ))) {
                    $agent->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Agente Agregado');
                $res['redirect'] = route('agents');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function singleStore( Request $request ){
        $res = ['success' => false];
        $agent = new Agents;

        $rules = [];
        $messages = [];

        $rules['name']  = 'required|min:3';
        $rules['email'] = 'required|email|unique:agents';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['email.required'] = 'El email es obligatorio';
        $messages['email.email']    = 'El email tiene formato incorrecto';
        $messages['email.unique']   = 'El email ya se encuentra registrado';

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            if ($request->input( "is_broker" ) == 'on') {
                $is_broker = 1;
            }else{
                $is_broker = 0;
            }

            if (!empty($request->input( "mobile" ))) {
                $agent->mobile = $request->input( "mobile" );
            }

            $agent->name      = ucfirst($request->input( "name" ));
            $agent->email     = $request->input( "email" );
            $agent->mobile    = $request->input( "mobile" );
            $agent->agency_id = $request->input( "agency_select" );
            $agent->is_broker = $is_broker;
            $agent->user_id   = \UsersHelpers::getUserID();

            if( $agent->save() ){
                $agent->event()->create(['description' => 'Agente Agregado']);
                if (!empty($request->input( "comments" ))) {
                    $agent->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Agente Agregado');
                $res['redirect'] = route('agents');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( Agents $agent ){
        $agency    = Agencies::where('id', $agent->agency_id)->first();
        $agencies  = Agencies::all();
        $countries = Countries::all();
        return view ('agents.view', compact('agent', 'countries', 'agencies', 'agency') );
    }

    public function update( Agents $agent, Request $request ){
        $currentAgent = Agents::where('id', $agent->id)->first();
        $keyComment = count($currentAgent->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];


        $rules['name']  = 'required|min:3';
        $rules['email'] = ['required', 'email', Rule::unique('agents')->ignore($agent->id) ];

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['email.required'] = 'El email es obligatorio';
        $messages['email.email']    = 'El email tiene formato incorrecto';
        $messages['email.unique']   = 'El email ya se encuentra registrado';

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El celular tiene formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            if ($request->input( "is_broker" ) == 'on') {
                $is_broker = 1;
            }else{
                $is_broker = 0;
            }

            if (!empty($request->input( "mobile" ))) {
                $agent->mobile = $request->input( "mobile" );
            }

            $agent->name      = ucfirst($request->input( "name" ));
            $agent->email     = $request->input( "email" );
            $agent->mobile    = $request->input( "mobile" );
            if (!empty($request->input( "agency_select" ))) {
                $agent->agency_id = $request->input("agency_select");
            }
            $agent->is_broker = $is_broker;

            if( $agent->save() ){
                $agent->event()->create(['description' => 'Agente Actualizado']);
                if($agent->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentAgent->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $agent->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $agent->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                $res['success'] = true;
                Session::flash('msg', 'Agente Actualizado');
                $res['redirect'] = route('agentView', $agent->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function delete( Agents $agent ){
        $res = ['success' => true];
        if( $agent->delete() ){
            $agent->event()->create(['description' => 'Agente Eliminado']);
            Session::flash('msg', 'Agente borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
