<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Session;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

use App\Models\{
    AcquisitionsMainSources, AcquisitionsSecondarySources, ContactWays
};

class AcquisitionsSecondarySourcesController extends Controller
{
    //FUNCTION LIST
    public function index(){
        $sources = AcquisitionsSecondarySources::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "admin.acquisitions.secondary_sources.index", compact('type', 'sources') );
    }

    //FUNCTION SEARCH
    public function search( Request $request ){
        $search = $request->input("search");
        $sources = AcquisitionsSecondarySources::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('description', "like", "%".$search."%");
        })->orderBy('created_at', 'desc')->paginate( 50 );

        return view( 'admin.acquisitions.secondary_sources.index', compact('type', 'sources', 'search') );
    }

    //FUNCTION CREATE
    public function create(){
        $path = app_path('Models/Sources');
        $models = ContactWays::getModels($path);
        return view ('admin.acquisitions.secondary_sources.create', compact('models'));
    }

    //FUNCTION SAVE
    public function store( Request $request ){
        $res = ['success' => false];
        $source = new AcquisitionsSecondarySources;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:acquisitions_secondary_sources';

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $source->name           = ucfirst($request->input( "name" ));
            $source->description    = ucfirst($request->input( "description" ));
            $source->model          = $request->input( "model" );

            if( $source->save() ){
                $source->event()->create(['description' => 'Recurso Agregado']);
                $res['success'] = true;
                Session::flash('msg', 'Recurso Agregado');
                $res['redirect'] = route('acquisitionSecondarySources');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION EDIT/VIEW
    public function edit ( AcquisitionsSecondarySources $source ){
        $path = app_path('Models/Sources');
        $models = ContactWays::getModels($path);
        return view( "admin.acquisitions.secondary_sources.edit", compact('source' , 'models') );
    }

    //FUNCTION UPDATE
    public function update ( AcquisitionsSecondarySources $source, Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('acquisitions_secondary_sources')->ignore($source->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $source->name        = ucfirst($request->input( "name" ));
            $source->description = ucfirst($request->input( "description" ));
            if ( $request->input( "model" ) != '' ){
                $source->model = $request->input( "model" );
            }

            if( $source->save() ){
                $source->event()->create(['description' => 'Recurso Actualizado']);
                $res['success'] = true;
                Session::flash('msg', 'Recurso Actualizado');
                $res['redirect'] = route('acquisitionSecondarySourcesEdit', $source->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DELETE
    public function delete( AcquisitionsSecondarySources $source ){
        $res = ['success' => true];
        if( $source->delete() ){
            $source->event()->create(['description' => 'Recurso Eliminado']);
            Session::flash('msg', 'Recurso borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
