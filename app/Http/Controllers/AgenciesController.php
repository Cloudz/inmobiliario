<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Countries
};

use App\Models\Sources\{
    Agencies, Agents
};

class AgenciesController extends Controller
{
    public function index(){
        $agencies = Agencies::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "agencies.index", compact('agencies') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $agencies = Agencies::sortable()->where(function ($query) use ($search) {
                        $query->where('name', "like", "%".$search."%")
                        ->orWhere('website', "like", "%".$search."%")
                        ->orWhere('main_office_phone', "like", "%".$search."%")
                        ->orWhere('secondary_office_phone', "like", "%".$search."%")
                        ->orWhere('address', "like", "%".$search."%");
                })->orderBy('created_at', 'desc')->paginate( 50 );
        return view( 'agencies.index', compact('agencies', 'search') );
    }

    public function create(){
        $countries = Countries::all();
        return view ('agencies.create', compact('countries') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $agency = new Agencies;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:agencies';

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        if (!empty($request->input( "main_office_phone" ))) {
            $rules['main_office_phone']            = 'numeric';
            $messages['main_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        if (!empty($request->input( "secondary_office_phone" ))) {
            $rules['secondary_office_phone']            = 'numeric';
            $messages['secondary_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $agency->name = ucfirst($request->input( "name" ));

            if (!empty($request->input( "website" ))) {
                $agency->website = $request->input( "website" );
            }

            if (!empty($request->input( "main_office_phone" ))) {
                $agency->main_office_phone = $request->input( "main_office_phone" );
            }

            if (!empty($request->input( "secondary_office_phone" ))) {
                $agency->secondary_office_phone = $request->input( "secondary_office_phone" );
            }

            if (!empty($request->input( "address" ))) {
                $agency->address = $request->input( "address" );
            }

            if (!empty($request->input( "nationality" ))) {
                $agency->country_id = $request->input( "nationality" );
            }

            $agency->user_id = \UsersHelpers::getUserID();

            if( $agency->save() ){
                $agency->event()->create(['description' => 'Agencia Agregada']);
                if (!empty($request->input( "comments" ))) {
                    $agency->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Agencia Agregada');
                $res['redirect'] = route('agencies');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function view( Agencies $agency ){
        $countries = Countries::all();
        $agents    = Agents::where('agency_id', $agency->id)->get();
        return view( "agencies.view", compact('agency', 'agents', 'countries') );
    }

    public function update( Agencies $agency, Request $request ){
        $currentAgency = Agencies::where('id', $agency->id)->first();
        $keyComment = count($currentAgency->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('agencies')->ignore($agency->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        if (!empty($request->input( "main_office_phone" ))) {
            $rules['main_office_phone']            = 'numeric';
            $messages['main_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        if (!empty($request->input( "secondary_office_phone" ))) {
            $rules['secondary_office_phone']            = 'numeric';
            $messages['secondary_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $agency->name = ucfirst($request->input( "name" ));

            if (!empty($request->input( "website" ))) {
                $agency->website = $request->input( "website" );
            }

            if (!empty($request->input( "main_office_phone" ))) {
                $agency->main_office_phone = $request->input( "main_office_phone" );
            }

            if (!empty($request->input( "secondary_office_phone" ))) {
                $agency->secondary_office_phone = $request->input( "secondary_office_phone" );
            }

            if (!empty($request->input( "address" ))) {
                $agency->address = $request->input( "address" );
            }

            if (!empty($request->input( "nationality" ))) {
                $agency->country_id = $request->input( "nationality" );
            }

            if( $agency->save() ){
                $agency->event()->create(['description' => 'Agencia Actualizada']);
                if($agency->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentAgency->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $agency->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $agency->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                $res['success'] = true;
                Session::flash('msg', 'Agencia Editada');
                $res['redirect'] = route('agencyView', $agency->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Agencies $agency ){
        $res = ['success' => true];
        if( $agency->delete() ){
            $agency->event()->create(['description' => 'Agencia Eliminada']);
            Session::flash('msg', 'Agencia borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
