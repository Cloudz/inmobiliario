<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    Countries
};

use App\Models\Sources\{
    SalesOffices
};

class SalesOfficesController extends Controller
{
    public function index(){
        $offices = SalesOffices::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "offices.index", compact('offices') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $offices = SalesOffices::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('main_phone', "like", "%".$search."%")
                ->orWhere('secondary_phone', "like", "%".$search."%")
                ->orWhere('fax', "like", "%".$search."%");
        })->orderBy('created_at', 'desc')->paginate( 50 );
        return view( 'offices.index', compact('offices', 'search') );
    }

    public function create( Request $request ){
        $countries = Countries::all();
        return view ('offices.create', compact('countries') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $office = new SalesOffices;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:sales_offices';
        $rules['main_phone'] = 'required|numeric';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';
        $messages['main_phone.required'] = 'El teléfono es obligatorio';
        $messages['main_phone.numeric'] = 'El teléfono tiene un formato incorrecto';

        if (!empty($request->input( "secondary_phone" ))) {
            $rules['secondary_phone']            = 'numeric';
            $messages['secondary_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $office->name = ucfirst($request->input( "name" ));

            if (!empty($request->input( "main_phone" ))) {
                $office->main_phone = $request->input( "main_phone" );
            }

            if (!empty($request->input( "fax" ))) {
                $office->fax = $request->input( "fax" );
            }

            if (!empty($request->input( "street" ))) {
                $office->street = $request->input( "street" );
            }

            if (!empty($request->input( "number_int" ))) {
                $office->number_int = $request->input( "number_int" );
            }

            if (!empty($request->input( "number_ext" ))) {
                $office->number_ext = $request->input( "number_ext" );
            }

            if (!empty($request->input( "cp" ))) {
                $office->cp = $request->input( "cp" );
            }

            $office->country_id = $request->input( "country" );
            $office->state_id   = $request->input( "state" );
            $office->city_id    = $request->input( "city" );
            $office->active     = 1;

            if( $office->save() ){
                $office->event()->create(['description' => 'Oficina Agregada']);
                if (!empty($request->input( "comments" ))) {
                    $office->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Oficina Agregada');
                $res['redirect'] = route('offices');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( SalesOffices $office ){
        $countries = Countries::all();
        return view( "offices.view", compact('office', 'countries') );
    }

    public function update( SalesOffices $office, Request $request ){
        $currentOffice = SalesOffices::where('id', $office->id)->first();
        $keyComment = count($currentOffice->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('sales_offices')->ignore($office->id) ];
        $rules['main_phone'] = 'required|numeric';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';
        $messages['main_phone.required'] = 'El teléfono es obligatorio';
        $messages['main_phone.numeric'] = 'El teléfono tiene un formato incorrecto';

        if (!empty($request->input( "secondary_phone" ))) {
            $rules['secondary_phone']            = 'numeric';
            $messages['secondary_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $office->name = ucfirst($request->input( "name" ));
            $office->main_phone = ucfirst($request->input( "main_phone" ));

            if (!empty($request->input( "secondary_phone" ))) {
                $office->secondary_phone = $request->input( "secondary_phone" );
            }

            if (!empty($request->input( "fax" ))) {
                $office->fax = $request->input( "fax" );
            }

            if (!empty($request->input( "street" ))) {
                $office->street = $request->input( "street" );
            }

            if (!empty($request->input( "number_int" ))) {
                $office->number_int = $request->input( "number_int" );
            }

            if (!empty($request->input( "number_ext" ))) {
                $office->number_ext = $request->input( "number_ext" );
            }

            if (!empty($request->input( "cp" ))) {
                $office->cp = $request->input( "cp" );
            }

            $office->country_id = $request->input( "country" );
            $office->state_id   = $request->input( "state" );
            $office->city_id    = $request->input( "city" );

            if ($request->input( "active" ) == 'on') {
                $active = 1;
            }else{
                $active = 0;
            }

            $office->active = $active;

            if( $office->save() ){
                $office->event()->create(['description' => 'Oficina Actualizada']);
                if($office->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentOffice->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $office->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $office->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                $res['success'] = true;
                Session::flash('msg', 'Oficina Editada');
                $res['redirect'] = route('officeView', $office->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( SalesOffices $office ){
        $res = ['success' => true];
        if( $office->delete() ){
            $office->event()->create(['description' => 'Oficina Eliminada']);
            Session::flash('msg', 'Oficina borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
