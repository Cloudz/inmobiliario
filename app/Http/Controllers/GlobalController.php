<?php

namespace App\Http\Controllers;

use View;
use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    AcquisitionsMainSources, AcquisitionsSecondarySources, Countries
};

class GlobalController extends Controller
{
    public function create( $source ){
        $mainSource   = AcquisitionsMainSources::where('id', $source)->orderBy('created_at', 'desc')->first();
        $pathModel    = 'App\Models\Sources\\'.$mainSource->model;
        $customSource = $pathModel::first();
        $routeView    = 'modal.create';
        $countries    = Countries::all();
        return View::make($routeView, compact('countries', 'mainSource', 'customSource') )->render();
    }

    public function subCreate( $source, $sourceId ){
        $parentSource       = AcquisitionsMainSources::where('id', $source)->orderBy('created_at', 'desc')->first();
        $parentPathModel    = 'App\Models\Sources\\'.$parentSource->model;
        $parentCustomSource = $parentPathModel::where('id',$sourceId)->first();
        $mainSource   = AcquisitionsSecondarySources::where('id', $parentSource->secondary_id)->orderBy('created_at', 'desc')->first();
        $pathModel    = 'App\Models\Sources\\'.$mainSource->model;
        $customSource = $pathModel::first();
        $routeView    = 'modal.create';
        $countries    = Countries::all();
        return View::make($routeView, compact('countries', 'parentCustomSource', 'mainSource', 'customSource') )->render();
    }

    public function store( $source, Request $request ){
        $res = ['success' => false];
        $path_model = 'App\Models\Sources\\'.$source;
        $customSource = new $path_model;
        $nameLower  = strtolower($source);

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:'.$nameLower;

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        if (!empty($request->input( "main_office_phone" ))) {
            $rules['main_office_phone']            = 'numeric';
            $messages['main_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        if (!empty($request->input( "secondary_office_phone" ))) {
            $rules['secondary_office_phone']            = 'numeric';
            $messages['secondary_office_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        if (!empty($request->input( "secondary_email" ))) {
            $rules['secondary_email']           = 'email';
            $messages['secondary_email.email']  = 'El email tiene formato incorrecto';
        }

        if (!empty($request->input( "home_phone" ))) {
            $rules['home_phone']            = 'numeric';
            $messages['home_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "office_phone" ))) {
            $rules['office_phone']            = 'numeric';
            $messages['office_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "main_mobile" ))) {
            $rules['main_mobile']            = 'numeric|min:10';
            $messages['main_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['main_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "secondary_mobile" ))) {
            $rules['secondary_mobile']            = 'numeric|min:10';
            $messages['secondary_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['secondary_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "occupation" ))) {
            $rules['occupation'] = 'min:3';
            $messages['occupation.min'] = 'La ocupación debe tener al menos 3 caracteres';
        }

        if (!empty($request->input( "secondary_phone" ))) {
            $rules['secondary_phone']            = 'numeric';
            $messages['secondary_phone.numeric'] = 'El teléfono tiene un formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $customSource->name = ucfirst($request->input( "name" ));
            if($request->input( "email" )){
                $customSource->email = $request->input( "email" );
            }
            if($request->input( "mobile" )){
                $customSource->mobile = $request->input( "mobile" );
            }
            if($request->input( "is_broker" )){
                if ($request->input( "is_broker" ) == 'on') {
                    $is_broker = 1;
                }else{
                    $is_broker = 0;
                }
                $customSource->is_broker = $is_broker;
            }
            if($request->input( "agency_id" )) {
                $customSource->agency_id = $request->input( "agency_id" );
            }
            if($request->input( "main_office_phone" )){
                $customSource->main_office_phone = $request->input( "main_office_phone" );
            }
            if($request->input( "secondary_office_phone" )){
                $customSource->secondary_office_phone = $request->input( "secondary_office_phone" );
            }
            if($request->input( "address" )){
                $customSource->address = $request->input( "address" );
            }
            if($request->input( "last_name" )){
                $customSource->last_name = $request->input( "last_name" );
            }
            if($request->input( "main_email" )){
                $customSource->main_email = $request->input( "main_email" );
            }
            if($request->input( "secondary_email" )){
                $customSource->secondary_email = $request->input( "secondary_email" );
            }
            if($request->input( "home_phone" )){
                $customSource->home_phone = $request->input( "home_phone" );
            }
            if($request->input( "tel_office" )){
                $customSource->tel_office = $request->input( "tel_office" );
            }
            if($request->input( "main_mobile" )){
                $customSource->main_mobile = $request->input( "main_mobile" );
            }
            if($request->input( "secondary_mobile" )){
                $customSource->secondary_mobile = $request->input( "secondary_mobile" );
            }
            if($request->input( "sex" )){
                $customSource->gender_id = $request->input( "sex" );
            }
            if($request->input( "occupation" )){
                $customSource->occupation = $request->input( "occupation" );
            }
            if($request->input( "main_phone" )){
                $customSource->main_phone = $request->input( "main_phone" );
            }
            if($request->input( "secondary_phone" )){
                $customSource->secondary_phone = $request->input( "secondary_phone" );
            }
            if($request->input( "fax" )){
                $customSource->fax = $request->input( "fax" );
            }
            if($request->input( "street" )){
                $customSource->street = $request->input( "street" );
            }
            if($request->input( "number_int" )){
                $customSource->number_int = $request->input( "number_int" );
            }
            if($request->input( "number_ext" )){
                $customSource->number_ext = $request->input( "number_ext" );
            }
            if($request->input( "cp" )){
                $customSource->cp = $request->input( "cp" );
            }
            if($request->input( "nationality" )){
                $customSource->country_id = $request->input( "nationality" );
            }
            if($request->input( "address" )){
                $customSource->address = $request->input( "address" );
            }
            $customSource->user_id = \UsersHelpers::getUserID();

            if( $customSource->save() ){
                $customSource->event()->create(['description' => 'Recurso Agregado en creación de Cliente']);
                if (!empty($request->input("comments"))) {
                    $customSource->comment()->create(['comments' => $request->input("comments")]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Recurso Agregado');
                $res['redirect'] = url()->previous();
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }
}
