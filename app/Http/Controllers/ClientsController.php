<?php

namespace App\Http\Controllers;

use Auth;
use View;
use Session;
use Storage;
use Config;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Mailgun\Mailgun;
use App\Exports\ClientsExport;
use App\Imports\ClientsImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\{
    AcquisitionsMainMediums, AcquisitionsSecondaryMediums, clientTreatment, ContactMediums, ConversationMail, User, AcquisitionsMainSources, AcquisitionsSecondarySources, Clients, FollowUps, MarketingInformation, Countries, ContactWays, States, Interests
};

use App\Models\Sources\{
    Agencies, Agents, Referrors, Directs
};

class ClientsController extends Controller
{
    //FUNCTION LIST
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $clients = Clients::sortable()->orderBy('created_at', 'desc')->paginate(50);
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $clients = Clients::sortable()->where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->paginate(50);
        }else{
            $clients = Clients::sortable()->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate(50);
        }
        return view( "clients.index", compact('clients') );
    }

    //FUNCTION SEARCH
    public function search( Request $request ){

        $search = $request->input("search");
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $clients = Clients::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('main_email', "like", "%".$search."%")
                    ->orWhere('secondary_email', "like", "%".$search."%")
                    ->orWhere('main_mobile', "like", "%".$search."%")
                    ->orWhere('secondary_mobile', "like", "%".$search."%")
                    ->orWhere('home_phone', "like", "%".$search."%")
                    ->orWhere('office_phone', "like", "%".$search."%")
                    ->orWhere('street', "like", "%".$search."%")
                    ->orWhere('number_int', "like", "%".$search."%")
                    ->orWhere('number_ext', "like", "%".$search."%")
                    ->orWhere('zipcode', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $clients = Clients::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('main_email', "like", "%".$search."%")
                    ->orWhere('secondary_email', "like", "%".$search."%")
                    ->orWhere('main_mobile', "like", "%".$search."%")
                    ->orWhere('secondary_mobile', "like", "%".$search."%")
                    ->orWhere('home_phone', "like", "%".$search."%")
                    ->orWhere('office_phone', "like", "%".$search."%")
                    ->orWhere('street', "like", "%".$search."%")
                    ->orWhere('number_int', "like", "%".$search."%")
                    ->orWhere('number_ext', "like", "%".$search."%")
                    ->orWhere('zipcode', "like", "%".$search."%");
            })->where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $clients = Clients::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('main_email', "like", "%".$search."%")
                    ->orWhere('secondary_email', "like", "%".$search."%")
                    ->orWhere('main_mobile', "like", "%".$search."%")
                    ->orWhere('secondary_mobile', "like", "%".$search."%")
                    ->orWhere('home_phone', "like", "%".$search."%")
                    ->orWhere('office_phone', "like", "%".$search."%")
                    ->orWhere('street', "like", "%".$search."%")
                    ->orWhere('number_int', "like", "%".$search."%")
                    ->orWhere('number_ext', "like", "%".$search."%")
                    ->orWhere('zipcode', "like", "%".$search."%");
            })->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }

        return view( 'clients.index', compact('clients', 'search') );
    }

    //FUNCTION CREATE STEP ONE
    public function create( Request $request ){
        $client = $request->session()->get('client');
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->get();
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->whereIn('id', \UsersHelpers::getUsers())->get();
        }
        return view ('clients.create.step_one', compact('client', $client, 'agents') );
    }

    //FUNCTION SAVE STEP ONE
    public function saveDataStepOne( Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        //STEP 1
        $rules['name'] = 'required|min:3';
        $rules['last_name']  = 'required|min:3';
        $rules['main_email'] = 'required|email';

        //STEP 1
        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required']  = 'El apellido es obligatorio';
        $messages['last_name.min']       = 'El apellido debe tener al menos 3 caracteres';
        $messages['main_email.required'] = 'El email es obligatorio';
        $messages['main_email.email']    = 'El email tiene formato incorrecto';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            //STEP 1
            if(empty($request->session()->get('client'))){
                $client = new Clients;
                $client->name       = ucfirst($request->input( "name" ));
                $client->last_name  = $request->input( "last_name" );
                $client->main_email = $request->input( "main_email" );
                if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                    $client->user_id = $request->input( "agent_id" );
                }
                $request->session()->put('client', $client);
            }else{
                $client = $request->session()->get('client');
                $client->name       = ucfirst($request->input( "name" ));
                $client->last_name  = $request->input( "last_name" );
                $client->main_email = $request->input( "main_email" );
                if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                    $client->user_id = $request->input( "agent_id" );
                }
                $request->session()->put('client', $client);
            }

            $res['success'] = true;
            $res['redirect'] = route('createStepTwo');
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION CREATE STEP TWO
    public function createStepTwo( Request $request ){
        $treatments = clientTreatment::all();
        if(session()->has('client')){
            $client = $request->session()->get('client');
            return view('clients.create.step_two', compact('client', $client, 'treatments'));
        }else{
            return view ('clients.create.step_one');
        }
    }

    //FUNCTION SAVE STEP TWO
    public function saveDataStepTwo( Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        //STEP 2
        if (!empty($request->input( "secondary_email" ))) {
            $rules['secondary_email']          = 'email';
            $messages['secondary_email.email'] = 'El email tiene formato incorrecto';
        }

        if (!empty($request->input( "home_phone" ))) {
            $rules['home_phone']            = 'numeric';
            $messages['home_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "office_phone" ))) {
            $rules['office_phone']            = 'numeric';
            $messages['office_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "main_mobile" ))) {
            $rules['main_mobile']            = 'numeric|min:10';
            $messages['main_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['main_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "secondary_mobile" ))) {
            $rules['secondary_mobile']            = 'numeric|min:10';
            $messages['secondary_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['secondary_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            //STEP 2
            $client = $request->session()->get('client');
            if (!empty($request->input( "treatment" ))) {
                $client->treatment_id = $request->input( "treatment" );
            }

            if (!empty($request->input( "secondary_email" ))) {
                $client->secondary_email = $request->input( "secondary_email" );
            }

            if (!empty($request->input( "home_phone" ))) {
                $client->home_phone = $request->input( "home_phone" );
            }

            if (!empty($request->input( "office_phone" ))) {
                $client->office_phone = $request->input( "office_phone" );
            }

            if (!empty($request->input( "main_mobile" ))) {
                $client->main_mobile = $request->input( "main_mobile" );
            }

            if (!empty($request->input( "secondary_mobile" ))) {
                $client->secondary_mobile = $request->input( "secondary_mobile" );
            }

            $request->session()->put('client', $client);

            $res['success'] = true;
            $res['redirect'] = route('createStepThree');
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTIONS CREATE STEP THREE
    public function createStepThree(Request $request){
        if(session()->has('client')){
            $client         = $request->session()->get('client');
            $countries      = Countries::all();
            $contactWays    = ContactWays::all();
            $contactMediums = ContactMediums::all();
            $interests      = Interests::where('lang', Config::get('app.locale'))->get();
            return view ('clients.create.step_three', compact('client', $client, 'countries', 'contactWays', 'contactMediums', 'interests') );
        }else{
            return view ('clients.create.step_one');
        }
    }

    //FUNCTION SAVE STEP THREE
    public function saveDataStepThree( Request $request ){
        $res = ['success' => false];

        if (!empty($request->input( "first_contact" ))) {
            $tz = 'America/Mexico_City';
            $reqDate = strtotime($request->input( "first_contact" ));
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($reqDate);
            $date = Carbon::parse($dt->format('H:i:s'));
            $date = $date->format('Y-m-d H:i:s');
            $first_contact = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        }

        $rules = [];
        $messages = [];

        //STEP 3
        $rules['first_contact']  = 'required';
        $rules['sex']            = 'required';
        $rules['country']        = 'required';
        $rules['state']          = 'required';
        $rules['interest']       = 'required';
        $rules['level_interest'] = 'required';
        $rules['contact_way']    = 'required';

        $messages['first_contact.required']  = 'La fecha de contacto es obligatoria';
        $messages['sex.required']            = 'El sexo es obligatorio';
        $messages['country.required']        = 'El país es obligatorio';
        $messages['state.required']          = 'El estado es obligatorio';
        $messages['interest.required']       = 'El tipo interés es obligatorio';
        $messages['level_interest.required'] = 'El nivel de interés es obligatorio';
        $messages['contact_way.required']    = 'La forma de contacto es obligatoria';

        if (empty($request->input( "main_source" ))) {
            $rules['main_source']             = 'required';
            $messages['main_source.required'] = 'El recurso principal es obligatorio';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            //STEP 3
            $client = $request->session()->get('client');
            $client->first_contact = $first_contact;

            if (!empty($request->input( "street" ))) {
                $client->street = $request->input( "street" );
            }
            if (!empty($request->input( "number_int" ))) {
                $client->number_int = $request->input( "number_int" );
            }
            if (!empty($request->input( "number_ext" ))) {
                $client->number_ext = $request->input( "number_ext" );
            }
            if (!empty($request->input( "zipcode" ))) {
                $client->zipcode = $request->input( "zipcode" );
            }

            $client->gender_id  = $request->input( "sex" );
            $client->country_id = $request->input( "country" );
            $client->state_id   = $request->input( "state" );
            if (!empty($request->input( "city" ))) {
                $client->city_id = $request->input( "city" );
            }else{
                $client->city_id = NULL;
            }

            $client->contact_way_id      = $request->input( "contact_way" );
            $client->secondary_source_id = $request->input( "secondary_source" );

            $client->interest_id       = $request->input( "interest" );
            $client->level_interest_id = $request->input( "level_interest" );
            $client->is_multi          = $request->input( "is_multi" );

            if ($request->input( "is_multi" ) == 1) {
                $client->main_source_id = (empty($request->input("main_source"))) ? [] : $request->input("main_source");
            }else{
                $client->main_source_id = $request->input( "main_source" );
            }

            $client->contact_medium_id          = $request->input( "contact_medium" );
            $client->secondary_medium_source_id = $request->input( "secondary_medium_source" );
            $client->is_multi_medium            = $request->input( "is_multi_medium" );

            if ($request->input( "is_multi_medium" ) == 1) {
                $client->main_medium_source_id = (empty($request->input("main_medium_source"))) ? [] : $request->input("main_medium_source");
            }else{
                $client->main_medium_source_id = $request->input( "main_medium_source" );
            }

            $request->session()->put('client', $client);

            $res['success'] = true;
            $res['redirect'] = route('createStepFour');
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTIONS CREATE STEP FOUR
    public function createStepFour(Request $request){
        if(session()->has('client')){
            $client = $request->session()->get('client');
            return view('clients.create.step_four', compact('client', $client));
        }else{
            return view ('clients.create.step_one');
        }
    }

    //FUNCTION SAVE STEP FOUR
    public function store( Request $request ){
        $res = ['success' => false];

        //STEP 4
        $client = $request->session()->get('client');

        $redoClient = new Clients;

        $redoClient->name             = $client->name;
        $redoClient->last_name        = $client->last_name;
        $redoClient->main_email       = $client->main_email;
        $redoClient->secondary_email  = $client->secondary_email;
        $redoClient->main_mobile      = $client->main_mobile;
        $redoClient->secondary_mobile = $client->secondary_mobile;
        $redoClient->home_phone       = $client->home_phone;
        $redoClient->office_phone     = $client->office_phone;
        $redoClient->country_id       = $client->country_id;
        $redoClient->state_id         = $client->state_id;
        $redoClient->city_id          = $client->city_id;
        $redoClient->street           = $client->street;
        $redoClient->number_int       = $client->number_int;
        $redoClient->number_ext       = $client->number_ext;
        $redoClient->zipcode          = $client->zipcode;
        $redoClient->gender_id        = $client->gender_id;
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
            $redoClient->user_id = $client->user_id;
        }else{
            $redoClient->user_id = \UsersHelpers::getUserID();
        }
        $redoClient->first_contact = $client->first_contact;
        $redoClient->treatment_id  = $client->treatment_id;

        $redoClient->status = 1;

        if( $redoClient->save() ){
            $redoClient->client_key = $this->setCC($redoClient->name, $redoClient->created_at, $redoClient->id, 'GRV' );
        }

        if( $redoClient->save() ){

            if ($request->input( "send_information" ) == 'on') {

                if (!empty($request->input("f_message"))) {
                    $message = trim($request->input("f_message"));
                }else{
                    $message = 'Tu solicitud esta en seguimiento';
                }

                $render = View::make('emails.mails.quick_mail', [
                    'message' => $message,
                ])->render();

                $tmp = $render;
                $mg = Mailgun::create('key-3e0cde9342a2a366578641c36660720d');

                $domain = 'm.vlmg.mx';
                $mg->messages()->send($domain, array(
                    'from' => 'Seguimiento Inmobiliario  <'.\UsersHelpers::getUser()->email.'>',
                    'to' => $client->main_email,
                    'subject' => 'Notificación de Seguimiento - Seguimiento Inmobiliario',
                    'html' => $tmp
                ));
            }

            $followup = new FollowUps;

            $followup->folio     = 'FOLIO-'.$redoClient->id;
            $followup->status    = 1;
            $followup->type      = 'General';
            $followup->client_id = $redoClient->id;
            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                $followup->user_id = $client->user_id;
            }else{
                $followup->user_id = \UsersHelpers::getUserID();
            }

            $followup->save();

            $marketingInfo = new MarketingInformation;

            $marketingInfo->contact_way_id      = $client->contact_way_id;
            $marketingInfo->contact_medium_id   = $client->contact_medium_id;

            if ($client->is_multi != 1) {
                $marketingInfo->main_source_id  = $client->main_source_id;
            }else{
                $marketingInfo->main_source_id  = 0;
            }
            if ($client->is_multi_medium != 1) {
                $marketingInfo->main_medium_source_id = $client->main_medium_source_id;
            }else{
                $marketingInfo->main_medium_source_id = 0;
            }

            $marketingInfo->secondary_source_id        = $client->secondary_source_id;
            $marketingInfo->secondary_medium_source_id = $client->secondary_medium_source_id;
            $marketingInfo->interest_id                = $client->interest_id;
            $marketingInfo->level_interest_id          = $client->level_interest_id;
            $marketingInfo->client_id                  = $redoClient->id;
            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                $marketingInfo->user_id = $client->user_id;
            }else{
                $marketingInfo->user_id = \UsersHelpers::getUserID();
            }

            if( $marketingInfo->save() ){
                if ($client->is_multi == 1) {
                    $mainModelSource = AcquisitionsMainSources::where('contact_way_id', $client->contact_way_id)->orderBy('created_at', 'desc')->first();
                    $mainLowerSource = strtolower($mainModelSource->model);
                    $marketingInfo->$mainLowerSource()->sync($client->main_source_id);
                }
                if ($client->is_multi_medium == 1) {
                    $mainModelMedium = AcquisitionsMainMediums::where('contact_way_id', $client->contact_medium_id)->orderBy('created_at', 'desc')->first();
                    $mainLowerMedium = 'mediums_'.strtolower($mainModelMedium->model);
                    $marketingInfo->$mainLowerMedium()->sync($client->main_medium_source_id);
                }
            }

            $redoClient->event()->create(['description' => 'Cliente Agregado']);

            if (!empty($request->input( "comments" ))) {
                $redoClient->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
            }

            $request->session()->forget('client');

            $res['success'] = true;
            Session::flash('msg', 'Cliente Agregado');
            $res['redirect'] = route('clients');
        }else{
            $res['errors'][] = array('Ocurrió un error inesperado');
        }

        return $res;
    }

    //FUNCTION VIEW/EDIT
    public function view( Clients $client ){
        $countries      = Countries::all();
        $contactWays    = ContactWays::all();
        $contactMediums = ContactMediums::all();
        $marketing      = MarketingInformation::where('client_id', $client->id)->orderBy('created_at', 'desc')->first();
        $contactWay     = ContactWays::where('id', $marketing->contact_way_id)->first();
        $contactMedium  = ContactMediums::where('id', $marketing->contact_medium_id)->first();
        $conversations  = ConversationMail::where('sender', $client->main_email)->where('recipient', \UsersHelpers::getUser()->email)->latest()->paginate(20, ['*'], 'mails');
        $interests      = Interests::where('lang', Config::get('app.locale'))->get();
        $followups      = FollowUps::where('client_id', $client->id)->paginate(50, ['*'], 'followups');
        $path_model     = 'App\Models\Sources\\';
        if($contactWay){
            if($contactWay->is_multi){
                $multiSourcesModel = strtolower($contactWay->model);
                $multiSources = $marketing->$multiSourcesModel()->get();
            }else{
                $multiSources = NULL;
            }
        }else{
            $multiSources = NULL;
        }
        if($contactMedium){
            if($contactMedium->is_multi){
                $multiMediumsModel = 'mediums_'.strtolower($contactMedium->model);
                $multiMediums = $marketing->$multiMediumsModel()->get();
            }else{
                $multiMediums = NULL;
            }
        }else{
            $multiMediums = NULL;
        }
        if($marketing->main_source_id){
            if(!is_null($marketing->main_source_id)){
                $mainModel     = AcquisitionsMainSources::where('contact_way_id', $marketing->contact_way_id)->orderBy('created_at', 'desc')->first();
                $mainModelName = $path_model.$mainModel->model;
                $mainSource    = $mainModelName::where('id', $marketing->main_source_id)->orderBy('created_at', 'desc')->first();
                if(!is_null($mainSource)) {
                    $mainSource = $mainSource;
                }else{
                    $mainSource = NULL;
                }
            }else{
                $mainSource = 0;
            }
        }else{
            $mainSource = 0;
        }
        if($marketing->main_medium_source_id){
            if(!is_null($marketing->main_medium_source_id)){
                $mainModelMedium     = AcquisitionsMainMediums::where('contact_way_id', $marketing->contact_medium_id)->orderBy('created_at', 'desc')->first();
                $mainModelNameMedium = $path_model . $mainModelMedium->model;
                $mainMedium          = $mainModelNameMedium::where('id', $marketing->main_medium_source_id)->orderBy('created_at', 'desc')->first();

                if(!is_null($mainMedium)) {
                    $mainMedium = $mainMedium;
                }else{
                    $mainMedium = 0;
                }
            }else{
                $mainMedium = 0;
            }
        }else{
            $mainMedium = 0;
        }
        if($marketing->secondary_source_id){
            if(!is_null($marketing->secondary_source_id)){
                $secondaryModel     = AcquisitionsSecondarySources::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();
                $secondaryModelName = $path_model . $secondaryModel->model;
                $secondarySource    = $secondaryModelName::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();

            }else{
                $secondarySource = 0;
            }
        }else{
            $secondarySource = 0;
        }
        if($marketing->secondary_medium_source_id){
            if(!is_null($marketing->secondary_medium_source_id)){
                $secondaryModelMedium = AcquisitionsSecondaryMediums::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();
                $secondaryModelNameMedium = $path_model . $secondaryModelMedium->model;
                $secondaryMedium = $secondaryModelNameMedium::where('id', $marketing->secondary_medium_source_id)->orderBy('created_at', 'desc')->first();

            }else{
                $secondaryMedium = 0;
            }
        }else{
            $secondaryMedium = 0;
        }
        return view ('clients.view', compact('client', 'countries', 'contactWays', 'contactMediums', 'marketing', 'interests', 'mainSource', 'mainMedium', 'secondarySource', 'secondaryMedium', 'multiSources', 'multiMediums', 'conversations', 'followups') );
    }

    //FUNCTION UPDATE
    public function update( Clients $client, Request $request ){
        $marketingInfo = MarketingInformation::where('client_id', $client->id)->first();
        if($client->status) {
            $contactWay        = ContactWays::where('id', $marketingInfo->contact_way_id)->first();
            $contactMedium     = ContactMediums::where('id', $marketingInfo->contact_medium_id)->first();
        }else{
            $contactWay        = ContactWays::where('id', $request->input( "contact_way" ))->first();
            $contactMedium     = ContactMediums::where('id', $request->input( "contact_medium" ))->first();
        }
        $multiSourcesModel = strtolower($contactWay->model);
        $multiMediumsModel = 'mediums_' . strtolower($contactMedium->model);

        $currentClient = Clients::where('id', $client->id)->first();
        $keyComment    = count($currentClient->comment) - 1;
        $res = ['success' => false];

        if (!empty($request->input( "first_contact" ))) {
            $tz = 'America/Mexico_City';
            $reqDate = strtotime($request->input( "first_contact" ));
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($reqDate);
            $date = Carbon::parse($dt->format('H:i:s'));
            $date = $date->format('Y-m-d H:i:s');
            $first_contact = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        }

        $rules = [];
        $messages = [];

        //STEP 1
        $rules['name']       = 'required|min:3';
        $rules['last_name']  = 'required|min:3';
        $rules['main_email'] = ['required', 'email', Rule::unique('clients')->ignore($client->id) ];

        //STEP 1
        $messages['name.required']       = 'El nombre es obligatorio';
        $messages['name.min']            = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required']  = 'El apellido es obligatorio';
        $messages['last_name.min']       = 'El apellido debe tener al menos 3 caracteres';
        $messages['main_email.required'] = 'El email es obligatorio';
        $messages['main_email.email']    = 'El email tiene formato incorrecto';
        $messages['main_email.unique']   = 'El email ya se encuentra registrado';

        if(!$client->status){
            $rules['contact_way']             = 'required';
            $rules['main_source']             = 'required';
            $rules['contact_medium']          = 'required';
            $rules['main_medium_source']      = 'required';
            $rules['interest']                = 'required';
            $rules['level_interest']          = 'required';

            $messages['contact_way.required']             = 'La forma de contacto es obligatoria';
            $messages['main_source.required']             = 'El recurso principal de la forma es obligatorio';
            $messages['contact_medium.required']          = 'El medio de contacto es obligatorio';
            $messages['main_medium_source.required']      = 'El recurso principal del medio es obligatorio';
            $messages['interest.required']                = 'El tipo interés es obligatorio';
            $messages['level_interest.required']          = 'El nivel de interés es obligatorio';

            if (!empty($request->input( "has_secondary_source" ))) {
                $rules['secondary_source']             = 'required';
                $messages['secondary_source.required'] = 'El recurso secundario de la forma es obligatorio';
            }

            if (!empty($request->input( "has_secondary_medium_source" ))) {
                $rules['secondary_medium_source']             = 'required';
                $messages['secondary_medium_source.required'] = 'El recurso secundario del medio es obligatorio';
            }
        }

        //STEP 2
        if (!empty($request->input( "secondary_email" ))) {
            $rules['secondary_email']           = 'email';
            $messages['secondary_email.email']  = 'El email tiene formato incorrecto';
        }

        if (!empty($request->input( "home_phone" ))) {
            $rules['home_phone']            = 'numeric';
            $messages['home_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "office_phone" ))) {
            $rules['office_phone']            = 'numeric';
            $messages['office_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "main_mobile" ))) {
            $rules['main_mobile']            = 'numeric|min:10';
            $messages['main_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['main_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "secondary_mobile" ))) {
            $rules['secondary_mobile']            = 'numeric|min:10';
            $messages['secondary_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['secondary_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            //STEP 1
            $client->name       = ucfirst($request->input( "name" ));
            $client->last_name  = $request->input( "last_name" );
            $client->main_email = $request->input( "main_email" );

            //STEP 2
            if (!empty($request->input( "treatment" ))) {
                $client->treatment_id = $request->input( "treatment" );
            }

            if (!empty($request->input( "secondary_email" ))) {
                $client->secondary_email = $request->input( "secondary_email" );
            }

            if (!empty($request->input( "home_phone" ))) {
                $client->home_phone = $request->input( "home_phone" );
            }

            if (!empty($request->input( "office_phone" ))) {
                $client->office_phone = $request->input( "office_phone" );
            }

            if (!empty($request->input( "main_mobile" ))) {
                $client->main_mobile = $request->input( "main_mobile" );
            }

            if (!empty($request->input( "secondary_mobile" ))) {
                $client->secondary_mobile = $request->input( "secondary_mobile" );
            }

            if ($request->input( "client_edit" )) {
                //STEP 3
                if (!empty($request->input( "first_contact" ))) {
                    $client->first_contact = $first_contact;
                }
                if (!empty($request->input( "street" ))) {
                    $client->street = $request->input( "street" );
                }
                if (!empty($request->input( "number_int" ))) {
                    $client->number_int = $request->input( "number_int" );
                }
                if (!empty($request->input( "number_ext" ))) {
                    $client->number_ext = $request->input( "number_ext" );
                }
                if (!empty($request->input( "zipcode" ))) {
                    $client->zipcode = $request->input( "zipcode" );
                }
                if (!empty($request->input( "sex" ))) {
                    $client->gender_id  = $request->input( "sex" );
                }
                if (!empty($request->input( "country" ))) {
                    $client->country_id = $request->input( "country" );
                }
                if (!empty($request->input( "state" ))) {
                    $client->state_id   = $request->input( "state" );
                }
                if (!empty($request->input( "city" ))) {
                    $client->city_id = $request->input( "city" );
                }else{
                    $client->city_id = NULL;
                }

                $client->status = 1;

                if (!empty($request->input( "contact_way" ))) {
                    $marketingInfo->contact_way_id = $request->input("contact_way");
                }
                if (!empty($request->input( "main_source" ))) {
                    if ($request->input( "is_multi" ) == 1) {
                        $multiSources = (empty($request->input("main_source"))) ? [] : $request->input("main_source");
                        $marketingInfo->main_source_id = 0;
                        $marketingInfo->$multiSourcesModel()->sync($multiSources);
                    }else{
                        $marketingInfo->main_source_id = $request->input("main_source");
                        $marketingInfo->$multiSourcesModel()->sync([]);
                    }
                }
                if (!empty($request->input( "contact_medium" ))) {
                    $marketingInfo->contact_medium_id = $request->input("contact_medium");
                }
                if (!empty($request->input( "main_medium_source" ))) {
                    if ($request->input( "is_multi_medium" ) == 1) {
                        $multiMediums = (empty($request->input("main_medium_source"))) ? [] : $request->input("main_medium_source");
                        $marketingInfo->main_medium_source_id = 0;
                        $marketingInfo->$multiMediumsModel()->sync($multiMediums);
                    }else{
                        $marketingInfo->main_medium_source_id = $request->input("main_medium_source");
                        $marketingInfo->$multiMediumsModel()->sync([]);
                    }
                }
                if (!empty($request->input( "secondary_source" ))) {
                    $marketingInfo->secondary_source_id = $request->input("secondary_source");
                }
                if (!empty($request->input( "secondary_medium_source" ))) {
                    $marketingInfo->secondary_medium_source_id = $request->input("secondary_medium_source");
                }
                if (!empty($request->input( "interest" ))) {
                    $marketingInfo->interest_id = $request->input( "interest" );
                }
                if (!empty($request->input( "level_interest" ))) {
                    $marketingInfo->level_interest_id = $request->input( "level_interest" );
                }


                if ($request->input( "has_secondary_source" ) != 1) {
                    $marketingInfo->secondary_source_id = NULL;
                }

                if ($request->input( "has_secondary_medium_source" ) != 1) {
                    $marketingInfo->secondary_medium_source_id = NULL;
                }

                $marketingInfo->save();
            }

            if( $client->save() ){

                $client->event()->create(['description' => 'Cliente Actualizado']);

                if($client->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentClient->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $client->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $client->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }

                $res['success'] = true;
                $res['redirect'] = route('clientView', $client->id);
                Session::flash('msg', 'Cliente Actualizado');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DELETE
    public function delete( Clients $client ){
        $res = ['success' => true];
        if( $client->delete() ){
            $client->event()->create(['description' => 'Cliente Eliminado']);
            Session::flash('msg', 'Cliente borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }

    //FUNCTION VERIFY EMAIL
    public function verifyEmail( $email, $name ){
        $res = ['success' => false];
        $emailVerify = Clients::where('main_email', $email)->first();
        $clients = Clients::where('main_email', $email)->orWhere('name', 'like', '%' . $name . '%')->get();
        $clients->map(function ($client) use ($email) {
            if ($client->main_email == $email) {
                $client['exist'] = 'bg-red';
            }else{
                $client['exist'] = '';
            }
            $client['agent'] = $client->user->name;
            return $client;
        });

        if (is_null($emailVerify)) {
            $res['clients'] = $clients;
            $res['success'] = true;
        }else{
            $res['clients'] = $clients;
            $res['success'] = false;
        }

        return $res;
    }

    //FUNCTION GET STATES INFO
    public function getStates(Countries $country){
        return $country->states()->select('id', 'name')->get();
    }

    //FUNCTION GET CITIES INFO
    public function getCities(States $state){
        return $state->cities()->select('id', 'name')->get();
    }

    //FUNCTION GET MAIN SOURCES INFO
    public function getMainSources($source){
        $model = AcquisitionsMainSources::where('contact_way_id', $source)->first();
        $path_model = 'App\Models\Sources\\';
        $model_name = $path_model.$model->model;
        return $model_name::all();
    }

    //FUNCTION GET SECONDARY SOURCES INFO
    public function getSecondarySources($mainSource, $idSource){
        $idMainSource = AcquisitionsMainSources::where('id', $mainSource)->first();
        $secondarySource = AcquisitionsSecondarySources::where('id', $idMainSource->secondary_id)->first();
        $path_model = 'App\Models\Sources\\';
        $model_name = $path_model.$secondarySource->model;
        return $model_name::where('agency_id', $idSource)->get();
    }

    //FUNCTION GET MAIN MEDIUMS INFO
    public function getMainMediums($source){
        $model = AcquisitionsMainMediums::where('contact_way_id', $source)->first();
        $path_model = 'App\Models\Sources\\';
        $model_name = $path_model.$model->model;
        return $model_name::all();
    }

    //FUNCTION GET SECONDARY MEDIUMS INFO
    public function getSecondaryMediums($mainSource, $idSource){
        $idMainSource = AcquisitionsMainMediums::where('id', $mainSource)->first();
        $secondarySource = AcquisitionsSecondaryMediums::where('id', $idMainSource->secondary_id)->first();
        $path_model = 'App\Models\Sources\\';
        $model_name = $path_model.$secondarySource->model;
        return $model_name::where('agency_id', $idSource)->get();
    }

    //FUNCTION GENERATE KEY OF CLIENT
    private function setCC($str1, $str2, $str3, $str4){
        $cad1 = str_split($str1, 2);
        $search = array(':','-' ,' ' );
        $cad2 = str_replace($search, "", $str2);
        $cad3 = str_pad($str3, 3, "0", STR_PAD_LEFT);
        $cad4 = $str4;

        return strtoupper($cad1[0]).$cad2.$cad3.$cad4;
    }

    //FUNCTION EXPORT CLIENTS
    public function export(){
        return Excel::download(new ClientsExport, 'clients.csv');
    }

    //FUNCTION FORM IMPORT CLIENTS
    public function formImport(){
        return view( "imports.clients");
    }

    //FUNCTION IMPORT CLIENTS
    public function import(Request $request){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            $file          = $request->file('file_clients');
            $code          = substr(md5(uniqid(mt_rand(), true)) , 0, 5);
            $ext           = $file->getClientOriginalExtension();
            $title         = 'clients';
            $filename      = $title.'_'.$code.'.'.$ext;
            $cleanFileName = \GlobalHelpers::cleanTitle($filename);
            Storage::disk('public')->put( "imports/".$cleanFileName, \File::get( $file ) );
            $getFile   = "imports/".$cleanFileName;
            if( Excel::import(new ClientsImport, $getFile, 'public') ){
                $res['success'] = true;
                Session::flash('msg', 'Clientes Importados');
                $res['redirect'] = route('clients');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION RE-ASSIGN CLIENT
    public function reassign( Clients $client){
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view( "clients.reassign", compact('agents', 'client') );
    }

    //FUNCTION RE-ASSIGN UPDATE CLIENT
    public function reassignUpdate( Clients $client, Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $rules['user_id'] = 'required';
        $messages['user_id.required'] = 'El usuario es obligatorio';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $marketingInfo = MarketingInformation::where('client_id', $client->id)->first();
            $client->user_id = $request->input( "user_id" );
            $client->status  = 0;

            if( $client->save() ){
                $currentFollowUp = FollowUps::where('type', 'General')->where('client_id', $client->id)->first();
                if( $currentFollowUp->delete() ){
                    $currentFollowUp->users()->detach();
                    $followup = new FollowUps;

                    $followup->folio     = 'FOLIO-'.$client->id;
                    $followup->status    = 1;
                    $followup->type      = 'General';
                    $followup->client_id = $client->id;
                    if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])) {
                        $followup->user_id = $client->user_id;
                    }else{
                        $followup->user_id = \UsersHelpers::getUserID();
                    }

                    $followup->save();
                }

                $marketingInfo->user_id = $request->input( "user_id" );
                $marketingInfo->contact_way_id = 0;
                $marketingInfo->main_source_id = 0;
                $marketingInfo->secondary_source_id = NULL;
                $marketingInfo->contact_medium_id = NULL;
                $marketingInfo->main_medium_source_id = NULL;
                $marketingInfo->secondary_medium_source_id = NULL;
                $marketingInfo->interest_id = 0;
                $marketingInfo->level_interest_id = 0;
                $marketingInfo->save();
                $client->event()->create(['description' => 'Cliente Re-asignado']);

                $res['success'] = true;
                $res['redirect'] = route('clientView', $client->id);
                Session::flash('msg', 'Cliente Re-asignado');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION SHOW FORM
    public function formMail( Clients $client ){
        return view( "clients.mail", compact('client') );
    }

    //FUNCTION SEND MAIL
    public function sendMail( Clients $client, Request $request ){

        $followup = FollowUps::where('client_id', $client->id)->first();
        $collaborators = [];
        if($followup->users()->count() > 0){
            foreach ($followup->users() as $user){
                $collaborators[] = $user->email;
            }
        }

        $collaborators[] = User::find($followup->user_id)->first()->email;

        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['f_subject'] = 'required|min:3';
        $rules['f_message'] = 'required|min:3';

        $messages['f_subject.required'] = 'El asunto es obligatorio';
        $messages['f_subject.min']      = 'El asunto debe tener 3 caracteres';
        $messages['f_message.required'] = 'El mensaje es obligatorio';
        $messages['f_message.min']      = 'El mensaje debe tener 3 caracteres';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ) {
            $userName   = \UsersHelpers::getUser()->name;
            $clientName = $client->name;

            $subject = $request->input("f_subject");
            $message = $request->input("f_message");

            $render = View::make('emails.mails.agents', [
                'user_name'   => trim($userName),
                'client_name' => trim($clientName),
                'message'     => trim($message),
            ])->render();

            $tmp = $render;
            $mg = Mailgun::create('key-3e0cde9342a2a366578641c36660720d');

            $domain = 'm.vlmg.mx';
            if ($request->hasFile('f_file')){
                $src = $request->file('f_file');
                $ext = $src->getClientOriginalExtension();
                $date = new DateTime();
                $stamp = $date->getTimestamp();
                $filename = "tmp_file_" . $stamp . '.' . $ext;
                $urlFile = "mails/" . $filename;
                Storage::disk('public')->put($urlFile, \File::get($src));
                $getFile = public_path() . Storage::url($urlFile);
                $mg->messages()->send($domain, array(
                    'from' => 'Seguimiento Inmobiliario  <'.\UsersHelpers::getUser()->email.'>',
                    'to' => $collaborators,
                    'subject' => $subject,
                    'html' => $tmp,
                    'attachment' => [
                        ['filePath' => $getFile, 'filename' => 'tmp_file.' . $ext]
                    ]
                ));

                Storage::disk('public')->delete($urlFile);
            }else{
                $mg->messages()->send($domain, array(
                    'from' => 'Seguimiento Inmobiliario  <'.\UsersHelpers::getUser()->email.'>',
                    'to' => $collaborators,
                    'subject' => $subject,
                    'html' => $tmp,
                ));
            }

            $res['success'] = true;
            Session::flash('msg', 'Mensaje Enviado');
            $res['redirect'] = route('dashboard');

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION SHOW FORM MESSAGE
    public function formClientMail( Clients $client ){
        return view( "clients.response", compact('client') );
    }
}
