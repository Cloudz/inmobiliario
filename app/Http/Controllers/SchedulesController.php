<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Schedules
};

use App\Models\Sources\{
    Agents, SalesOffices
};

class SchedulesController extends Controller
{
    public function index(){
        $schedules = Schedules::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "schedules.index", compact('schedules') );
    }

    public function create(){
        $agents  = Agents::all();
        $offices = SalesOffices::all();
        return view ('schedules.create', compact('agents', 'offices') );
    }

    public function store( Request $request ){
        $res      = ['success' => false];
        $schedule = new Schedules;

        $rules    = [];
        $messages = [];

        $rules['agent']    = 'required';
        $rules['office']   = 'required';
        $rules['start_at'] = 'required';
        $rules['end_at']   = 'required';

        $messages['agent.required']    = 'El agente es obligatorio';
        $messages['office.required']   = 'La oficina es obligatoria';
        $messages['start_at.required'] = 'La fecha de inicio es obligatoria';
        $messages['end_at.required']   = 'La fecha de terminación es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $schedule->agent_id  = $request->input( "agent" );
            $schedule->office_id = $request->input( "office" );
            $schedule->start_at  = $request->input( "start_at" );
            $schedule->end_at    = $request->input( "end_at" );

            if( $schedule->save() ){
                $schedule->event()->create(['description' => 'Se creo un horario de guardia']);
                if (!empty($request->input( "comments" ))) {
                    $schedule->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Horario de Guardia Agregado');
                $res['redirect'] = route('schedules');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( Schedules $schedule ){
        $agents  = Agents::all();
        $offices = SalesOffices::all();
        return view ('schedules.view', compact('agents', 'offices', 'schedule') );
    }

    public function update( Schedules $schedule, Request $request ){

        $currentSchedule = Schedules::where('id', $schedule->id)->first();
        $keyComment = count($currentSchedule->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['agent']    = 'required';
        $rules['office']   = 'required';
        $rules['start_at'] = 'required';
        $rules['end_at']   = 'required';

        $messages['agent.required']    = 'El agente es obligatorio';
        $messages['office.required']   = 'La oficina es obligatoria';
        $messages['start_at.required'] = 'La fecha de inicio es obligatoria';
        $messages['end_at.required']   = 'La fecha de terminación es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $schedule->agent_id  = $request->input( "agent" );
            $schedule->office_id = $request->input( "office" );
            $schedule->start_at  = $request->input( "start_at" );
            $schedule->end_at    = $request->input( "end_at" );

            if( $schedule->save() ){
                $schedule->event()->create(['description' => 'Horario de Guardia Actualizado']);
                if($schedule->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentSchedule->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $schedule->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $schedule->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                $res['success'] = true;
                Session::flash('msg', 'Horario de Guardia Editado');
                $res['redirect'] = route('scheduleView', $schedule->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Schedules $schedule ){
        $res = ['success' => true];
        if( $schedule->delete() ){
            $schedule->event()->create(['description' => 'Horario de Guardia Eliminado']);
            Session::flash('msg', 'Horario de Guardia borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
