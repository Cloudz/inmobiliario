<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Session;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Webklex\IMAP\Client;

use App\Models\{
    Clients, ConversationMail, Visits
};

use App\Models\Sources\{
    Referrors, Agencies, Agents
};

class DashboardController extends Controller
{
    public function index(Request $request){
        $getMails = ConversationMail::where('recipient', \UsersHelpers::getUser()->email)->get();

        $conversationsArray = [];

        foreach($getMails as $k=>$mail)
        {
            $conversationsArray[$mail->sender]['sender']                      = $mail->sender;
            $conversationsArray[$mail->sender]['messages'][$k]['subject']     = $mail->subject;
            $conversationsArray[$mail->sender]['messages'][$k]['htmlBody']    = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$mail->htmlBody);
            $conversationsArray[$mail->sender]['messages'][$k]['textBody']    = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$mail->textBody);
            $conversationsArray[$mail->sender]['messages'][$k]['created_at']  = $mail->created_at;
            $conversationsArray[$mail->sender]['messages'][$k]['attachments'] = \GlobalHelpers::unserialize($mail->attachments);
        }

        $conversations = \GlobalHelpers::customPagination($conversationsArray, 10, 'conversations');

        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $clients = Clients::latest()->get();
            $referrors = Referrors::all();
            $agencies = Agencies::all();
            $agents = Agents::all();
            $visits = Visits::latest()->get();
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])){
            $clients     = Clients::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->latest()->get();
            $referrors   = Referrors::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->get();
            $agencies    = Agencies::all();
            $agents      = Agents::all();
            $visits      = Visits::latest()->get();
        }else{
            $clients     = Clients::where('user_id', \UsersHelpers::getUserID())->latest()->get();
            $referrors   = Referrors::where('user_id', \UsersHelpers::getUserID())->get();
            $agencies    = Agencies::all();
            $agents      = Agents::all();
            $visits      = Visits::latest()->get();
        }
        $lastClientsArray = [];
        $lastVisitsArray  = [];

        foreach ($clients as $client){
            $tz = 'America/Mexico_City';
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($timestamp);
            $now = Carbon::parse($dt->format('Y-m-d H:i:s'));
            $end = $client->created_at;
            $end = Carbon::parse($end);
            $diffInHours = $end->diffInDays($now);
            if ($diffInHours <= 7){
                $lastClientsArray[] = $client;
            }
        }

        $lastClients = \GlobalHelpers::customPagination($lastClientsArray, 10, 'clients');

        foreach ($visits as $visit){
            $tz = 'America/Mexico_City';
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($timestamp);
            $now = Carbon::parse($dt->format('Y-m-d H:i:s'));
            $end = $visit->created_at;
            $end = Carbon::parse($end);
            $diffInHours = $end->diffInDays($now);
            if ($diffInHours <= 7){
                $lastVisitsArray[] = $visit;
            }
        }

        $lastVisits = \GlobalHelpers::customPagination($lastVisitsArray, 10, 'visits');

        return view( 'dashboard.index', compact('clients','lastClients', $lastClients, 'referrors', 'agencies', 'agents', 'lastVisits', $lastVisits, 'conversations', $conversations, 'newMails') );
    }
}
