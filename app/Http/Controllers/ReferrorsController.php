<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Clients, Countries, User
};

use App\Models\Sources\{
    Referrors
};


class ReferrorsController extends Controller
{
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $referrors = Referrors::sortable()->paginate(50);
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $referrors = Referrors::sortable()->where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->paginate(50);
        }else{
            $referrors = Referrors::sortable()->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate(50);

        }
        return view( "referrors.index", compact('referrors') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $referrors = Referrors::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('main_email', "like", "%".$search."%")
                    ->orWhere('secondary_email', "like", "%".$search."%")
                    ->orWhere('main_mobile', "like", "%".$search."%")
                    ->orWhere('secondary_mobile', "like", "%".$search."%")
                    ->orWhere('home_phone', "like", "%".$search."%")
                    ->orWhere('office_phone', "like", "%".$search."%")
                    ->orWhere('address', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        }elseif(\UsersHelpers::getUser()->hasRole(['Personal Assistant'])) {
            $referrors = Referrors::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('main_email', "like", "%".$search."%")
                    ->orWhere('secondary_email', "like", "%".$search."%")
                    ->orWhere('main_mobile', "like", "%".$search."%")
                    ->orWhere('secondary_mobile', "like", "%".$search."%")
                    ->orWhere('home_phone', "like", "%".$search."%")
                    ->orWhere('office_phone', "like", "%".$search."%")
                    ->orWhere('address', "like", "%".$search."%");
            })->where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $referrors = Referrors::sortable()->where(function ($query) use ($search) {
                            $query->where('name', "like", "%".$search."%")
                            ->orWhere('last_name', "like", "%".$search."%")
                            ->orWhere('main_email', "like", "%".$search."%")
                            ->orWhere('secondary_email', "like", "%".$search."%")
                            ->orWhere('main_mobile', "like", "%".$search."%")
                            ->orWhere('secondary_mobile', "like", "%".$search."%")
                            ->orWhere('home_phone', "like", "%".$search."%")
                            ->orWhere('office_phone', "like", "%".$search."%")
                            ->orWhere('address', "like", "%".$search."%");
                    })->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( 'referrors.index', compact('referrors', 'search') );
    }

    public function create( Request $request ){
        $users     = User::all();
        $countries = Countries::all();
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view ('referrors.create', compact('users', 'countries', 'current_tab', 'agents') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $referror = new Referrors;

        $rules = [];
        $messages = [];

        $rules['name']  = 'required|min:3';
        $rules['last_name']   = 'required|min:3';
        $rules['main_email']  = 'required|email|unique:referrors';
        $rules['sex']         = 'required';
        $rules['nationality'] = 'required';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required']   = 'El apellido es obligatorio';
        $messages['last_name.min']        = 'El apellido debe tener al menos 3 caracteres';
        $messages['main_email.required']  = 'El email es obligatorio';
        $messages['main_email.email']     = 'El email tiene formato incorrecto';
        $messages['main_email.unique']    = 'El email ya se encuentra registrado';
        $messages['sex.required']         = 'El sexo es obligatorio';
        $messages['nationality.required'] = 'La nacionalidad es obligatoria';

        if (!empty($request->input( "secondary_email" ))) {
            $rules['secondary_email']           = 'email';
            $messages['secondary_email.email']  = 'El email tiene formato incorrecto';
        }

        if (!empty($request->input( "home_phone" ))) {
            $rules['home_phone']            = 'numeric';
            $messages['home_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "office_phone" ))) {
            $rules['office_phone']            = 'numeric';
            $messages['office_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "main_mobile" ))) {
            $rules['main_mobile']            = 'numeric|min:10';
            $messages['main_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['main_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "secondary_mobile" ))) {
            $rules['secondary_mobile']            = 'numeric|min:10';
            $messages['secondary_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['secondary_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "occupation" ))) {
            $rules['occupation'] = 'min:3';
            $messages['occupation.min'] = 'La ocupación debe tener al menos 3 caracteres';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $referror->name    = ucfirst($request->input( "name" ));
            $referror->last_name     = $request->input( "last_name" );
            $referror->main_email    = $request->input( "main_email" );

            if (!empty($request->input( "secondary_email" ))) {
                $referror->secondary_email = $request->input( "secondary_email" );
            }

            if (!empty($request->input( "home_phone" ))) {
                $referror->home_phone = $request->input( "home_phone" );
            }

            if (!empty($request->input( "office_phone" ))) {
                $referror->office_phone = $request->input( "office_phone" );
            }

            if (!empty($request->input( "main_mobile" ))) {
                $referror->main_mobile = $request->input( "main_mobile" );
            }

            if (!empty($request->input( "secondary_mobile" ))) {
                $referror->secondary_mobile = $request->input( "secondary_mobile" );
            }

            $referror->gender_id     = $request->input( "sex" );
            $referror->country_id    = $request->input( "nationality" );
            $referror->occupation    = $request->input( "occupation" );

            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
                $referror->user_id = $request->input( "agent_id" );
            }else{
                $referror->user_id = \UsersHelpers::getUserID();
            }

            if( $referror->save() ){
                $referror->event()->create(['description' => 'Referido Agregado']);
                if (!empty($request->input( "comments" ))) {
                    $referror->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Referido Agregado');
                $res['redirect'] = route('referrors');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function view( Referrors $referror ){
        $users     = User::all();
        $countries = Countries::all();
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
            $clients = Clients::all();
        }else{
            $clients = Clients::where('user_id', \UsersHelpers::getUserID())->get();
        }
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view( "referrors.view", compact('users', 'referror', 'clients', 'countries', 'agents') );
    }

    public function update( Referrors $referror, Request $request ){
        $currentReferror = Referrors::where('id', $referror->id)->first();
        $keyComment = count($currentReferror->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name']  = 'required|min:3';
        $rules['last_name']   = 'required|min:3';
        $rules['main_email']  = ['required', 'email', Rule::unique('referrors')->ignore($referror->id) ];
        $rules['sex']         = 'required';
        $rules['nationality'] = 'required';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required']   = 'El apellido es obligatorio';
        $messages['last_name.min']        = 'El apellido debe tener al menos 3 caracteres';
        $messages['main_email.required']  = 'El email es obligatorio';
        $messages['main_email.email']     = 'El email tiene formato incorrecto';
        $messages['main_email.unique']    = 'El email ya se encuentra registrado';
        $messages['sex.required']         = 'El sexo es obligatorio';
        $messages['nationality.required'] = 'La nacionalidad es obligatoria';

        if (!empty($request->input( "secondary_email" ))) {
            $rules['secondary_email']           = 'email';
            $messages['secondary_email.email']  = 'El email tiene formato incorrecto';
        }

        if (!empty($request->input( "home_phone" ))) {
            $rules['home_phone']            = 'numeric';
            $messages['home_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "office_phone" ))) {
            $rules['office_phone']            = 'numeric';
            $messages['office_phone.numeric'] = 'El celular tiene un formato incorrecto';
        }

        if (!empty($request->input( "main_mobile" ))) {
            $rules['main_mobile']            = 'numeric|min:10';
            $messages['main_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['main_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "secondary_mobile" ))) {
            $rules['secondary_mobile']            = 'numeric|min:10';
            $messages['secondary_mobile.numeric'] = 'El celular tiene un formato incorrecto';
            $messages['secondary_mobile.min']     = 'El celular debe tener al menos 10 numeros';
        }

        if (!empty($request->input( "occupation" ))) {
            $rules['occupation'] = 'min:3';
            $messages['occupation.min'] = 'La ocupación debe tener al menos 3 caracteres';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            $referror->name    = ucfirst($request->input( "name" ));
            $referror->last_name     = $request->input( "last_name" );
            $referror->main_email    = $request->input( "main_email" );

            if (!empty($request->input( "secondary_email" ))) {
                $referror->secondary_email = $request->input( "secondary_email" );
            }

            if (!empty($request->input( "home_phone" ))) {
                $referror->home_phone = $request->input( "home_phone" );
            }

            if (!empty($request->input( "office_phone" ))) {
                $referror->office_phone = $request->input( "office_phone" );
            }

            if (!empty($request->input( "main_mobile" ))) {
                $referror->main_mobile = $request->input( "main_mobile" );
            }

            if (!empty($request->input( "secondary_mobile" ))) {
                $referror->secondary_mobile = $request->input( "secondary_mobile" );
            }

            $referror->gender_id     = $request->input( "sex" );
            $referror->country_id    = $request->input( "nationality" );
            $referror->occupation    = $request->input( "occupation" );

            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])) {
                $referror->user_id = $request->input( "agent_id" );
            }

            if( $referror->save() ){
                $referror->event()->create(['description' => 'Referido Editado']);
                if($referror->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentReferror->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $referror->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $referror->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                $res['success'] = true;
                $res['redirect'] = route('referrors', $referror->id);
                Session::flash('msg', 'Referido actualizado');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Referrors $referror ){
        $res = ['success' => true];
        if( $referror->delete() ){
            $referror->event()->create(['description' => 'Referido Eliminado']);
            Session::flash('msg', 'Referido borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
