<?php

namespace App\Http\Controllers;

use Storage;

use App\Models\ConversationMail;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MailgunWidgetsController extends Controller
{
    public function store(Request $request)
    {
        $files = collect(json_decode($request->input('attachments'), true));

        $mail = new ConversationMail;

        $mail->subject  = $request->input('subject');
        $mail->sender   = $request->input('sender');

        if($request->input('body-html')){
            $mail->htmlBody = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$request->input('body-html'));
        }else{
            $mail->htmlBody = NULL;
        }

        if($request->input('body-plain')){
            $mail->textBody = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$request->input('body-plain'));
        }else{
            $mail->textBody = NULL;
        }

        $mail->recipient = $request->input('recipient');
        $mail->type      = 'Answer';

        $storageFiles = [];
        foreach ($files as $file){
            $code = substr(md5(uniqid(mt_rand(), true)) , 0, 50);
            $title_file = str_replace(':', '', $mail->subject).'_'.$code;
            $ext = strrchr($file['name'], '.');
            $filename = $title_file.$ext;

            $response = (new Client())->get($file['url'], [
                'auth' => ['api', 'key-3e0cde9342a2a366578641c36660720d'],
            ]);

            $cleanFileName = \GlobalHelpers::cleanTitle($filename);
            $resource = Storage::disk('public')->put( "mails/".$cleanFileName, $response->getBody() );

            if ( $resource ){
                array_push($storageFiles, $cleanFileName);
            }
        }


        $mail->attachments = serialize($storageFiles);

        $mail->save();


        return response()->json(['status' => 'ok'], 200);
    }
}
