<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Session;
use DB;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\{
    Clients, FollowUps, User
};

class FollowUpsController extends Controller
{
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $followups = FollowUps::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $followups = FollowUps::where('user_id', \UsersHelpers::getUserID())
                ->orWhereHas('users', function($q){
                $q->where('followups_collaborators.user_id', '=', \UsersHelpers::getUserID());
            })->sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( "admin.followups.index", compact('followups') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $followups = FollowUps::sortable()->where(function ($query) use ($search) {
                $query->where('folio', "like", "%".$search."%")
                    ->orWhere('status', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $followups = FollowUps::where('user_id', \UsersHelpers::getUserID())
                ->orWhereHas('users', function($q){
                    $q->where('followups_collaborators.user_id', '=', \UsersHelpers::getUserID());
            })->where(function ($query) use ($search) {
                    $query->where('folio', "like", "%".$search."%")
                        ->orWhere('status', "like", "%".$search."%");
            })->sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( 'admin.followups.index', compact('followups', 'search') );
    }

    public function open( FollowUps $followup, $route, $client ){
        $res = ['success' => false];
        if($client){
            $newClient = Clients::where('id', $client)->first();
            $redirectRoute = route($route, $newClient);
        }else{
            $redirectRoute = route($route);
        }

        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $followup = FollowUps::where('id', $followup->id)->first();
        }else{
            $followup = FollowUps::where('user_id', \UsersHelpers::getUserID())->where('id', $followup->id)->first();
        }

        $followup->status = 1;

        if( $followup->save() ){
            $followup->event()->create(['description' => 'Seguimiento Abierto']);
            $res['success'] = true;
            Session::flash('msg', 'Seguimiento Abierto');
            $res['redirect'] = $redirectRoute;
        }

        return $res;
    }

    public function process( FollowUps $followup, $route, $client ){
        $res = ['success' => false];
        if($client){
            $newClient = Clients::where('id', $client)->first();
            $redirectRoute = route($route, $newClient);
        }else{
            $redirectRoute = route($route);
        }

        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $followup = FollowUps::where('id', $followup->id)->first();
        }else{
            $followup = FollowUps::where('user_id', \UsersHelpers::getUserID())->where('id', $followup->id)->first();
        }

        $followup->status = 2;

        if( $followup->save() ){
            $followup->event()->create(['description' => 'Seguimiento en Proceso']);
            $res['success'] = true;
            Session::flash('msg', 'Seguimiento en Proceso');
            $res['redirect'] = $redirectRoute;
        }

        return $res;
    }

    public function close( FollowUps $followup, $route, $client ){
        $res = ['success' => false];
        if($client){
            $newClient = Clients::where('id', $client)->first();
            $redirectRoute = route($route, $newClient);
        }else{
            $redirectRoute = route($route);
        }

        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $followup = FollowUps::where('id', $followup->id)->first();
        }else{
            $followup = FollowUps::where('user_id', \UsersHelpers::getUserID())->where('id', $followup->id)->first();
        }

        $followup->status = 3;

        if( $followup->save() ){
            $followup->event()->create(['description' => 'Seguimiento Cerrado']);
            $res['success'] = true;
            Session::flash('msg', 'Seguimiento Cerrado');
            $res['redirect'] = $redirectRoute;
        }

        return $res;
    }

    public function share( User $agent, FollowUps $followup ){
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->where('id', '!=', $agent->id)->get();
        return view( "admin.followups.share", compact('followup', 'agents') );
    }

    public function shareStore( FollowUps $followup, Request $request ){
        $res = ['success' => false];

        $collaboration = FollowUps::find($followup->id);

        $rules = [];
        $messages = [];

        $rules['agents'] = 'required';

        $messages['agents.required']  = 'No se ha seleccionado ningun agente';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $collaboration->users()->sync($request->input( "agents" ));

            if( $collaboration->save() ){
                $collaboration->event()->create(['description' => 'Seguimiento Compartido']);
                $res['success'] = true;
                Session::flash('msg', 'Seguimiento Compartido');
                $res['redirect'] = route('clientView', $followup->client_id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }
}
