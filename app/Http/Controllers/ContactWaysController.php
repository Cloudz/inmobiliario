<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Session;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

use App\Models\{
    AcquisitionsMainSources, ContactWays
};

class ContactWaysController extends Controller
{
    //FUNCTION LIST
    public function index(){
        $contactWays = ContactWays::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "admin.contact_ways.index", compact('contactWays') );
    }

    //FUNCTION SEARCH
    public function search( Request $request ){
        $search = $request->input("search");
        $contactWays = ContactWays::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('description', "like", "%".$search."%");
        })->orderBy('created_at', 'desc')->paginate( 50 );

        return view( 'admin.contact_ways.index', compact('contactWays', 'search') );
    }

    //FUNCTION CREATE
    public function create(){
        $path        = app_path('Models/Sources');
        $models      = ContactWays::getModels($path);
        $mainSources = AcquisitionsMainSources::all();
        return view ('admin.contact_ways.create', compact('mainSources', 'models'));
    }

    //FUNCTION SAVE
    public function store( Request $request ){
        $res = ['success' => false];
        $way = new ContactWays;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:contact_ways';

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $way->name            = ucfirst($request->input( "name" ));
            $way->description     = ucfirst($request->input( "description" ));
            if ($request->input( "multi_select" ) == 'on') {
                $multi = 1;
            }else{
                $multi = 0;
            }
            $way->model          = $request->input('model');
            $way->is_multi       = $multi;
            $way->main_source_id = $request->input('main_source');

            if( $way->save() ){
                $way->event()->create(['description' => 'Forma de Contacto Agregada']);
                $res['success'] = true;
                Session::flash('msg', 'Forma de Contacto Agregada');
                $res['redirect'] = route('contactWays');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION EDIT/VIEW
    public function edit (ContactWays $way ){
        $path = app_path('Models/Sources');
        $models  = ContactWays::getModels($path);
        $mainSources = AcquisitionsMainSources::all();
        return view ('admin.contact_ways.edit', compact('way', 'mainSources', 'models'));
    }

    //FUNCTION UPDATE
    public function update (ContactWays $way, Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('contact_ways')->ignore($way->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $way->name            = ucfirst($request->input( "name" ));
            $way->description     = ucfirst($request->input( "description" ));
            if ($request->input( "multi_select" ) == 'on') {
                $multi = 1;
            }else{
                $multi = 0;
            }
            $way->model          = $request->input('model');
            $way->main_source_id = $request->input('main_source');
            $way->is_multi       = $multi;

            if( $way->save() ){
                $way->event()->create(['description' => 'Forma de Contacto Actualizada']);
                $res['success'] = true;
                Session::flash('msg', 'Forma de Contacto Actualizada');
                $res['redirect'] = route('contactWaysEdit', $way->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DELETE
    public function delete(ContactWays $way ){
        $res = ['success' => true];
        if( $way->delete() ){
            $way->event()->create(['description' => 'Forma de Contacto Eliminada']);
            Session::flash('msg', 'Forma de Contacto borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
