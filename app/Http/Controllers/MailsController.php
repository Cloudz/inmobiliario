<?php

namespace App\Http\Controllers;

use App\Models\ConversationMail;
use Storage;
use View;
use Session;
use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mailgun\Mailgun;

class MailsController extends Controller
{
    public function quickSend( Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['f_email']   = 'required|email';
        $rules['f_subject'] = 'required|min:3';
        $rules['f_message'] = 'required|min:3';

        $messages['f_email.required']   = 'El email es obligatorio';
        $messages['f_email.email']      = 'El email tiene un formato incorrecto';
        $messages['f_subject.min']      = 'El asunto debe tener 3 caracteres';
        $messages['f_message.required'] = 'El mensaje es obligatorio';
        $messages['f_message.min']      = 'El mensaje debe tener 3 caracteres';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $email    = $request->input("f_email");
            $subject  = $request->input("f_subject");
            $message  = $request->input("f_message");

            $storageFiles = [];
            if ($request->hasFile('f_file')){
                $code = substr(md5(uniqid(mt_rand(), true)) , 0, 50);
                $src = $request->file('f_file');
                $ext = $src->getClientOriginalExtension();
                $filename = "tmp_file_" . $code . '.' . $ext;
                $urlFile = "mails/" . $filename;
                $resource = Storage::disk('public')->put($urlFile, \File::get($src));
                $getFile = public_path() . Storage::url($urlFile);
                if ( $resource ){
                    array_push($storageFiles, $filename);
                }
            }
            $render   = View::make('emails.mails.quick_mail', [
                'message' => trim($message),
            ])->render();

            $tmp = $render;
            $mg = Mailgun::create('key-3e0cde9342a2a366578641c36660720d');

            $domain = 'm.vlmg.mx';

            if ($request->hasFile('f_file')){
                $response = $mg->messages()->send($domain, array(
                    'from' => 'Seguimiento Inmobiliario  <'.\UsersHelpers::getUser()->email.'>',
                    'to' => $email,
                    'subject' => $subject,
                    'html' => $tmp,
                    'attachment' => [
                        ['filePath' => $getFile, 'filename' => 'tmp_file.' . $ext]
                    ]
                ));

                //Storage::disk('public')->delete($urlFile);
            }else{
                $response = $mg->messages()->send($domain, array(
                    'from' => 'Seguimiento Inmobiliario  <'.\UsersHelpers::getUser()->email.'>',
                    'to' => $email,
                    'subject' => $subject,
                    'html' => $tmp,
                ));
            }

            if ($response) {
                $mail = new ConversationMail;

                $mail->subject   = $request->input("f_subject");
                $mail->sender    = $request->input("f_email");
                $mail->htmlBody  = $tmp;
                $mail->recipient = \UsersHelpers::getUser()->email;
                $mail->type      = 'Sent';
                if ($request->hasFile('f_file')) {
                    $mail->attachments = serialize($storageFiles);
                }

                $mail->save();
            }

            $res['success'] = true;
            Session::flash('msg', 'Mensaje Enviado');
            $res['redirect'] = route('dashboard');

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION SHOW FORM
    public function formMail( ConversationMail $mail ){
        return view( "dashboard.response", compact('mail') );
    }
}
