<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    PropertiesStyles
};

class PropertiesStylesController extends Controller
{
    public function index(){
        $styles = PropertiesStyles::sortable()->orderBy('name', 'asc')->paginate( 50 );
        return view( "properties.styles.index", compact('styles') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $styles = PropertiesStyles::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('slug', "like", "%".$search."%");
        })->orderBy('name', 'asc')->paginate( 50 );
        return view( 'properties.styles.index', compact('styles', 'search') );
    }

    public function create(){
        return view( "properties.styles.create");
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $style = new PropertiesStyles;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties_styles';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $style->name = ucfirst($request->input( "name" ));
            $style->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $style->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Estilo Agregado');
                $res['redirect'] = route('propertiesStyles');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( PropertiesStyles $style ){
        return view( "properties.styles.view", compact('style') );
    }

    public function update( PropertiesStyles $style, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties_styles')->ignore($style->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $style->name = ucfirst($request->input( "name" ));
            $style->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $style->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Estilo Editado');
                $res['redirect'] = route('propertiesStyleView', $style->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( PropertiesStyles $style ){
        $res = ['success' => true];
        if( $style->delete() ){
            Session::flash('msg', 'Estilo borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
