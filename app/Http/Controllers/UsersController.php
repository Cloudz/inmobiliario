<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Role, User
};

class UsersController extends Controller
{
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer'])) {
            $users = User::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }elseif(\UsersHelpers::getUser()->hasRole(['Broker', 'Office Manager'])) {
            $users = User::whereDoesntHave('roles', function ($query) {
                $query->where('name', 'Administrator')
                ->orWhere('name', 'Developer');
            })->sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $users = User::sortable()->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( "admin.users.index", compact('users') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        if(\UsersHelpers::getUser()->hasRole('Administrator', 'Developer')) {
            $users = User::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('email', "like", "%".$search."%")
                    ->orWhere('mobile', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        }elseif(\UsersHelpers::getUser()->hasRole(['Broker', 'Office Manager'])){
            $users = User::whereDoesntHave('roles', function ($query){
                $query->where('name', 'Administrator')
                    ->orWhere('name', 'Developer');
            })->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('email', "like", "%".$search."%")
                    ->orWhere('mobile', "like", "%".$search."%");
            })->sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $users = User::sortable()->where(function ($query) use ($search) {
                $query->where('name', "like", "%".$search."%")
                    ->orWhere('last_name', "like", "%".$search."%")
                    ->orWhere('email', "like", "%".$search."%")
                    ->orWhere('mobile', "like", "%".$search."%");
            })->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }
        return view( 'admin.users.index', compact('users', 'search') );
    }

    public function create(){
        $roles = Role::whereNotIn('name',['Developer'])->get();
        return view ('admin.users.create', compact('roles') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $user = new User;
        $getUsers = User::all();
        $admin = false;
        $hasAdmin = false;

        $roles = $request->input( "roles" );

        foreach ($roles as $rol){
            if($rol == 1){
                $hasAdmin = true;
            }
        }

        if($hasAdmin){
            foreach ($getUsers as $userCheck){
                if($userCheck->hasRole('Administrator')){
                    $admin = true;
                }
            }
        }

        $rules = [];
        $messages = [];

        $rules['name']      = 'required|min:3';
        $rules['last_name'] = 'required|min:3';
        $rules['email']     = 'required|email|unique:users';
        $rules['password']  = 'required|min:8|confirmed';
        $rules['roles']     = 'required';

        $messages['name.required']      = 'El nombre es obligatorio';
        $messages['name.min']           = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required'] = 'El apellido es obligatorio';
        $messages['last_name.min']      = 'El apellido debe tener al menos 3 caracteres';
        $messages['email.required']     = 'El email es obligatorio';
        $messages['email.email']        = 'El email tiene formato incorrecto';
        $messages['email.unique']       = 'El email ya se encuentra registrado';
        $messages['password.required']  = 'La contraseña es obligatoria';
        $messages['password.min']       = 'La contraseña debe tener al menos 8 caracteres';
        $messages['password.confirmed'] = 'Las contraseñas no coinciden';
        $messages['roles.required']     = 'Al menos un Rol debe ser asignado';

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El celular tiene formato incorrecto';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            if($admin){
                $res['errors'][] = array('Solo un Usuario puede tener el perfil Administrador');
            }else{
                $user->name      = ucfirst($request->input( "name" ));
                $user->last_name = $request->input( "last_name" );
                $user->email     = $request->input( "email" );
                $user->mobile    = $request->input( "mobile" );
                $user->password  = bcrypt($request->input( "password" ));
                $user->user_id   = \UsersHelpers::getUserID();

                if( $user->save() ){
                    $user->event()->create(['description' => 'Usuario Creado']);
                    $user->attachRoles($request->input( "roles" ));

                    $res['success'] = true;
                    Session::flash('msg', 'Usuario Agregado');
                    $res['redirect'] = route('users');
                }else{
                    $res['errors'][] = array('Ocurrió un error inesperado');
                }
            }


        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( User $user ){
        $roles = Role::all();
        return view ('admin.users.view', compact('user', 'roles') );
    }

    public function update( User $user, Request $request ){
        $res = ['success' => false];
        $getUsers = User::all();
        $admin = false;
        $hasAdmin = false;

        $roles = $request->input( "roles" );

        foreach ($roles as $rol){
            if($rol == 1){
                $hasAdmin = true;
            }
        }

        if($hasAdmin){
            foreach ($getUsers as $userCheck){
                if($userCheck->hasRole('Administrator')){
                    $admin = true;
                }
            }
        }

        $rules = [];
        $messages = [];

        $rules['name']      = 'required|min:3';
        $rules['last_name'] = 'required|min:3';
        $rules['email']     = ['required', 'email', Rule::unique('users')->ignore($user->id) ];

        $messages['name.required']      = 'El nombre es obligatorio';
        $messages['name.min']           = 'El nombre debe tener al menos 3 caracteres';
        $messages['last_name.required'] = 'El apellido es obligatorio';
        $messages['last_name.min']      = 'El apellido debe tener al menos 3 caracteres';
        $messages['email.required']     = 'El email es obligatorio';
        $messages['email.email']        = 'El email tiene formato incorrecto';
        $messages['email.unique']       = 'El email ya se encuentra registrado';

        if (!empty($request->input( "mobile" ))) {
            $rules['mobile']            = 'numeric';
            $messages['mobile.numeric'] = 'El celular tiene formato incorrecto';
        }

        if (!empty($request->input( "password" ))) {
            $rules['password']              = 'required|min:8|confirmed';
            $messages['password.required']  = 'La contraseña es obligatoria';
            $messages['password.min']       = 'La contraseña debe tener al menos 8 caracteres';
            $messages['password.confirmed'] = 'Las contraseñas no coinciden';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            if($admin){
                $res['errors'][] = array('Solo un Usuario puede tener el perfil Administrador');
            }else{
                $user->name      = ucfirst($request->input( "name" ));
                $user->last_name = $request->input( "last_name" );
                $user->email     = $request->input( "email" );
                $user->mobile    = $request->input( "mobile" );

                if (!empty($request->input( "password" ))) {
                    $user->password  = bcrypt($request->input( "password" ));
                }

                if( $user->save() ){
                    $user->event()->create(['description' => 'Usuario Actualizado']);
                    $user->detachRoles($user->roles);

                    if (empty($request->input( "roles" ))) {
                        $user->attachRole(4);
                    }else{
                        $user->attachRoles($request->input( "roles" ));
                    }

                    $res['success'] = true;
                    Session::flash('msg', 'Usuario Actualizado');
                    $res['redirect'] = route('users');
                }else{
                    $res['errors'][] = array('Ocurrió un error inesperado');
                }
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function active( User $user ){
        $res = ['success' => false];

        $user->is_active = 1;

        if( $user->save() ){
            $user->event()->create(['description' => 'Usuario Activado']);
            $res['success'] = true;
            Session::flash('msg', 'Usuario Activado');
            $res['redirect'] = route('users');
        }

        return $res;
    }

    public function deactivate( User $user ){
        $res = ['success' => false];

        $user->is_active = 0;

        if( $user->save() ){
            $user->event()->create(['description' => 'Usuario Desactivado']);
            $res['success'] = true;
            Session::flash('msg', 'Usuario Desactivado');
            $res['redirect'] = route('users');
        }

        return $res;
    }

    public function delete( User $user ){
        $res = ['success' => true];
        if( $user->delete() ){
            $user->event()->create(['description' => 'Usuario Borrado']);
            Session::flash('msg', 'Usuario borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }

    //FUNCTION RE-ASSIGN USER
    public function assist( User $user ){
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view( "admin.users.assist", compact('user', 'agents') );
    }

    //FUNCTION ASSIST UPDATE USER
    public function assistUpdate( User $user, Request $request ){
        $res = ['success' => false];

        $currentUser = User::find($user->id);

        $rules = [];
        $messages = [];

        $rules['agents'] = 'required';
        $messages['agents.required']  = 'No se ha seleccionado ningun agente';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $currentUser->users()->sync($request->input( "agents" ));

            if( $currentUser->save() ){
                $currentUser->event()->create(['description' => 'Agentes Asistidos']);

                $res['success'] = true;
                $res['redirect'] = route('users');
                Session::flash('msg', 'Agentes Asistidos');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }
}
