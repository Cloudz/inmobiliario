<?php

namespace App\Http\Controllers;

use App\Models\Reminders;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\View\View;
use Mailgun\Mailgun;

class CronController extends Controller
{
    public function reminderCronJob(){
        $tz = 'America/Mexico_City';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp( $timestamp );
        $date = $dt->format('Y-m-d H:i:s');
        $reminders = Reminders::all();
        $redoReminders = [];

        foreach ($reminders as $reminder){
            $started_on = date('Y-m-d H:i:s', strtotime($reminder->started_on));
            if ($started_on >= $date){
                $redoReminders[] = $reminder;
            }
        }
        $this->reminderMail( $reminders );
    }

    private function reminderMail( $items ){
        $tz = 'America/Mexico_City';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);
        $now = Carbon::parse($dt->format('Y-m-d H:i:s'));
        foreach ($items as $item) {
            $userEmail = User::where('id', $item->user_id)->first();
            $end  = date('Y-m-d H:i:s', strtotime($item->started_on));
            $end  = Carbon::parse($end);
            $diffInHours = $end->diffInHours($now); // Diferencia entre la fecha asignada y ahora en horas.
            if ($diffInHours == intval($item->interval)) {
                $request = View::make('emails.crons.reminders_cron',[
                    'itemTitle'  => trim(strip_tags($item->subject)),
                    'itemDesc'   => trim(strip_tags($item->description)),
                    'itemDate'   => $end,
                    'itemClient' => $item->client_id,
                    'itemUrl'    => route('reminderEdit', [$item->type, $item->client_id, $item->id]),
                    'urlClient'  => route('clientView', $item->client_id)
                ])->render();

                $tmp = $request;

                $mg = Mailgun::create('key-3e0cde9342a2a366578641c36660720d');

                $domain = 'm.vlmg.mx';
                $mg->messages()->send($domain, array(
                    'from'    => 'Seguimiento Inmobiliario <no-reply@inmo.com>',
                    'to'      => $userEmail->email,
                    'subject' => 'Su recordatorio esta por empezar',
                    'html'    => $tmp
                ));
            }elseif ($diffInHours <= intval($item->interval)) {
                $item->overdue = 1;
            }
        }
    }
}
