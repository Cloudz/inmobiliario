<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Exports\PropertiesExport;
use App\Imports\PropertiesImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\{
    Properties, PropertiesAmenities, PropertiesDevelopments, PropertiesLocations, PropertiesStyles, PropertiesTypes
};

class PropertiesController extends Controller
{
    public function index(){
        $properties = Properties::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "properties.index", compact('properties') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $properties = Properties::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('description', "like", "%".$search."%")
                ->orWhere('amenities', "like", "%".$search."%")
                ->orWhere('address', "like", "%".$search."%")
                ->orWhere('baths', "like", "%".$search."%")
                ->orWhere('beds', "like", "%".$search."%")
                ->orWhere('parking', "like", "%".$search."%");
            })->orderBy('created_at', 'desc')->paginate( 50 );
        return view( 'properties.index', compact('properties', 'search') );
    }

    public function create(){
        $types        = PropertiesTypes::all();
        $styles       = PropertiesStyles::all();
        $amenities    = PropertiesAmenities::all();
        $locations    = PropertiesLocations::all();
        $developments = PropertiesDevelopments::all();
        return view( "properties.create", compact( 'types', 'styles', 'amenities', 'locations', 'developments') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $property = new Properties;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        if ($request->hasFile('files')){
            $rules['files'] = 'max:5000';
        }

        if ($request->hasFile('files')){
            $messages['files.max'] = 'Las imagenes deben ser de 5MB máximo';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $property->name           = ucfirst($request->input( "name" ));
            $property->description    = $request->input( "description" );
            $property->address        = $request->input( "address" );
            $property->baths          = $request->input( "baths" );
            $property->beds           = $request->input( "beds" );
            $property->parking        = $request->input( "parking" );
            $property->m2             = $request->input( "m2" );
            $property->ft2            = $request->input( "ft2" );
            $property->type           = $request->input( "type_property");
            $property->type_id        = $request->input( "type_id" );
            $property->development_id = $request->input( "development_id" );
            $property->location_id    = $request->input( "location_id" );
            $property->style_id       = $request->input( "style_id" );

            $files = [];
            if ($request->hasFile('files')){
                foreach ($request->file( 'files' ) as $file) {
                    $code = substr(md5(uniqid(mt_rand(), true)) , 0, 50);
                    $title_files = $property->name.'_'.$code;
                    $ext = $file->getClientOriginalExtension();
                    $filename = $title_files.'.'.$ext;
                    $cleanFileName = \GlobalHelpers::cleanTitle($filename);
                    $resource = Storage::disk('public')->put( "properties/".$cleanFileName, \File::get( $file ) );
                    if ( $resource ){
                        array_push($files, $cleanFileName);
                    }
                }
            }
            $property->files = serialize($files);

            if( $property->save() ){
                $property->event()->create(['description' => 'Propiedad Agregada']);
                if (!empty($request->input( "comments" ))) {
                    $property->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                }
                $property->amenities()->sync($request->input( "amenities" ));
                $res['success'] = true;
                Session::flash('msg', 'Propiedad Agregada');
                $res['redirect'] = route('properties');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( Properties $property ){
        $types        = PropertiesTypes::all();
        $styles       = PropertiesStyles::all();
        $amenities    = PropertiesAmenities::all();
        $locations    = PropertiesLocations::all();
        $developments = PropertiesDevelopments::all();
        return view( "properties.view", compact( 'property', 'types', 'styles', 'amenities', 'locations', 'developments') );
    }

    public function update( Properties $property, Request $request ){
        $currentProperty = Properties::where('id', $property->id)->first();
        $keyComment = count($currentProperty->comment) - 1;
        $orgFiles = unserialize($property->files);
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties')->ignore($property->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        if ($request->hasFile('files')){
            $rules['files'] = 'max:5000';
        }

        if ($request->hasFile('files')){
            $messages['files.max'] = 'Las imagenes deben ser de 5MB máximo';
        }

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $property->name           = ucfirst($request->input( "name" ));
            $property->description    = $request->input( "description" );
            $property->address        = $request->input( "address" );
            $property->baths          = $request->input( "baths" );
            $property->beds           = $request->input( "beds" );
            $property->parking        = $request->input( "parking" );
            $property->m2             = $request->input( "m2" );
            $property->ft2            = $request->input( "ft2" );
            $property->type           = $request->input( "type_property");

            if (!empty($request->input( "type_id" ))) {
                $property->type_id = $request->input("type_id");
            }
            if (!empty($request->input( "development_id" ))) {
                $property->development_id = $request->input( "development_id" );
            }
            if (!empty($request->input( "location_id" ))) {
                $property->location_id    = $request->input( "location_id" );
            }
            if (!empty($request->input( "style_id" ))) {
                $property->style_id       = $request->input( "style_id" );
            }
            if ($request->hasFile('files')){
                foreach ($request->file( 'files' ) as $file) {
                    $code = substr(md5(uniqid(mt_rand(), true)) , 0, 50);
                    $title_files = $property->name.'_'.$code;
                    $ext = $file->getClientOriginalExtension();
                    $filename = $title_files.'.'.$ext;
                    $cleanFileName = \GlobalHelpers::cleanTitle($filename);
                    $resource = Storage::disk('public')->put( "properties/".$cleanFileName, \File::get( $file ) );
                    if ( $resource ){
                        array_push($orgFiles, $cleanFileName);
                    }
                }
                $property->files = serialize($orgFiles);
            }


            if( $property->save() ){
                $property->event()->create(['description' => 'Propiedad Actualizada']);
                if($property->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentProperty->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $property->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $property->comment()->create(['comments' => $request->input( "comments" ), 'user_id' => \UsersHelpers::getUserID()]);
                    }
                }
                if (!empty($request->input( "amenities" ))) {
                    $property->amenities()->sync($request->input( "amenities" ));
                }
                $res['success'] = true;
                Session::flash('msg', 'Propiedad Editada');
                $res['redirect'] = route('propertyView', $property->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION EXPORT PROPERTIES
    public function export(){
        return Excel::download(new PropertiesExport, 'properties.csv');
    }

    //FUNCTION FORM IMPORT PROPERTIES
    public function formImport(){
        return view( "imports.properties");
    }

    //FUNCTION IMPORT PROPERTIES
    public function import(Request $request){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            $file          = $request->file('file_properties');
            $code          = substr(md5(uniqid(mt_rand(), true)) , 0, 5);
            $ext           = $file->getClientOriginalExtension();
            $title         = 'properties';
            $filename      = $title.'_'.$code.'.'.$ext;
            $cleanFileName = \GlobalHelpers::cleanTitle($filename);
            Storage::disk('public')->put( "imports/".$cleanFileName, \File::get( $file ) );
            $getFile   = "imports/".$cleanFileName;
            if( Excel::import(new PropertiesImport, $getFile, 'public') ){
                $res['success'] = true;
                Session::flash('msg', 'Propiedades Importadas');
                $res['redirect'] = route('properties');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DOWNLOAD PROPERTIES
    public function imagesDownload(Properties $property){
        $propertyName = $property->name;
        if(count($property->files)>0){
            $results = unserialize($property->files);
            $files = [];
            foreach($results as $res){
                $files[] = public_path().'/storage/properties/'.$res;
            }
            \Zipper::make(public_path().'/storage/properties/'.$propertyName.'.zip')->add($files)->close();
        }
        Storage::disk('public')->get('/properties/'.$propertyName.'.zip');

        return response()->download(public_path().'/storage/properties/'.$propertyName.'.zip');
    }

    public function deleteFile( Properties $property, Request $request ){
        $res = ['success' => false];
        $orgFiles = unserialize($property->files);

        foreach ($orgFiles as $key => $v) {
            if ($request->input('currenFile') == $v) {
                unset($orgFiles[$key]);
            }
        }

        $serFiles = serialize($orgFiles);
        $property->files = $serFiles;

        if( $property->save() ){
            $res['success'] = true;
            Session::flash('msg', 'Archivo Borrado');
            $res['redirect'] = route('propertyView', $property->id);
        }else{
            $res['errors'][] = array('Ocurrió un error inesperado');
        }

        return $res;
    }

    public function delete( Properties $property ){
        $res = ['success' => true];
        if( $property->delete() ){
            $property->event()->create(['description' => 'Propiedad Eliminada']);
            Session::flash('msg', 'Propiedad borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
