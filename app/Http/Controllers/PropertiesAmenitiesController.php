<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    PropertiesAmenities
};

class PropertiesAmenitiesController extends Controller
{
    public function index(){
        $amenities = PropertiesAmenities::sortable()->orderBy('name', 'asc')->paginate( 50 );
        return view( "properties.amenities.index", compact('amenities') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $amenities = PropertiesAmenities::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('slug', "like", "%".$search."%");
        })->orderBy('name', 'asc')->paginate( 50 );
        return view( 'properties.amenities.index', compact('amenities', 'search') );
    }

    public function create(){
        return view( "properties.amenities.create" );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $amenity = new PropertiesAmenities;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties_amenities';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $amenity->name = ucfirst($request->input( "name" ));
            $amenity->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $amenity->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Amenidad Agregada');
                $res['redirect'] = route('propertiesAmenities');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( PropertiesAmenities $amenity ){
        return view( "properties.amenities.view", compact('amenity') );
    }

    public function update( PropertiesAmenities $amenity, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties_amenities')->ignore($amenity->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $amenity->name = ucfirst($request->input( "name" ));
            $amenity->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $amenity->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Amenidad Editada');
                $res['redirect'] = route('propertiesAmenityView', $amenity->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( PropertiesAmenities $amenity ){
        $res = ['success' => true];
        if( $amenity->delete() ){
            Session::flash('msg', 'Propiedad borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
