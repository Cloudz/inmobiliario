<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\{
    Events
};

class EventsController extends Controller
{
    public function index(){
        $events = Events::orderBy('created_at', 'desc')->paginate( 50 );
        return view( "events.index", compact('events') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $events = Events::where(function ($query) use ($search) {
                        $query->where('description', "like", "%".$search."%");
                })->orderBy('created_at', 'desc')->paginate( 50 );
        return view( 'events.index', compact('events', 'search') );
    }

    public function delete( Events $event ){
        $res = ['success' => true];
        if( $event->delete() ){
            Session::flash('msg', 'Evento borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
