<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Comments
};

class CommentsController extends Controller
{

    public function update( Comments $comment, Request $request){
        $comment = Comments::where('id', $comment->id)->first();
        $res = ['success' => false];

        $rules    = [];
        $messages = [];

        $rules['f_comment']             = 'required';
        $messages['f_comment.required'] = 'El comentario es obligatorio';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){
            $comment->comments = $request->input('f_comment');
            if( $comment->save() ){
                $res['success'] = true;
                $res['redirect'] = route($request->input('f_url'), $request->input('f_source'));
                Session::flash('msg', 'Comentario Actualizado');
            }else{
                $res['errors'][] = array('Ocurrió un error');
            }
        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Comments $comment ){
        $res = ['success' => true];
        if( $comment->delete() ){
            Session::flash('msg', 'Comentario borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
