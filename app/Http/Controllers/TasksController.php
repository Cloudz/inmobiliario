<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Tasks, User
};

class TasksController extends Controller
{
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $tasks = Tasks::all();
        }else{
            $tasks = Tasks::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->get();
        }
        return view( "tasks.index", compact('tasks') );
    }
    
    public function create( Request $request ){
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view ('tasks.create', compact('agents'));
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $task = new Tasks;

        $rules = [];
        $messages = [];

        $rules['subject']     = 'required|min:3';
        $rules['description'] = 'required|min:3';
        $rules['started_on']  = 'required';

        $messages['subject.required']     = 'El nombre es obligatorio';
        $messages['subject.min']          = 'El nombre debe tener al menos 3 caracteres';
        $messages['description.required'] = 'La descripción es obligatoria';
        $messages['description.min']      = 'La descripción debe tener al menos 3 caracteres';
        $messages['started_on.required']  = 'La fecha de inicio es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $task->subject      = ucfirst($request->input( "subject" ));
            $task->description  = $request->input( "description" );
            $task->started_on   = $request->input( "started_on" );
            $task->reminded_on  = $request->input( "reminded_on" );
            $task->accomplished = 0;
            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
                $task->user_id = $request->input( "agent_id" );
            }else{
                $task->user_id = \UsersHelpers::getUserID();
            }

            if( $task->save() ){
                $task->event()->create(['description' => 'Tarea Agregada']);
                $res['success'] = true;
                Session::flash('msg', 'Tarea Agregada');
                $res['redirect'] = route('tasks');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function edit( Tasks $task ){
        $started_on  = date('Y-m-d H:i:s', strtotime($task->started_on));
        if ($task->reminded_on) {
            $reminded_on = date('Y-m-d H:i:s', strtotime($task->reminded_on));
        }else{
            $reminded_on = '';
        }
        $agents = User::whereHas('roles', function ($query) {
            $query->where('name', 'Sales Agent');
        })->get();
        return view( "tasks.edit", compact('task', 'started_on', 'reminded_on', 'agents') );
    }

    public function update( Tasks $task, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['subject']     = 'required|min:3';
        $rules['description'] = 'required|min:3';
        $rules['started_on']  = 'required';

        $messages['subject.required']     = 'El nombre es obligatorio';
        $messages['subject.min']          = 'El nombre debe tener al menos 3 caracteres';
        $messages['description.required'] = 'La descripción es obligatoria';
        $messages['description.min']      = 'La descripción debe tener al menos 3 caracteres';
        $messages['started_on.required']  = 'La fecha de inicio es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            if ($request->input( "accomplished" ) == 'on') {
                $accomplished = 1;
            }else{
                $accomplished = 0;
            }

            $task->subject      = ucfirst($request->input( "subject" ));
            $task->description  = ucfirst($request->input( "description" ));
            $task->started_on   = $request->input( "started_on" );
            $task->reminded_on  = $request->input( "reminded_on" );
            $task->accomplished = $accomplished;

            if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
                $task->user_id = $request->input( "agent_id" );
            }

            if( $task->save() ){
                $task->event()->create(['description' => 'Tarea Actualizada']);
                $res['success'] = true;
                Session::flash('msg', 'Tarea Actualizada');
                $res['redirect'] = route('tasks');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function delete( Tasks $task ){
        $res = ['success' => true];
        if( $task->delete() ){
            $task->event()->create(['description' => 'Tarea Eliminada']);
            Session::flash('msg', 'Tarea borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
