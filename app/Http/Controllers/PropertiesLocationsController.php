<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    PropertiesLocations
};

class PropertiesLocationsController extends Controller
{
    public function index(){
        $locations = PropertiesLocations::sortable()->orderBy('name', 'asc')->paginate( 50 );
        return view( "properties.locations.index", compact('locations') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $locations = PropertiesLocations::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('slug', "like", "%".$search."%");
        })->orderBy('name', 'asc')->paginate( 50 );
        return view( 'properties.locations.index', compact('locations', 'search') );
    }

    public function create(){
        return view( "properties.locations.create");
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $location = new PropertiesLocations;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties_locations';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $location->name = ucfirst($request->input( "name" ));
            $location->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $location->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Locación Agregada');
                $res['redirect'] = route('propertiesLocationCreate');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( PropertiesLocations $location ){
        return view( "properties.locations.view", compact('location') );
    }

    public function update( PropertiesLocations $location, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties_locations')->ignore($location->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $location->name = ucfirst($request->input( "name" ));
            $location->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $location->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Locación Editada');
                $res['redirect'] = route('propertiesLocationView', $location->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( PropertiesLocations $location ){
        $res = ['success' => true];
        if( $location->delete() ){
            Session::flash('msg', 'Locación borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
