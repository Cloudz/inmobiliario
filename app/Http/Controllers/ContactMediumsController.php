<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Session;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

use App\Models\{
    AcquisitionsMainMediums, ContactMediums
};

class ContactMediumsController extends Controller
{
    //FUNCTION LIST
    public function index(){
        $contactMediums = ContactMediums::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "admin.contact_mediums.index", compact('contactMediums') );
    }

    //FUNCTION SEARCH
    public function search( Request $request ){
        $search = $request->input("search");
        $contactMediums = ContactMediums::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('description', "like", "%".$search."%");
        })->orderBy('created_at', 'desc')->paginate( 50 );

        return view( 'admin.contact_mediums.index', compact('contactMediums', 'search') );
    }

    //FUNCTION CREATE
    public function create(){
        $path        = app_path('Models/Sources');
        $models      = ContactMediums::getModels($path);
        $mainMediums = AcquisitionsMainMediums::all();
        return view ('admin.contact_mediums.create', compact('mainMediums', 'models'));
    }

    //FUNCTION SAVE
    public function store( Request $request ){
        $res = ['success' => false];
        $medium = new ContactMediums;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:contact_mediums';

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $medium->name        = ucfirst($request->input( "name" ));
            $medium->description = ucfirst($request->input( "description" ));
            if ($request->input( "multi_select" ) == 'on') {
                $multi = 1;
            }else{
                $multi = 0;
            }
            $medium->model          = $request->input('model');
            $medium->is_multi       = $multi;
            $medium->main_source_id = $request->input('main_source');

            if( $medium->save() ){
                $medium->event()->create(['description' => 'Medio de Contacto Agregada']);
                $res['success'] = true;
                Session::flash('msg', 'Medio de Contacto Agregada');
                $res['redirect'] = route('contactMediums');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION EDIT/VIEW
    public function edit (ContactMediums $medium ){
        $path = app_path('Models/Sources');
        $models  = ContactMediums::getModels($path);
        $mainMediums = AcquisitionsMainMediums::all();
        return view ('admin.contact_mediums.edit', compact('medium', 'mainMediums', 'models'));
    }

    //FUNCTION UPDATE
    public function update (ContactMediums $medium, Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('contact_mediums')->ignore($medium->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $medium->name            = ucfirst($request->input( "name" ));
            $medium->description     = ucfirst($request->input( "description" ));
            if ($request->input( "multi_select" ) == 'on') {
                $multi = 1;
            }else{
                $multi = 0;
            }
            $medium->model          = $request->input('model');
            $medium->main_source_id = $request->input('main_source');
            $medium->is_multi       = $multi;

            if( $medium->save() ){
                $medium->event()->create(['description' => 'Medio de Contacto Actualizada']);
                $res['success'] = true;
                Session::flash('msg', 'Medio de Contacto Actualizada');
                $res['redirect'] = route('contactMediumsEdit', $medium->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DELETE
    public function delete(ContactMediums $medium ){
        $res = ['success' => true];
        if( $medium->delete() ){
            $medium->event()->create(['description' => 'Medio de Contacto Eliminado']);
            Session::flash('msg', 'Medio de Contacto borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
