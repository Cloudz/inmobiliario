<?php

namespace App\Http\Controllers;

use Auth;
use View;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Clients, Reminders, User
};

class RemindersController extends Controller
{
    public function index($type){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            if ($type == 'Call') {
                $reminders = Reminders::where('type', 'Call')->get();
                return view("reminders.calls.index", compact('reminders'));
            } elseif ($type == 'Mail') {
                $reminders = Reminders::where('type', 'Mail')->get();
                return view("reminders.mails.index", compact('reminders'));
            } elseif ($type == 'Showing') {
                $reminders = Reminders::where('type', 'Showing')->get();
                return view("reminders.showings.index", compact('reminders'));
            }
        }else{
            if ($type == 'Call') {
                $reminders = Reminders::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->where('type', 'Call')->get();
                return view("reminders.calls.index", compact('reminders'));
            } elseif ($type == 'Mail') {
                $reminders = Reminders::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->where('type', 'Mail')->get();
                return view("reminders.mails.index", compact('reminders'));
            } elseif ($type == 'Showing') {
                $reminders = Reminders::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->where('type', 'Showing')->get();
                return view("reminders.showings.index", compact('reminders'));
            }
        }
    }

    public function create( $type, Clients $client){
        if ($type == 'Call') {
            return view( "reminders.calls.create", compact('client') );
        }elseif ($type == 'Mail') {
            return view( "reminders.mails.create", compact('client') );
        }elseif ($type == 'Showing') {
            return view( "reminders.showings.create", compact('client') );
        }
    }

    public function store(Request $request ){
        $res = ['success' => false];
        $reminder = new Reminders;

        $rules = [];
        $messages = [];

        $rules['subject']     = 'required|min:3';
        $rules['description'] = 'required|min:3';
        $rules['started_on']  = 'required';

        $messages['subject.required']     = 'El nombre es obligatorio';
        $messages['subject.min']          = 'El nombre debe tener al menos 3 caracteres';
        $messages['description.required'] = 'La descripción es obligatoria';
        $messages['description.min']      = 'La descripción debe tener al menos 3 caracteres';
        $messages['started_on.required']  = 'La fecha de inicio es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $reminder->subject     = ucfirst($request->input( "subject" ));
            $reminder->description = $request->input( "description" );
            $reminder->started_on  = $request->input( "started_on" );
            $reminder->interval    = intval($request->input( "interval" ));
            $reminder->type        = $request->input( "reminder_type" );
            $reminder->client_id   = $request->input( "client_id" );
            $reminder->user_id     = $request->input( "agent_id" );

            if( $reminder->save() ){
                $reminder->event()->create(['description' => 'Recordatorio Agregado']);
                $res['success'] = true;
                Session::flash('msg', 'Recordatorio Agregado');
                $res['redirect'] = route('reminders', ['type' => $request->input( "reminder_type" )]);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();   
        }

        return $res;
    }

    public function edit( $type, Clients $client, Reminders $reminder ){
        $started_on  = date('Y-m-d H:i:s', strtotime($reminder->started_on));
        switch($type){
            case 'Call':
                $route = "reminders.calls.edit";
            break;
            case 'Mail':
                $route = "reminders.mails.edit";
            break;
            case 'Showings':
                $route = "reminders.showings.edit";
            break;
        }
        return view( $route, compact('type', 'client', 'reminder', 'started_on') );
    }

    public function update( $type, Reminders $reminder, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['subject']     = 'required|min:3';
        $rules['description'] = 'required|min:3';
        $rules['started_on']  = 'required';

        $messages['subject.required']     = 'El nombre es obligatorio';
        $messages['subject.min']          = 'El nombre debe tener al menos 3 caracteres';
        $messages['description.required'] = 'La descripción es obligatoria';
        $messages['description.min']      = 'La descripción debe tener al menos 3 caracteres';
        $messages['started_on.required']  = 'La fecha de inicio es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $reminder->subject     = ucfirst($request->input( "subject" ));
            $reminder->description = ucfirst($request->input( "description" ));
            $reminder->started_on  = $request->input( "started_on" );
            $reminder->interval    = intval($request->input( "interval" ));

            if( $reminder->save() ){
                $reminder->event()->create(['description' => 'Recordatorio Actualizado']);
                $res['success'] = true;
                Session::flash('msg', 'Recordatorio Actualizado');
                $res['redirect'] = route('reminders', $type);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Reminders $reminder ){
        $res = ['success' => true];
        if( $reminder->delete() ){
            $reminder->event()->create(['description' => 'Recordatorio Eliminado']);
            Session::flash('msg', 'Recordatorio borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }  
        return $res;
    }
}
