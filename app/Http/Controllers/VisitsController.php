<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Models\{
    Clients, User, Visits
};

use App\Models\Sources\{
    Agents, SalesOffices
};

class VisitsController extends Controller
{
    public function index(){
        $offices = SalesOffices::all();
        $visits  = Visits::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "visits.index", compact('offices', 'visits') );
    }

    public function create(){
        $offices = SalesOffices::all();
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assitant'])) {
            $clients = Clients::all();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->get();
        }elseif(\UsersHelpers::getUser()->hasRole('Personal Assistant')){
            $clients = Clients::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->get();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->whereIn('id', \UsersHelpers::getUsers())->get();
        }else {
            $clients = Clients::where('user_id', \UsersHelpers::getUserID())->get();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->where('user_id', \UsersHelpers::getUserID())->get();
        }
        return view ('visits.create', compact('offices', 'clients', 'agents') );
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $visit = new Visits;

        $rules = [];
        $messages = [];

        $rules['visit_date'] = 'required';
        $rules['office']     = 'required';
        $rules['client']     = 'required';
        $rules['adviser']    = 'required';

        $messages['visit_date.required'] = 'La fecha de visita es obligatoria';
        $messages['office.required']     = 'La oficina es obligatoria';
        $messages['client.min']          = 'El cliente es obligatorio';
        $messages['adviser.min']         = 'El asesor es obligatorio';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $visit->agent_id       = $request->input( "adviser" );
            $visit->sale_office_id = $request->input( "office" );
            $visit->client_id      = $request->input( "client" );
            $visit->visit_date     = $request->input( "visit_date" );
            $visit->user_id        = \UsersHelpers::getUserID();

            if( $visit->save() ){
                $visit->event()->create(['description' => 'Visita Agregada']);
                if (!empty($request->input( "comments" ))) {
                    $visit->comment()->create(['comments' => $request->input( "comments" )]);
                }
                $res['success'] = true;
                Session::flash('msg', 'Visita Agregada');
                $res['redirect'] = route('visits');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( Visits $visit ){
        $offices = SalesOffices::all();
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assitant'])) {
            $clients = Clients::all();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->get();
        }elseif(\UsersHelpers::getUser()->hasRole('Personal Assistant')){
            $clients = Clients::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->get();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->whereIn('id', \UsersHelpers::getUsers())->get();
        }else {
            $clients = Clients::where('user_id', \UsersHelpers::getUserID())->get();
            $agents = User::whereHas('roles', function ($query) {
                $query->where('name', 'Sales Agent');
            })->where('user_id', \UsersHelpers::getUserID())->get();
        }
        $visit_date  = date('Y-m-d H:i:s', strtotime( $visit->visit_date ) );
        return view( "visits.view", compact('visit', 'visit_date', 'offices', 'clients', 'agents') );
    }

    public function update( Visits $visit, Request $request ){
        $currentVisit = Visits::where('id', $visit->id)->first();
        $keyComment = count($currentVisit->comment) - 1;
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['visit_date'] = 'required';
        $rules['office']     = 'required';
        $rules['client']     = 'required';
        $rules['adviser']     = 'required';

        $messages['visit_date.required'] = 'La fecha de visita es obligatoria';
        $messages['office.required']     = 'La oficina es obligatoria';
        $messages['client.min']          = 'El cliente es obligatorio';
        $messages['adviser.min']         = 'El asesor es obligatorio';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $visit->agent_id       = $request->input( "adviser" );
            $visit->sale_office_id = $request->input( "office" );
            $visit->client_id      = $request->input( "client" );
            $visit->visit_date     = $request->input( "visit_date" );

            if( $visit->save() ){
                $visit->event()->create(['description' => 'Visita Actualizada']);
                if($visit->comment->count() > 0) {
                    if (!empty($request->input( "comments" )) && $currentVisit->comment[$keyComment]->comments != $request->input( "comments" )) {
                        $visit->comment()->create(['comments' => $request->input( "comments" )]);
                    }
                }else{
                    if (!empty($request->input( "comments" ))) {
                        $visit->comment()->create(['comments' => $request->input( "comments" )]);
                    }
                }
                $res['success'] = true;
                Session::flash('msg', 'Visita Editada');
                $res['redirect'] = route('visitView', $visit->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( Visits $visit ){
        $res = ['success' => true];
        if( $visit->delete() ){
            $visit->event()->create(['description' => 'Visita Eliminada']);
            Session::flash('msg', 'Visita borrada correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
