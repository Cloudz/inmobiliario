<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Session;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

use App\Models\{
    AcquisitionsMainSources, AcquisitionsSecondarySources, ContactWays
};

class AcquisitionsMainSourcesController extends Controller
{
    //FUNCTION LIST
    public function index(){
        $mainSources = AcquisitionsMainSources::sortable()->orderBy('created_at', 'desc')->paginate( 50 );
        return view( "admin.acquisitions.main_sources.index", compact('mainSources') );
    }

    //FUNCTION SEARCH
    public function search( Request $request ){
        $search      = $request->input("search");
        $mainSources = AcquisitionsMainSources::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('description', "like", "%".$search."%");
        })->orderBy('created_at', 'desc')->paginate( 50 );

        return view( 'admin.acquisitions.main_sources.index', compact('type', 'mainSources', 'search') );
    }

    //FUNCTION CREATE
    public function create(){
        $path = app_path('Models/Sources');
        $models  = ContactWays::getModels($path);
        $contactWays = ContactWays::all();
        $secondarySources = AcquisitionsSecondarySources::all();
        return view ('admin.acquisitions.main_sources.create', compact('contactWays', 'models', 'secondarySources'));
    }

    //FUNCTION SAVE
    public function store( Request $request ){
        $res = ['success' => false];
        $mainSource = new AcquisitionsMainSources;

        $rules = [];
        $messages = [];

        $rules['name']        = 'required|min:3|unique:acquisitions_main_sources';
        $rules['contact_way'] = 'required';

        $messages['name.required']        = 'El nombre es obligatorio';
        $messages['name.min']             = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']          = 'El nombre ya se encuentra registrado';
        $messages['contact_way.required'] = 'La Forma de contacto es obligatoria';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $mainSource->name           = ucfirst($request->input( "name" ));
            $mainSource->description    = ucfirst($request->input( "description" ));
            $mainSource->model          = $request->input( "model" );
            $mainSource->contact_way_id = $request->input( "contact_way" );
            $mainSource->secondary_id   = $request->input( "secondary" );

            if( $mainSource->save() ){
                $mainSource->event()->create(['description' => 'Recurso Principal Agregado']);
                $res['success'] = true;
                Session::flash('msg', 'Recurso Principal Agregado');
                $res['redirect'] = route('acquisitionMainSources');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION EDIT/VIEW
    public function edit ( AcquisitionsMainSources $mainSource ){
        $path = app_path('Models/Sources');
        $models  = ContactWays::getModels($path);
        $contactWays = ContactWays::all();
        $secondarySources = AcquisitionsSecondarySources::all();
        return view( "admin.acquisitions.main_sources.edit", compact('contactWays', 'secondarySources', 'mainSource', 'models' ) );
    }

    //FUNCTION UPDATE
    public function update ( AcquisitionsMainSources $mainSource, Request $request ){
        $res = ['success' => false];

        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('acquisitions_main_sources')->ignore($mainSource->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $mainSource->name        = ucfirst($request->input( "name" ));
            $mainSource->description = ucfirst($request->input( "description" ));
            if ( $request->input( "model" ) != ''){
                $mainSource->model = $request->input( "model" );
            }
            if ( $request->input( "secondary" ) != ''){
                $mainSource->secondary_id = $request->input( "secondary" );
            }
            $mainSource->contact_way_id = $request->input( "contact_way" );

            if( $mainSource->save() ){
                $mainSource->event()->create(['description' => 'Recurso Principal Actualizado']);
                $res['success'] = true;
                Session::flash('msg', 'Recurso Principal Actualizado');
                $res['redirect'] = route('acquisitionMainSourcesEdit', $mainSource->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    //FUNCTION DELETE
    public function delete( AcquisitionsMainSources $mainSource ){
        $res = ['success' => true];
        if( $mainSource->delete() ){
            $mainSource->event()->create(['description' => 'Recurso Principal Eliminado']);
            Session::flash('msg', 'Recurso Principal borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
