<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    PropertiesTypes
};

class PropertiesTypesController extends Controller
{
    public function index(){
        $types = PropertiesTypes::sortable()->orderBy('name', 'asc')->paginate( 50 );
        return view( "properties.types.index", compact('types') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $types = PropertiesTypes::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('slug', "like", "%".$search."%");
        })->orderBy('name', 'asc')->paginate( 50 );
        return view( 'properties.types.index', compact('types', 'search') );
    }

    public function create(){
        return view( "properties.types.create");
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $type = new PropertiesTypes;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties_types';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $type->name = ucfirst($request->input( "name" ));
            $type->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $type->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Tipo Agregado');
                $res['redirect'] = route('propertiesTypes');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( PropertiesTypes $type ){
        return view( "properties.types.view", compact('type') );
    }

    public function update( PropertiesTypes $type, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties_types')->ignore($type->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $type->name = ucfirst($request->input( "name" ));
            $type->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $type->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Tipo Editado');
                $res['redirect'] = route('propertiesTypeView', $type->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( PropertiesTypes $type ){
        $res = ['success' => true];
        if( $type->delete() ){
            Session::flash('msg', 'Tipo borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
