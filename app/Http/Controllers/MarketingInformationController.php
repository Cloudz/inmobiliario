<?php

namespace App\Http\Controllers;

use Route;
use Session;

use Illuminate\Http\Request;

use App\Models\{
    AcquisitionsMainMediums, AcquisitionsMainSources, AcquisitionsSecondaryMediums, AcquisitionsSecondarySources, ContactMediums, ContactWays, MarketingInformation
};

class MarketingInformationController extends Controller
{
    //FUNCTION LIST
    public function index(){
        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager'])) {
            $marketings = MarketingInformation::orderBy('created_at', 'desc')->paginate( 50 );
        }else{
            $marketings = MarketingInformation::where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->paginate( 50 );
        }
        $redoMarketings = [];
        foreach ($marketings as $marketing) {
            $contactWay     = ContactWays::where('id', $marketing->contact_way_id)->first();
            $contactMedium  = ContactMediums::where('id', $marketing->contact_medium_id)->first();
            $path_model  = 'App\Models\Sources\\';
            if($contactWay){
                if($contactWay->is_multi){
                    $multiSourcesModel = strtolower($contactWay->model);
                    $marketing->multiSources = $marketing->$multiSourcesModel()->get();
                }else{
                    $marketing->multiSources = NULL;
                }
            }else{
                $marketing->multiSources = NULL;
            }
            if($contactMedium){
                if($contactMedium->is_multi){
                    $multiMediumsModel       = 'mediums_'.strtolower($contactMedium->model);
                    $marketing->multiMediums = $marketing->$multiMediumsModel()->get();
                }else{
                    $marketing->multiMediums = NULL;
                }
            }else{
                $marketing->multiMediums = NULL;
            }
            if($marketing->main_source_id){
                if(!is_null($marketing->main_source_id)){
                    $mainModel     = AcquisitionsMainSources::where('contact_way_id', $marketing->contact_way_id)->orderBy('created_at', 'desc')->first();
                    $mainModelName = $path_model.$mainModel->model;
                    $mainSource    = $mainModelName::where('id', $marketing->main_source_id)->orderBy('created_at', 'desc')->first();
                    if(!is_null($mainSource)) {
                        $marketing->mainSource = $mainSource;
                    }else{
                        $marketing->mainSource = NULL;
                    }
                }else{
                    $marketing->mainSource = 0;
                }
            }else{
                $marketing->mainSource = 0;
            }
            if($marketing->main_medium_source_id){
                if(!is_null($marketing->main_medium_source_id)){
                    $mainModelMedium       = AcquisitionsMainMediums::where('contact_way_id', $marketing->contact_medium_id)->orderBy('created_at', 'desc')->first();
                    $mainModelNameMedium   = $path_model . $mainModelMedium->model;
                    $mainMedium            = $mainModelNameMedium::where('id', $marketing->main_medium_source_id)->orderBy('created_at', 'desc')->first();

                    if(!is_null($mainMedium)) {
                        $marketing->mainMedium = $mainMedium;
                    }else{
                        $marketing->mainMedium = NULL;
                    }
                }else{
                    $marketing->mainMedium = 0;
                }
            }else{
                $marketing->mainMedium = 0;
            }
            if($marketing->secondary_source_id){
                if(!is_null($marketing->secondary_source_id)){
                    $secondaryModel             = AcquisitionsSecondarySources::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();
                    $secondaryModelName         = $path_model . $secondaryModel->model;
                    $marketing->secondarySource = $secondaryModelName::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();

                }else{
                    $marketing->secondarySource = 0;
                }
            }else{
                $marketing->secondarySource = 0;
            }
            if($marketing->secondary_medium_source_id){
                if(!is_null($marketing->secondary_medium_source_id)){
                    $secondaryModelMedium = AcquisitionsSecondaryMediums::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();
                    $secondaryModelNameMedium = $path_model . $secondaryModelMedium->model;
                    $marketing->secondaryMedium = $secondaryModelNameMedium::where('id', $marketing->secondary_medium_source_id)->orderBy('created_at', 'desc')->first();

                }else{
                    $marketing->secondaryMedium = 0;
                }
            }else{
                $marketing->secondaryMedium = 0;
            }

            $redoMarketings[] = $marketing;
        }
        return view('admin.marketing.index', compact('marketings', 'redoMarketings', 'mainLower'));
    }
}
