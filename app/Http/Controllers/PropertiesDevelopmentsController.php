<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\{
    PropertiesDevelopments
};

class PropertiesDevelopmentsController extends Controller
{
    public function index(){
        $developments = PropertiesDevelopments::sortable()->orderBy('name', 'asc')->paginate( 50 );
        return view( "properties.developments.index", compact('developments') );
    }

    public function search( Request $request ){
        $search = $request->input("search");
        $developments = PropertiesDevelopments::sortable()->where(function ($query) use ($search) {
            $query->where('name', "like", "%".$search."%")
                ->orWhere('slug', "like", "%".$search."%");
        })->orderBy('name', 'asc')->paginate( 50 );
        return view( 'properties.developments.index', compact('developments', 'search') );
    }

    public function create(){
        return view( "properties.developments.create");
    }

    public function store( Request $request ){
        $res = ['success' => false];
        $development = new PropertiesDevelopments;

        $rules = [];
        $messages = [];

        $rules['name'] = 'required|min:3|unique:properties_developments';

        $messages['name.required']  = 'El nombre es obligatorio';
        $messages['name.min']       = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']    = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $development->name = ucfirst($request->input( "name" ));
            $development->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $development->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Desarrollo Agregado');
                $res['redirect'] = route('propertiesDevelopments');
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function view( PropertiesDevelopments $development ){
        return view( "properties.developments.view", compact('development') );
    }

    public function update( PropertiesDevelopments $development, Request $request ){
        $res = ['success' => false];
        $rules = [];
        $messages = [];

        $rules['name'] = ['required', 'min:3', Rule::unique('properties_developments')->ignore($development->id) ];

        $messages['name.required'] = 'El nombre es obligatorio';
        $messages['name.min']      = 'El nombre debe tener al menos 3 caracteres';
        $messages['name.unique']   = 'El nombre ya se encuentra registrado';

        $validator = Validator::make( $request->all(), $rules, $messages );

        if( !$validator->fails() ){

            $development->name = ucfirst($request->input( "name" ));
            $development->slug = trim(str_replace(' ', '-', strtolower($request->input( "name" ))));

            if( $development->save() ){
                $res['success'] = true;
                Session::flash('msg', 'Desarrollo Editado');
                $res['redirect'] = route('propertiesDevelopmentView', $development->id);
            }else{
                $res['errors'][] = array('Ocurrió un error inesperado');
            }

        }else{
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function delete( PropertiesDevelopments $development ){
        $res = ['success' => true];
        if( $development->delete() ){
            Session::flash('msg', 'Desarrollo borrado correctamente');
            Session::flash('msg_status', true);
        }else{
            Session::flash('msg', 'Hubo un error al eliminar');
            Session::flash('msg_status', false);
            $res['success'] = false;
        }
        return $res;
    }
}
