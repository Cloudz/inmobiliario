<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Role;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){

        if ( Auth::guard($guard)->check() ) {
            if( Auth::user()->is_active ) {
                header('Location: ' . route('dashboard'));exit;
            }else{
                Auth::guard($guard)->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                abort(403, 'Unauthorized action.');
            }
        }

        return $next($request);
    }
}
