@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agencies.create_agency') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agencies') }}"><i class="fa fa-undo"></i> {{ trans('agencies.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('agencies.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('agencyStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">{{ trans('agencies.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="website">{{ trans('agencies.website') }}</label>
                            <input type="text" class="form-control" id="website" name="website" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="main_office_phone">{{ trans('agencies.main_phone') }}</label>
                            <input type="text" class="form-control" id="main_office_phone" name="main_office_phone">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="secondary_office_phone">{{ trans('agencies.secondary_phone') }}</label>
                            <input type="text" class="form-control" id="secondary_office_phone" name="secondary_office_phone">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="nationality">{{ trans('agencies.nationality') }}*</label>
                            <select class="form-control" id="nationality" name="nationality" required="required">
                                <option value="">{{ trans('agencies.select_country') }} ...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="address">{{ trans('agencies.address') }}</label>
                            <textarea rows="6" class="form-control" id="address" name="address"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('agencies.comments') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('agencies.add_agency') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection