@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agencies.view_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agencies') }}"><i class="fa fa-undo"></i> {{ trans('agencies.back_list') }}</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('agencies.enable_edition') }}</a>
            <a class="btn action_btn btn-xs" href="{{ route('agentCreate', $agency->id) }}"><i class="fa fa-plus"></i> {{ trans('agencies.create_agent') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('agencies.view_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('agencyUpdate', $agency->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">{{ trans('agencies.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $agency->name }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="website">{{ trans('agencies.website') }}</label>
                            <input type="text" class="form-control" id="website" name="website" required="required" value="{{ $agency->website }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="main_office_phone">{{ trans('agencies.main_phone') }}</label>
                            <input type="text" class="form-control" id="main_office_phone" name="main_office_phone" value="{{ $agency->main_office_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="secondary_office_phone">{{ trans('agencies.secondary_phone') }}</label>
                            <input type="text" class="form-control" id="secondary_office_phone" name="secondary_office_phone" value="{{ $agency->secondary_office_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-3 col-lg-3 c-padding-left-input">
                            <label for="nationality">{{ trans('agencies.nationality') }}*</label>
                            <select class="form-control" id="nationality" name="nationality" required="required">
                                <option value="">{{ trans('agencies.select_country') }} ...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" {{ ($agency->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="address">{{ trans('agencies.address') }}</label>
                            <textarea rows="6" class="form-control" id="address" name="address" readonly>{{ $agency->address }}</textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('agencies.title_agents') }}</h3>
                            <div class="container_list">
                                @foreach ($agents as $agent)
                                    @if($agent->is_broker == 1)
                                        <?php $titleAgent = trans('agencies.broker') ?>
                                    @else
                                        <?php $titleAgent = trans('agencies.agent') ?>
                                    @endif
                                    <div class="item_list flex-between">
                                        <div class="desc_list">
                                            <i class="fa fa-user"></i> {{ $agent->name}} - <span>({{ $titleAgent }})</span>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="{{ route('agentView', $agent->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('agentDelete', $agent->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('agencies.make_comment') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('agencies.comments') }}</h3>
                            <div class="container_list">
                                @foreach ($agency->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="agencyView" data-source="{{ $agency->id }}" class="edit-trigger btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('agencies.send_information') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection