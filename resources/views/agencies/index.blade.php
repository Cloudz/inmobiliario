@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agencies.index_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>{{ trans('agencies.index_section') }} - <i class="fa fa-th-list"></i> {{ $agencies->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agencyCreate') }}"><i class="fa fa-plus"></i> {{ trans('agencies.create_agency') }}</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('agencies') }}"><i class="fa fa-eraser"></i> {{ trans('agencies.clean_search') }}</a>
            @endif
        </div>
        <form action="{{ route('agencySearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="{{ trans('agencies.search_agency') }}..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="{{ trans('global.search') }}" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $agencies->appends(['search' => $search])->render() }}
            @else
                {{ $agencies->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>{{ trans('agencies.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.index_website') }} @sortablelink('website', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.phone') }} @sortablelink('main_office_phone', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.city') }}</th>
                            <th>{{ trans('agencies.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($agencies)>0)
                            @foreach($agencies as $agency)
                                <tr role="row">
                                    <td style="width: 10%">{{ $agency->name }}</td>
                                    <td style="width: 10%">{{ $agency->website }}</td>
                                    <td style="width: 10%">{{ $agency->main_office_phone }}</td>
                                    <td style="width: 10%">{{ $agency->countryName->name }}</td>
                                    <td style="width: 10%">
                                        <a href="{{ route('agencyView', $agency->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('agencyDelete', $agency->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                            <i class="fa fa-fw fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('agencies.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.index_website') }} @sortablelink('website', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.phone') }} @sortablelink('main_office_phone', trans('global.orderby'))</th>
                            <th>{{ trans('agencies.city') }}</th>
                            <th>{{ trans('agencies.actions') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $agencies->appends(['search' => $search])->render() }}
        @else
            {{ $agencies->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
