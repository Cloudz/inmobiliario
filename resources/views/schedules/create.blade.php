@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('schedules.create_schedule') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('schedules') }}"><i class="fa fa-undo"></i> {{ trans('schedules.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('schedules.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('scheduleStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="agent">{{ trans('schedules.select_agent') }}*</label>
                            <select class="form-control" id="agent" name="agent" required="required">
                                <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="office">{{ trans('schedules.select_office') }}*</label>
                            <select class="form-control" id="office" name="office" required="required">
                                <option value="">{{ trans('schedules.select_office') }} ...</option>
                                @foreach ($offices as $office)
                                    <option value="{{ $office->id }}">{{ $office->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="start_at">{{ trans('schedules.create_start_at') }}*</label>
                            <input type="text" class="form-control" id="start_at" name="start_at" placeholder="{{ trans('schedules.create_start_at') }} ..." value="" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="end_at">{{ trans('schedules.create_end_at') }}*</label>
                            <input type="text" class="form-control" id="end_at" name="end_at" placeholder="{{ trans('schedules.create_end_at') }} ..." value="" required="required">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="comments">{{ trans('schedules.comments') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('schedules.add_schedule') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection