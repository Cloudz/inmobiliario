@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agencies.view_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('schedules') }}"><i class="fa fa-undo"></i> {{ trans('schedules.back_list') }}</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('schedules.enable_edition') }}</a>
            <a class="btn action_btn btn-xs" href="{{ route('scheduleCreate', $schedule->id) }}"><i class="fa fa-plus"></i> {{ trans('schedules.create_schedule') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('schedules.view_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('scheduleUpdate', $schedule->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="agent">{{ trans('schedules.select_agent') }}*</label>
                            <select class="form-control" id="agent" name="agent" disabled>
                                <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}" {{ ($schedule->agent_id == $agent->id) ? 'selected' : '' }}>{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="office">{{ trans('schedules.select_office') }}</label>
                            <select class="form-control" id="office" name="office" disabled>
                                <option value="">{{ trans('schedules.select_office') }} ...</option>
                                @foreach ($offices as $office)
                                    <option value="{{ $office->id }}" {{ ($schedule->office_id == $office->id) ? 'selected' : '' }}>{{ $office->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="start_at">{{ trans('schedules.select_start_at') }}</label>
                            <input type="text" class="form-control" id="start_at" name="start_at" placeholder="{{ trans('schedules.select_start_at') }} ..." value="{{ $schedule->start_at }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="end_at">{{ trans('schedules.select_end_at') }}</label>
                            <input type="text" class="form-control" id="end_at" name="end_at" placeholder="{{ trans('schedules.select_end_at') }} ..." value="{{ $schedule->end_at }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('agencies.make_comment') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('agencies.comments') }}</h3>
                            <div class="container_list">
                                @foreach ($schedule->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="scheduleView" data-source="{{ $schedule->id }}" class="edit-trigger btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('schedules.send_information') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection