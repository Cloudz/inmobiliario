@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('schedules.index_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>{{ trans('schedules.index_section') }} - <i class="fa fa-th-list"></i> {{ $schedules->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('scheduleCreate') }}"><i class="fa fa-plus"></i> {{ trans('schedules.create_schedule') }}</a>
        </div>
        <div class="col-xs-12">
            {{ $schedules->links() }}
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>{{ trans('schedules.agent') }} </th>
                            <th>{{ trans('schedules.office') }} </th>
                            <th>{{ trans('schedules.start_at') }} @sortablelink('start_at', trans('global.orderby'))</th>
                            <th>{{ trans('schedules.end_at') }} @sortablelink('end_at', trans('global.orderby'))</th>
                            <th>{{ trans('schedules.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($schedules)>0)
                            <tr>
                                @foreach($schedules as $schedule)
                                    <tr role="row">
                                        <td style="width: 10%">{{ $schedule->agent->name }}</td>
                                        <td style="width: 10%">{{ $schedule->office->name }}</td>
                                        <td style="width: 10%">{{ $schedule->start_at }}</td>
                                        <td style="width: 10%">{{ $schedule->end_at }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('scheduleView', $schedule->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('scheduleDelete', $schedule->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('schedules.agent') }} </th>
                            <th>{{ trans('schedules.office') }} </th>
                            <th>{{ trans('schedules.start_at') }} @sortablelink('start_at', trans('global.orderby'))</th>
                            <th>{{ trans('schedules.end_at') }} @sortablelink('end_at', trans('global.orderby'))</th>
                            <th>{{ trans('schedules.actions') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
            {{ $schedules->links() }}
      </div>
      <!-- /.row -->
    </section>
@endsection
