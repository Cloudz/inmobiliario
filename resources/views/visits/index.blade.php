@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Visitas
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN VISITAS - <i class="fa fa-th-list"></i> {{ $visits->total() }}</h1>
        </div>
        <div class="main-head-actions">
            @if( $offices->count() > 0)
                <a class="btn action_btn btn-xs" href="{{ route('visitCreate') }}"><i class="fa fa-plus"></i> Agregar Visita</a>
            @else
                <div class="legend-office bg-red">No se han encontrado Oficinas registradas para añadir visitas.</div>
            @endif
            @role(['Administrator', 'Developer', 'Broker', 'Sales Agent', 'General Assistant'])
                <a class="btn action_btn btn-xs" href="{{ route('officeCreate') }}"><i class="fa fa-plus"></i> Agregar Oficina</a>
            @endrole
        </div>
        <div class="col-xs-12">
        {{ $visits->links() }}
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>FECHA Y HORA @sortablelink('visit_date', trans('global.orderby'))</th>
                            <th>CLIENTE</th>
                            <th>OFICINA</th>
                            <th>ASESOR</th>
                            <th>COMENTARIO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($visits)>0)
                            <tr>
                                @foreach($visits as $visit)
                                    @if($visit->comment->count() > 0)
                                        <?php
                                        $keyComment = count($visit->comment) - 1;
                                        $comment = strip_tags($visit->comment[$keyComment]->comments);
                                        ?>
                                    @else
                                        <?php $comment = 'No Hay Comentarios'; ?>
                                    @endif
                                    <tr role="row">
                                        <td style="width: 10%">{{ $visit->visit_date }}</td>
                                        <td style="width: 10%"><a href="{{ route('clientView', $visit->client->id) }}">{{ $visit->client->name }}</a></td>
                                        @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant']))
                                            <td style="width: 10%"><a href="{{ route('officeView', $visit->office->id) }}">{{ $visit->office->name }}</a></td>
                                            <td style="width: 10%"><a href="{{ route('userView', $visit->adviser->id) }}">{{ $visit->adviser->name }}</a></td>
                                        @else
                                            <td style="width: 10%">{{ $visit->office->name }}</td>
                                            <td style="width: 10%">{{ $visit->adviser->name }}</td>
                                        @endif
                                        <td style="width: 10%">{{ $comment }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('visitView', $visit->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('visitDelete', $visit->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>FECHA Y HORA @sortablelink('visit_date', trans('global.orderby'))</th>
                            <th>CLIENTE</th>
                            <th>OFICINA</th>
                            <th>ASESOR</th>
                            <th>COMENTARIO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        {{ $visits->links() }}
      </div>
      <!-- /.row -->
    </section>
@endsection
