@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Visita
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('visits') }}"><i class="fa fa-undo"></i> Volver al Listado de Visitas</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">CREAR VISITA</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body tabs-menu-wrapper">
                        <form id="form_update" action="{{ route('visitStore') }}" method="post" class="ajax-form">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="date_visit">Fecha y Hora de Visita*</label>
                                    <input type="text" class="form-control" id="date_visit" name="date_visit" placeholder="Escoger Fecha ...">
                                </div>
                                <input type="hidden" id="visit_date" name="visit_date" required="required">
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="office">Oficina*</label>
                                    <select class="form-control" id="office" name="office">
                                        <option value="">Selecciona Oficina ...</option>
                                        @foreach ($offices as $office)
                                            <option value="{{ $office->id }}">{{ $office->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="client">Cliente*</label>
                                    <select class="form-control" id="client" name="client">
                                        <option value="">Selecciona Cliente ...</option>
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="adviser">Agente*</label>
                                    <select class="form-control" id="adviser" name="adviser">
                                        <option value="">Selecciona Agente ...</option>
                                        @foreach ($agents as $agent)
                                            <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="comments">Hacer Comentario</label>
                                    <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn save_btn pull-right">Agregar Visita</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $("#date_visit").on("dp.change", function(e) {
            selectedDate = moment($(this).val(),"DD/MM/YYYY LT");
            tzoffset = (new Date()).getTimezoneOffset() * 60000;
            localISOTime = (new Date(selectedDate - tzoffset)).toISOString().slice(0, -1);
            $('#visit_date').val(localISOTime);
        });
    </script>
@endsection