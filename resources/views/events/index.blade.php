@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Eventos
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN EVENTOS - <i class="fa fa-th-list"></i> {{ $events->total() }}</h1>
        </div>
        <div class="main-head-actions">
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('events') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('eventSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Evento..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $events->appends(['search' => $search])->render() }}
            @else
                {{ $events->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>EVENTO</th>
                            <th>ELEMENTO</th>
                            <th>TIPO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($events)>0)
                            <tr>
                                @foreach($events as $event)
                                    <?php
                                        $eventType = stripslashes($event->eventable_type);
                                        $eventType = str_replace('App', '', $eventType);
                                        $eventName = $event->eventable_type::where('id', $event->eventable_id)->withTrashed()->first();
                                        if ($eventType == 'Agencies'){
                                            $eventTypeSingle = 'agency';
                                        }else{
                                            $eventTypeSingle = strtolower(substr($eventType, 0, -1));
                                        }
                                        
                                        $routeEvent = $eventTypeSingle.'Edit';
                                    ?>
                                    <tr role="row">
                                        <td style="width: 5%">{{ $event->id }}</td>
                                        <td style="width: 35%">{{ $event->description }}</td>
                                        <td style="width: 35%"><a href="{{ route($routeEvent, $event->eventable_id) }}">{{ ($eventName['name'])?$eventName['name']:$eventName['name'] }}</a></td>
                                        <td style="width: 20%">{{ $eventType }}</td>
                                        <td style="width: 5%">
                                            <a href="{{ route('eventDelete', $event->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>EVENTO</th>
                            <th>ELEMENTO</th>
                            <th>TIPO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $events->appends(['search' => $search])->render() }}
        @else
            {{ $events->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
