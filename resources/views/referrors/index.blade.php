@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('referrors.index_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>{{ trans('referrors.index_section') }} - <i class="fa fa-th-list"></i> {{ $referrors->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('referrorCreate') }}"><i class="fa fa-plus"></i> {{ trans('referrors.create_referror') }}</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('referrors') }}"><i class="fa fa-eraser"></i> {{ trans('referrors.clean_search') }}</a>
            @endif
        </div>
        <form action="{{ route('referrorSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="{{ trans('referrors.search_referror') }}..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="{{ trans('global.search') }}" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $referrors->appends(['search' => $search])->render() }}
            @else
                {{ $referrors->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>{{ trans('referrors.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.email') }} @sortablelink('main_email', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.phone') }} @sortablelink('main_mobile', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.index_occupation') }} @sortablelink('occupation', trans('global.orderby'))</th>
                            @role(['Administrator','Developer', 'Broker', 'Office Manager', 'General Assistant'])
                            <th>{{ trans('referrors.agent') }}</th>
                            @endrole
                            <th>{{ trans('referrors.clients_referrors') }}</th>
                            <th>{{ trans('referrors.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($referrors)>0)
                            <tr>
                                @foreach($referrors as $referror)
                                    <tr role="row">
                                        <td style="width: 10%">
                                            @if($referror->gender_id ==0)
                                                <i class="fa fa-venus" style="color: red"></i>
                                            @elseif($referror->gender_id == 1)
                                                <i class="fa fa-mars" style="color: blue"></i>
                                            @else
                                                <i class="fa fa-mars-double" style="color: magenta"></i>
                                            @endif
                                            {{ $referror->name }}
                                        </td>
                                        <td style="width: 10%">{{ $referror->last_name }}</td>
                                        <td style="width: 10%">{{ $referror->main_email }}</td>
                                        <td style="width: 10%">{{ $referror->main_mobile }}</td>
                                        <td style="width: 10%">{{ $referror->occupation }}</td>
                                        @if(Auth::user()->hasRole(['Administrator','Developer', 'Broker', 'Office Manager']))
                                            <td style="width: 10%"><a href="{{ route('userView', $referror->user_id) }}">{{ $referror->user->name }}</a></td>
                                        @elseif(Auth::user()->hasRole(['General Assistant']))
                                            <td style="width: 10%">{{ $referror->user->name }}</td>
                                        @endif
                                        <td style="width: 10%">{{ $referror->marketingInformation->count() }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('referrorView', $referror->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('referrorDelete', $referror->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('referrors.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.email') }} @sortablelink('main_email', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.phone') }} @sortablelink('main_mobile', trans('global.orderby'))</th>
                            <th>{{ trans('referrors.index_occupation') }} @sortablelink('occupation', trans('global.orderby'))</th>
                            @role(['Administrator','Developer', 'Broker', 'Office Manager', 'General Assistant'])
                            <th>{{ trans('referrors.agent') }}</th>
                            @endrole
                            <th>{{ trans('referrors.clients_referrors') }}</th>
                            <th>{{ trans('referrors.actions') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $referrors->appends(['search' => $search])->render() }}
        @else
            {{ $referrors->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
