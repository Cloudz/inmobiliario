@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('referrors.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('referrors') }}"><i class="fa fa-undo"></i> {{ trans('referrors.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('referrors.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('referrorStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])
                        <div class="form-group col-xs-12 c-padding-left-input">
                            <label for="agent_id">{{ trans('schedules.select_agent') }}*</label>
                            <select class="form-control" id="agent_id" name="agent_id" required="required">
                                <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endrole
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="name">{{ trans('referrors.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="last_name">{{ trans('referrors.last_name') }}*</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="main_email">{{ trans('referrors.main_email') }}*</label>
                            <input type="email" class="form-control" id="main_email" name="main_email" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="secondary_email">{{ trans('referrors.secondary_email') }}</label>
                            <input type="email" class="form-control" id="secondary_email" name="secondary_email">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="home_phone">{{ trans('referrors.home_phone') }}</label>
                            <input type="text" class="form-control" id="home_phone" name="home_phone">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="tel_office">{{ trans('referrors.office_phone') }}</label>
                            <input type="text" class="form-control" id="tel_office" name="tel_office">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="main_mobile">{{ trans('referrors.main_mobile') }}</label>
                            <input type="text" class="form-control" id="main_mobile" name="main_mobile">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="secondary_mobile">{{ trans('referrors.secondary_mobile') }}</label>
                            <input type="text" class="form-control" id="secondary_mobile" name="secondary_mobile">
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="address">{{ trans('referrors.address') }}</label>
                            <textarea rows="6" class="form-control" id="address" name="address"></textarea>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                            <label for="sex">{{ trans('referrors.sex') }}*</label>
                            <select class="form-control" id="sex" name="sex" required="required">
                                <option value="0">{{ trans('referrors.male') }}</option>
                                <option value="1">{{ trans('referrors.female') }}</option>
                                <option value="2">{{ trans('referrors.lgbt') }}</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                            <label for="nationality">{{ trans('referrors.nationality') }}*</label>
                            <select class="form-control" id="nationality" name="nationality" required="required">
                                <option value="">{{ trans('referrors.select_country') }} ...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="occupation">{{ trans('referrors.view_occupation') }}</label>
                            <input type="text" class="form-control" id="occupation" name="occupation">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12">
                            <label for="comments">{{ trans('referrors.comments') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('referrors.add_referror') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection