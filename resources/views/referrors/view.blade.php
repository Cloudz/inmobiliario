@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('referrors.view_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('referrors') }}"><i class="fa fa-undo"></i> {{ trans('referrors.back_list') }}</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('referrors.enable_edition') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('referrors.view_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('referrorUpdate', $referror->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])
                        <div class="form-group col-xs-12 c-padding-left-input">
                            <label for="agent_id">{{ trans('schedules.select_agent') }}*</label>
                            <select class="form-control" id="agent_id" name="agent_id" required="required" disabled="disabled">
                                <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}" {{ ($referror->user_id == $agent->id) ? 'selected' : '' }}>{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endrole
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="name">{{ trans('referrors.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $referror->name }}" required="required" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="last_name">{{ trans('referrors.last_name') }}*</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $referror->last_name }}" required="required" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="main_email">{{ trans('referrors.main_email') }}*</label>
                            <input type="email" class="form-control" id="main_email" name="main_email" value="{{ $referror->main_email }}" required="required" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="secondary_email">{{ trans('referrors.secondary_email') }}</label>
                            <input type="email" class="form-control" id="secondary_email" name="secondary_email" value="{{ $referror->secondary_email }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="home_phone">{{ trans('referrors.home_phone') }}</label>
                            <input type="text" class="form-control" id="home_phone" name="home_phone" value="{{ $referror->home_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="tel_office">{{ trans('referrors.office_phone') }}</label>
                            <input type="text" class="form-control" id="tel_office" name="tel_office" value="{{ $referror->tel_office }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="main_mobile">{{ trans('referrors.main_mobile') }}</label>
                            <input type="text" class="form-control" id="main_mobile" name="main_mobile" value="{{ $referror->main_mobile }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="secondary_mobile">{{ trans('referrors.secondary_mobile') }}</label>
                            <input type="text" class="form-control" id="secondary_mobile" name="secondary_mobile" value="{{ $referror->secondary_mobile }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                            <label for="sex">{{ trans('referrors.sex') }}*</label>
                            <select class="form-control" id="sex" name="sex" required="required">
                                <option value="0" {{ ($referror->gender_id == '0') ? 'selected' : '' }}>{{ trans('referrors.male') }}</option>
                                <option value="1" {{ ($referror->gender_id == '1') ? 'selected' : '' }}>{{ trans('referrors.female') }}</option>
                                <option value="2" {{ ($referror->gender_id == '2') ? 'selected' : '' }}>{{ trans('referrors.lgbt') }}</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                            <label for="nationality">{{ trans('referrors.nationality') }}*</label>
                            <select class="form-control" id="nationality" name="nationality" required="required">
                                <option value="">{{ trans('referrors.select_country') }} ...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" {{ ($referror->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="occupation">{{ trans('referrors.view_occupation') }}</label>
                            <input type="text" class="form-control" id="occupation" name="occupation" value="{{ $referror->occupation }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="address">{{ trans('referrors.address') }}</label>
                            <textarea rows="6" class="form-control" id="address" name="address" readonly>{{ $referror->address }}</textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('referrors.related_customers') }}</h3>
                            <div class="container_list">
                                @foreach ($referror->marketingInformation as $marketing)
                                    <?php
                                        if(\UsersHelpers::getUser()->hasRole(['Administrator', 'Developer'])){
                                            $client = \App\Models\Clients::where('id', $marketing->client_id)->first();
                                        }else{
                                            $client = \App\Models\Clients::where('id', $marketing->client_id)->where('user_id', \UsersHelpers::getUserID())->first();
                                        }
                                    ?>
                                    <div class="item_list flex-between">
                                        <div class="desc_list">
                                            <i class="fa fa-user"></i> {{ $client->name}}
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="{{ route('clientView', $client->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('referrors.make_comment') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('referrors.comments') }}</h3>
                            <div class="container_list">
                                @foreach ($referror->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="referrorView" data-source="{{ $referror->id }}" class="edit-trigger btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('referrors.send_information') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection