@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Perfil de Oficina
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('offices') }}"><i class="fa fa-undo"></i> Volver al Listado de Oficinas</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> Habilitar Edición</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">PERFIL DE OFICINA</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('officeUpdate', $office->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input class="getState" type="hidden" value="{{ $office->state_id }}">
                    <input class="getCity" type="hidden" value="{{ $office->city_id }}">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="name">Nombre*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $office->name }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="main_phone">Tel. Principal</label>
                            <input type="text" class="form-control" id="main_phone" name="main_phone" value="{{ $office->main_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="secondary_phone">Tel. Secundario</label>
                            <input type="text" class="form-control" id="secondary_phone" name="secondary_phone" value="{{ $office->secondary_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="fax">Fax</label>
                            <input type="text" class="form-control" id="fax" name="fax" value="{{ $office->fax }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                        <label for="country">Nacionalidad*</label>
                        <select class="form-control" id="country" name="country">
                            <option value="">Selecciona País ...</option>
                            @foreach ($countries as $country)
                                <option value="{{ $country->id }}" {{ ($office->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                        <label for="state">Estado*</label>
                        <select class="form-control" id="state" name="state" required>
                            <option value="">Selecciona Estado ...</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                        <label for="city">Ciudad*</label>
                        <select class="form-control" id="city" name="city" required>
                            <option value="">Selecciona Ciudad ...</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                        <label for="street">Calle</label>
                        <input type="text" class="form-control" id="street" name="street" value="{{ $office->street }}" readonly>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                        <label for="number_int">Número Interno</label>
                        <input type="text" class="form-control" id="number_int" name="number_int" value="{{ $office->number_int }}" readonly>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                        <label for="number_ext">Número Externo</label>
                        <input type="text" class="form-control" id="number_ext" name="number_ext" value="{{ $office->number_ext }}" readonly>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                        <label for="cp">C.P.</label>
                        <input type="text" class="form-control" id="cp" name="cp" value="{{ $office->cp }}" readonly>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">Hacer Comentario</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">Comentarios</h3>
                            <div class="container_list">
                                @foreach ($office->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="officeView" data-source="{{ $office->id }}" class="edit-trigger btn edit_btn btn-xs" title="Editar" alt="Editar">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="container-complete form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                        <input type="checkbox" id="active" name="active" {{ ($office->active) ? 'checked' : '' }} >
                        <label for="active" class="{{ ($office->active) ? 'bg-green' : 'bg-red' }}">  {{ ($office->active) ? ' Activa' : ' No Activa' }}</label>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Enviar Información</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $(function() {

            getState($('#country').val());
            setTimeout(function(){ getCity($('#state').val()); }, 500);

            $('#country').change(function() {
                getState($(this).val());
            });

            $('#state').change(function() {
                getCity($(this).val());
            });

            function getState(country){
                var url = "{{ url('admin') }}" + "/country/states/" + country;
                var idState = parseInt($('.getState').val());
                $.get(url, function(data) {
                    var select = $('#state');
                    var selectCity = $('#city');
                    select.empty();
                    selectCity.empty();
                    selectCity.append('<option>Selecciona Ciudad ...</option>');
                    $.each(data,function(key, value) {
                        if (idState == value.id) {
                            select.append("<option value=" + value.id + " selected>" + value.name + "</option>");
                        }else{
                            select.append("<option value=" + value.id + ">" + value.name + "</option>");
                        }
                    });
                });
            }

            function getCity(state){
                var url = "{{ url('admin') }}" + "/state/cities/" + state;
                var idCity = parseInt($('.getCity').val());
                $.get(url, function(data) {
                    var city = $('#city');
                    city.empty();
                    $.each(data,function(key, value) {
                        if (idCity == value.id) {
                            city.append("<option value=" + value.id + " selected>" + value.name + "</option>");
                        }else{
                            city.append("<option value=" + value.id + ">" + value.name + "</option>");
                        }
                    });
                });
            }
        });
    </script>
@endsection