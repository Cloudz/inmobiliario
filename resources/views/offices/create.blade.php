@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Oficina
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('offices') }}"><i class="fa fa-undo"></i> Volver al Listado de Oficinas</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">CREAR OFICINA</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body tabs-menu-wrapper">
                        <form id="form_update" action="{{ route('officeStore') }}" method="post" class="ajax-form">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="name">Nombre*</label>
                                    <input type="text" class="form-control" id="name" name="name" required="required">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="main_phone">Tel. Principal*</label>
                                    <input type="text" class="form-control" id="main_phone" name="main_phone" required="required">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="secondary_phone">Tel. Secundario</label>
                                    <input type="text" class="form-control" id="secondary_phone" name="secondary_phone">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="fax">Fax</label>
                                    <input type="text" class="form-control" id="fax" name="fax">
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                <label for="country">Nacionalidad*</label>
                                <select class="form-control" id="country" name="country">
                                    <option value="">Selecciona País ...</option>
                                    @foreach ($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                <label for="state">Estado*</label>
                                <select class="form-control" id="state" name="state" required>
                                    <option value="">Selecciona Estado ...</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                <label for="city">Ciudad*</label>
                                <select class="form-control" id="city" name="city" required>
                                    <option value="">Selecciona Ciudad ...</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="street">Calle</label>
                                <input type="text" class="form-control" id="street" name="street">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="number_int">Número Interno</label>
                                <input type="text" class="form-control" id="number_int" name="number_int">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="number_ext">Número Externo</label>
                                <input type="text" class="form-control" id="number_ext" name="number_ext">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="cp">C.P.</label>
                                <input type="text" class="form-control" id="cp" name="cp">
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="comments">Hacer Comentario</label>
                                    <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn save_btn pull-right">Agregar Oficina</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $(function() {
            $('#country').change(function() {
                getState($(this).val());
            });

            $('#state').change(function() {
                getCity($(this).val());
            });

            function getState(country){
                var url = "{{ url('admin') }}" + "/country/states/" + country;
                $.get(url, function(data) {
                    var select = $('#state');
                    var selectCity = $('#city');
                    select.empty();
                    selectCity.empty();
                    selectCity.append('<option>Selecciona Ciudad ...</option>');
                    $.each(data,function(key, value) {
                        select.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }

            function getCity(state){
                var url = "{{ url('admin') }}" + "/state/cities/" + state;
                $.get(url, function(data) {
                    var city = $('#city');
                    city.empty();
                    $.each(data,function(key, value) {
                        city.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }
        });
    </script>
@endsection