@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.import_clients') }}
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('clients.import_clients') }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body tabs-menu-wrapper">
                        <form action="{{ route('clientsImport') }}" method="post" class="ajax-form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="file_clients">{{ trans('clients.file') }}*</label>
                                    <input type="file" class="form-control" name="file_clients" id="file_clients" required>
                                </div>
                            </div>
                            <button type="submit" class="btn save_btn pull-right">{{ trans('clients.import_clients') }}</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection