@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.assign_client') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clientView', $client->id) }}"><i class="fa fa-undo"></i> {{ trans('global.back') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('dashboard.send_message') }} - {{ $client->main_email }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('quickSendMail') }}" method="post" class="ajax-form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" name="f_email" value="{{ $client->main_email }}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="f_subject" placeholder="{{ trans('dashboard.subject') }}">
                    </div>
                    <div class="form-group">
                        <textarea rows="8" class="form-control" id="f_message" name="f_message" placeholder="{{ trans('dashboard.message') }}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="f_file">{{ trans('dashboard.file') }}</label>
                        <input type="file" id="f_file" name="f_file">
                    </div>
                    <div class="box-footer clearfix">
                        <button type="submit" class="btn save_btn pull-right">{{ trans('dashboard.send') }}</button>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection