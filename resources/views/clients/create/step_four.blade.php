@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('clients.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ route('clientStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="page" id="page" value="">
                    <div id="register">
                        <h3 class="title-tab">{{ trans('clients.end_register') }}</h3>
                        <div class="form-row col-step">
                            <div class="form-group col-xs-12">
                                <label for="comments">{{ trans('clients.comments') }}</label>
                                <textarea rows="12" class="form-control" id="comments" name="comments"></textarea>
                            </div>
                            <div class="container-complete form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                <input type="checkbox" id="send_information" name="send_information">
                                <label for="send_information">{{ trans('clients.send_notification') }}</label>
                            </div>
                            <div class="form-group col-xs-12 f_message">
                                <label for="f_message">{{ trans('clients.custom_message') }}</label>
                                <textarea rows="12" class="form-control" id="f_message" name="f_message"></textarea>
                            </div>
                        </div>
                        <div class="container-btns-steps">
                            <a href="{{ route('createStepThree') }}" class="btn-prev-step">{{ trans('clients.previous') }}</a>
                            <button type="submit" class="btn-next-step">{{ trans('clients.add_title') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection