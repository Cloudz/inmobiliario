@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('clients.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form id="form_create_step_one" action="{{ route('saveDataStepOne') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="page" id="page" value="">
                    <div id="general">
                        <h3 class="title-tab">{{ trans('clients.title_step_one') }}</h3>
                        <div class="form-row col-step">
                            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant', 'Personal Assistant'])
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="agent_id">{{ trans('schedules.select_agent') }}*</label>
                                    <select class="form-control" id="agent_id" name="agent_id" required="required">
                                        <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                        @foreach ($agents as $agent)
                                            <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endrole
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="name">{{ trans('clients.create_name') }}*</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ (session()->has('client'))?$client->name:'' }}">
                            </div>
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="last_name">{{ trans('clients.create_last_name') }}*</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" value="{{ (session()->has('client'))?$client->last_name:'' }}">
                            </div>
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="main_email">{{ trans('clients.main_email') }}*</label>
                                <input type="email" class="form-control" id="main_email" name="main_email" value="{{ (session()->has('client'))?$client->main_email:'' }}">
                            </div>
                            <a href="#" class="btn action_btn verify_client">{{ trans('clients.verify_client') }}</a>
                        </div>
                        <div class="container-btns-steps">
                            <button type="submit" class="next_verify bgLinkNext btn-next-step" disabled>{{ trans('clients.next') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection