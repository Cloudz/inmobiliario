@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('clients.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ route('saveDataStepTwo') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="page" id="page" value="">
                    <div id="information">
                        <h3 class="title-tab">{{ trans('clients.title_step_two') }} <small style="color: #bbb;">{{ trans('clients.optional') }}</small></h3>
                        <div class="form-row col-step">
                                <div class="form-group col-xs-12">
                                    <label for="treatment">{{ trans('clients.treatment') }}</label>
                                    <select class="form-control" id="treatment" name="treatment">
                                        <option value="">{{ trans('clients.select_treatment') }} ...</option>
                                        @foreach ($treatments as $treatment)
                                            <option value="{{ $treatment->id }}" {{ (session()->has('client') && $client->treatment_id == $treatment->id) ? 'selected' : '' }}>{{ $treatment->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="secondary_email">{{ trans('clients.secondary_email') }}</label>
                                    <input type="email" class="form-control" id="secondary_email" name="secondary_email" value="{{ (session()->has('client'))?$client->secondary_email:'' }}">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="home_phone">{{ trans('clients.home_phone') }}</label>
                                    <input type="text" class="form-control" id="home_phone" name="home_phone" value="{{ (session()->has('client'))?$client->home_phone:'' }}">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="office_phone">{{ trans('clients.office_phone') }}</label>
                                    <input type="text" class="form-control" id="office_phone" name="office_phone" value="{{ (session()->has('client'))?$client->office_phone:'' }}">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="main_mobile">{{ trans('clients.main_mobile') }}</label>
                                    <input type="text" class="form-control" id="main_mobile" name="main_mobile" value="{{ (session()->has('client'))?$client->main_mobile:'' }}">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="secondary_mobile">{{ trans('clients.secondary_mobile') }}</label>
                                    <input type="text" class="form-control" id="secondary_mobile" name="secondary_mobile" value="{{ (session()->has('client'))?$client->secondary_mobile:'' }}">
                                </div>
                            </div>
                        <div class="container-btns-steps">
                            <a href="{{ route('clientCreate') }}" class="btn-prev-step">{{ trans('clients.previous') }}</a>
                            <button type="submit" class="btn-next-step">{{ trans('clients.next') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection