@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('clients.create_section') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ route('saveDataStepThree') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="page" id="page" value="">
                    <input type="hidden" name="is_multi" id="is_multi" value="">
                    <input type="hidden" name="is_multi_medium" id="is_multi_medium" value="">
                    <input type="hidden" name="has_session" id="has_session" value="{{ (session()->has('client')) ? $client->country_id : 0 }}">
                    <div id="marketing">
                        <h3 class="title-tab">{{ trans('clients.title_step_three') }}</h3>
                        <div class="form-row col-step">
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="first_contact">{{ trans('clients.contact_date') }}*</label>
                                    <input type="text" class="form-control" id="first_contact" name="first_contact" placeholder="{{ trans('clients.choose_date') }} ..." value="{{ (session()->has('client'))?$client->first_contact:'' }}">
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="sex">{{ trans('clients.sex') }}*</label>
                                    <select class="form-control" id="sex" name="sex">
                                        <option value="0" {{ (session()->has('client') && $client->gender_id == '0') ? 'selected' : '' }}>{{ trans('clients.male') }}</option>
                                        <option value="1" {{ (session()->has('client') && $client->gender_id == '1') ? 'selected' : '' }}>{{ trans('clients.female') }}</option>
                                        <option value="2" {{ (session()->has('client') && $client->gender_id == '2') ? 'selected' : '' }}>{{ trans('clients.lgbt') }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="country">{{ trans('clients.nationality') }}*</label>
                                    <select class="form-control" id="country" name="country" required>
                                        <option value="">{{ trans('clients.select_country') }} ...</option>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}" {{ (session()->has('client') && $client->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="state">{{ trans('clients.state') }}*</label>
                                    <select class="form-control" id="state" name="state" required>
                                        <option value="">{{ trans('clients.select_state') }} ...</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="city">{{ trans('clients.city') }}</label>
                                    <select class="form-control" id="city" name="city">
                                        <option value="">{{ trans('clients.select_city') }} ...</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                    <label for="street">{{ trans('clients.street') }}</label>
                                    <input type="text" class="form-control" id="street" name="street" {{ (session()->has('client'))?$client->street:'' }}>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                    <label for="number_int">{{ trans('clients.number_int') }}</label>
                                    <input type="text" class="form-control" id="number_int" name="number_int" {{ (session()->has('client'))?$client->number_int:'' }}>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                    <label for="number_ext">{{ trans('clients.number_ext') }}</label>
                                    <input type="text" class="form-control" id="number_ext" name="number_ext" {{ (session()->has('client'))?$client->number_ext:'' }}>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                    <label for="zipcode">{{ trans('clients.zipcode') }}</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" {{ (session()->has('client'))?$client->zipcode:'' }}>
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="contact_way">{{ trans('clients.title_contact_way') }}*</label>
                                    <select class="form-control" id="contact_way" name="contact_way" data-route="{{ url('admin') }}">
                                        <option value="">{{ trans('clients.choose_option') }}</option>
                                        @foreach ($contactWays as $way)
                                            <?php
                                                $currentMainSource = \App\Models\AcquisitionsMainSources::where('id', $way->main_source_id)->first();
                                            ?>
                                            <option value="{{ $way->id }}" {{ (session()->has('client') && $client->contact_way_id == $way->id) ? 'selected' : '' }} data-name-source="{{ $way->name }}" data-multi="{{ $way->is_multi }}" data-main-source="{{ $way->main_source_id }}" data-secondary-source="{{ $currentMainSource->secondary_id }}">{{ $way->name }}</option>
                                        @endforeach
                                    </select>
                                    <p class="help-block">
                                        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> {{ trans('clients.view_details') }}
                                        @foreach ($contactWays as $way)
                                            <a href="#" class="show-information" data-title="{{ $way->name }}" data-content="{{ $way->description }}">{{ $way->name }}</a>,
                                        @endforeach
                                    </p>
                                </div>
                                <div id="source_single" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="main_source" class="main_source"></label>
                                    <select class="form-control" id="main_source" name="main_source">
                                        <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                    </select>
                                </div>
                                <div id="source_multi" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="main_source" class="main_source"></label>
                                    <select class="form-control multipleSelect" multiple id="main_source_multi" name="main_source[]">
                                        <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                    </select>
                                </div>
                                <div id="source_secondary" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="secondary_source" class="secondary_source">{{ trans('clients.secondary_source') }}</label>
                                    <select class="form-control" id="secondary_source" name="secondary_source">
                                        <option value="">{{ trans('clients.select_secondary_source') }} ...</option>
                                    </select>
                                </div>
                                <!-- START MEDIUMS -->
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="contact_medium">{{ trans('clients.title_contact_medium') }}*</label>
                                    <select class="form-control" id="contact_medium" name="contact_medium" data-route="{{ url('admin') }}">
                                        <option value="">{{ trans('clients.choose_option') }}</option>
                                        @foreach ($contactMediums as $medium)
                                            <?php
                                                $currentMainMedium = \App\Models\AcquisitionsMainMediums::where('id', $medium->main_source_id)->first();
                                            ?>
                                            <option value="{{ $medium->id }}" {{ (session()->has('client') && $client->contact_medium_id == $medium->id) ? 'selected' : '' }} data-name-medium="{{ $medium->name }}" data-multi="{{ $medium->is_multi }}" data-main-medium="{{ $medium->main_source_id }}" data-secondary-medium="{{ $currentMainMedium->secondary_id }}">{{ $medium->name }}</option>
                                        @endforeach
                                    </select>
                                    <p class="help-block">
                                        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> {{ trans('clients.view_details') }}
                                        @foreach ($contactMediums as $medium)
                                            <a href="#" class="show-information" data-title="{{ $medium->name }}" data-content="{{ $medium->description }}">{{ $medium->name }}</a>,
                                        @endforeach
                                    </p>
                                </div>
                                <div id="source_single_medium" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="main_medium_source" class="main_medium_source"></label>
                                    <select class="form-control" id="main_medium_source" name="main_medium_source">
                                        <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                    </select>
                                </div>
                                <div id="source_multi_medium" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="main_medium_source" class="main_medium_source"></label>
                                    <select class="form-control multipleSelect" multiple id="main_medium_source_multi" name="main_medium_source[]">
                                        <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                    </select>
                                </div>
                                <div id="source_secondary_medium" class="form-group col-xs-12 c-padding-left-input">
                                    <label for="secondary_medium_source" class="secondary_medium_source">{{ trans('clients.secondary_source') }}</label>
                                    <select class="form-control" id="secondary_medium_source" name="secondary_medium_source">
                                        <option value="">{{ trans('clients.select_secondary_source') }} ...</option>
                                    </select>
                                </div>
                                <!-- END MEDIUMS -->
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label>{{ trans('clients.interested') }}&nbsp;&nbsp;</label>
                                    @foreach ($interests as $interest)
                                        <input type="radio" id="label_{{$interest->name}}" name="interest" value="{{ $interest->id }}" {{ (session()->has('client') && $client->interest_id == $interest->id)?'checked="checked"':'' }} required><label for="label_{{$interest->name}}">&nbsp;&nbsp;&nbsp;{{ $interest->name }}&nbsp;&nbsp;&nbsp;</label>
                                    @endforeach
                                </div>
                                <div class="form-group col-xs-12 c-padding-left-input">
                                    <label for="level_interest">{{ trans('clients.create_level_interest') }}</label>
                                    <input type="hidden" readonly id="level_interest" name="level_interest" value="{{ (session()->has('client'))?$client->level_interest_id:'2' }}" data-interest-one="{{ trans('clients.interest_one') }}" data-interest-two="{{ trans('clients.interest_two') }}" data-interest-three="{{ trans('clients.interest_three') }}" data-interest-four="{{ trans('clients.interest_four') }}">
                                    <div id="slider"></div>
                                    <br>
                                    <div id="levelInterestLabel">{{ trans('clients.interest_one') }}</div>
                                </div>
                            </div>
                        <div class="container-btns-steps">
                            <a href="{{ route('createStepTwo') }}" class="btn-prev-step">{{ trans('clients.previous') }}</a>
                            <button type="submit" class="btn-next-step">{{ trans('clients.next') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
    $(function() {
        if($('#has_session').val()){
            getState($('#country').val());
            setTimeout(function(){ getCity($('#state').val()); }, 500);
        }
        $('#country').change(function() {
            getState($(this).val());
        });

        $('#state').change(function() {
            getCity($(this).val());
        });

        $('#contact_way').change(function() {
            var nameSource = $(this).find(':selected').data('name-source');
            var mainSource = $(this).find(':selected').data('main-source');
            var route      = $(this).data('route');
            $('.main_source').html(nameSource+' - <a href="#" class="create-trigger" data-route="'+route+'" data-main-source="'+mainSource+'">{{ trans('clients.create') }} '+nameSource+'</a>');
            getMainSources($(this).val());
        });

        $('#contact_medium').change(function() {
            var nameSource = $(this).find(':selected').data('name-medium');
            var mainSource = $(this).find(':selected').data('main-medium');
            var route      = $(this).data('route');
            $('.main_medium_source').html(nameSource+' - <a href="#" class="create-trigger" data-route="'+route+'" data-main-medium="'+mainSource+'">{{ trans('clients.create') }} '+nameSource+'</a>');
            getMainMediums($(this).val());
        });

        $('#main_source').change(function() {
            var mainSourceId = $(this).find(':selected').val();
            var mainSource   = $('#contact_way').find(':selected').data('main-source');
            var route        = $('#contact_way').data('route');
            $('.secondary_source').html("{{ trans('clients.secondary_source') }} "+'- <a href="#" class="create-trigger-sub" data-route="'+route+'" data-main-source-id="'+mainSourceId+'" data-main-source="'+mainSource+'">{{ trans('clients.create_source') }}</a>');
            getSecondarySources($('#contact_way').find(':selected').data('main-source'), $(this).val());
        });

        $('#main_medium').change(function() {
            var mainSourceId = $(this).find(':selected').val();
            var mainSource   = $('#contact_medium').find(':selected').data('main-medium');
            var route        = $('#contact_medium').data('route');
            $('.secondary_source').html("{{ trans('clients.secondary_source') }} "+'- <a href="#" class="create-trigger-sub" data-route="'+route+'" data-main-medium-id="'+mainSourceId+'" data-main-medium="'+mainSource+'">{{ trans('clients.create_source') }}</a>');
            getSecondaryMediums($('#contact_medium').find(':selected').data('main-medium'), $(this).val());
        });

        function getMainSources(source){
            var url = "{{ url('admin') }}" + "/sources/main/" + source;
            var source_multi = $('#contact_way').find(':selected').data('multi');
            var has_children = $('#contact_way').find(':selected').data('secondary-source');
            $('#is_multi').val(source_multi);
            if (has_children == 1){
                $('#source_secondary').show();
            }else{
                $('#source_secondary').hide();
            }
            if (source_multi == 1){
                $('#source_multi').show();
                $('#source_single').hide();
                $('#main_source_multi').fastselect({
                    placeholder: '{{ trans('clients.select_source') }} '+$('#contact_way').find(':selected').data('name-source'),
                    noResultsText: '{{ trans('clients.select_source') }}',
                });
            }else{
                $('#source_single').show();
                $('#source_multi').hide();
            }
            $.get(url, function(data) {
                if (source_multi == 1){
                    var source = $('#main_source_multi');
                }else{
                    var source = $('#main_source');
                }
                source.empty();
                if (source_multi != 1){
                    source.append('<option value="">{{ trans('clients.select_source') }} '+$('#contact_way').find(':selected').data('name-source')+' ...</option>');
                }
                $.each(data,function(key, value) {
                    source.append("<option value=" + value.id + " data-secondary="+ value.secondary_id +">" + value.name + "</option>");
                });
            });
        }

        function getMainMediums(source){
            var url = "{{ url('admin') }}" + "/mediums/main/" + source;
            var source_multi = $('#contact_medium').find(':selected').data('multi');
            var has_children = $('#contact_medium').find(':selected').data('secondary-medium');
            $('#is_multi_medium').val(source_multi);
            if (has_children == 1){
                $('#source_secondary_medium').show();
            }else{
                $('#source_secondary_medium').hide();
            }
            if (source_multi == 1){
                $('#source_multi_medium').show();
                $('#source_single_medium').hide();
                $('#main_medium_source_multi').fastselect({
                    placeholder: '{{ trans('clients.select_source') }} '+$('#contact_medium').find(':selected').data('name-medium'),
                    noResultsText: '{{ trans('clients.select_source') }}',
                });
            }else{
                $('#source_single_medium').show();
                $('#source_multi_medium').hide();
            }
            $.get(url, function(data) {
                if (source_multi == 1){
                    var source = $('#main_medium_source_multi');
                }else{
                    var source = $('#main_medium_source');
                }
                source.empty();
                if (source_multi != 1){
                    source.append('<option value="">{{ trans('clients.select_source') }} '+$('#contact_medium').find(':selected').data('name-medium')+' ...</option>');
                }
                $.each(data,function(key, value) {
                    source.append("<option value=" + value.id + " data-secondary-medium="+ value.secondary_id +">" + value.name + "</option>");
                });
            });
        }

        function getSecondarySources(mainSource, idSource){
            var url = "{{ url('admin') }}" + "/sources/secondary/" + mainSource + "/" + idSource;
            $.get(url, function(data) {
                var source = $('#secondary_source');
                source.empty();
                source.append('<option value="">{{ trans('clients.select_secondary_source') }} ...</option>');
                $.each(data,function(key, value) {
                    source.append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        }

        function getSecondaryMediums(mainSource, idSource){
            var url = "{{ url('admin') }}" + "/mediums/secondary/" + mainSource + "/" + idSource;
            $.get(url, function(data) {
                var source = $('#secondary_medium_source');
                source.empty();
                source.append('<option value="">{{ trans('clients.select_secondary_source') }} ...</option>');
                $.each(data,function(key, value) {
                    source.append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        }

        function getState(country){
            var url = "{{ url('admin') }}" + "/country/states/" + country;
            $.get(url, function(data) {
                var select = $('#state');
                var selectCity = $('#city');
                select.empty();
                selectCity.empty();
                selectCity.append("<option value=''>{{ trans('clients.select_city') }} ...</option>");
                select.append("<option value=''>{{ trans('clients.select_state') }} ...</option>");
                $.each(data,function(key, value) {
                    select.append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        }

        function getCity(state){
            var url = "{{ url('admin') }}" + "/state/cities/" + state;
            $.get(url, function(data) {
                var city = $('#city');
                city.empty();
                city.append("<option value=''>{{ trans('clients.select_city') }} ...</option>");
                $.each(data,function(key, value) {
                    city.append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        }

    });
</script>
@endsection