@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.assign_client') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('clients.assign_client') }}: <a href="{{ route('clientView', $client->id) }}">{{ $client->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('reassignUpdate', $client->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 c-padding-left-input">
                            <label for="user_id">{{ trans('clients.agents') }}*</label>
                            <select class="form-control" id="user_id" name="user_id" required>
                                <option value="">{{ trans('clients.select_agent') }} ...</option>
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}" {{ ($client->user_id == $agent->id) ? 'selected' : '' }}>{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('clients.assign_client') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection