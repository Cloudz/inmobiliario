@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.index_title') }}
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box-title-custom">
                <h1>{{ trans('clients.index_section') }} - <i class="fa fa-th-list"></i> {{ $clients->total() }}</h1>
            </div>
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('clientCreate') }}"><i class="fa fa-plus"></i> {{ trans('clients.add_client') }}</a>
                @if(isset($_GET['search']))
                    <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-eraser"></i> {{ trans('clients.clean_search') }}</a>
                @endif
            </div>
            <form action="{{ route('clientSearch') }}" class="c-form-search" method="get">
                <div class="input-group input-group-md">
                    <input type="text" class="form-control" placeholder="{{ trans('clients.search_client') }}..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                    <span class="input-group-btn">
                        <input type="submit" class="c-btn-search btn btn-primary" value="{{ trans('global.search') }}" >
                    </span>
                </div>
            </form>
            <div class="col-xs-12">
                @if(isset($_GET['search']))
                    {{ $clients->appends(['search' => $search])->render() }}
                @else
                    {{ $clients->links() }}
                @endif
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th>{{ trans('clients.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                                <th>{{ trans('clients.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                                <th>{{ trans('clients.email') }} @sortablelink('main_email', trans('global.orderby'))</th>
                                <th>{{ trans('clients.mobile') }} @sortablelink('main_mobile', trans('global.orderby'))</th>
                                <th>{{ trans('clients.index_interest') }}</th>
                                <th>{{ trans('clients.index_level_interest') }}</th>
                                <th>{{ trans('clients.agent') }}</th>
                                <th>{{ trans('clients.status') }}</th>
                                <th>{{ trans('clients.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($clients)>0)
                                <tr>
                                    @foreach($clients as $client)
                                        @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant']))
                                            <?php
                                                $marketing = App\Models\MarketingInformation::where('client_id', $client->id)->orderBy('created_at', 'desc')->first();

                                                $interest  = App\Models\Interests::where('id', $marketing->interest_id)->first();
                                            ?>
                                        @elseif(Auth::user()->hasRole(['Personal Assistant']))
                                            <?php
                                                $marketing = App\Models\MarketingInformation::where('client_id', $client->id)->orWhere('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->first();
                                                $interest  = App\Models\Interests::where('id', $marketing->interest_id)->first();
                                            ?>
                                        @else
                                            <?php
                                                $marketing = App\Models\MarketingInformation::where('client_id', $client->id)->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->first();
                                                $interest  = App\Models\Interests::where('id', $marketing->interest_id)->first();
                                            ?>
                                        @endif
                                        <tr role="row">
                                            <td style="width: 10%">
                                                @if($client->gender_id ==0)
                                                    <i class="fa fa-venus" style="color: red"></i>
                                                @elseif($client->gender_id == 1)
                                                    <i class="fa fa-mars" style="color: blue"></i>
                                                @else
                                                    <i class="fa fa-mars-double" style="color: magenta"></i>
                                                @endif
                                                {{ $client->name }}
                                            </td>
                                            <td style="width: 10%">{{ $client->last_name }}</td>
                                            <td style="width: 10%">{{ $client->main_email }}</td>
                                            <td style="width: 10%">{{ $client->main_mobile }}</td>
                                            <td style="width: 10%">
                                                @if(isset($interest->name))
                                                    {{ $interest->name }}
                                                @endif
                                            </td>
                                            <td style="width: 10%">
                                                @for ($i = 0; $i < 4; ++$i)
                                                    <i class="fa fa-star{{ $marketing->level_interest_id<=$i?'-o':'' }}" aria-hidden="true"></i>
                                                @endfor
                                            </td>
                                            @if(Auth::user()->hasRole(['Administrator','Developer', 'Broker', 'Office Manager', 'General Assistant']))
                                                <td style="width: 10%"><a href="{{ route('userView', $client->user_id) }}">{{ $client->user->name }}</a></td>
                                            @else
                                                <td style="width: 10%">{{ $client->user->name }}</td>
                                            @endif
                                            <td style="width: 10%">
                                                @if($client->status)
                                                    <div class="btn edit_btn btn-xs" title="Completo" alt="Completo">
                                                        {{ trans('clients.complete') }}
                                                    </div>
                                                @else
                                                    <div class="btn delete_btn btn-xs bg-maroon" title="Completo" alt="Completo">
                                                        {{ trans('clients.incomplete') }}
                                                    </div>
                                                @endif
                                            </td>
                                            <td style="width: 10%">
                                                @role(['Administrator', 'Developer', 'Broker', 'Referror'])
                                                <a href="{{ route('clientMail', $client->id) }}" class="btn edit_btn btn-xs bg-aqua" title="Mensaje" alt="Mensaje">
                                                    <i class="fa fa-envelope"></i>
                                                </a>
                                                @endrole
                                                <a href="{{ route('clientView', $client->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="{{ route('clientDelete', $client->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                    <i class="fa fa-fw fa-remove"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tr>
                            @else
                                <tr>
                                    <tr role="row">
                                        <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                    </tr>
                                </tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>{{ trans('clients.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                                <th>{{ trans('clients.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                                <th>{{ trans('clients.email') }} @sortablelink('main_email', trans('global.orderby'))</th>
                                <th>{{ trans('clients.mobile') }} @sortablelink('main_mobile', trans('global.orderby'))</th>
                                <th>{{ trans('clients.index_interest') }}</th>
                                <th>{{ trans('clients.index_level_interest') }}</th>
                                <th>{{ trans('clients.agent') }}</th>
                                <th>{{ trans('clients.status') }}</th>
                                <th>{{ trans('clients.actions') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
            @if(isset($_GET['search']))
                {{ $clients->appends(['search' => $search])->render() }}
            @else
                {{ $clients->links() }}
            @endif
        </div>
        <!-- /.row -->
    </section>
@endsection
