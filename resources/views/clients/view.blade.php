@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('clients.view_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('clients') }}"><i class="fa fa-undo"></i> {{ trans('clients.back_list') }}</a>
            <a id="btn_edit_form_client" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('clients.enable_edition') }}</a>
            @role(['Administrator','Developer', 'Broker', 'Office Manager'])
                <a class="btn edit_btn_large btn-xs" href="{{ route('reassign', $client->id) }}"><i class="fa fa-refresh"></i> {{ trans('clients.assign_client') }}</a>
            @endrole
            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant', 'Personal Assistant'])
                <a href="{{ route('reminderCreate', ['Call', $client->id]) }}" class="btn reminder_btn"><i class="fa fa-phone"></i> {{ trans('clients.call') }} - <i class="fa fa-clock-o"></i></a>
                <a href="{{ route('reminderCreate', ['Mail', $client->id]) }}" class="btn reminder_btn"><i class="fa fa-envelope"></i> {{ trans('clients.mail') }} - <i class="fa fa-clock-o"></i></a>
                <a href="{{ route('reminderCreate', ['Showing', $client->id]) }}" class="btn reminder_btn"><i class="fa fa-home"></i> {{ trans('clients.showing') }} - <i class="fa fa-clock-o"></i></a>
            @endrole
            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant'])
                <a class="btn action_btn btn-xs" href="{{ route('responseClientMail', $client->id) }}"><i class="fa fa-envelope"></i> {{ trans('clients.send') }}</a>
            @endrole
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ trans('clients.view_section') }} {{ $client->name }} @if($client->status)<span class="btn edit_btn btn-xs">{{ trans('clients.complete') }}</span>@else<span class="btn delete_btn btn-xs bg-maroon">{{ trans('clients.incomplete') }}</span>@endif - {{ trans('clients.key_section') }} {{ $client->client_key }} - {{ trans('clients.agent') }}: <a href="{{ route('userView', $client->user_id) }}">{{ $client->user->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="form-row column-form col-xs-12 col-lg-6" style="margin-bottom: 50px; padding: 10px">
                  <h3 class="title-tab">{{ trans('clients.followups') }} - {{ $followups->total() }}</h3>
                  <table class="table table-hover" style="width: 100%">
                      <thead>
                      <tr>
                          <th>STATUS @sortablelink('status', trans('global.orderby'))</th>
                          <th>FOLIO @sortablelink('folio', trans('global.orderby'))</th>
                          <th>TIPO @sortablelink('type', trans('global.orderby'))</th>
                          <th>CLIENTE</th>
                          <th>AGENTE</th>
                          <th>COLABORADORES</th>
                          @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                          <th>ACCIONES</th>
                          @endrole
                      </tr>
                      </thead>
                      <tbody>
                      @if(count($followups)>0)
                          <tr role="row">
                          @foreach($followups as $followup)
                              <?php
                              switch ($followup->status){
                                  case 1:
                                      $status    = 'Abierta';
                                      $bg_status = 'bg-aqua';
                                      break;
                                  case 2:
                                      $status    = 'Seguimiento';
                                      $bg_status = 'bg-yellow';
                                      break;
                                  case 3:
                                      $status    = 'Cerrada';
                                      $bg_status = 'bg-maroon';
                                      break;
                                  default:
                                      $status    = 'Abierta';
                                      $bg_status = 'bg-aqua';
                                      break;
                              }
                              ?>
                                  <td style="width: 5%"><div class="legend-status {{ $bg_status }}"> {{ $status }}</div></td>
                                  <td style="width: 10%">{{ $followup->folio }}</td>
                                  <td style="width: 10%">{{ $followup->type }}</td>
                                  <td style="width: 10%">{{ $followup->client->name }}</td>
                                  <td style="width: 10%">
                                      @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker']))
                                          <a href="{{ route('userView', $followup->user->id) }}">{{ $followup->user->name }}</a><br>
                                      @else
                                          {{ $followup->user->name }}<br>
                                      @endif
                                  </td>
                                  <td style="width: 10%">
                                      @foreach($followup->users as $collaborator)
                                          @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker']))
                                              <a href="{{ route('userView', $collaborator->id) }}">{{ $collaborator->name }}</a><br>
                                          @else
                                              {{ $collaborator->name }}<br>
                                          @endif
                                      @endforeach
                                  </td>
                                  @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                                  <td style="width: 30%">
                                      @if($followup->status != 1)
                                          <a href="{{ route('followupStatusOpen', [$followup->id, 'clientView', $client->id]) }}" class="change_status btn edit_btn btn-xs bg-aqua" title="Abrir" alt="Abrir">
                                              <i class="fa fa-unlock"></i> Abrir
                                          </a>
                                      @endif
                                      @if($followup->status != 2)
                                          <a href="{{ route('followupStatusProcess', [$followup->id, 'clientView', $client->id]) }}" class="change_status btn delete_btn btn-xs bg-yellow" title="Seguir" alt="Seguir">
                                              <i class="fa fa-paper-plane"></i> Seguir
                                          </a>
                                      @endif
                                      @if($followup->status != 3)
                                          <a href="{{ route('followupStatusClose', [$followup->id, 'clientView', $client->id]) }}" class="change_status btn delete_btn btn-xs bg-maroon" title="Cerrar" alt="Cerrar">
                                              <i class="fa fa-lock"></i> Cerrar
                                          </a>
                                      @endif
                                      <a href="{{ route('followupShare', [$followup->user_id, $followup->id])}}" type="button" class="btn edit_btn btn-xs" title="Compartir" alt="Compartir">
                                          <i class="fa fa-share"></i>
                                      </a>
                                      <a href="{{ route('followupDelete', $followup->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                          <i class="fa fa-fw fa-remove"></i>
                                      </a>
                                  </td>
                                  @endrole
                              </tr>
                              @endforeach
                              @else
                              <tr role="row">
                                  <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                              </tr>
                          @endif
                      </tbody>
                  </table>
              </div>
            <div class="form-row column-form col-xs-12 col-lg-6" style="margin-bottom: 50px; padding: 10px">
                  <h3 class="title-tab">{{ trans('dashboard.title_messages') }} - {{ $conversations->total() }}</h3>
              @if($conversations->count() > 0)
                  @foreach($conversations as $message)
                      @php
                          $htmlBody = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$message->htmlBody);
                          $textBody = preg_replace("~<blockquote(.*?)>(.*)</blockquote>~si","",$message->textBody);
                      @endphp
                      <!-- chat item -->
                          <div class="item">
                              <div class="container-conversation-client">
                                  <div class="container-subject-msg">
                                      <div class="title-msg">
                                          <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ $message->subject }}
                                      </div>
                                      <div class="date-btns-msg">
                                          <div class="container-date-msg">
                                              <i class="fa fa-clock-o"></i> {{date('d/m/y H:i:s', strtotime($message->created_at))}}
                                          </div>
                                          <div class="container-btns-msg">
                                              <a href="#" type="button" class="show-body-msg btn edit_btn btn-xs" style="background-color: #f7f7f7; color:#1f2d33; border: 1px #1f2d33 solid;" title="{{ trans('global.show') }}" alt="{{ trans('global.show') }}">
                                                  <i class="fa fa-eye"></i>
                                              </a>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="container-body-msg">
                                      @if($message->htmlBody)
                                          {!! $htmlBody !!}
                                      @else
                                          {{ $textBody }}
                                      @endif
                                      @if(count($message->attachments)>0)
                                          @php
                                              $attachments = \GlobalHelpers::unserialize($message->attachments);
                                              $supported_image = array(
                                                  'gif',
                                                  'jpg',
                                                  'jpeg',
                                                  'png',
                                                  'bmp'
                                              );
                                          @endphp
                                          <div class="attachment">
                                              <div class="title-attachments">
                                                  {{ trans('dashboard.title_attachments') }}
                                              </div>
                                              @foreach($attachments as $file)
                                                  @php
                                                      $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                                  @endphp
                                                  @if(in_array($ext, $supported_image))
                                                      <a href="{{ '/storage/mails/'.$file }}" download>
                                                          <div class='btn-download-file'>
                                                              <i class='fa fa-picture-o'></i>&nbsp;&nbsp;&nbsp;<strong>{{ trans('dashboard.download_image') }}{{ '.'.$ext }}</strong>
                                                          </div>
                                                      </a>
                                                  @else
                                                      <a href="{{ '/storage/mails/'.$file }}" download>
                                                          <div class='btn-download-file'>
                                                              <i class='fa fa-file-o'></i>&nbsp;&nbsp;&nbsp;<strong>{{ trans('dashboard.download_file') }}{{ '.'.$ext }}</strong>
                                                          </div>
                                                      </a>
                                                  @endif
                                              @endforeach
                                          </div>
                                  @endif
                                  <!-- /.attachment -->
                                  </div>
                              </div>
                          </div>
                          <!-- /.item -->
                      @endforeach
                  @else
                      <div>{{ trans('global.not_found') }}</div>
              @endif
              {{ $conversations->links() }}
              <!-- /.box-body -->
              </div>
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('clientUpdate', $client->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="user_id" id="user_id" value="{{ $client->id }}">
                    <input type="hidden" name="page" id="page" value="Edit">
                    <input type="hidden" name="client_edit" id="client_edit" value="0">
                    <input type="hidden" name="client_complete" id="client_complete" value="{{ $client->status }}">
                    <input type="hidden" name="is_multi" id="is_multi" value="">
                    <input type="hidden" name="is_multi_medium" id="is_multi_medium" value="">
                    <input type="hidden" name="has_secondary_source" id="has_secondary_source" value="{{($secondarySource)?1:0}}">
                    <input type="hidden" name="has_secondary_medium_source" id="has_secondary_medium_source" value="{{($secondaryMedium)?1:0}}">
                    <div class="form-row column-form col-xs-12">
                        <h3 class="title-tab" style="margin-bottom: 50px;">{{ trans('clients.view_title_one') }}</h3>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-12 col-lg-4">
                            <label for="name">{{ trans('clients.create_name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $client->name }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="last_name">{{ trans('clients.create_last_name') }}*</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $client->last_name }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input" style="margin-bottom: 50px;">
                            <label for="main_email">{{ trans('clients.main_email') }}*</label>
                            <input type="email" class="form-control" id="main_email" name="main_email" value="{{ $client->main_email }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12" style="margin-bottom: 50px;">
                        <h3 class="title-tab">{{ trans('clients.title_step_two') }} <small style="color: #bbb;">{{ trans('clients.optional') }}</small></h3>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="secondary_email">{{ trans('clients.secondary_email') }}</label>
                            <input type="email" class="form-control" id="secondary_email" name="secondary_email" value="{{ $client->secondary_email }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4">
                            <label for="home_phone">{{ trans('clients.home_phone') }}</label>
                            <input type="text" class="form-control" id="home_phone" name="home_phone" value="{{ $client->home_office }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="office_phone">{{ trans('clients.office_phone') }}</label>
                            <input type="text" class="form-control" id="office_phone" name="office_phone" value="{{ $client->office_phone }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="main_mobile">{{ trans('clients.main_mobile') }}</label>
                            <input type="text" class="form-control" id="main_mobile" name="main_mobile" value="{{ $client->main_mobile }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input" style="margin-bottom: 50px;">
                            <label for="secondary_mobile">{{ trans('clients.secondary_mobile') }}</label>
                            <input type="text" class="form-control" id="secondary_mobile" name="secondary_mobile" value="{{ $client->secondary_mobile }}" readonly>
                        </div>
                    </div>
                    <div class="container-edit-marketing">
                        <div class="form-row column-form col-xs-12" style="margin-bottom: 50px;">
                            <h3 class="title-tab">{{ trans('clients.edit_information') }}{{ trans('clients.interested') }}</h3>
                        </div>
                        <div class="form-row column-form">
                            <div class="form-group col-xs-12 col-md-12 col-lg-4">
                                <label for="first_contact">{{ trans('clients.contact_date') }}</label>
                                <input type="text" class="form-control marketing" id="first_contact" name="first_contact" placeholder="Escoger Fecha ..." value="">
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                <label for="sex">{{ trans('clients.sex') }}</label>
                                <select class="form-control marketing" id="sex" name="sex">
                                    <option value="">{{ trans('clients.choose_gender') }} ...</option>
                                    <option value="0">{{ trans('clients.male') }}</option>
                                    <option value="1">{{ trans('clients.female') }}</option>
                                    <option value="2">{{ trans('clients.lgbt') }}</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                <label for="country">{{ trans('clients.nationality') }}</label>
                                <select class="form-control marketing" id="country" name="country">
                                    <option value="">{{ trans('clients.select_country') }} ...</option>
                                    @foreach ($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                <label for="state">{{ trans('clients.state') }}</label>
                                <select class="form-control marketing" id="state" name="state">
                                    <option value="">{{ trans('clients.select_state') }} ...</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                <label for="city">{{ trans('clients.city') }}</label>
                                <select class="form-control" id="city" name="city">
                                    <option value="">{{ trans('clients.select_city') }} ...</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3">
                                <label for="street">{{ trans('clients.street') }}</label>
                                <input type="text" class="form-control marketing" id="street" name="street" value="">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="number_int">{{ trans('clients.number_int') }}</label>
                                <input type="text" class="form-control marketing" id="number_int" name="number_int" value="">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="number_ext">{{ trans('clients.number_ext') }}</label>
                                <input type="text" class="form-control marketing" id="number_ext" name="number_ext" value="">
                            </div>
                            <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                                <label for="zipcode">{{ trans('clients.zipcode') }}</label>
                                <input type="text" class="form-control marketing" id="zipcode" name="zipcode" value="">
                            </div>
                            <div class="form-group col-xs-12">
                                <label for="contact_way">{{ trans('clients.title_contact_way') }}</label>
                                <select class="form-control marketing" id="contact_way" name="contact_way" data-route="{{ url('admin') }}">
                                    <option value="">{{ trans('clients.choose_option') }}</option>
                                    @foreach ($contactWays as $way)
                                        <?php
                                        $currentMainSource = \App\Models\AcquisitionsMainSources::where('id', $way->main_source_id)->first();
                                        ?>
                                        <option value="{{ $way->id }}" data-name-source="{{ $way->name }}" data-multi="{{ $way->is_multi }}" data-main-source="{{ $way->main_source_id }}" data-secondary-source="{{ $currentMainSource->secondary_id }}">{{ $way->name }}</option>
                                    @endforeach
                                </select>
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> {{ trans('clients.view_details') }}
                                    @foreach ($contactWays as $way)
                                        <a href="#" class="show-information" data-title="{{ $way->name }}" data-content="{{ $way->description }}">{{ $way->name }}</a>,
                                    @endforeach
                                </p>
                            </div>
                            <div id="source_single" class="form-group col-xs-12">
                                <label for="main_source" class="main_source"></label>
                                <select class="form-control" id="main_source" name="main_source">
                                    <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                </select>
                            </div>
                            <div id="source_multi" class="form-group col-xs-12">
                                <label for="main_source" class="main_source"></label>
                                <select class="form-control multipleSelect" multiple id="main_source_multi" name="main_source[]">
                                    <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                </select>
                            </div>
                            <div id="source_secondary" class="form-group col-xs-12">
                                <label for="secondary_source" class="secondary_source">{{ trans('clients.secondary_source') }}</label>
                                <select class="form-control" id="secondary_source" name="secondary_source">
                                    <option value="">{{ trans('clients.select_secondary_source') }} ...</option>
                                </select>
                            </div>
                            <!-- START MEDIUMS -->
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="contact_medium">{{ trans('clients.title_contact_medium') }}</label>
                                <select class="form-control marketing" id="contact_medium" name="contact_medium" data-route="{{ url('admin') }}">
                                    <option value="">{{ trans('clients.choose_option') }}</option>
                                    @foreach ($contactMediums as $medium)
                                        <?php
                                        $currentMainMedium = \App\Models\AcquisitionsMainMediums::where('id', $medium->main_source_id)->first();
                                        ?>
                                        <option value="{{ $medium->id }}" {{ (session()->has('client') && $client->contact_medium_id == $medium->id) ? 'selected' : '' }} data-name-medium="{{ $medium->name }}" data-multi="{{ $medium->is_multi }}" data-main-medium="{{ $medium->main_source_id }}" data-secondary-medium="{{ $currentMainMedium->secondary_id }}">{{ $medium->name }}</option>
                                    @endforeach
                                </select>
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> {{ trans('clients.view_details') }}
                                    @foreach ($contactMediums as $medium)
                                        <a href="#" class="show-information" data-title="{{ $medium->name }}" data-content="{{ $medium->description }}">{{ $medium->name }}</a>,
                                    @endforeach
                                </p>
                            </div>
                            <div id="source_single_medium" class="form-group col-xs-12 c-padding-left-input">
                                <label for="main_medium_source" class="main_medium_source"></label>
                                <select class="form-control" id="main_medium_source" name="main_medium_source">
                                    <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                </select>
                            </div>
                            <div id="source_multi_medium" class="form-group col-xs-12 c-padding-left-input">
                                <label for="main_medium_source" class="main_medium_source"></label>
                                <select class="form-control multipleSelect" multiple id="main_medium_source_multi" name="main_medium_source[]">
                                    <option value="">{{ trans('clients.select_main_source') }} ...</option>
                                </select>
                            </div>
                            <div id="source_secondary_medium" class="form-group col-xs-12 c-padding-left-input">
                                <label for="secondary_medium_source" class="secondary_medium_source">{{ trans('clients.secondary_source') }}</label>
                                <select class="form-control" id="secondary_medium_source" name="secondary_medium_source">
                                    <option value="">{{ trans('clients.select_secondary_source') }} ...</option>
                                </select>
                            </div>
                            <!-- END MEDIUMS -->
                            <div class="form-group col-xs-12">
                                <label>{{ trans('clients.interested') }}&nbsp;&nbsp;</label>
                                @foreach ($interests as $interest)
                                    <input class="marketing" type="radio" id="label_{{$interest->name}}" name="interest" value="{{ $interest->id }}"><label for="label_{{$interest->name}}">&nbsp;&nbsp;&nbsp;{{ $interest->name }}&nbsp;&nbsp;&nbsp;</label>
                                @endforeach
                            </div>
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="level_interest">{{ trans('clients.create_level_interest') }}</label>
                                <input class="marketing" type="hidden" readonly id="level_interest" name="level_interest" value="" data-interest-one="{{ trans('clients.interest_one') }}" data-interest-two="{{ trans('clients.interest_two') }}" data-interest-three="{{ trans('clients.interest_three') }}" data-interest-four="{{ trans('clients.interest_four') }}">
                                <div id="slider"></div>
                                <br>
                                <div id="levelInterestLabel">{{ trans('clients.interest_one') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12" style="margin-bottom: 50px;">
                        <h3 class="title-tab">{{ trans('clients.interested') }}</h3>
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('clients.contact_date') }}</th>
                                        <th>{{ trans('clients.sex') }}</th>
                                        <th>{{ trans('clients.nationality') }}</th>
                                        <th>{{ trans('clients.address') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row">
                                        <td style="width: 10%">
                                            {{ $client->first_contact }}
                                        </td>
                                        <td style="width: 10%">
                                            @if($client->gender_id == '0')
                                                {{ trans('clients.male') }}
                                            @elseif($client->gender_id == '1')
                                                {{ trans('clients.female') }}
                                            @else
                                                {{ trans('clients.lgbt') }}
                                            @endif
                                        </td>
                                        <td style="width: 10%">
                                            {{ $client->countryName->name }}
                                            {{ ($client->stateName)?', '.$client->stateName->name:'' }}
                                            {{ ($client->cityName)?', '.$client->cityName->name:'' }}
                                        </td>
                                        <td style="width: 10%">
                                            {{ $client->street }}
                                            {{ ($client->number_int)?', '.$client->number_int:'' }}
                                            {{ ($client->number_ext)?', '.$client->number_ext:'' }}
                                            {{ ($client->zipcode)?', '.$client->zipcode:'' }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-hover" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('clients.contact_way') }}</th>
                                        <th>{{ trans('clients.main_source') }}</th>
                                        @if(!is_null($secondarySource) && ($secondarySource != 0))
                                            <th>{{ trans('clients.view_secondary_source') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row">
                                        <td style="width: 10%">
                                            @if(isset($marketing->getContactWay))
                                                {{ $marketing->getContactWay->name }}
                                            @endif
                                        </td>
                                        <td style="width: 10%">
                                            @if(!is_null($mainSource))
                                                @if($marketing->getContactWay->is_multi)
                                                    @if(!is_null($multiSources))
                                                        @foreach ($multiSources as $multiSource)
                                                            {{ $multiSource->name }},
                                                        @endforeach
                                                    @endif
                                                @else
                                                    {{ $mainSource->name }}
                                                @endif
                                            @endif
                                        </td>
                                        @if(!is_null($secondarySource) && ($secondarySource != 0))
                                            <td style="width: 10%">
                                                {{ $secondarySource->name }}
                                            </td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-hover" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('clients.contact_medium') }}</th>
                                        <th>{{ trans('clients.main_medium') }}</th>
                                        @if(!is_null($secondaryMedium) && ($secondaryMedium != 0))
                                            <th>{{ trans('clients.view_secondary_medium') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row">
                                        <td style="width: 10%">
                                            @if(isset($marketing->getContactMedium))
                                                {{ $marketing->getContactMedium->name }}
                                            @endif
                                        </td>
                                        <td style="width: 10%">
                                            @if(!is_null($mainMedium))
                                                @if($marketing->getContactMedium->is_multi)
                                                    @if(!is_null($multiMediums))
                                                        @foreach ($multiMediums as $multiMedium)
                                                            {{ $multiMedium->name }},
                                                        @endforeach
                                                    @endif
                                                @else
                                                    {{ $mainMedium->name }}
                                                @endif
                                            @endif
                                        </td>
                                        @if(!is_null($secondaryMedium) && ($secondaryMedium != 0))
                                            <td style="width: 10%">
                                                {{ $secondaryMedium->name }}
                                            </td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-hover" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('clients.view_interest') }}</th>
                                        <th>{{ trans('clients.create_level_interest') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row">
                                        <td style="width: 10%">
                                            @if(isset($marketing->getInterest))
                                                {{ $marketing->getInterest->name }}
                                            @endif
                                        </td>
                                        <td style="width: 10%">
                                            <?php
                                            switch($marketing->level_interest_id){
                                                case 1:
                                                    $status = trans('clients.interest_one');
                                                    break;
                                                case 2:
                                                    $status = trans('clients.interest_two');
                                                    break;
                                                case 3:
                                                    $status = trans('clients.interest_three');
                                                    break;
                                                case 4:
                                                    $status = trans('clients.interest_four');
                                                    break;
                                            }
                                            ?>
                                            @for ($i = 0; $i < 4; ++$i)
                                                <i class="fa fa-star{{ $marketing->level_interest_id<=$i?'-o':'' }}" aria-hidden="true"></i>
                                            @endfor
                                            {{ $status }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12" style="margin-bottom: 50px;">
                        <h3 class="title-tab">{{ trans('clients.comments') }}</h3>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('clients.make_comment') }}</label>
                            <textarea rows="12" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('clients.comments') }}</h3>
                            <div class="container_list">
                                @foreach ($client->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-user"></i> {{ $comment->user->name }} - <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="clientView" data-source="{{ $client->id }}" class="edit-trigger btn edit_btn btn-xs" title="{{ trans('global.edit') }}" alt="{{ trans('global.edit') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('clients.send_information') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $(function() {
            $('#country').change(function() {
                getState($(this).val());
            });

            $('#state').change(function() {
                getCity($(this).val());
            });

            $('#contact_way').change(function() {
                var nameSource = $(this).find(':selected').data('name-source');
                var mainSource = $(this).find(':selected').data('main-source');
                var route      = $(this).data('route');
                $('.main_source').html(nameSource+' - <a href="#" class="create-trigger" data-route="'+route+'" data-main-source="'+mainSource+'">{{ trans('clients.create') }} '+nameSource+'</a>');
                getMainSources($(this).val());
            });

            $('#contact_medium').change(function() {
                var nameSource = $(this).find(':selected').data('name-medium');
                var mainSource = $(this).find(':selected').data('main-medium');
                var route      = $(this).data('route');
                $('.main_medium_source').html(nameSource+' - <a href="#" class="create-trigger" data-route="'+route+'" data-main-medium="'+mainSource+'">{{ trans('clients.create') }} '+nameSource+'</a>');
                getMainMediums($(this).val());
            });

            $('#main_source').change(function() {
                var mainSourceId = $(this).find(':selected').val();
                var mainSource   = $('#contact_way').find(':selected').data('main-source');
                var route        = $('#contact_way').data('route');
                $('.secondary_source').html("Recurso Secundario "+'- <a href="#" class="create-trigger-sub" data-route="'+route+'" data-main-source-id="'+mainSourceId+'" data-main-source="'+mainSource+'">{{ trans('clients.create_source') }}</a>');
                getSecondarySources($('#contact_way').find(':selected').data('main-source'), $(this).val());
            });

            $('#main_medium').change(function() {
                var mainSourceId = $(this).find(':selected').val();
                var mainSource   = $('#contact_medium').find(':selected').data('main-medium');
                var route        = $('#contact_medium').data('route');
                $('.secondary_source').html("{{ trans('clients.secondary_source') }} "+'- <a href="#" class="create-trigger-sub" data-route="'+route+'" data-main-medium-id="'+mainSourceId+'" data-main-medium="'+mainSource+'">{{ trans('clients.create_source') }}</a>');
                getSecondaryMediums($('#contact_medium').find(':selected').data('main-medium'), $(this).val());
            });

            function getMainSources(source){
                var url = "{{ url('admin') }}" + "/sources/main/" + source;
                var source_multi = $('#contact_way').find(':selected').data('multi');
                var has_children = $('#contact_way').find(':selected').data('secondary-source');
                $('#is_multi').val(source_multi);
                if (has_children == 1){
                    $('#source_secondary').show().find('select').attr('required', 'required');
                    $('#has_secondary_source').val(1);
                }else{
                    $('#source_secondary').hide().find('select').removeAttr('required');
                    $('#has_secondary_source').val(0);
                }
                if (source_multi == 1){
                    $('#source_multi').show().find('select').attr('required', 'required');
                    $('#source_single').hide().find('select').removeAttr('required');
                    $('#main_source_multi').fastselect({
                        placeholder: '{{ trans('clients.select_source') }} '+$('#contact_way').find(':selected').data('name-source'),
                        noResultsText: '{{ trans('clients.select_source') }}',
                    });
                }else{
                    $('#source_single').show().find('select').attr('required', 'required');
                    $('#source_multi').hide().find('select').removeAttr('required');
                }
                $.get(url, function(data) {
                    if (source_multi == 1){
                        var source = $('#main_source_multi');
                    }else{
                        var source = $('#main_source');
                    }
                    source.empty();
                    if (source_multi != 1){
                        source.append('<option value="">{{ trans('clients.select_source') }} '+$('#contact_way').find(':selected').data('name-source')+' ...</option>');
                    }
                    $.each(data,function(key, value) {
                        source.append("<option value=" + value.id + " data-secondary="+ value.secondary_id +">" + value.name + "</option>");
                    });
                });
            }

            function getMainMediums(source){
                var url = "{{ url('admin') }}" + "/mediums/main/" + source;
                var source_multi = $('#contact_medium').find(':selected').data('multi');
                var has_children = $('#contact_medium').find(':selected').data('secondary-medium');
                $('#is_multi_medium').val(source_multi);
                if (has_children == 1){
                    $('#source_secondary_medium').show().find('select').attr('required', 'required');
                }else{
                    $('#source_secondary_medium').hide().find('select').removeAttr('required');
                }
                if (source_multi == 1){
                    $('#source_multi_medium').show().find('select').attr('required', 'required');
                    $('#source_single_medium').hide().find('select').removeAttr('required');
                    $('#main_medium_source_multi').fastselect({
                        placeholder: '{{ trans('clients.select_source') }} '+$('#contact_medium').find(':selected').data('name-medium'),
                        noResultsText: '{{ trans('clients.select_source') }}',
                    });
                }else{
                    $('#source_single_medium').show().find('select').attr('required', 'required');
                    $('#source_multi_medium').hide().find('select').removeAttr('required');
                }
                $.get(url, function(data) {
                    if (source_multi == 1){
                        var source = $('#main_medium_source_multi');
                    }else{
                        var source = $('#main_medium_source');
                    }
                    source.empty();
                    if (source_multi != 1){
                        source.append('<option value="">{{ trans('clients.select_source') }} '+$('#contact_medium').find(':selected').data('name-medium')+' ...</option>');
                    }
                    $.each(data,function(key, value) {
                        source.append("<option value=" + value.id + " data-secondary-medium="+ value.secondary_id +">" + value.name + "</option>");
                    });
                });
            }

            function getSecondarySources(mainSource, idSource){
                var url = "{{ url('admin') }}" + "/sources/secondary/" + mainSource + "/" + idSource;
                $.get(url, function(data) {
                    var source = $('#secondary_source');
                    source.empty();
                    source.append('<option value="">{{ trans('clients.select_secondary_source') }} ...</option>');
                    $.each(data,function(key, value) {
                        source.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }

            function getSecondaryMediums(mainSource, idSource){
                var url = "{{ url('admin') }}" + "/mediums/secondary/" + mainSource + "/" + idSource;
                $.get(url, function(data) {
                    var source = $('#secondary_medium_source');
                    source.empty();
                    source.append('<option value="">{{ trans('clients.select_secondary_source') }} ...</option>');
                    $.each(data,function(key, value) {
                        source.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }

            function getState(country){
                var url = "{{ url('admin') }}" + "/country/states/" + country;
                $.get(url, function(data) {
                    var select = $('#state');
                    var selectCity = $('#city');
                    select.empty();
                    selectCity.empty();
                    selectCity.append('<option value="">{{ trans('clients.select_city') }} ...</option>');
                    $.each(data,function(key, value) {
                        select.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }

            function getCity(state){
                var url = "{{ url('admin') }}" + "/state/cities/" + state;
                $.get(url, function(data) {
                    var city = $('#city');
                    city.empty();
                    $.each(data,function(key, value) {
                        city.append("<option value=" + value.id + ">" + value.name + "</option>");
                    });
                });
            }

            $('.change_status').on('click', function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: {},
                    beforeSend: function() {
                        $('#loader').fadeIn();
                    },
                    success: function(res){
                        if(res.success){
                            if(res.redirect){
                                document.location = res.redirect;
                            }
                        }else if(res.errors){
                            var errElem = $('<ul></ul>');

                            for(var name in res.errors){
                                var errors = res.errors[name];
                                //errElem.append($('<li>' + name + ': ' + res.errors[name][0] + '</li>'));
                                errElem.append($('<li>' + res.errors[name][0] + '</li>'));
                            }
                            var errorTemplate = '<div class="alert alert-danger"><ul>' + errElem.html() + '</ul></div>';
                            $.bootstrapGrowl(errorTemplate, {
                                ele: 'body',
                                type: 'null',
                                offset: {from: 'top', amount: 40},
                                align: 'right',
                                width: 'auto',
                                delay: 3000,
                                allow_dismiss: true,
                                stackup_spacing: 5
                            });
                        }
                        proccessing_form = false;
                    },
                    error: function (data) {
                        $.bootstrapGrowl('Ha ocurrido un error, revise su conexion e intentelo nuevamente', {
                            ele: 'body',
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 40},
                            align: 'right',
                            width: 'auto',
                            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: false,
                            stackup_spacing: 5
                        });
                        proccessing_form = false;
                        $('#loader').remove();
                    },
                    complete: function() {
                        $('#loader').fadeOut();
                    },
                });
            });

        });
    </script>
@endsection