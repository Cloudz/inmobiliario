@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Recordatorio de Email
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('reminders', 'Mail') }}"><i class="fa fa-undo"></i> Volver a la Agenda de Emails</a>
            <a class="btn action_btn btn-xs" href="{{ route('clientView', $client->id) }}"><i class="fa fa-undo"></i> Volver al Cliente</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">CREAR RECORDATORIO DE EMAIL - CLIENTE: <a href="{{ route('clientView', $client->id) }}">{{ $client->name.' '.$client->last_name }}</a> - AGENTE: <a href="{{ route('userView', $client->user_id) }}">{{ $client->user->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('reminderStore', $client->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="client_id" value="{{ $client->id }}">
                    <input type="hidden" name="reminder_type" value="Mail">
                    <input type="hidden" name="agent_id" value="{{ $client->user_id }}">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="subject">Asunto*</label>
                            <input type="text" class="form-control" id="subject" name="subject" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="description">Descripción*</label>
                            <textarea rows="6" class="form-control" id="description" name="description" required="required"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="date_start">Fecha de Inicio*</label>
                            <input type="text" class="form-control" id="date_start" name="date_start" placeholder="Escoger Fecha ...">
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="interval">Intervalo de Recordatorio en Horas</label>
                            <select class="form-control" id="interval" name="interval" required="required">
                                @for ($i=1; $i <= 24; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <input type="hidden" id="started_on" name="started_on" required="required">
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Agregar Recordatorio</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
  <script>
    $("#date_start").on("dp.change", function(e) {
        selectedDate = moment($(this).val(),"DD/MM/YYYY LT");
        tzoffset = (new Date()).getTimezoneOffset() * 60000;
        localISOTime = (new Date(selectedDate - tzoffset)).toISOString().slice(0, -1);
        $('#started_on').val(localISOTime);
    });
  </script>
@endsection