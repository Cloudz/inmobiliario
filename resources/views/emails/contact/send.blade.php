<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Directorio HoySí</title>
</head>
<body>
    <div style="padding-top: 30px;padding-right: 20%;padding-left: 20%;">
        <div style="width: 100%; margin: 0 auto; text-align: center; margin-bottom: 20px;">
            <img src="http://portico.vlmg.mx/img/logoHoySi.png" width="300px">
        </div>
        <?php if($name != '') { ?>
            <div style="padding: 10px 10px 20px;">
                Apreciable {{ $name }}, Gracias por contactarnos. Nos pondremos en contacto con usted lo más pronto posible.
            </div>
        <?php } ?>
        <?php if($name != '') { ?>
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    Nombre:
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    {{ $name }}
                </div>
            </div>
        <?php } ?>
        <?php if($email != '') { ?>
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    Email:
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    {{ $email }}
                </div>
            </div>
        <?php } ?>
        <?php if($phone != '') { ?>
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    Teléfono:
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    {{ $phone }}
                </div>
            </div>
        <?php } ?>
        <?php if($message != '') { ?>
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 1400;">
                    <div><b>Comentarios:</b></div>
                </div>
            </div>
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 1400;">
                    <div>{{ ucfirst( $message ) }}</div>
                </div>
            </div>
        <?php } ?>
        <div style="padding: 10px 10px 20px;">
            Este es una notificación automática, favor de no responder este mensaje.
        </div>
        <div style="padding: 10px 10px 20px;">
            Si tienes alguna pregunta, favor de ponerte en contacto con nuestro equipo de soporte HoySí <a href="mail:info@hoysi.mx">info@hoysi.mx</a>.
        </div>
        <div style="padding: 10px 10px 20px;">
            Gracias, <br>
            Directorio HoySí<br>
            <a href="mail:info@hoysi.mx">info@hoysi.mx</a>
        </div>
    </div>
</body>
</html>