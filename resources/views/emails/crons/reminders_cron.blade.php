<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seguimiento Inmobiliario</title>
</head>
<body>
    <div style="padding-top: 30px;padding-right: 20%;padding-left: 20%;">
        <div style="width: 100%; margin: 0 auto; text-align: center; margin-bottom: 20px; color: #222d32; font-size: 40px; font-weight: bold;">
            SEGUIMIENTO INMOBILIARIO
        </div>
        @if($itemTitle != '')
            <div style="padding: 10px 10px 20px;">
                Su Recordatorio "{{ $itemTitle }}" esta próximo.
            </div>
        @endif
        @if($itemTitle != '')
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    Asunto:
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    {{ $itemTitle }}
                </div>
            </div>
        @endif
        @if($itemDesc != '')
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    <div>Descripción:</div>
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    <div>{{ ucfirst( $itemDesc ) }}</div>
                </div>
            </div>
        @endif
        @if($itemDate != '')
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    <div>Fecha Asignada:</div>
                </div>
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;color: #6b6b6b;font-weight: 700;">
                    <div>{{ $itemDate }}</div>
                </div>
            </div>
        @endif
        @if($itemUrl != '' || $urlClient != '')
            <div style="display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;width: 100%;padding: 10px;-webkit-justify-content: space-around;-ms-flex-pack: distribute;justify-content: space-around;-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;border-top: 1px dashed #c3c3c3;">
                <div style="-webkit-box-flex: 1;-webkit-flex: 1;-ms-flex: 1;flex: 1;font-weight: 700;">
                    @if($itemUrl != '')
                        <a href="{{ $itemUrl }}" target="_blank">Ver Recordatorio</a>
                    @endif
                    @if($itemUrl != '' || $urlClient != '')
                        | <a href="{{ $urlClient }}" target="_blank">Ver Cliente</a>
                    @endif
                </div>
            </div>
        @endif
        <div style="padding: 10px 10px 20px;">
            Este es una notificación automática, favor de no responder este mensaje.
        </div>
        <div style="padding: 10px 10px 20px;">
            Si tienes alguna pregunta, favor de ponerte en contacto con nuestro equipo de soporte "Seguimiento Inmobiliario" <a href="mail:info.devalan@gmail.com">info.devalan@gmail.com</a>.
        </div>
        <div style="padding: 10px 10px 20px;">
            Gracias, <br>
            Seguimiento Inmobiliario<br>
            <a href="mail:info.devalan@gmail.com">info.devalan@gmail.com</a>
        </div>
    </div>
</body>
</html>