<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Seguimiento Inmobiliario</title>
</head>
<body>
    <div style="padding-top: 30px;padding-right: 20%;padding-left: 20%;">
        @if($message != '')
            <div style="padding: 10px 10px 20px;">
                {!! $message !!}
            </div>
        @endif
        <div style="padding: 10px 10px 20px;">
            Si tienes alguna pregunta, favor de ponerte en contacto con nuestro equipo de soporte "Seguimiento Inmobiliario" <a href="mail:info.devalan@gmail.com">info.devalan@gmail.com</a>.
        </div>
    </div>
</body>
</html>