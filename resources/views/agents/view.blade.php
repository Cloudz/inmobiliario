@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Perfil de Agente{{ trans('agents.view_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agents') }}"><i class="fa fa-undo"></i> {{ trans('agents.back_list') }}</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('agents.enable_edition') }}</a>
            <a class="btn action_btn btn-xs" href="{{ route('agencyView', $agent->agency_id) }}"><i class="fa fa-eye"></i> {{ trans('agents.show_agency') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('agents.view_section') }} <a href="{{ route('agencyView', $agency->id) }}">{{ $agency->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('agentUpdate', $agent->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="agency_select">{{ trans('agents.view_agency') }}*</label>
                            <select class="form-control" id="agency_select" name="agency_select" required="required" disabled>
                                <option value="">{{ trans('agents.select_agency') }} ...</option>
                                @foreach ($agencies as $agency)
                                    <option value="{{ $agency->id }}" {{ ($agent->agency_id == $agency->id) ? 'selected' : '' }}>{{ $agency->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="name">{{ trans('agents.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $agent->name }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="email">{{ trans('agents.view_email') }}*</label>
                            <input type="text" class="form-control" id="email" name="email" required="required" value="{{ $agent->email }}" readonly>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                            <label for="mobile">{{ trans('agents.view_phone') }}</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="{{ $agent->mobile }}" readonly>
                        </div>
                    </div>
                    <div class="form-row column-form col-xs-12">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="comments">{{ trans('agents.make_comment') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <h3 class="title_list">{{ trans('agents.comments') }}</h3>
                            <div class="container_list">
                                @foreach ($agent->comment as $comment)
                                    <div class="item_list flex-between">
                                        <div class="container_info_list">
                                            <div class="date_list">
                                                <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                            </div>
                                            <div class="desc_list">
                                                <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                            </div>
                                        </div>
                                        <div class="container_actions_list">
                                            <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="agentView" data-source="{{ $agent->id }}" class="edit-trigger btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="container-complete form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                        <input type="checkbox" id="is_broker" name="is_broker" {{ ($agent->is_broker) ? 'checked' : '' }} disabled>
                        <label for="is_broker">{{ trans('agents.broker') }}</label>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('agents.send_information') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection