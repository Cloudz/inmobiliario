@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agents.create_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agents') }}"><i class="fa fa-undo"></i> {{ trans('agents.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('agents.create_section') }} <a href="{{ route('agencyView', $agency->id) }}">{{ $agency->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('agentStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <input type="hidden" name="agency_id" value="{{ $agency->id }}">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="name">{{ trans('agents.name') }}*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="email">{{ trans('agents.view_email') }}*</label>
                            <input type="text" class="form-control" id="email" name="email" required="required">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="mobile">{{ trans('agents.view_phone') }}</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 c-padding-left-input">
                            <label for="comments">{{ trans('agents.comments') }}</label>
                            <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <div class="container-complete form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                        <input type="checkbox" id="is_broker" name="is_broker">
                        <label for="is_broker">{{ trans('agents.is_broker') }}</label>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('agents.add_agent') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection