@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('agents.index_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>{{ trans('agents.index_section') }} - <i class="fa fa-th-list"></i> {{ $agents->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('agentSingleCreate') }}"><i class="fa fa-plus"></i> {{ trans('agents.create_agent') }}</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('agents') }}"><i class="fa fa-eraser"></i> {{ trans('agents.clean_search') }}</a>
            @endif
        </div>
        <form action="{{ route('agentSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="{{ trans('agents.search_agent') }}..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="{{ trans('global.search') }}" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $agents->appends(['search' => $search])->render() }}
            @else
                {{ $agents->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>{{ trans('agents.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('agents.email') }}  @sortablelink('email', trans('global.orderby'))</th>
                            <th>{{ trans('agents.phone') }}  @sortablelink('mobile', trans('global.orderby'))</th>
                            <th>{{ trans('agents.agency') }}</th>
                            <th>{{ trans('agents.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($agents)>0)
                            <tr>
                                @foreach($agents as $agent)
                                    <tr role="row">
                                        <td style="width: 10%">{{ $agent->name }}</td>
                                        <td style="width: 10%">{{ $agent->email }}</td>
                                        <td style="width: 10%">{{ $agent->mobile }}</td>
                                        <td style="width: 10%"><a href="{{ route('agencyView', $agent->agency_id) }}">{{ $agent->agencyName['name'] }}</a></td>
                                        <td style="width: 10%">
                                            <a href="{{ route('agentView', $agent->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('agentDelete', $agent->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('agents.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('agents.email') }}  @sortablelink('email', trans('global.orderby'))</th>
                            <th>{{ trans('agents.phone') }}  @sortablelink('mobile', trans('global.orderby'))</th>
                            <th>{{ trans('agents.agency') }}</th>
                            <th>{{ trans('agents.actions') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $agents->appends(['search' => $search])->render() }}
        @else
            {{ $agents->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
