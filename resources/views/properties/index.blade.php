@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Propiedades
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN PROPIEDADES - <i class="fa fa-th-list"></i> {{ $properties->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('propertyCreate') }}"><i class="fa fa-plus"></i> Agregar Propiedad</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('properties') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('propertiesSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Propiedad..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $properties->appends(['search' => $search])->render() }}
            @else
                {{ $properties->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>DESCRIPCIÓN @sortablelink('description', trans('global.orderby'))</th>
                            <th>BAÑOS @sortablelink('baths', trans('global.orderby'))</th>
                            <th>RECÁMARAS @sortablelink('beds', trans('global.orderby'))</th>
                            <th>ESTACIONAMIENTO @sortablelink('parking', trans('global.orderby'))</th>
                            <th>AMENIDADES</th>
                            <th>DIRECCIÓN @sortablelink('address', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($properties)>0)
                            <tr>
                                @foreach($properties as $property)
                                    <tr role="row">
                                        <td style="width: 10%">{{ $property->name }}</td>
                                        <td style="width: 10%">{{ $property->description }}</td>
                                        <td style="width: 10%">{{ $property->baths  }}</td>
                                        <td style="width: 10%">{{ $property->beds  }}</td>
                                        <td style="width: 10%">{{ $property->parking  }}</td>
                                        <td style="width: 10%">
                                            <?php foreach ($property->amenities()->get() as $amenity) { ?>
                                                {{ $amenity->name }}<br>
                                            <?php } ?>
                                        </td>
                                        <td style="width: 15%">{{ $property->address  }}</td>
                                        <td style="width: 10%">
                                            @if($property->files)
                                                <a href="{{ route('propertiesImagesDownload', $property->id) }}" class="btn edit_btn btn-xs bg-aqua" title="Descargar Imagenes" alt="Descargar Imagenes">
                                                    <i class="fa fa-download" aria-hidden="true"></i> &nbsp; <i class="fa fa-picture-o"></i>
                                                </a>
                                            @endif
                                            <a href="{{ route('propertyView', $property->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('propertyDelete', $property->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>DESCRIPCIÓN @sortablelink('description', trans('global.orderby'))</th>
                            <th>BAÑOS @sortablelink('baths', trans('global.orderby'))</th>
                            <th>RECÁMARAS @sortablelink('beds', trans('global.orderby'))</th>
                            <th>ESTACIONAMIENTO @sortablelink('parking', trans('global.orderby'))</th>
                            <th>AMENIDADES</th>
                            <th>DIRECCIÓN @sortablelink('address', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $properties->appends(['search' => $search])->render() }}
        @else
            {{ $properties->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
