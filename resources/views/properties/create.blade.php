@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Propiedad
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('properties') }}"><i class="fa fa-undo"></i> Volver al Listado de Propiedades</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">CREAR PROPIEDAD</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body tabs-menu-wrapper">
                        <form id="form_update" action="{{ route('propertyStore') }}" method="post" class="ajax-form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="name">Nombre*</label>
                                    <input type="text" class="form-control" id="name" name="name" required="required">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="description">Descripción</label>
                                    <textarea class="form-control" name="description" id="description" rows="6"></textarea>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="baths">Baños</label>
                                    <input type="number" step=".01" class="form-control" id="baths" name="baths">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="beds">Recámaras</label>
                                    <input type="number" step=".01" class="form-control" id="beds" name="beds">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="parking">Estacionamiento</label>
                                    <input type="number" step=".01" class="form-control" id="parking" name="parking">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="m2">M2</label>
                                    <input type="number" step=".01" class="form-control" id="m2" name="m2">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="ft2">FT2</label>
                                    <input type="text" step=".01" class="form-control" id="ft2" name="ft2">
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="address">Dirección</label>
                                    <textarea class="form-control" name="address" id="address" rows="6"></textarea>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                    <label for="type_property">Tipo de Propiedad*</label>
                                    <select class="form-control" id="type_property" name="type_property">
                                        <option value="1">Venta</option>
                                        <option value="2">Renta</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="type_id">Tipo de Propiedad*</label>
                                    <select class="form-control" id="type_id" name="type_id">
                                        <option value="">Selecciona un Tipo....</option>
                                        @foreach ($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="development_id">Desarrollo*</label>
                                    <select class="form-control" id="development_id" name="development_id">
                                        <option value="">Selecciona un Desarrollo....</option>
                                        @foreach ($developments as $development)
                                            <option value="{{ $development->id }}">{{ $development->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="location_id">Locación*</label>
                                    <select class="form-control" id="location_id" name="location_id">
                                        <option value="">Selecciona una Locación....</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}">{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="style_id">Estilo*</label>
                                    <select class="form-control" id="style_id" name="style_id">
                                        <option value="">Selecciona un Estilo....</option>
                                        @foreach ($styles as $style)
                                            <option value="{{ $style->id }}">{{ $style->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="assign_amenities">Amenidades</label>
                                    <select class="form-control" multiple="multiple" id="assign_amenities" name="amenities[]" required="required">
                                        @foreach ($amenities as $amenity)
                                            <option value="{{ $amenity->id }}">{{ $amenity->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="comments">Hacer Comentario</label>
                                    <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                                </div>
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <label for="files">Archivos e Imagenes</label>
                                <input type="file" class="form-control" id="files" name="files[]" multiple>
                                <div id="result">
                                </div>
                            </div>
                            <button type="submit" class="btn save_btn pull-right">Agregar Propiedad</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection