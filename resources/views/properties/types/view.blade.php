@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Perfil de Tipo
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('propertiesTypes') }}"><i class="fa fa-undo"></i> Volver al Listado de Tipos</a>
            <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> Habilitar Edición</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">PERFIL DE TIPO</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('propertiesTypeUpdate', $type->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">Nombre*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $type->name }}" readonly>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Enviar Información</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection