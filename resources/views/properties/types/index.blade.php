@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Tipos
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN TIPOS - <i class="fa fa-th-list"></i> {{ $types->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('propertiesTypeCreate') }}"><i class="fa fa-plus"></i> Agregar Tipo</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('propertiesTypes') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('propertiesTypesSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Tipo..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $types->appends(['search' => $search])->render() }}
            @else
                {{ $types->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($types)>0)
                            <tr>
                                @foreach($types as $type)
                                    <tr role="row">
                                        <td style="width: 90%">{{ $type->name }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('propertiesTypeView', $type->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('propertiesTypeDelete', $type->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $types->appends(['search' => $search])->render() }}
        @else
            {{ $types->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
