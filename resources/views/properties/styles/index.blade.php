@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Estilos
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN ESTILOS - <i class="fa fa-th-list"></i> {{ $styles->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('propertiesStyleCreate') }}"><i class="fa fa-plus"></i> Agregar Estilo</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('propertiesStyles') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('propertiesStylesSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Estilo..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $styles->appends(['search' => $search])->render() }}
            @else
                {{ $styles->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($styles)>0)
                            <tr>
                                @foreach($styles as $style)
                                    <tr role="row">
                                        <td style="width: 90%">{{ $style->name }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('propertiesStyleView', $style->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('propertiesStyleDelete', $style->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $styles->appends(['search' => $search])->render() }}
        @else
            {{ $styles->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
