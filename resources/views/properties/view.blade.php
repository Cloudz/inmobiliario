@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Perfil Propiedad
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('properties') }}"><i class="fa fa-undo"></i> Volver al Listado de Propiedades</a>
                <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> Habilitar Edición</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">PERFIL DE PROPIEDAD</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form id="form_update" action="{{ route('propertyUpdate', $property->id) }}" method="post" class="ajax-form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="name">Nombre*</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $property->name }}" required="required" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="description">Descripción</label>
                                    <textarea class="form-control" name="description" id="description" rows="6" disabled>{{ $property->description }}</textarea>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="baths">Baños</label>
                                    <input type="number" step=".01" class="form-control" id="baths" name="baths" value="{{ $property->baths }}" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="beds">Recámaras</label>
                                    <input type="number" step=".01" class="form-control" id="beds" name="beds" value="{{ $property->beds }}" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="parking">Estacionamiento</label>
                                    <input type="number" step=".01" class="form-control" id="parking" name="parking" value="{{ $property->parking }}" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="m2">M2</label>
                                    <input type="number" step=".01" class="form-control" id="m2" name="m2" value="{{ $property->m2 }}" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                                    <label for="ft2">FT2</label>
                                    <input type="text" step=".01" class="form-control" id="ft2" name="ft2" value="{{ $property->ft2 }}" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                                    <label for="address">Dirección</label>
                                    <textarea class="form-control" name="address" id="address" rows="6" disabled>{{ $property->address }}</textarea>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                    <label for="type_property">Tipo de Propiedad*</label>
                                    <select class="form-control" id="type_property" name="type_property" disabled>
                                        <option value="1" {{ ($property->type == 1) ? 'selected' : '' }}>Venta</option>
                                        <option value="2" {{ ($property->type == 2) ? 'selected' : '' }}>Renta</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="type_id">Tipo de Propiedad*</label>
                                    <select class="form-control" id="type_id" name="type_id" disabled>
                                        <option value="">Selecciona un Tipo....</option>
                                        @foreach ($types as $type)
                                            <option value="{{ $type->id }}" {{ ($property->type_id == $type->id) ? 'selected' : '' }}>{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="development_id">Desarrollo*</label>
                                    <select class="form-control" id="development_id" name="development_id" disabled>
                                        <option value="">Selecciona un Desarrollo....</option>
                                        @foreach ($developments as $development)
                                            <option value="{{ $development->id }}" {{ ($property->development_id == $development->id) ? 'selected' : '' }}>{{ $development->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="location_id">Locación*</label>
                                    <select class="form-control" id="location_id" name="location_id" disabled>
                                        <option value="">Selecciona una Locación....</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}" {{ ($property->location_id == $location->id) ? 'selected' : '' }}>{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="style_id">Estilo*</label>
                                    <select class="form-control" id="style_id" name="style_id" disabled>
                                        <option value="">Selecciona un Estilo....</option>
                                        @foreach ($styles as $style)
                                            <option value="{{ $style->id }}" {{ ($property->style_id == $style->id) ? 'selected' : '' }}>{{ $style->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="assign_amenities">Amenidades</label>
                                    <select class="form-control" multiple="multiple" id="assign_amenities" name="amenities[]" required="required" disabled>
                                        @foreach ($amenities as $amenity)
                                            <option value="{{ $amenity->id }}" <?=($property->amenities->contains($amenity->id)) ? 'selected' : ''?>>{{ $amenity->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                                    <label for="comments">Hacer Comentario</label>
                                    <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                                    <h3 class="title_list">Comentarios</h3>
                                    <div class="container_list">
                                        @foreach ($property->comment as $comment)
                                            <div class="item_list flex-between">
                                                <div class="container_info_list">
                                                    <div class="date_list">
                                                        <i class="fa fa-clock-o"></i> {{ $comment->created_at }}
                                                    </div>
                                                    <div class="desc_list">
                                                        <i class="fa fa-comment"></i> <i>{{ strip_tags($comment->comments) }}</i>
                                                    </div>
                                                </div>
                                                <div class="container_actions_list">
                                                    <a href="#" type="button" data-route="{{ route('commentUpdate', $comment->id) }}" data-comment="{{ strip_tags($comment->comments) }}" data-url="officeView" data-source="{{ $property->id }}" class="edit-trigger btn edit_btn btn-xs" title="Editar" alt="Editar">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a href="{{ route('commentDelete', $comment->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                        <i class="fa fa-fw fa-remove"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-row column-form col-xs-12">
                                <label for="files">Archivos e Imagenes</label>
                                <input type="file" class="form-control" id="files" name="files[]" multiple disabled>
                                <div id="result">
                                </div>
                            </div>
                            <div class="box-footer col-xs-12 col-no-padding"></div>
                            <button type="submit" class="btn save_btn pull-right">Actualizar Propiedad</button>
                        </form>
                        @if(count($property->files)>0)
                            <label for="files">Archivos e Imagenes Actuales</label>
                            <div id="resultEdit">
                                <?php
                                    $results = unserialize($property->files);
                                    $supported_image = array(
                                        'gif',
                                        'jpg',
                                        'jpeg',
                                        'png',
                                        'bmp'
                                    );
                                ?>
                                @foreach($results as $res)
                                    <?php
                                        $ext = strtolower(pathinfo($res, PATHINFO_EXTENSION));
                                    ?>
                                    @if(in_array($ext, $supported_image))
                                        <div class="c-container-file c-container-file-thumbnail" style="background-image:url('{{ '/storage/properties/'.$res }}')">
                                            <a href="{{ '/storage/properties/'.$res }}" download><div class='c-icon-image-edit'><i class='fa fa-picture-o'></i>&nbsp;&nbsp;&nbsp;<strong>Descargar Imagen</strong></div><img class='thumbnailEdit' src='{{ '/storage/properties/'.$res }}'/>
                                            </a>
                                            <form action="{{ route('propertyDeleteFile', $property->id) }}" method="post" class="ajax-form">
                                                {{csrf_field()}}
                                                <input type="hidden" name="currenFile" value="{{ $res }}">
                                                <button class="c-btn-delete-file" type="submit"><i class="fa fa-times"></i></button>
                                            </form>
                                        </div>
                                    @else
                                    <div class="c-container-file">
                                        <a href="{{ '/storage/properties/'.$res }}" download><div class='typeFileEdit'><i class='fa fa-download'></i>&nbsp;&nbsp;&nbsp;<strong>Descargar Archivo {{ '.'.$ext }}</strong></div></a>
                                        <form action="{{ route('propertyDeleteFile', $property->id) }}" method="post" class="ajax-form">
                                            {{csrf_field()}}
                                            <input type="hidden" name="currenFile" value="{{ $res }}">
                                            <button class="c-btn-delete-file" type="submit"><i class="fa fa-times"></i></button>
                                        </form>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection