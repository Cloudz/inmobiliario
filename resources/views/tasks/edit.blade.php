@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Editar Tarea
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('tasks') }}"><i class="fa fa-undo"></i> Volver a la Agenda</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">EDITAR TAREA - {{ ($task->accomplished) ? 'Completada' : ' No Completada' }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('taskUpdate', $task->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        @role(['Administrator', 'Developer', 'Broker', 'Office Manager'])
                            <div class="form-group col-xs-12 c-padding-left-input">
                                <label for="agent_id">{{ trans('schedules.select_agent') }}*</label>
                                <select class="form-control" id="agent_id" name="agent_id" required="required" >
                                    <option value="">{{ trans('schedules.select_agent') }} ...</option>
                                    @foreach ($agents as $agent)
                                        <option value="{{ $agent->id }}" {{ ($task->user_id == $agent->id) ? 'selected' : '' }}>{{ $agent->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endrole
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="subject">Asunto*</label>
                            <input type="text" class="form-control" id="subject" name="subject" required="required" value="{{ $task->subject }}">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="description">Descripción*</label>
                            <textarea rows="6" class="form-control" id="description" name="description" required="required">{{ $task->description }}</textarea>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="date_start">Fecha de Inicio* - {{ $started_on }}</label>
                            <input type="text" class="form-control" id="date_start" name="date_start" placeholder="Reemplazar Fecha ...">
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="date_reminded">Fecha de Recordatorio {{ ($reminded_on) ? '- '.$reminded_on : ''}}</label>
                            <input type="text" class="form-control" id="date_reminded" name="date_reminded" placeholder="Reemplazar Fecha ...">
                        </div>
                        <div class="container-complete form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                            <input type="checkbox" id="accomplished" name="accomplished" {{ ($task->accomplished) ? 'checked' : '' }} >
                            <label for="accomplished" class="{{ ($task->accomplished) ? 'bg-green' : 'bg-red' }}">  {{ ($task->accomplished) ? 'Completada' : ' No Completada' }}</label>
                        </div>
                        <input type="hidden" id="started_on" name="started_on" value="{{ $task->started_on }}" required="required">
                        <input type="hidden" id="reminded_on" name="reminded_on" value="{{ $task->reminded_on }}" required="required">
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Actualizar Tarea</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
  <script>
    $("#date_start").on("dp.change", function(e) {
        selectedDate = moment($(this).val(),"DD/MM/YYYY LT");
        tzoffset = (new Date()).getTimezoneOffset() * 60000;
        localISOTime = (new Date(selectedDate - tzoffset)).toISOString().slice(0, -1);
        $('#started_on').val(localISOTime);
    });
    $("#date_reminded").on("dp.change", function(e) {
        selectedDate = moment($(this).val(),"DD/MM/YYYY LT");
        tzoffset = (new Date()).getTimezoneOffset() * 60000;
        localISOTime = (new Date(selectedDate - tzoffset)).toISOString().slice(0, -1);
        $('#reminded_on').val(localISOTime);
    });
  </script>
@endsection