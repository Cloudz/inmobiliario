<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Crear Recurso {{ $mainSource->name }}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body tabs-menu-wrapper">
            <form action="{{ route('sourceStore', $mainSource->model) }}" method="post" class="ajax-form">
                {{csrf_field()}}
                <input type="hidden" name="redirect_id" value="">
                @if($customSource->hasAttribute('agency_id'))
                    <input type="hidden" name="agency_id" value="{{ $parentCustomSource->id  }}">
                @endif
                <div class="form-row column-form">
                    <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                        <label for="name">Nombre*</label>
                        <input type="text" class="form-control" id="name" name="name" required="required">
                    </div>
                    <!-- INPUTS FOR AGENTS -->
                    @if($customSource->hasAttribute('email'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="email">Email*</label>
                            <input type="text" class="form-control" id="email" name="email" required="required">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('mobile'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="mobile">Teléfono</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                    @endif
                    <!-- INPUTS FOR AGENCIES -->
                    @if($customSource->hasAttribute('main_office_phone'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="main_office_phone">Tel. Principal</label>
                            <input type="text" class="form-control" id="main_office_phone" name="main_office_phone">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('secondary_office_phone'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="secondary_office_phone">Tel. Secundario</label>
                            <input type="text" class="form-control" id="secondary_office_phone" name="secondary_office_phone">
                        </div>
                    @endif
                    <!-- INPUTS FOR REFERRORS -->
                    @if($customSource->hasAttribute('last_name'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="last_name">Apellido*</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" required="required">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('main_email'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="main_email">Email Principal*</label>
                            <input type="email" class="form-control" id="main_email" name="main_email" required="required">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('secondary_email'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="secondary_email">Email Secundario</label>
                            <input type="email" class="form-control" id="secondary_email" name="secondary_email">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('home_phone'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="home_phone">Tel. Casa</label>
                            <input type="text" class="form-control" id="home_phone" name="home_phone">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('tel_office'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="tel_office">Tel. Oficina</label>
                            <input type="text" class="form-control" id="tel_office" name="tel_office">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('main_mobile'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="main_mobile">Cel. Principal</label>
                            <input type="text" class="form-control" id="main_mobile" name="main_mobile">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('secondary_mobile'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="secondary_mobile">Cel. Secundario</label>
                            <input type="text" class="form-control" id="secondary_mobile" name="secondary_mobile">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('gender_id'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="sex">Sexo*</label>
                            <select class="form-control" id="sex" name="sex" required="required">
                                <option value="0">Masculino</option>
                                <option value="1">Femenino</option>
                                <option value="2">LGBT</option>
                            </select>
                        </div>
                    @endif
                    @if($customSource->hasAttribute('occupation'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-6 c-padding-left-input">
                            <label for="occupation">Ocupación</label>
                            <input type="text" class="form-control" id="occupation" name="occupation">
                        </div>
                    @endif
                    <!-- INPUTS FOR SALES OFFICES -->
                    @if($customSource->hasAttribute('main_phone'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="main_phone">Tel. Principal*</label>
                            <input type="text" class="form-control" id="main_phone" name="main_phone" required="required">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('secondary_phone'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="secondary_phone">Tel. Secundario</label>
                            <input type="text" class="form-control" id="secondary_phone" name="secondary_phone">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('fax'))
                        <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                            <label for="fax">Fax</label>
                            <input type="text" class="form-control" id="fax" name="fax">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('street'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="street">Calle</label>
                            <input type="text" class="form-control" id="street" name="street">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('number_int'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="number_int">Número Interno</label>
                            <input type="text" class="form-control" id="number_int" name="number_int">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('number_ext'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="number_ext">Número Externo</label>
                            <input type="text" class="form-control" id="number_ext" name="number_ext">
                        </div>
                    @endif
                    @if($customSource->hasAttribute('cp'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-3 c-padding-left-input">
                            <label for="cp">C.P.</label>
                            <input type="text" class="form-control" id="cp" name="cp">
                        </div>
                    @endif
                </div>
                <!-- INPUTS FOR AGENCIES/AGENTS/REFERRORS -->
                @if($customSource->hasAttribute('country_id'))
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-12 c-padding-left-input">
                            <label for="nationality">Nacionalidad*</label>
                            <select class="form-control" id="nationality" name="nationality" required="required">
                                <option value="">Selecciona País ...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="form-row column-form">
                    <!-- INPUTS FOR AGENCIES/AGENTS/REFERRORS -->
                    @if($customSource->hasAttribute('address'))
                        <div class="form-group col-xs-12 col-md-6 col-lg-12 c-padding-left-input">
                            <label for="address">Dirección</label>
                            <textarea rows="6" class="form-control" id="address" name="address"></textarea>
                        </div>
                    @endif
                    <div class="form-group col-xs-12 col-md-6 col-lg-12 c-padding-left-input">
                        <label for="comments">Comentarios</label>
                        <textarea rows="6" class="form-control" id="comments" name="comments"></textarea>
                    </div>
                    <!-- INPUTS FOR AGENTS -->
                    @if($customSource->hasAttribute('is_broker'))
                        <div class="container-complete form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <input type="checkbox" id="is_broker" name="is_broker">
                            <label for="is_broker">Es Corredor</label>
                        </div>
                    @endif
                </div>
                <button type="button" class="btn delete_btn_modal delete_confirmation_cancel" data-remodal-action="cancel"> <i class="fa fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                <button type="submit" class="btn btn-info" style="margin-left:20px;padding:10px 20px;"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Enviar Información</button>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>