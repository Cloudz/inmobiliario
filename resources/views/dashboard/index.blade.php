@extends('adminlte::layouts.app')
@section('htmlheader_title')
Inmobiliario
@endsection
@section('main-content')
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="container-statics-home row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <a href="{{ route('clients') }}" class="small-box bg-green">
        <div class="inner">
          <h3>{{ $clients->count() }}</h3>
          <p>{{ trans('dashboard.clients') }}</p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>
        </div>
        <div class="small-box-footer">{{ trans('dashboard.more_information') }} <i class="fa fa-arrow-circle-right"></i></div>
      </a>
    </div>
    <!-- ./col -->
    @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent'])
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{ route('agencies') }}" class="small-box bg-yellow">
          <div class="inner">
            <h3>{{ $agencies->count() }}</h3>
            <p>{{ trans('dashboard.agencies') }}</p>
          </div>
          <div class="icon">
            <i class="fa fa-exchange"></i>
          </div>
          <div class="small-box-footer">{{ trans('dashboard.more_information') }} <i class="fa fa-arrow-circle-right"></i></div>
        </a>
      </div>
      <!-- ./col -->
    @endrole
    @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{ route('agents') }}" class="small-box bg-aqua">
          <div class="inner">
            <h3>{{ $agents->count() }}</h3>
            <p>{{ trans('dashboard.agents') }}</p>
          </div>
          <div class="icon">
            <i class="fa fa-id-card-o"></i>
          </div>
          <div class="small-box-footer">{{ trans('dashboard.more_information') }} <i class="fa fa-arrow-circle-right"></i></div>
        </a>
      </div>
      <!-- ./col -->
    @endrole
    @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant'])
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{ route('referrors') }}" class="small-box bg-maroon">
          <div class="inner">
            <h3>{{ $referrors->count() }}</h3>
            <p>{{ trans('dashboard.referrors') }}</p>
          </div>
          <div class="icon">
            <i class="fa fa-handshake-o"></i>
          </div>
          <div class="small-box-footer">{{ trans('dashboard.more_information') }} <i class="fa fa-arrow-circle-right"></i></div>
        </a>
      </div>
      <!-- ./col -->
    @endrole
  </div>
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable ui-sortable">
      @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant'])
        <!-- quick email widget -->
        <div class="box box-info">
          <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-envelope"></i>
            <h3 class="box-title">{{ trans('dashboard.title_form') }}</h3>
          </div>
          <div class="box-body">
            <form action="{{ route('quickSendMail') }}" method="post" class="ajax-form">
              {{csrf_field()}}
              <div class="form-group">
                <input id="email-send-mail" type="email" class="form-control" name="f_email" placeholder="{{ trans('dashboard.email') }}">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="f_subject" placeholder="{{ trans('dashboard.subject') }}">
              </div>
              <div class="form-group">
                <textarea rows="8" class="form-control" id="f_message" name="f_message" placeholder="{{ trans('dashboard.message') }}"></textarea>
              </div>
              <div class="form-group">
                <label for="f_file">{{ trans('dashboard.file') }}</label>
                <input type="file" id="f_file" name="f_file">
              </div>
              <div class="box-footer clearfix">
                <button type="submit" class="btn save_btn pull-right">{{ trans('dashboard.send') }}</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.quick email -->
      @endrole
      <div class="box box-success">
        <div class="box-header ui-sortable-handle">
          <i class="fa fa-user"></i>
          <h3 class="box-title">{{ trans('dashboard.title_clients') }} - <span style="font-size: 12px">{{ trans('dashboard.last_days') }}</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body list-scroll">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th>{{ trans('dashboard.status') }}</th>
                <th>{{ trans('dashboard.name') }}</th>
                <th>{{ trans('dashboard.last_name') }}</th>
                <th>{{ trans('dashboard.email') }}</th>
                <th>{{ trans('dashboard.mobile') }}</th>
                <th>{{ trans('dashboard.interest') }}</th>
                <th>{{ trans('dashboard.level_interest') }}</th>
                <th>{{ trans('dashboard.actions') }}</th>
              </tr>
              </thead>
              <tbody>
                @if($lastClients->count() > 0)
                  @foreach($lastClients as $client)
                      <?php
                      if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])){
                          $marketing = App\Models\MarketingInformation::orderBy('created_at', 'desc')->first();
                      }elseif(Auth::user()->hasRole(['Personal Assistant'])){
                          $marketing = App\Models\MarketingInformation::where('user_id', \UsersHelpers::getUserID())->orWhereIn('user_id', \UsersHelpers::getUsers())->orderBy('created_at', 'desc')->first();
                      }else{
                          $marketing = App\Models\MarketingInformation::where('client_id', $client->id)->where('user_id', \UsersHelpers::getUserID())->orderBy('created_at', 'desc')->first();
                      }
                      if($client->status){
                        $getInterest   = App\Models\Interests::where('id', $marketing->interest_id)->first();
                        $interest      = $getInterest->name;
                        $levelInterest = $marketing->level_interest_id;
                      }else{
                          $interest      = '';
                          $levelInterest = 0;
                      }
                      ?>
                      <tr>
                        <td>
                        @if($client->status)
                          <span class="btn edit_btn btn-xs">
                            {{ trans('clients.complete') }}
                          </span>
                        @else
                          <span class="btn delete_btn btn-xs bg-maroon">
                              {{ trans('clients.incomplete') }}
                          </span>
                        @endif
                        </td>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->last_name }}</td>
                        <td>{{ $client->main_email }}</td>
                        <td>{{ $client->main_mobile }}</td>
                        <td>{{ $interest }}</td>
                        <td>
                          @for ($i = 0; $i < 4; ++$i)
                            <i class="fa fa-star{{ $levelInterest<=$i?'-o':'' }}" aria-hidden="true"></i>
                          @endfor
                        </td>
                        <td>
                          @role(['Administrator', 'Developer', 'Broker', 'Referror'])
                          <a href="{{ route('clientMail', $client->id) }}" class="btn edit_btn btn-xs bg-aqua" title="Mensaje" alt="Mensaje">
                            <i class="fa fa-envelope"></i>
                          </a>
                          @endrole
                          <a href="{{ route('clientView', $client->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                            <i class="fa fa-eye"></i>
                          </a>
                          <a href="{{ route('clientDelete', $client->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                            <i class="fa fa-fw fa-remove"></i>
                          </a>
                        </td>
                      </tr>
                  @endforeach
                @else
                  <tr>
                    <tr role="row">
                      <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                    </tr>
                  </tr>
                @endif
              </tbody>
            </table>
            @if($lastClients->total() > $lastClients->perPage())
              {{ $lastClients->appends(request()->input())->links() }}
            @endif
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="{{ route('clients') }}" class="btn btn-sm action_btn pull-right">{{ trans('dashboard.view_all_clients') }}</a>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable ui-sortable">
      @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant'])
        <!-- Chat box -->
        <div class="box box-success">
          <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-comments-o"></i>
            <h3 class="box-title">{{ trans('dashboard.title_messages') }} - {{ $conversations->total() }}</h3>
          </div>
          <div class="slimScrollDiv" style="position: relative; overflow: auto; width: auto; height: 500px;">
            <div class="box-body chat" id="chat-box" style="overflow: auto; width: auto; height: 500px;">
              @if($conversations->count() > 0)
                @foreach($conversations as $conversation)
                  <!-- chat item -->
                    <div class="item">
                      <div class="container-title-msg">
                        <div class="title-msg">
                          <i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ trans('dashboard.conversation') }} - {{ $conversation['sender'] }} ( {{ count($conversation['messages']) }} )
                        </div>
                        <div class="date-btns-msg">
                          <div class="container-btns-msg">
                            <a href="#" type="button" class="show-conversation btn edit_btn btn-xs" title="{{ trans('global.show') }}" alt="{{ trans('global.show') }}">
                              <i class="fa fa-eye"></i>
                            </a>
                            <a href="#" type="button" class="reply-msg btn edit_btn btn-xs bg-aqua" data-email="{{ $conversation['sender'] }}" title="{{ trans('global.response') }}" alt="{{ trans('global.response') }}">
                              <i class="fa fa-reply"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="container-conversation">
                        @foreach($conversation['messages'] as $message)
                          <div class="container-subject-msg">
                            <div class="title-msg">
                              <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ $message['subject'] }}
                            </div>
                            <div class="date-btns-msg">
                              <div class="container-date-msg">
                                <i class="fa fa-clock-o"></i> {{date('d/m/y H:i:s', strtotime($message['created_at']))}}
                              </div>
                              <div class="container-btns-msg">
                                <a href="#" type="button" class="show-body-msg btn edit_btn btn-xs" style="background-color: #f7f7f7; color:#1f2d33; border: 1px #1f2d33 solid;" title="{{ trans('global.show') }}" alt="{{ trans('global.show') }}">
                                  <i class="fa fa-eye"></i>
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="container-body-msg">
                            @if($message['htmlBody'])
                              {!! $message['htmlBody'] !!}
                            @else
                              {{ $message['textBody'] }}
                            @endif
                            @if($message['attachments'])
                              @php
                                $supported_image = array(
                                    'gif',
                                    'jpg',
                                    'jpeg',
                                    'png',
                                    'bmp'
                                );
                              @endphp
                              <div class="attachment">
                                <div class="title-attachments">
                                  {{ trans('dashboard.title_attachments') }}
                                </div>
                                @foreach($message['attachments'] as $file)
                                  @php
                                    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                  @endphp
                                  @if(in_array($ext, $supported_image))
                                    <a href="{{ '/storage/mails/'.$file }}" download>
                                      <div class='btn-download-file'>
                                        <i class='fa fa-picture-o'></i>&nbsp;&nbsp;&nbsp;<strong>{{ trans('dashboard.download_image') }}{{ '.'.$ext }}</strong>
                                      </div>
                                    </a>
                                  @else
                                    <a href="{{ '/storage/mails/'.$file }}" download>
                                      <div class='btn-download-file'>
                                        <i class='fa fa-file-o'></i>&nbsp;&nbsp;&nbsp;<strong>{{ trans('dashboard.download_file') }}{{ '.'.$ext }}</strong>
                                      </div>
                                    </a>
                                  @endif
                                @endforeach
                              </div>
                          @endif
                          <!-- /.attachment -->
                          </div>
                        @endforeach
                      </div>
                    </div>
                    <!-- /.item -->
                  @endforeach
              @else
                <div>{{ trans('global.not_found') }}</div>
              @endif
              @if($conversations->total() > $conversations->perPage())
                {{ $conversations->appends(request()->input())->links() }}
              @endif
            </div>
            <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 184.911px;"></div>
            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
          </div>
        </div>
        <!-- /.box (chat box) -->
      @endrole
      @role(['Administrator', 'Developer', 'Broker', 'Sales Agent', 'General Assistant', 'Personal Assistant'])
        <!-- TO DO List -->
        <div class="box box-warning">
          <div class="box-header ui-sortable-handle">
            <i class="fa fa-book"></i>
            <h3 class="box-title">{{ trans('dashboard.title_visits') }} - <span style="font-size: 12px">{{ trans('dashboard.last_days') }}</span></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body list-scroll">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>{{ trans('dashboard.date_visit') }}</th>
                  <th>{{ trans('dashboard.client') }}</th>
                  <th>{{ trans('dashboard.office') }}</th>
                  <th>{{ trans('dashboard.adviser') }}</th>
                  <th>{{ trans('dashboard.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                  @if($lastVisits->count() > 0)
                    @foreach($lastVisits as $visit)
                        <tr>
                          <td>{{ $visit->visit_date }}</td>
                          <td><a href="{{ route('clientView', $visit->client->id) }}">{{ $visit->client->name }}</a></td>
                          <td><a href="{{ route('officeView', $visit->office->id) }}">{{ $visit->office->name }}</a></td>
                          <td><a href="{{ route('agentView', $visit->adviser->id) }}">{{ $visit->adviser->name }}</a></td>
                          <td>
                            <a href="{{ route('visitView', $visit->id) }}" class="btn edit_btn btn-xs" title="Ver" alt="Ver">
                              <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('visitDelete', $visit->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                              <i class="fa fa-fw fa-remove"></i>
                            </a>
                          </td>
                        </tr>
                    @endforeach
                  @else
                    <tr>
                    <tr role="row">
                      <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                    </tr>
                    </tr>
                  @endif
                </tbody>
              </table>
              @if($lastVisits->total() > $lastVisits->perPage())
                {{ $lastVisits->appends(request()->input())->links() }}
              @endif
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="{{ route('visits') }}" class="btn btn-sm action_btn pull-right">{{ trans('dashboard.view_all_visits') }}</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      @endrole
    </section>
    <!-- right col -->
  </div>
</section>
@endsection