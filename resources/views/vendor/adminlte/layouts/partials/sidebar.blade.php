<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif
        @role(['Administrator', 'Developer'])
            <ul class="sidebar-menu" data-widget="tree">
                <li @if (Request::is('users') || Request::is('users/*')) class="active" @endif><a href="{{ route('users') }}"><i class="fa fa-users" aria-hidden="true"></i> <span>{{ trans('sidebar.users') }}</span></a></li>
                <li class="treeview"  @if (Request::is('acquisition') || Request::is('acquisition/*')) class="active" @endif>
                    <a href="#"><i class='fa fa-comment'></i> <span>{{ trans('sidebar.sources') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('contactWays') }}">{{ trans('sidebar.contact_ways') }}</a></li>
                        <li><a href="{{ route('acquisitionMainSources') }}">{{ trans('sidebar.main_sources') }}</a></li>
                        <li><a href="{{ route('acquisitionSecondarySources') }}">{{ trans('sidebar.secondary_sources') }}</a></li>
                    </ul>
                </li>
                <li class="treeview"  @if (Request::is('acquisitionMediums') || Request::is('acquisitionMediums/*')) class="active" @endif>
                    <a href="#"><i class='fa fa-comments'></i> <span>{{ trans('sidebar.mediums') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('contactMediums') }}">{{ trans('sidebar.contact_mediums') }}</a></li>
                        <li><a href="{{ route('acquisitionMainMediums') }}">{{ trans('sidebar.main_mediums') }}</a></li>
                        <li><a href="{{ route('acquisitionSecondaryMediums') }}">{{ trans('sidebar.secondary_mediums') }}</a></li>
                    </ul>
                </li>
            </ul><!-- /.sidebar-menu -->
            <div style="border: 1px solid #b4c6cf"></div>
        @endrole
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li @if (Request::is('dashboard') || Request::is('dashboard/*')) class="active" @endif><a href="{{ route('dashboard') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>{{ trans('sidebar.dashboard') }}</span></a></li>
            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'Referror'])
                    <li @if (Request::is('followups') || Request::is('followups/*')) class="active" @endif><a href="{{ route('followups') }}"><i class="fa fa-paper-plane" aria-hidden="true"></i> <span>{{ trans('sidebar.followups') }}</span></a></li>
            @endrole
            @role(['Administrator', 'Developer', 'Broker', 'Office Manager'])
                <li @if (Request::is('marketing') || Request::is('marketing/*')) class="active" @endif><a href="{{ route('marketing') }}"><i class="fa fa-line-chart" aria-hidden="true"></i> <span>{{ trans('sidebar.marketing') }}</span></a></li>
                <li @if (Request::is('schedules') || Request::is('schedules/*')) class="active" @endif><a href="{{ route('schedules') }}"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>{{ trans('sidebar.schedules') }}</span></a></li>
            @endrole
            <li @if (Request::is('clients') || Request::is('clients/*')) class="active" @endif><a href="{{ route('clients') }}"><i class="fa fa-user" aria-hidden="true"></i> <span>{{ trans('sidebar.clients') }}</span></a></li>

            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent'])
                <li @if (Request::is('agencies') || Request::is('agencies/*')) class="active" @endif><a href="{{ route('agencies') }}"><i class="fa fa-exchange" aria-hidden="true"></i> <span>{{ trans('sidebar.agencies') }}</span></a></li>
            @endrole

            @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                <li @if (Request::is('agents') || Request::is('agents/*')) class="active" @endif><a href="{{ route('agents') }}"><i class="fa fa-id-card-o" aria-hidden="true"></i> <span>{{ trans('sidebar.agents') }}</span></a></li>
            @endrole

            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant'])
                <li @if (Request::is('referrors') || Request::is('referrors/*')) class="active" @endif><a href="{{ route('referrors') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> <span>{{ trans('sidebar.referrors') }}</span></a></li>
            @endrole

            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant', 'Personal Assistant'])
                <li @if (Request::is('tasks') || Request::is('tasks/*')) class="active" @endif><a href="{{ route('tasks') }}"><i class="fa fa-tasks" aria-hidden="true"></i> <span>{{ trans('sidebar.tasks') }}</span></a></li>
                <li class="treeview"  @if (Request::is('reminders') || Request::is('reminders/*')) class="active" @endif>
                    <a href="#"><i class='fa fa-thumb-tack'></i> <span>{{ trans('sidebar.reminders') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('reminders', 'Call') }}"><i class="fa fa-phone" aria-hidden="true"></i> {{ trans('sidebar.calls') }}</a></li>
                        <li><a href="{{ route('reminders', 'Mail') }}"><i class="fa fa-envelope" aria-hidden="true"></i> {{ trans('sidebar.mails') }}</a></li>
                        <li><a href="{{ route('reminders', 'Showing') }}"><i class="fa fa-bullhorn" aria-hidden="true"></i> {{ trans('sidebar.showings') }}</a></li>
                    </ul>
                </li>
            @endrole

            @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'General Assistant'])
                <li @if (Request::is('offices') || Request::is('offices/*')) class="active" @endif><a href="{{ route('offices') }}"><i class="fa fa-flag" aria-hidden="true"></i> <span>{{ trans('sidebar.offices') }}</span></a></li>
                <li class="treeview"  @if (Request::is('properties') || Request::is('properties/*')) class="active" @endif>
                    <a href="#"><i class='fa fa-home'></i> <span>{{ trans('sidebar.properties') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('properties') }}"><i class="fa fa-bars" aria-hidden="true"></i> <span>{{ trans('sidebar.list_properties') }}</span></a></li>
                        <li><a href="{{ route('propertiesDevelopments') }}"><i class="fa fa-building" aria-hidden="true"></i> {{ trans('sidebar.developments') }}</a></li>
                        <li><a href="{{ route('propertiesAmenities') }}"><i class="fa fa-sitemap" aria-hidden="true"></i> {{ trans('sidebar.amenities') }}</a></li>
                        <li><a href="{{ route('propertiesLocations') }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ trans('sidebar.locations') }}</a></li>
                        <li><a href="{{ route('propertiesTypes') }}"><i class="fa fa-cubes" aria-hidden="true"></i> {{ trans('sidebar.types') }}</a></li>
                        <li><a href="{{ route('propertiesStyles') }}"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('sidebar.styles') }}</a></li>
                    </ul>
                </li>
            @endrole

            @role(['Administrator', 'Developer', 'Broker', 'Sales Agent', 'General Assistant', 'Personal Assistant'])
                <li @if (Request::is('visits') || Request::is('visits/*')) class="active" @endif><a href="{{ route('visits') }}"><i class="fa fa-book" aria-hidden="true"></i> <span>{{ trans('sidebar.logbook') }}</span></a></li>
            @endrole
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
