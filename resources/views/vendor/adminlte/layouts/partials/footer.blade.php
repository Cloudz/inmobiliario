<!-- Main Footer -->
<footer class="main-footer">
</footer>

<!-- popup delete confirmation template -->
<div id="delete_confirmation" style="display:none;">
    <div class="col-md-12">
        <div class="box box-primary" style="text-align: center; border:0">
            <div class=" box-body">
                <h3>¿Deseas borrar completamente esta registro?</h3>
            </div>
            <br>
            <div class="box-footer">
                <form class="delete_popup_form" action="" method="POST"> 
                    {{ csrf_field() }}
                    <button type="button" class="btn action_btn delete_confirmation_cancel" data-remodal-action="cancel"> 
                        <i class="fa fa-times"></i>&nbsp;&nbsp;Cancelar
                    </button>
                    <button type="submit" class="btn delete_btn_modal" style="margin-left:20px;">
                        <i class="fa fa-trash"></i>&nbsp;&nbsp;Borrar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- popup information template -->
<div id="modal_confirmation" class="remodal" data-remodal-id="modal_confirmation"></div>
<div id="modal_information" class="remodal" data-remodal-id="modal_information"></div>
<div id="modal_form" class="remodal" data-remodal-id="modal_form"></div>
<div id="modal_create" class="remodal" data-remodal-id="modal_create"></div>
<div id="modal_verify_data" class="remodal" data-remodal-id="modal_verify_data">
    <h3 class="title_remodal"><span class="count_registers"></span> Registros Similares</h3>
    <div class="success-registers">El Email esta disponible</div>
    <div class="error-registers">El Email ya esta registrado</div>
    <div class="box">
      <!-- /.box-header -->
      <div class="box-body table-responsive">
          <table class="table table-bordered table-hover" style="width: 100%">
              <thead>
                  <tr>
                      <th style="text-align: center;">NOMBRE</th>
                      <th style="text-align: center;">APELLIDO</th>
                      <th style="text-align: center;">AGENTE</th>
                      <th style="text-align: center;">CREACIÓN</th>
                  </tr>
              </thead>
              <tbody class="list_registers">
              </tbody>
              <tfoot>
                  <tr>
                      <th style="text-align: center;">NOMBRE</th>
                      <th style="text-align: center;">APELLIDO</th>
                      <th style="text-align: center;">AGENTE</th>
                      <th style="text-align: center;">CREACIÓN</th>
                  </tr>
              </tfoot>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
    <button class="btn-accept-register btn action_btn btn-xls">Aceptar</button>
    <!-- /.box -->
</div>

@if(Session::has('msg'))
    <!-- flash message -->
    <div class="flash-msg" data-success="{{ Session::get('msg_status') }}" style="display: none;">
        <div class="col-xs-12">
            <div 
                class='<?=(Session::has('msg_status') && Session::get('msg_status') == false) ? 'action-fail':'action-success'?>' 
                style="text-align: center">
                    {{ Session::get('msg') }}
            </div>
        </div> 
    </div>
@endif