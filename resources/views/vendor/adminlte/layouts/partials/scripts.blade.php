<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=25qnmilwto6bj9x1hket85jd31lougl0j7pq66yxhtplwtb8"></script>
<script src="{{ asset('/js/jquery.bootstrap-growl.min.js') }}" type='text/javascript'></script>
<script src="{{ asset('/js/remodal.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/fullcalendar.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/lang/es.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/es.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/fastselect.standalone.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/scripts.js') }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
