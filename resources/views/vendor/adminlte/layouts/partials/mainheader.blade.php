<?php
    $calls    = \RemindersHelpers::getReminders('Call');
    $mails    = \RemindersHelpers::getReminders('Mail');
    $showings = \RemindersHelpers::getReminders('Showing');

    if ((\Route::current()->getName() != 'clientCreate') && (\Route::current()->getName() != 'saveDataStepOne') && (\Route::current()->getName() != 'createStepTwo') && (\Route::current()->getName() != 'saveDataStepTwo') && (\Route::current()->getName() != 'createStepThree') && (\Route::current()->getName() != 'saveDataStepThree') && (\Route::current()->getName() != 'createStepFour') && (\Route::current()->getName() != 'clientStore') && (\Route::current()->getName() != 'contactWays') && (\Route::current()->getName() != 'acquisitionMainSources') && (\Route::current()->getName() != 'acquisitionSecondarySources') && (\Route::current()->getName() != 'contactMediums') && (\Route::current()->getName() != 'acquisitionMainMediums') && (\Route::current()->getName() != 'acquisitionSecondaryMediums') && (\Route::current()->getName() != 'getForm') && (\Route::current()->getName() != 'getSubForm')) {
        \Session::forget('client');
    }
?>
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>S </b>Inmo</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Seguimiento</b>Inmobiliario </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav container-btns-header">
                @role(['Administrator', 'Developer'])
                    <?php
                        $properties = \App\Models\Properties::all();
                    ?>
                    @if($properties->count() > 0)
                        <li>
                            <a href="{{ route('propertiesExport') }}">{{ trans('top.button_export_properties') }}</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('propertiesFormImport') }}">{{ trans('top.button_import_properties') }}</a>
                    </li>
                @endrole
                @role(['Administrator', 'Developer', 'Broker'])
                    <?php
                        $clients = \App\Models\Clients::all();
                    ?>
                    @if($clients->count() > 0)
                        <li>
                            <a href="{{ route('clientsExport') }}">{{ trans('top.button_export_clients') }}</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('clientsFormImport') }}">{{ trans('top.button_import_clients') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('agentSingleCreate') }}">{{ trans('top.button_agent') }}</a>
                    </li>
                @endrole
                <li>
                    <a href="{{ route('clientCreate') }}">{{ trans('top.button_client') }}</a>
                </li>
                @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent'])
                    <li>
                        <a href="{{ route('agencyCreate') }}">{{ trans('top.button_agency') }}</a>
                    </li>
                @endrole
            </ul>
            <ul class="nav navbar-nav">
                @role(['Administrator', 'Developer', 'Broker', 'Office Manager', 'Sales Agent', 'General Assistant', 'Personal Assistant'])
                    @php
                        $matchThese = ['type' => 'Answer', 'recipient' => \UsersHelpers::getUser()->email];
                        $newMails = \App\Models\ConversationMail::where($matchThese)->limit(5)->get();
                        $lastMails = [];
                        foreach ($newMails as $email){
                            $tz = 'America/Mexico_City';
                            $timestamp = time();
                            $dt = new DateTime("now", new DateTimeZone($tz));
                            $dt->setTimestamp($timestamp);
                            $now = Carbon\Carbon::parse($dt->format('Y-m-d H:i:s'));
                            $end = $email->created_at;
                            $end = Carbon\Carbon::parse($end);
                            $diffInHours = $end->diffInDays($now);
                            if ($diffInHours <= 7){
                                $lastMails[] = $email;
                            }
                        }
                    @endphp
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i>
                            <span class="label label-success">{{ count($lastMails) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('top.you_have') }} {{ count($lastMails) }} {{ (count($lastMails) > 1)?trans('top.messages'):trans('top.message') }}</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @foreach ($lastMails as $mail)
                                        <li>
                                            <a class="task-notification bg-green" href="{{ route('responseMail', $mail->id) }}">
                                                <i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;{{ $mail->subject }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('dashboard') }}">{{ trans('top.view_all_b') }}</a>
                            </li>
                        </ul>
                    </li><!-- /.messages-menu -->

                    <!-- Reminders Calls Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-clock-o"></i> <i class="fa fa-phone"></i>
                            <span class="label label-info">{{ count($calls) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('top.you_have') }} {{ count($calls) }} {{ (count($calls) > 1)?trans('top.calls'):trans('top.call') }}</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @foreach ($calls as $call)
                                        <li>
                                            <a class="task-notification {{ ($call->overdue == 0)?'bg-green':'bg-red' }}" href="{{ route('reminderEdit', [$call->type, $call->client_id, $call->id]) }}">
                                                {{ $call->subject }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('reminders', 'Call') }}">{{ trans('top.view_all_a') }}</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Reminders Mails Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-clock-o"></i> <i class="fa fa-envelope"></i>
                            <span class="label label-info">{{ count($mails) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('top.you_have') }} {{ count($mails) }} {{ (count($mails) > 1)?trans('top.mails'):trans('top.mail') }}</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @foreach ($mails as $mail)
                                        <li>
                                            <a class="task-notification {{ ($mail->overdue == 0)?'bg-green':'bg-red' }}" href="{{ route('reminderEdit', [$mail->type, $mail->client_id, $mail->id]) }}">
                                                {{ $mail->subject }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('reminders', 'Mail') }}">{{ trans('top.view_all_b') }}</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Reminders Showings Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-clock-o"></i> <i class="fa fa-home"></i>
                            <span class="label label-info">{{ count($showings) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('top.you_have') }} {{ count($showings) }} {{ (count($showings) > 1)?trans('top.showings'):trans('top.showing') }}</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @foreach ($showings as $showing)
                                        <li>
                                            <a class="task-notification {{ ($showing->overdue == 0)?'bg-green':'bg-red' }}" href="{{ route('reminderEdit', [$showing->type, $showing->client_id, $showing->id]) }}">
                                                {{ $showing->subject }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('reminders', 'Showing') }}">{{ trans('top.view_all_b') }}</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Tasks Menu -->
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-thumb-tack"></i>
                            <span class="label label-danger">{{ App\Models\Tasks::where('accomplished', 0)->count() }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('top.you_have') }} {{ App\Models\Tasks::where('accomplished', 0)->count() }} {{ (App\Models\Tasks::where('accomplished', 0)->count() > 1)?trans('top.tasks'):trans('top.task') }}</li>
                            <li>
                                <!-- Inner menu: contains the tasks -->
                                <ul class="menu">
                                    @foreach (App\Models\Tasks::where('accomplished', 0)->get() as $task)
                                        <li>
                                            <a class="task-notification bg-red" href="{{ route('taskEdit', $task->id) }}">
                                                {{ $task->subject }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('tasks') }}">{{ trans('top.view_all_a') }}</a>
                            </li>
                        </ul>
                    </li>
                @endrole
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu" style="max-width: 280px;white-space: nowrap;">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="max-width: 280px;white-space: nowrap;overflow: hidden;overflow-text: ellipsis">
                            <!-- The user image in the navbar-->
                            <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                                <p>
                                    <span data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</span>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown link-lang">
                    @foreach (Config::get('languages') as $lang => $language)
                        @if ($lang != App::getLocale())
                            <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                        @endif
                    @endforeach
                </li>
            </ul>
        </div>
    </nav>
</header>
