<table>
    <thead>
    <tr>
        <th>{{ trans('export.name') }}</th>
        <th>{{ trans('export.last_name') }}</th>
        <th>{{ trans('export.client_key') }}</th>
        <th>{{ trans('export.main_email') }}</th>
        <th>{{ trans('export.secondary_email') }}</th>
        <th>{{ trans('export.main_mobile') }}</th>
        <th>{{ trans('export.secondary_mobile') }}</th>
        <th>{{ trans('export.home_phone') }}</th>
        <th>{{ trans('export.office_phone') }}</th>
        <th>{{ trans('export.country') }}</th>
        <th>{{ trans('export.state') }}</th>
        <th>{{ trans('export.city') }}</th>
        <th>{{ trans('export.street') }}</th>
        <th>{{ trans('export.number_int') }}</th>
        <th>{{ trans('export.number_ext') }}</th>
        <th>{{ trans('export.zipcode') }}</th>
        <th>{{ trans('export.sex') }}</th>
        <th>{{ trans('export.user') }}</th>
        <th>{{ trans('export.contact_date') }}</th>
        <th>{{ trans('export.contact_way') }}</th>
        <th>{{ trans('export.main_source') }}</th>
        <th>{{ trans('export.secondary_source') }}</th>
        <th>{{ trans('export.contact_medium') }}</th>
        <th>{{ trans('export.main_medium_source') }}</th>
        <th>{{ trans('export.secondary_medium_source') }}</th>
        <th>{{ trans('export.interest') }}</th>
        <th>{{ trans('export.level_interest') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        @if($client->gender_id ==0)
            <?php $gender = 'Hombre'; ?>
        @elseif($client->gender_id == 1)
            <?php $gender = 'Mujer'; ?>
        @else
            <?php $gender = 'LGBT'; ?>
        @endif
        <?php
            $marketing  = $client->marketingInformation;
            $contactWay = \App\Models\ContactWays::where('id', $marketing->contact_way_id)->first();
            $contactMedium  = \App\Models\ContactMediums::where('id', $marketing->contact_medium_id)->first();
            $path_model = 'App\Models\Sources\\';
            if($contactWay->is_multi){
                $multiSourcesModel = strtolower($contactWay->model);
                $multiSources = $marketing->$multiSourcesModel()->get();
            }else{
                $multiSources = NULL;
            }
            if($contactMedium->is_multi){
                $multiMediumsModel = 'mediums_'.strtolower($contactMedium->model);
                $multiMediums = $marketing->$multiMediumsModel()->get();
            }else{
                $multiMediums = NULL;
            }
            if(!is_null($marketing->main_source_id)){
                $mainModel     = \App\Models\AcquisitionsMainSources::where('contact_way_id', $marketing->contact_way_id)->orderBy('created_at', 'desc')->first();
                $mainModelName = $path_model.$mainModel->model;
                $mainSource    = $mainModelName::where('id', $marketing->main_source_id)->orderBy('created_at', 'desc')->first();
                if(!is_null($mainSource)) {
                    $mainSource = $mainSource;
                }else{
                    $mainSource = 0;
                }
            }else{
                $mainSource = 0;
            }
            if(!is_null($marketing->main_medium_source_id)){
                $mainModelMedium     = \App\Models\AcquisitionsMainMediums::where('contact_way_id', $marketing->contact_medium_id)->orderBy('created_at', 'desc')->first();
                $mainModelNameMedium = $path_model.$mainModelMedium->model;
                $mainMedium          = $mainModelNameMedium::where('id', $marketing->main_medium_source_id)->orderBy('created_at', 'desc')->first();
                if(!is_null($mainMedium)) {
                    $mainMedium = $mainMedium;
                }else{
                    $mainMedium = 0;
                }
            }else{
                $mainMedium = 0;
            }
            if(!is_null($marketing->secondary_source_id)){
                $secondaryModel     = \App\Models\AcquisitionsSecondarySources::where('id', $mainModel->secondary_id)->orderBy('created_at', 'desc')->first();
                $secondaryModelName = $path_model.$secondaryModel->model;
                $secondarySource    = $secondaryModelName::where('id', $marketing->secondary_source_id)->orderBy('created_at', 'desc')->first();
            }else{
                $secondarySource = 0;
            }
            if(!is_null($marketing->secondary_medium_source_id)){
                $secondaryModelMedium     = \App\Models\AcquisitionsSecondaryMediums::where('id', $mainModelMedium->secondary_id)->orderBy('created_at', 'desc')->first();
                $secondaryModelNameMedium = $path_model.$secondaryModelMedium->model;
                $secondaryMedium          = $secondaryModelNameMedium::where('id', $marketing->secondary_medium_source_id)->orderBy('created_at', 'desc')->first();
            }else{
                $secondaryMedium = 0;
            }
        ?>
        <tr>
            <td>{{ $client->name }}</td>
            <td>{{ $client->last_name }}</td>
            <td>{{ $client->client_key }}</td>
            <td>{{ $client->main_email }}</td>
            <td>{{ $client->secondary_email }}</td>
            <td>{{ $client->main_mobile }}</td>
            <td>{{ $client->secondary_mobile }}</td>
            <td>{{ $client->home_phone }}</td>
            <td>{{ $client->office_phone }}</td>
            @if(count($client->countryName()))
                <td>{{ $client->countryName->name }}</td>
            @else
                <td></td>
            @endif
            @if(count($client->stateName()))
                <td>{{ $client->stateName->name }}</td>
            @else
                <td></td>
            @endif
            @if(count($client->cityName()) && $client->cityName != NULL)
                <td>{{ $client->cityName->name }}</td>
            @else
                <td></td>
            @endif
            <td>{{ $client->street }}</td>
            <td>{{ $client->number_int }}</td>
            <td>{{ $client->number_ext }}</td>
            <td>{{ $client->zipcode }}</td>
            <td>{{ $gender }}</td>
            @if(count($client->user()))
                <td>{{ $client->user->name }}</td>
            @else
                <td></td>
            @endif
            <td>{{ $client->first_contact }}</td>
            <td>{{ $marketing->getContactWay->name }}</td>
            <td>
                @if(!is_null($mainSource))
                    @if($marketing->getContactWay->is_multi)
                        @if(!is_null($multiSources))
                            @foreach ($multiSources as $multiSource)
                                {{ $multiSource->name }},
                            @endforeach
                        @endif
                    @else
                        {{ $mainSource->name }}
                    @endif
                @endif
            </td>
            @if($secondarySource)
                <td>{{ $secondarySource->name }}</td>
            @else
                <td></td>
            @endif
            <td>{{ $marketing->getContactMedium->name }}</td>
            <td>
                @if(!is_null($mainMedium))
                    @if($marketing->getContactMedium->is_multi)
                        @if(!is_null($multiMediums))
                            @foreach ($multiMediums as $multiMedium)
                                {{ $multiMedium->name }},
                            @endforeach
                        @endif
                    @else
                        {{ $mainMedium->name }}
                    @endif
                @endif
            </td>
            @if($secondaryMedium)
                <td>{{ $secondaryMedium->name }}</td>
            @else
                <td></td>
            @endif
            <td>{{ $marketing->getInterest->name }}</td>
            <td>{{ $marketing->level_interest_id }}</td>
        </tr>
    @endforeach
    </tbody>
</table>