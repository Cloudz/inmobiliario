<table>
    <thead>
    <tr>
        <th>{{ trans('export.name') }}</th>
        <th>{{ trans('export.description') }}</th>
        <th>{{ trans('export.address') }}</th>
        <th>{{ trans('export.baths') }}</th>
        <th>{{ trans('export.beds') }}</th>
        <th>{{ trans('export.parking') }}</th>
        <th>{{ trans('export.m2') }}</th>
        <th>{{ trans('export.ft2') }}</th>
        <th>{{ trans('export.type') }}</th>
        <th>{{ trans('export.type_id') }}</th>
        <th>{{ trans('export.development_id') }}</th>
        <th>{{ trans('export.location_id') }}</th>
        <th>{{ trans('export.style_id') }}</th>
        <th>{{ trans('export.amenities') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($properties as $property)
        <tr>
            <td>{{ $property->name }}</td>
            <td>{{ $property->description }}</td>
            <td>{{ $property->address }}</td>
            <td>{{ $property->baths }}</td>
            <td>{{ $property->beds }}</td>
            <td>{{ $property->parking }}</td>
            <td>{{ $property->m2 }}</td>
            <td>{{ $property->ft2 }}</td>
            <td>{{ $property->type }}</td>
            <td>{{ $property->getType->name }}</td>
            <td>{{ $property->getDevelopment->name }}</td>
            <td>{{ $property->getLocation->name }}</td>
            <td>{{ $property->getStyle->name }}</td>
            <td>
                @foreach($property->amenities()->get() as $amenity)
                    {{ $amenity->name }}<br>
                @endforeach
            </td>
        </tr>
    @endforeach
    </tbody>
</table>