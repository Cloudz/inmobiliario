@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Editar Medio de Contacto
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('contactMediums') }}"><i class="fa fa-undo"></i> Volver al Listado de Medios de Contacto</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionMainSourcesCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Principal</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionSecondarySourcesCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Secundario</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">EDITAR MEDIO DE CONTACTO</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('contactMediumsUpdate', $medium->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">Nombre*</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $medium->name }}" required="required">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="description">Descripción</label>
                            <textarea rows="12" class="form-control" id="description" name="description">{{ $medium->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                        <label for="main_source">Selecciona el Recurso Principal dependiente si es que aplica</label>
                        <select class="form-control" id="main_source" name="main_source" required="required">
                            <option value="">Selecciona Recurso Principal ...</option>
                            @foreach ($mainMediums as $main)
                                <option value="{{ $main->id }}" {{ ($medium->main_source_id == $main->id) ? 'selected' : '' }}>{{ $main->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                        <label for="model">Selecciona un Recurso al que pertenece si es que aplica</label>
                        <select class="form-control" id="model" name="model">
                            <option value="">Selecciona Recurso ...</option>
                            @foreach ($models as $model)
                                <option value="{{ $model }}" {{ ($medium->model == $model) ? 'selected' : '' }}>{{ $model }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="container-complete form-group col-xs-12 col-md-12 col-lg-2 c-padding-left-input">
                        <input type="checkbox" id="multi_select" name="multi_select" {{ ($medium->is_multi) ? 'checked' : '' }}>
                        <label for="multi_select" class="bg-green">  Es de selección Multiple</label>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Actualizar Recurso</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection