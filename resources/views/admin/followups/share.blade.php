@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Colaboración
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('followups') }}"><i class="fa fa-undo"></i> Volver al Listado de Seguimientos</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">CREAR COLABORACIÓN - CLIENTE: <a href="{{ route('clientView', $followup->client_id) }}">{{ $followup->client->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('followupShareStore', $followup->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                        <label for="assign_agents">Selecciona Agentes para Colaborar</label>
                        <select class="form-control" multiple="multiple" id="assign_agents" name="agents[]" required="required">
                            @foreach ($agents as $agent)
                                <option value="{{ $agent->id }}" <?=($followup->users->contains($agent->id)) ? 'selected' : ''?>>{{ $agent->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Enviar Colaboración</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection