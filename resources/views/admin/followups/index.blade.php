@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Seguimientos
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN SEGUIMIENTOS - <i class="fa fa-th-list"></i> {{ $followups->total() }}</h1>
        </div>
        <div class="main-head-actions">
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('followups') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('followupSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Seguimiento..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $followups->appends(['search' => $search])->render() }}
            @else
                {{ $followups->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>STATUS @sortablelink('status', trans('global.orderby'))</th>
                            <th>FOLIO @sortablelink('folio', trans('global.orderby'))</th>
                            <th>TIPO @sortablelink('type', trans('global.orderby'))</th>
                            <th>CLIENTE</th>
                            <th>AGENTE</th>
                            <th>COLABORADORES</th>
                            @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                            <th>ACCIONES</th>
                            @endrole
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($followups)>0)
                            @foreach($followups as $followup)
                                <?php
                                    switch ($followup->status){
                                        case 1:
                                            $status    = 'Abierta';
                                            $bg_status = 'bg-aqua';
                                        break;
                                        case 2:
                                            $status    = 'Seguimiento';
                                            $bg_status = 'bg-yellow';
                                        break;
                                        case 3:
                                            $status    = 'Cerrada';
                                            $bg_status = 'bg-maroon';
                                        break;
                                        default:
                                            $status    = 'Abierta';
                                            $bg_status = 'bg-aqua';
                                        break;
                                    }
                                ?>
                                <tr role="row">
                                    <td style="width: 5%"><div class="legend-status {{ $bg_status }}"> {{ $status }}</div></td>
                                    <td style="width: 10%">{{ $followup->folio }}</td>
                                    <td style="width: 10%">{{ $followup->type }}</td>
                                    <td style="width: 10%"><a href="{{ route('clientView', $followup->client_id) }}">{{ $followup->client->name }}</a></td>
                                    <td style="width: 10%">
                                        @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker']))
                                            <a href="{{ route('userView', $followup->user->id) }}">{{ $followup->user->name }}</a><br>
                                        @else
                                            {{ $followup->user->name }}<br>
                                        @endif
                                    </td>
                                    <td style="width: 10%">
                                        @foreach($followup->users as $collaborator)
                                            @if(Auth::user()->hasRole(['Administrator', 'Developer', 'Broker']))
                                                <a href="{{ route('userView', $collaborator->id) }}">{{ $collaborator->name }}</a><br>
                                            @else
                                                {{ $collaborator->name }}<br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                                        <td style="width: 10%">
                                                @if($followup->status != 1)
                                                    <a href="{{ route('followupStatusOpen', [$followup->id, 'followups', 0]) }}" class="change_status btn edit_btn btn-xs bg-aqua" title="Abrir" alt="Abrir">
                                                        <i class="fa fa-unlock"></i> Abrir
                                                    </a>
                                                @endif
                                                @if($followup->status != 2)
                                                    <a href="{{ route('followupStatusProcess', [$followup->id, 'followups', 0]) }}" class="change_status btn delete_btn btn-xs bg-yellow" title="Seguir" alt="Seguir">
                                                        <i class="fa fa-paper-plane"></i> Seguir
                                                    </a>
                                                @endif
                                                @if($followup->status != 3)
                                                    <a href="{{ route('followupStatusClose', [$followup->id, 'followups', 0]) }}" class="change_status btn delete_btn btn-xs bg-maroon" title="Cerrar" alt="Cerrar">
                                                        <i class="fa fa-lock"></i> Cerrar
                                                    </a>
                                                @endif
                                                <a href="{{ route('followupShare', [$followup->user_id, $followup->id])}}" type="button" class="btn edit_btn btn-xs" title="Compartir" alt="Compartir">
                                                    <i class="fa fa-share"></i>
                                                </a>
                                                <a href="{{ route('followupDelete', $followup->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                    <i class="fa fa-fw fa-remove"></i>
                                                </a>
                                        </td>
                                    @endrole
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>STATUS @sortablelink('status', trans('global.orderby'))</th>
                            <th>FOLIO @sortablelink('folio', trans('global.orderby'))</th>
                            <th>TIPO @sortablelink('type', trans('global.orderby'))</th>
                            <th>CLIENTE</th>
                            <th>AGENTE</th>
                            <th>COLABORADORES</th>
                            @role(['Administrator', 'Developer', 'Broker', 'Sales Agent'])
                            <th>ACCIONES</th>
                            @endrole
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $followups->appends(['search' => $search])->render() }}
        @else
            {{ $followups->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $(function() {

            $('.change_status').on('click', function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: {},
                    beforeSend: function() {
                        $('#loader').fadeIn();
                    },
                    success: function(res){
                        if(res.success){
                            if(res.redirect){
                                document.location = res.redirect;
                            }
                        }else if(res.errors){
                            var errElem = $('<ul></ul>');

                            for(var name in res.errors){
                                var errors = res.errors[name];
                                //errElem.append($('<li>' + name + ': ' + res.errors[name][0] + '</li>'));
                                errElem.append($('<li>' + res.errors[name][0] + '</li>'));
                            }
                            var errorTemplate = '<div class="alert alert-danger"><ul>' + errElem.html() + '</ul></div>';
                            $.bootstrapGrowl(errorTemplate, {
                                ele: 'body',
                                type: 'null',
                                offset: {from: 'top', amount: 40},
                                align: 'right',
                                width: 'auto',
                                delay: 3000,
                                allow_dismiss: true,
                                stackup_spacing: 5
                            });
                        }
                        proccessing_form = false;
                    },
                    error: function (data) {
                        $.bootstrapGrowl('Ha ocurrido un error, revise su conexion e intentelo nuevamente', {
                            ele: 'body',
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 40},
                            align: 'right',
                            width: 'auto',
                            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: false,
                            stackup_spacing: 5
                        });
                        proccessing_form = false;
                        $('#loader').remove();
                    },
                    complete: function() {
                        $('#loader').fadeOut();
                    },
                });
            });

        });
    </script>
@endsection