@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Registros de Marketing
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN MARKETING - <i class="fa fa-th-list"></i> {{ $marketings->total() }}</h1>
        </div>
        <div class="main-head-actions">
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('marketing') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <div class="col-xs-12">
            {{ $marketings->links() }}
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>CLIENTE</th>
                            <th>FORMA DE CONTACTO</th>
                            <th>RECURSO PRINCIPAL</th>
                            <th>RECURSO SECUNDARIO</th>
                            <th>MEDIO DE CONTACTO</th>
                            <th>MEDIO PRINCIPAL</th>
                            <th>MEDIO SECUNDARIO</th>
                            <th>INTERÉS</th>
                            <th>NIVEL DE INTERÉS</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($marketings)>0)
                            @foreach($redoMarketings as $marketing)
                                <tr role="row">
                                    <td style="width: 10%"><a href="{{ route('clientView', $marketing->getClient->id) }}">{{ $marketing->getClient->name }}</a></td>
                                    <td style="width: 10%">{{ $marketing->getContactWay->name }}</td>
                                    <td style="width: 10%">
                                        @if(!is_null($marketing->mainSource))
                                            @if($marketing->getContactWay->is_multi)
                                                @if(!is_null($marketing->multiSources))
                                                    @foreach ($marketing->multiSources as $multiSource)
                                                        {{ $multiSource->name }},
                                                    @endforeach
                                                @endif
                                            @else
                                                {{ $marketing->mainSource->name }}
                                            @endif
                                        @endif
                                    </td>
                                    <td style="width: 10%">
                                        @if(!is_null($marketing->secondarySource) && ($marketing->secondarySource != 0))
                                            {{ $marketing->secondarySource->name }}
                                        @endif
                                    </td>
                                    <td style="width: 10%">{{ $marketing->getContactMedium->name }}</td>
                                    <td style="width: 10%">
                                        @if(!is_null($marketing->mainMedium))
                                            @if($marketing->getContactMedium->is_multi)
                                                @if(!is_null($marketing->multiMediums))
                                                    @foreach ($marketing->multiMediums as $multiMediums)
                                                        {{ $multiMediums->name }},
                                                    @endforeach
                                                @endif
                                            @else
                                                {{ $marketing->mainMedium->name }}
                                            @endif
                                        @endif
                                    </td>
                                    <td style="width: 10%">
                                        @if(!is_null($marketing->secondarySource) && ($marketing->secondarySource != 0))
                                            {{ $marketing->secondarySource->name }}
                                        @endif
                                    </td>
                                    <td style="width: 10%">{{ $marketing->getInterest->name }}</td>
                                    <td style="width: 10%">
                                        <?php
                                        switch($marketing->level_interest_id){
                                            case 1:
                                                $status = "Poco Interesado";
                                                break;
                                            case 2:
                                                $status = "Interesado";
                                                break;
                                            case 3:
                                                $status = "Muy Interesado";
                                                break;
                                            case 4:
                                                $status = "Urgente";
                                                break;
                                        }
                                        ?>
                                        @for ($i = 0; $i < 4; ++$i)
                                            <i class="fa fa-star{{ $marketing->level_interest_id<=$i?'-o':'' }}" aria-hidden="true"></i>
                                        @endfor
                                        {{ $status }}
                                    </td>
                                    <td>EXPORTAR</td>
                                </tr>
                            @endforeach
                        @else
                            <tr role="row">
                                <td style="width: 100%" colspan="7">No se encontraron registros</td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>CLIENTE</th>
                            <th>FORMA DE CONTACTO</th>
                            <th>RECURSO PRINCIPAL</th>
                            <th>RECURSO SECUNDARIO</th>
                            <th>MEDIO DE CONTACTO</th>
                            <th>MEDIO PRINCIPAL</th>
                            <th>MEDIO SECUNDARIO</th>
                            <th>INTERÉS</th>
                            <th>NIVEL DE INTERÉS</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
          {{ $marketings->links() }}
      </div>
      <!-- /.row -->
    </section>
@endsection
