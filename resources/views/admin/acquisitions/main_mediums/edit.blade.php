@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Editar Recurso Principal
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionMainMediums') }}"><i class="fa fa-undo"></i> Volver al Listado de Recursos Principales</a>
            <a class="btn action_btn btn-xs" href="{{ route('contactMediumsCreate') }}"><i class="fa fa-plus"></i> Agregar Medio de Contacto</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionSecondaryMediumsCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Secundario</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">EDITAR RECURSO PRINCIPAL</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form id="form_update" action="{{ route('acquisitionMainMediumsUpdate', $mainSource->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">Nombre*</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $mainSource->name }}" required="required">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="description">Descripción</label>
                            <textarea rows="12" class="form-control" id="description" name="description">{{ $mainSource->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="contact_way">Selecciona la Forma de contacto a la que pertenece</label>
                            <select class="form-control" id="contact_way" name="contact_way" required="required">
                                <option value="">Selecciona Medio de Contacto ...</option>
                                {{$mainSource->source_id}}
                                @foreach ($contactMediums as $way)
                                    <option value="{{ $way->id }}" {{ ($mainSource->contact_way_id == $way->id) ? 'selected' : '' }}>{{ $way->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="model">Selecciona el Modelo al que pertenece si es que aplica{{ ($mainSource->model)?' - Actual: '.$mainSource->model:'' }}</label>
                            <select class="form-control" id="model" name="model" required="required">
                                <option value="">Selecciona Modelo ...</option>
                                @foreach ($models as $model)
                                    <option value="{{ $model }}" {{ ($mainSource->model == $model) ? 'selected' : '' }}>{{ $model }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="secondary">Selecciona un Recurso Secundario, Ejemplo: Si es "Agencias" su hijo sera "Agentes" si es que aplica</label>
                            <select class="form-control" id="secondary" name="secondary">
                                <option value="">Selecciona Recurso Hijo...</option>
                                @foreach ($secondarySources as $secondary)
                                    <option value="{{ $secondary->id }}" {{ ($mainSource->secondary_id == $secondary->id) ? 'selected' : '' }}>{{ $secondary->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Actualizar Recurso</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection