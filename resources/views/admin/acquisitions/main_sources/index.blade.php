@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Mis Recursos Principales
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>SECCIÓN RECURSOS PRINCIPALES- <i class="fa fa-th-list"></i> {{ $mainSources->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('contactWaysCreate') }}"><i class="fa fa-plus"></i> Agregar Forma de Contacto</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionMainSourcesCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Principal</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionSecondarySourcesCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Secundario</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('acquisitionMainSources') }}"><i class="fa fa-eraser"></i> Limpiar Búsqueda</a>
            @endif
        </div>
        <form action="{{ route('acquisitionMainSourcesSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="Buscar Recurso..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="Buscar" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $mainSources->appends(['search' => $search])->render() }}
            @else
                {{ $mainSources->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>DESCRIPCIÓN @sortablelink('description', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($mainSources)>0)
                            <tr>
                                @foreach($mainSources as $main)
                                    <tr role="row">
                                        <td style="width: 10%">{{ $main->name }}</td>
                                        <td style="width: 10%">{{ $main->description }}</td>
                                        <td style="width: 10%">
                                            <a href="{{ route('acquisitionMainSourcesEdit', $main->id) }}" class="btn edit_btn btn-xs" title="Editar" alt="Editar">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('acquisitionMainSourcesDelete', $main->id) }}" type="button" class="delete-trigger btn delete_btn btn-xs" title="Eliminar" alt="Eliminar">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">No se encontraron registros</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>NOMBRE @sortablelink('name', trans('global.orderby'))</th>
                            <th>DESCRIPCIÓN @sortablelink('description', trans('global.orderby'))</th>
                            <th>ACCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $mainSources->appends(['search' => $search])->render() }}
        @else
            {{ $mainSources->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
