@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Recurso Secundario
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionSecondaryMediums') }}"><i class="fa fa-undo"></i> Volver al Listado de Recursos</a>
            <a class="btn action_btn btn-xs" href="{{ route('contactMediumsCreate') }}"><i class="fa fa-plus"></i> Agregar Medio de Contacto</a>
            <a class="btn action_btn btn-xs" href="{{ route('acquisitionMainMediumsCreate') }}"><i class="fa fa-plus"></i> Agregar Recurso Principal</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">CREAR RECURSO SECUNDARIO</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('acquisitionSecondaryMediumsStore') }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="name">Nombre*</label>
                            <input type="text" class="form-control" id="name" name="name" required="required">
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-12 col-lg-12 c-padding-left-input">
                            <label for="description">Descripción</label>
                            <textarea rows="12" class="form-control" id="description" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6 c-padding-left-input">
                            <label for="model">Selecciona el tipo de Recurso</label>
                            <select class="form-control" id="model" name="model" required="required">
                                <option value="">Selecciona Recurso ...</option>
                                @foreach ($models as $model)
                                    <option value="{{ $model }}">{{ $model }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">Agregar Recurso</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection