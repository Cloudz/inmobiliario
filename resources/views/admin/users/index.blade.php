@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('users.index_title') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="box-title-custom">
            <h1>{{ trans('users.index_section') }} - <i class="fa fa-th-list"></i> {{ $users->total() }}</h1>
        </div>
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('userCreate') }}"><i class="fa fa-plus"></i> {{ trans('users.add_user') }}</a>
            @if(isset($_GET['search']))
                <a class="btn action_btn btn-xs" href="{{ route('users') }}"><i class="fa fa-eraser"></i> {{ trans('users.clean_search') }}</a>
            @endif
        </div>
        <form action="{{ route('userSearch') }}" class="c-form-search" method="get">
            <div class="input-group input-group-md">
                <input type="text" class="form-control" placeholder="{{ trans('users.search_user') }}..." name="search" value="{{ (isset($_GET['search'])) ? $_GET['search'] : '' }}">
                <span class="input-group-btn">
                    <input type="submit" class="c-btn-search btn btn-primary" value="{{ trans('global.search') }}" >
                </span>
            </div>   
        </form>
        <div class="col-xs-12">
            @if(isset($_GET['search']))
                {{ $users->appends(['search' => $search])->render() }}
            @else
                {{ $users->links() }}
            @endif
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>{{ trans('users.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('users.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                            <th>{{ trans('users.email') }} @sortablelink('email', trans('global.orderby'))</th>
                            <th>{{ trans('users.mobile') }}</th>
                            <th>{{ trans('users.index_roles') }}</th>
                            <th>{{ trans('users.index_assisted') }}</th>
                            <th>{{ trans('users.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($users)>0)
                            <tr>
                                @foreach($users as $user)
                                    <tr role="row">
                                        <td style="width: 10%">{{ $user->name }}</td>
                                        <td style="width: 10%">{{ $user->last_name }}</td>
                                        <td style="width: 10%">{{ $user->email }}</td>
                                        <td style="width: 10%">{{ $user->mobile }}</td>
                                        <td style="width: 10%">
                                            @foreach($user->roles as $role)
                                                <div class="role-user">{{ $role->name }}</div>
                                            @endforeach
                                        </td>
                                        <td style="width: 10%">
                                            @foreach($user->users as $agent)
                                                @if(Auth::user()->hasRole(['Administrator', 'Developer']))
                                                    <a href="{{ route('userView', $agent->id) }}">{{ $agent->name }}</a><br>
                                                @else
                                                    {{ $agent->name }}<br>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td style="width: 10%">
                                            @role(['Administrator', 'Developer'])
                                                @if($user->hasRole(['Personal Assistant']))
                                                    <a href="{{ route('assist', $user->id) }}" class="btn edit_btn btn-xs bg-aqua" title="{{ trans('global.assist') }}" alt="{{ trans('global.assist') }}">
                                                        <i class="fa fa-refresh"></i>
                                                    </a>
                                                @endif
                                            @endrole
                                            <a href="{{ route('userView', $user->id) }}" class="btn edit_btn btn-xs" title="{{ trans('global.view') }}" alt="{{ trans('global.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('userDelete', $user->id)}}" type="button" class="delete-trigger btn delete_btn btn-xs" title="{{ trans('global.delete') }}" alt="{{ trans('global.delete') }}">
                                                <i class="fa fa-fw fa-remove"></i>
                                            </a>
                                            @if($user->is_active)
                                                <a href="{{ route('userDeactivate', $user->id) }}" class="change_status btn delete_btn btn-xs bg-yellow" title="{{ trans('global.deactivate') }}" alt="{{ trans('global.deactivate') }}">
                                                    <i class="fa fa-bell-slash"></i> {{ trans('global.deactivate') }}
                                                </a>
                                            @endif
                                            @if(!$user->is_active)
                                                <a href="{{ route('userActive', $user->id) }}" class="change_status btn edit_btn btn-xs bg-aqua" title="{{ trans('global.activate') }}" alt="{{ trans('global.activate') }}">
                                                    <i class="fa fa-bell"></i> {{ trans('global.activate') }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        @else
                            <tr>
                                <tr role="row">
                                    <td style="width: 100%" colspan="7">{{ trans('global.not_found') }}</td>
                                </tr>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('users.index_name') }} @sortablelink('name', trans('global.orderby'))</th>
                            <th>{{ trans('users.index_last_name') }} @sortablelink('last_name', trans('global.orderby'))</th>
                            <th>{{ trans('users.email') }} @sortablelink('email', trans('global.orderby'))</th>
                            <th>{{ trans('users.mobile') }}</th>
                            <th>{{ trans('users.index_roles') }}</th>
                            <th>{{ trans('users.index_assisted') }}</th>
                            <th>{{ trans('users.actions') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        @if(isset($_GET['search']))
            {{ $users->appends(['search' => $search])->render() }}
        @else
            {{ $users->links() }}
        @endif
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('inyectable-styles')
    <script>
        $(function() {

            $('.change_status').on('click', function(e){
                console.log($(this).attr('href'));
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: {},
                    beforeSend: function() {
                        $('#loader').fadeIn();
                    },
                    success: function(res){
                        if(res.success){
                            if(res.redirect){
                                document.location = res.redirect;
                            }
                        }else if(res.errors){
                            var errElem = $('<ul></ul>');

                            for(var name in res.errors){
                                var errors = res.errors[name];
                                //errElem.append($('<li>' + name + ': ' + res.errors[name][0] + '</li>'));
                                errElem.append($('<li>' + res.errors[name][0] + '</li>'));
                            }
                            var errorTemplate = '<div class="alert alert-danger"><ul>' + errElem.html() + '</ul></div>';
                            $.bootstrapGrowl(errorTemplate, {
                                ele: 'body',
                                type: 'null',
                                offset: {from: 'top', amount: 40},
                                align: 'right',
                                width: 'auto',
                                delay: 3000,
                                allow_dismiss: true,
                                stackup_spacing: 5
                            });
                        }
                        proccessing_form = false;
                    },
                    error: function (data) {
                        $.bootstrapGrowl('Ha ocurrido un error, revise su conexion e intentelo nuevamente', {
                            ele: 'body',
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 40},
                            align: 'right',
                            width: 'auto',
                            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: false,
                            stackup_spacing: 5
                        });
                        proccessing_form = false;
                        $('#loader').remove();
                    },
                    complete: function() {
                        $('#loader').fadeOut();
                    },
                });
            });

        });
    </script>
@endsection
