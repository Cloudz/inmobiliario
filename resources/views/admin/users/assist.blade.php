@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('users.assign_user') }}
@endsection

@section('main-content')
    <section class="content">
      <div class="row">
        <div class="main-head-actions">
            <a class="btn action_btn btn-xs" href="{{ route('users') }}"><i class="fa fa-undo"></i> {{ trans('users.back_list') }}</a>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ trans('users.current_user') }}: <a href="{{ route('userView', $user->id) }}">{{ $user->name }}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body tabs-menu-wrapper">
                <form action="{{ route('assistUpdate', $user->id) }}" method="post" class="ajax-form">
                    {{csrf_field()}}
                    <input type="hidden" name="redirect_id" value="">
                    <div class="form-row column-form">
                        <div class="form-group col-xs-12 c-padding-left-input">
                            <label for="assist_agents">{{ trans('users.select_agent') }}</label>
                            <select class="form-control" multiple="multiple" id="assist_agents" name="agents[]" required="required">
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}" <?=($user->users->contains($agent->id)) ? 'selected' : ''?>>{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn save_btn pull-right">{{ trans('users.assist_agent') }}</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection