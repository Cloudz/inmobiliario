@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('users.create_title') }}
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="main-head-actions">
                <a class="btn action_btn btn-xs" href="{{ route('users') }}"><i class="fa fa-undo"></i> {{ trans('users.back_list') }}</a>
                <a id="btn_edit_form" class="btn edit_btn_large btn-xs" href="#"><i class="fa fa-pencil"></i> {{ trans('users.enable_edition') }}</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('users.title_edit') }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body tabs-menu-wrapper">
                        <form id="form_update" action="{{ route('userUpdate', $user->id) }}" method="post" class="ajax-form">
                            {{csrf_field()}}
                            <input type="hidden" name="redirect_id" value="">
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="name">{{ trans('users.name') }}*</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required="required" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="last_name">{{ trans('users.last_name') }}*</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}" required="required" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="email">{{ trans('users.view_email') }}*</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required="required" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-3 c-padding-left-input">
                                    <label for="mobile">{{ trans('users.view_mobile') }}</label>
                                    <input type="text" class="form-control" id="mobile" value="{{ $user->mobile }}" name="mobile" readonly>
                                </div>
                            </div>
                            <div class="form-row column-form">
                                <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                    <label for="password">{{ trans('users.password') }}</label>
                                    <input type="password" class="form-control" id="password" name="password" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-12 col-lg-4 c-padding-left-input">
                                    <label for="password_confirmation">{{ trans('users.verify_pass') }}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" readonly>
                                </div>
                                <div class="form-group col-xs-12 col-md-6 col-lg-4 c-padding-left-input">
                                    <label for="assign_roles">{{ trans('users.roles') }}</label>
                                    <select class="form-control" multiple="multiple" id="assign_roles" name="roles[]">
                                        @foreach ($roles as $role)
                                            {{ $user->role->contains($role->id) }}
                                            <option value="{{ $role->id }}" <?=($user->role->contains($role->id)) ? 'selected' : ''?>>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn save_btn pull-right">{{ trans('users.update_user') }}</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection