<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Horario',
    'back_list'      => 'Volver al listado de Horarios',
    'create_section' => 'CREAR HORARIO',

    //INDEX
    'index_title'     => 'Mis Horarios',
    'index_section'   => 'SECCIÓN HORARIOS',
    'create_schedule' => 'Crear Horario',

    'agent'    => 'AGENTE',
    'office'   => 'OFICINA',
    'start_at' => 'EMPIEZA',
    'end_at'   => 'TERMINA',
    'actions'  => 'ACCIONES',

    //CREATE
    'select_agent'     => 'Selecciona Agente',
    'select_office'    => 'Selecciona Oficina',
    'select_start_at'  => 'Empieza',
    'select_end_at'    => 'Termina',
    'comments'         => 'Comentarios',
    'add_schedule'     => 'Agregar Horario',
    'enable_edition'   => 'Habilitar Edición',
    'send_information' => 'Enviar Información',

];