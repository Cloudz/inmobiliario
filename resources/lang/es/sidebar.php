<?php

return [

    'users'     => 'USUARIOS',
    'sources'   => 'FUENTES',
        'contact_ways' => 'FORMAS DE CONTACTO',
        'main_sources'      => 'FUENTES PRINCIPALES',
        'secondary_sources' => 'FUENTES SECUNDARIOS',
    'mediums'   => 'MEDIOS',
        'contact_mediums' => 'MEDIOS DE CONTACTO',
        'main_mediums'      => 'MEDIOS PRINCIPALES',
        'secondary_mediums' => 'MEDIOS SECUNDARIOS',

    'followups' => 'SEGUIMIENTOS',
    'marketing' => 'MARKETING',
    'schedules' => 'HORARIOS DE GUARDIA',

    'dashboard'  => 'TABLERO',
    'clients'    => 'CLIENTES',
    'referrors'  => 'REFERIDORES',
    'agencies'   => 'AGENCIAS',
    'agents'     => 'AGENTES',
    'tasks'      => 'TAREAS',
    'reminders'  => 'RECORDATORIOS',
    'calls'      => 'LLAMADAS',
    'mails'      => 'CORREOS',
    'showings'   => 'SHOWINGS',
    'offices'    => 'OFICINAS',
    'logbook'    => 'LOGBOOK',
    'properties' => 'PROPIEDADES',
        'list_properties' => 'LISTADO',
        'developments'    => 'DESARROLLOS',
        'amenities'       => 'AMENIDADES',
        'locations'       => 'LOCACIONES',
        'types'           => 'TIPOS',
        'styles'          => 'ESTILOS',
];
