<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Usuario',
    'back_list'      => 'Volver al Listado de Usuarios',
    'create_section' => 'CREAR USUARIO',
    'previous'       => 'Anterior',
    'next'           => 'Siguiente',

    //INDEX
    'index_title'   => 'Usuarios',
    'index_section' => 'SECCIÓN USUARIOS',
    'add_user'      => 'Agregar Usuario',
    'clean_search'  => 'Limpiar Búsqueda',
    'search_user'   => 'Buscar Usuario',

    'index_name'      => 'NOMBRE',
    'index_last_name' => 'APELLIDO',
    'email'           => 'EMAIL',
    'mobile'          => 'CELULAR',
    'index_roles'     => 'ROLES',
    'index_assisted'  => 'AGENTES ASISTIDOS',
    'actions'         => 'ACCIONES',

    //VIEW
    'name'             => 'Nombre',
    'last_name'        => 'Apellido',
    'view_email'       => 'Email',
    'view_mobile'      => 'Teléfono',
    'password'         => 'Contraseña',
    'verify_pass'      => 'Verificar Contraseña',
    'roles'            => 'Roles',
    'send_information' => 'Enviar Información',
    'enable_edition'   => 'Habilitar Edición',
    'title_edit'       => 'EDITAR USUARIO',
    'update_user'      => 'Actualizar Usuario',

    //ASSIST CLIENTS
    'current_user' => 'Usuario',
    'assist_agent' => 'Asistir Agentes',
    'agents'       => 'Agentes',
    'select_agent' => 'Selecciona Agentes para asistir',
    'agent'        => 'AGENTE',

];
