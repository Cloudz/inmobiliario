<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Referidor',
    'back_list'      => 'Volver al Listado de Referidores',
    'create_section' => 'CREAR REFERIDOR',

    //INDEX
    'index_title'     => 'Mis Referridores',
    'index_section'   => 'SECCIÓN REFERIDORES',
    'create_referror' => 'Crear Referidor',
    'clean_search'    => 'Limpiar Búsqueda',
    'search_referror' => 'Buscar Referidor',

    'index_name'        => 'NOMBRE',
    'index_last_name'   => 'APELLIDO',
    'email'             => 'EMAIL',
    'phone'             => 'TELÉFONO',
    'index_occupation'  => 'OCUPACIÓN',
    'clients_referrors' => 'CLIENTES REFERIDOS',
    'actions'           => 'ACCIONES',

    //CREATE
    'user'              => 'Usuario',
    'select_user'       => 'Selecciona Usuario',

    //VIEW
    'view_title'        => 'Perfil de Referidor',
    'view_section'      => 'PERFIL DE REFERIDOR',
    'enable_edition'    => 'Habilitar Edición',
    'name'              => 'Nombre',
    'last_name'         => 'Apellido',
    'main_email'        => 'Email Principal',
    'secondary_email'   => 'Email Secundario',
    'home_phone'        => 'Tel. Casa',
    'office_phone'      => 'Tel. Oficina',
    'main_mobile'       => 'Cel. Principal',
    'secondary_mobile'  => 'Cel. Secundario',
    'sex'               => 'Sexo',
    'male'              => 'Masculino',
    'female'            => 'Femenino',
    'lgbt'              => 'LGBT',
    'nationality'       => 'Nacionalidad',
    'select_country'    => 'Selecciona País',
    'view_occupation'   => 'Ocupación',
    'address'           => 'Dirección',
    'related_customers' => 'Clientes Relacionados',
    'comments'          => 'Comentarios',
    'make_comment'      => 'Hacer Comentario',
    'send_information'  => 'Enviar Información',
    'add_referror'      => 'Agregar Referidor',

    //RE-ASSIGN CLIENTS
    'agent' => 'AGENTE',

];