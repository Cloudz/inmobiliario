<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Agencia',
    'back_list'      => 'Volver al Listado de Agencias',
    'create_section' => 'CREAR AGENCY',

    //INDEX
    'index_title'   => 'Mis Agencias',
    'index_section' => 'SECCIÓN AGENCIAS',
    'create_agency' => 'Crear Agencia',
    'create_agent'  => 'Crear Agente',
    'clean_search'  => 'Limpiar Búsqueda',
    'search_agency' => 'Buscar Agencia',

    'index_name'    => 'NOMBRE',
    'index_website' => 'WEBSITE',
    'phone'         => 'TELÉFONO',
    'city'          => 'CIUDAD',
    'actions'       => 'ACCIONES',

    //VIEW
    'view_title'       => 'Perfil de Agencia',
    'view_section'     => 'PERFIL DE AGENCIA',
    'title_agents'     => 'Agentes',
    'broker'           => 'Corredor',
    'agent'            => 'Agente de Ventas',
    'enable_edition'   => 'Habilitar Edición',
    'name'             => 'Nombre',
    'website'          => 'Website',
    'main_phone'       => 'Tel. Principal',
    'secondary_phone'  => 'Tel. Secundario',
    'nationality'      => 'Nacionalidad',
    'select_country'   => 'Selecciona País',
    'address'          => 'Dirección',
    'make_comment'     => 'Hacer Comentario',
    'comments'         => 'Comentarios',
    'send_information' => 'Enviar Información',
    'add_agency'       => 'Agregar Agencia',

];