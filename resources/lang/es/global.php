<?php

return [
    'orderby'        => 'Ordenar',
    'loading'        => 'Procesando',
    'view'           => 'Ver',
    'delete'         => 'Eliminar',
    'search'         => 'Buscar',
    'deactivate'     => 'Desactivar',
    'activate'       => 'Activar',
    'assist'         => 'Asistir',
    'not_found'      => 'No se encontraron registros',
    'response'       => 'Responder',
    'show'           => 'Mostrar',
    'back_dashboard' => 'Volver al Inicio',
    'back'           => 'Volver',
];