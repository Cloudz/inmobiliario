<?php

return [

    //GLOBAL
    'name'                    => 'Nombre',
    'last_name'               => 'Apellido',
    'client_key'              => 'Llave de Cliente',
    'main_email'              => 'Correo Principal',
    'secondary_email'         => 'Correo Secundario',
    'main_mobile'             => 'Celular Principal',
    'secondary_mobile'        => 'Celular Secundario',
    'home_phone'              => 'Teléfono de Casa',
    'office_phone'            => 'Teléfono de Oficina',
    'country'                 => 'País',
    'state'                   => 'Estado',
    'city'                    => 'Ciudad',
    'street'                  => 'Calle',
    'number_int'              => 'Número Interior',
    'number_ext'              => 'Número Exterior',
    'zipcode'                 => 'Código Postal',
    'sex'                     => 'Sexo',
    'user'                    => 'Usuario',
    'contact_date'            => 'Fecha de Contacto',
    'contact_way'             => 'Forma de Contacto',
    'main_source'             => 'Recurso Principal',
    'secondary_source'        => 'Recurso Secundario',
    'contact_medium'          => 'Medio de Contacto',
    'main_medium_source'      => 'Medio Principal',
    'secondary_medium_source' => 'Medio Secundario',
    'interest'                => 'Interés',
    'level_interest'          => 'Nivel de Interés',

    //PROPERTIES
    'description'    => 'Descripción',
    'address'        => 'Dirección',
    'baths'          => 'Baños',
    'beds'           => 'Recámaras',
    'parking'        => 'Estacionamiento',
    'm2'             => 'M2',
    'ft2'            => 'FT2',
    'type'           => 'Tipo',
    'type_id'        => 'Tipo de Propiedad',
    'development_id' => 'Desarrollo',
    'location_id'    => 'Locación',
    'style_id'       => 'Estilo',
    'amenities'      => 'Amenidades',

];