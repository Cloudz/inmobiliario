<?php

return [

    //BTNS
    'button_export_clients'           => 'EXPORTAR CLIENTES',
    'button_export_properties'        => 'EXPORTAR PROPIEDADES',
    'button_export_images_properties' => 'EXPORTAR IMAGENES DE PROPIEDADES',
    'button_import_clients'           => 'IMPORTAR CLIENTES',
    'button_import_properties'        => 'IMPORTAR PROPIEDADES',
    'button_client'                   => 'CREAR CLIENTE',
    'button_agency'                   => 'CREAR AGENCIA',
    'button_agent'                    => 'CREAR AGENTE',

    //NOTIFICATIONS
    'you_have'   => 'Tienes',
    'tasks'      => 'Tareas Incompletas',
    'task'       => 'Tarea Incompleta',
    'calls'      => 'Recordatorios de Llamadas',
    'call'       => 'Recordatorio de Llamada',
    'view_all_a' => 'Ver Todas',
    'view_all_b' => 'Ver Todos',
    'mails'      => 'Recordatorios de Correos',
    'mail'       => 'Recordatorio de Correo',
    'showings'   => 'Exhibiciones Vencidos',
    'showing'    => 'Exhibicion Vencido',
    'messages'   => 'Nuevos Mensajes',
    'message'    => 'Nuevo Mensaje',

];