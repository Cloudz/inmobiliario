<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Cliente',
    'back_list'      => 'Volver al Listado de Clientes',
    'create_section' => 'CREAR CLIENTE',
    'previous'       => 'Anterior',
    'next'           => 'Siguiente',

    //INDEX
    'index_title'   => 'Mis Clientes',
    'index_section' => 'SECCIÓN CLIENTES',
    'add_client'    => 'Agregar Cliente',
    'clean_search'  => 'Limpiar Búsqueda',
    'search_client' => 'Buscar Cliente',
    'complete'      => 'Completo',
    'incomplete'    => 'Incompleto',

    'index_name'           => 'NOMBRE',
    'index_last_name'      => 'APELLIDO',
    'email'                => 'EMAIL',
    'mobile'               => 'CELULAR',
    'index_interest'       => 'INTERÉS',
    'index_level_interest' => 'NIVEL DE INTERÉS',
    'actions'              => 'ACCIONES',
    'status'               => 'ESTATUS',

    //STEP ONE / EDIT / VIEW
    'title_step_one'   => 'Datos Generales - PASO 1',
    'create_name'      => 'Nombre',
    'create_last_name' => 'Apellido',
    'main_email'       => 'Email Principal',
    'verify_client'    => 'Verificar Cliente',
    'select_city'      => 'Selecciona Ciudad',

    //STEP TWO / EDIT / VIEW
    'title_step_two'   => 'Información Contacto',
    'optional'         => '(Opcional)',
    'treatment'        => 'Tratamiento',
    'select_treatment' => 'Seleccionar Tratamiento',
    'secondary_email'  => 'Email Secundario',
    'home_phone'       => 'Tel. Casa',
    'office_phone'     => 'Tel. Oficina',
    'main_mobile'      => 'Cel. Principal',
    'secondary_mobile' => 'Cel. Secundario',

    //STEP THREE / EDIT / VIEW
    'title_step_three'        => 'Información de Marketing - PASO 3',
    'contact_date'            => 'Primer Contacto',
    'choose_date'             => 'Escoger Fecha',
    'sex'                     => 'Sexo',
    'male'                    => 'Masculino',
    'female'                  => 'Femenino',
    'lgbt'                    => 'LGBT',
    'nationality'             => 'Nacionalidad',
    'select_country'          => 'Selecciona País',
    'state'                   => 'Estado',
    'select_state'            => 'Selecciona Estado',
    'city'                    => 'Ciudad',
    'street'                  => 'Calle',
    'number_int'              => 'Número Interno',
    'number_ext'              => 'Número Externo',
    'zipcode'                 => 'C.P.',
    'title_contact_way'       => '¿Cómo te contactó el cliente? - Forma de Contacto',
    'title_contact_medium'    => '¿Cómo te contactó el cliente? - Medio de Contacto',
    'choose_option'           => 'Elegir opción',
    'view_details'            => 'Haz click en cada opción para ver detalles',
    'select_main_source'      => 'Selecciona Recurso Principal',
    'secondary_source'        => 'Recurso Secundario',
    'select_secondary_source' => 'Selecciona Recurso Secundario',
    'interested'              => 'Interesado en',
    'create_level_interest'   => 'Nivel de interés',
    'select_source'           => 'Selecciona Recurso',
    'create'                  => 'Crear',
    'create_source'           => 'Crear Recurso',
    'not_sources'             => 'No Hay Recursos',
    'interest_one'            => 'Poco Interesado',
    'interest_two'            => 'Interesado',
    'interest_three'          => 'Muy Interesado',
    'interest_four'           => 'Urgente',

    //STEP FOUR / EDIT / VIEW
    'end_register'      => 'Fin del registro',
    'comments'          => 'Comentarios',
    'send_notification' => 'Enviar Notificación al Cliente',
    'custom_message'    => 'Mensaje Personalizado',
    'add_title'         => 'Agregar Cliente',

    //VIEW
    'view_title'            => 'Perfil de Cliente',
    'view_section'          => 'PERFIL DE CLIENTE:',
    'key_section'           => 'LLAVE DE CLIENTE:',
    'view_title_one'        => 'Datos Generales',
    'edit_information'      => 'Editar Información de Marketing',
    'title_information'     => 'Información de Marketing Actual',
    'choose_gender'         => 'Escoger Género',
    'address'               => 'Dirección',
    'contact_way'           => 'Forma de Contacto',
    'contact_medium'        => 'Medio de Contacto',
    'main_source'           => 'Recurso Principal - Forma de Contacto',
    'main_medium'           => 'Recurso Principal - Medio de Contacto',
    'view_secondary_source' => 'Recurso Secundario - Forma de Contacto',
    'view_secondary_medium' => 'Recurso Secundario - Medio de Contacto',
    'view_interest'         => 'Interés',
    'make_comment'          => 'Hacer Comentario',
    'send_information'      => 'Enviar Información',
    'enable_edition'        => 'Habilitar Edición',
    'call'                  => 'Llamada',
    'mail'                  => 'Correo',
    'showing'               => 'Demostración',
    'inbox'                 => 'BANDEJA DE ENTRADA',
    'followups'             => 'SEGUIMIENTOS',

    //RE-ASSIGN CLIENTS
    'assign_client'  => 'Re-asignar cliente',
    'agents'         => 'Agentes',
    'select_agent'   => 'Selecciona Agente',
    'agent'          => 'AGENTE',

    //SEND MESSAGE
    'send'         => 'Enviar Mensaje',
    'send_message' => 'Enviar Mensaje a Agentes relacionados con el Cliente',

    //IMPORT
    'import_clients' => 'Importar Clientes',
    'file'           => 'Archivo',

];