<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Propiedad',
    'back_list'      => 'Volver al Listado de Propieades',
    'create_section' => 'CREAR PROPIEDAD',

    //INDEX
    'index_title'     => 'Mis Propiedades',
    'index_section'   => 'SECCIÓN PROPIEDADES',
    'create_property' => 'Crear Propiedad',
    'clean_search'    => 'Limpiar Búsqueda',
    'search_property' => 'Buscar Propiedad',

    'index_name' => 'NOMBRE',
    'actions'    => 'ACCIONES',

    //VIEW
    'view_title'       => 'Perfil de Propiedad',
    'view_section'     => 'PERFIL DE PROPIEDAD',
    'address'          => 'Dirección',
    'make_comment'     => 'Hacer Comentario',
    'comments'         => 'Comentarios',
    'send_information' => 'Enviar Información',
    'add_property'     => 'Agregar Propiedad',

    //IMPORT
    'import_properties' => 'Importar Propiedades',
    'file'              => 'Archivo',

];