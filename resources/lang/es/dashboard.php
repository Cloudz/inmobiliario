<?php

return [

    // GLOBAL
    'last_days'        => 'últimos 7 días',
    'actions'          => 'Acciones',
    'more_information' => 'Más Información',
    'email'            => 'Email',

    // TOTALS
    'clients'   => 'CLIENTES',
    'agencies'  => 'AGENCIAS',
    'agents'    => 'AGENTES',
    'referrors' => 'REFERIDORES',

    // CONTACT FORM
    'title_form'   => 'Mensaje Rápido',
    'subject'      => 'Asunto',
    'message'      => 'Mensaje',
    'file'         => 'Adjuntar Archivo',
    'send'         => 'Enviar',
    'send_message' => 'Enviar Mensaje',

    // LIST LAST CLIENTS
    'title_clients'    => 'Clientes Registrados',
    'status'           => 'Estatus',
    'name'             => 'Nombre',
    'last_name'        => 'Apellido',
    'mobile'           => 'Celular',
    'interest'         => 'Interés',
    'level_interest'   => 'Nivel de Interés',
    'view_all_clients' => 'Ver Todos los Clientes',

    // MESSAGES
    'title_messages'    => 'Bandeja de Entrada',
    'conversation'      => 'Mensajes',
    'title_attachments' => 'Archivos',
    'download_file'     => 'Descargar Archivo',
    'download_image'    => 'Descargar Imagen',

    // LIST LAST VISITS
    'title_visits'    => 'Visitas Registradas',
    'date_visit'      => 'Fecha y Hora',
    'client'          => 'Cliente',
    'office'          => 'Oficina',
    'adviser'         => 'Asesor',
    'view_all_visits' => 'Ver Todas las Visitas',

];