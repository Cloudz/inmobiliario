<?php

return [

    //GLOBAL
    'create_title'   => 'Crear Agente',
    'back_list'      => 'Volver al Listado de Agentes',
    'create_section' => 'CREAR AGENTE - Agencia:',

    //INDEX
    'index_title'   => 'Mis Agentes',
    'index_section' => 'SECCIÓN AGENTES',
    'create_agent'  => 'Crear Agente',
    'clean_search'  => 'Limpiar Búsqueda',
    'search_agent' => 'Buscar Agente',

    'index_name' => 'NOMBRE',
    'email'      => 'EMAIL',
    'phone'      => 'TELÉFONO',
    'agency'     => 'AGENCIA',
    'actions'    => 'ACCIONES',

    //VIEW
    'single_create'    => 'CREAR AGENTE',
    'view_title'       => 'Perfil de Agente',
    'view_section'     => 'PERFIL DE AGENTE - Agencia:',
    'enable_edition'   => 'Habilitar Edición',
    'show_agency'      => 'Ver Agencia',
    'name'             => 'Nombre',
    'view_agency'      => 'Agencia',
    'select_agency'    => 'Seleccionar Agencia',
    'view_email'       => 'Email',
    'view_phone'       => 'Teléfono',
    'is_broker'        => 'Es Corredor',
    'broker'           => 'Corredor',
    'make_comment'     => 'Hacer Comentario',
    'comments'         => 'Comentarios',
    'send_information' => 'Enviar Información',
    'add_agent'        => 'Agregar Agente',

];