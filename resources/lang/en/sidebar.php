<?php

return [

    'users'     => 'USERS',
    'sources'   => 'SOURCES',
        'contact_ways' => 'CONTACT WAYS',
        'main_sources'      => 'MAIN SOURCES',
        'secondary_sources' => 'SECONDARY SOURCES',
    'mediums'   => 'MEDIUMS',
        'contact_mediums' => 'CONTACT MEDIUMS',
        'main_mediums'      => 'MAIN MEDIUMS',
        'secondary_mediums' => 'SECONDARY MEDIUMS',

    'followups' => 'FOLLOW UPS',
    'marketing' => 'MARKETING',
    'schedules' => 'GUARD SCHEDULES',

    'dashboard'  => 'DASHBOARD',
    'clients'    => 'CLIENTS',
    'referrors'  => 'REFERRORS',
    'agencies'   => 'AGENCIES',
    'agents'     => 'AGENTS',
    'tasks'      => 'TASKS',
    'reminders'  => 'REMINDERS',
    'calls'      => 'CALLS',
    'mails'      => 'MAILS',
    'showings'   => 'SHOWINGS',
    'offices'    => 'OFFICES',
    'logbook'    => 'LOGBOOK',
    'properties' => 'PROPERTIES',
        'list_properties' => 'LISTING',
        'developments'    => 'DEVELOPMENTS',
        'amenities'       => 'AMENITIES',
        'locations'       => 'LOCATIONS',
        'types'           => 'TYPES',
        'styles'          => 'STYLES',
];
