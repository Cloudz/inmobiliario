<?php

return [

    //GLOBAL
    'create_title'   => 'Create User',
    'back_list'      => 'Back to Index of Users',
    'create_section' => 'ADD USER',
    'previous'       => 'Previous',
    'next'           => 'Next',

    //INDEX
    'index_title'   => 'Users',
    'index_section' => 'SECTION USERS',
    'add_user'      => 'Create User',
    'clean_search'  => 'Clean Search',
    'search_user'   => 'Search User',

    'index_name'      => 'NAME',
    'index_last_name' => 'LAST NAME',
    'email'           => 'EMAIL',
    'mobile'          => 'MOBILE',
    'index_roles'     => 'ROLES',
    'index_assisted'  => 'ASSISTED AGENTS',
    'actions'         => 'ACTIONS',

    //VIEW
    'name'             => 'Name',
    'last_name'        => 'Last Name',
    'view_email'       => 'Email',
    'view_mobile'      => 'Mobile',
    'password'         => 'Password',
    'verify_pass'      => 'Verify Password',
    'roles'            => 'Roles',
    'send_information' => 'Send Information',
    'enable_edition'   => 'Enable Edition',
    'title_edit'       => 'EDIT USER',
    'update_user'      => 'Update User',

    //ASSIST CLIENTS
    'current_user' => 'User',
    'assist_agent' => 'Assist Agents',
    'agents'       => 'Agents',
    'select_agent' => 'Select Agents to assist',
    'agent'        => 'AGENT',

];