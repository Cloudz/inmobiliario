<?php

return [

    // GLOBAL
    'last_days'        => 'Last 7 days',
    'actions'          => 'Actions',
    'more_information' => 'More Information',
    'email'            => 'Email',

    // TOTALS
    'clients'   => 'CLIENTS',
    'agencies'  => 'AGENCIES',
    'agents'    => 'AGENTS',
    'referrors' => 'REFERRORS',

    // CONTACT FORM
    'title_form'   => 'Quick Mail',
    'subject'      => 'Subject',
    'message'      => 'Message',
    'file'         => 'Attach File',
    'send'         => 'Send',
    'send_message' => 'Send Message',

    // MESSAGES
    'title_messages'    => 'Inbox',
    'conversation'      => 'Messages',
    'title_attachments' => 'Attachments',
    'download_file'     => 'Download File',
    'download_image'    => 'Download Image',

    // LIST LAST CLIENTS
    'title_clients'    => 'Registered Clients',
    'status'           => 'Status',
    'name'             => 'Name',
    'last_name'        => 'Last Name',
    'mobile'           => 'Mobile',
    'interest'         => 'Interest',
    'level_interest'   => 'Level of interest',
    'view_all_clients' => 'View All Clients',

    // LIST LAST VISITS
    'title_visits'    => 'Registered Visits',
    'date_visit'      => 'Date and Time',
    'client'          => 'Client',
    'office'          => 'Office',
    'adviser'         => 'Adviser',
    'view_all_visits' => 'View All Visits',

];