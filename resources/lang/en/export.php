<?php

return [

    //GLOBAL
    'name'                    => 'Name',
    'last_name'               => 'Last Name',
    'client_key'              => 'Client Key',
    'main_email'              => 'Main Email',
    'secondary_email'         => 'Secondary Email',
    'main_mobile'             => 'Main Mobile',
    'secondary_mobile'        => 'Secondary Mobile',
    'home_phone'              => 'Home Phone',
    'office_phone'            => 'Office Phone',
    'country'                 => 'Country',
    'state'                   => 'State',
    'city'                    => 'City',
    'street'                  => 'Street',
    'number_int'              => 'Internal Number',
    'number_ext'              => 'External Number',
    'zipcode'                 => 'Zip Code',
    'sex'                     => 'Sex',
    'user'                    => 'User',
    'contact_date'            => 'Date of Contact',
    'contact_way'             => 'Contact Way',
    'main_source'             => 'Main Source',
    'secondary_source'        => 'Secondary Source',
    'contact_medium'          => 'Contact Medium',
    'main_medium_source'      => 'Main Medium',
    'secondary_medium_source' => 'Secondary Medium',
    'interest'                => 'Interest',
    'level_interest'          => 'Level of Interest',

    //PROPERTIES
    'description'    => 'Description',
    'address'        => 'Address',
    'baths'          => 'Baths',
    'beds'           => 'Beds',
    'parking'        => 'Parking',
    'm2'             => 'M2',
    'ft2'            => 'FT2',
    'type'           => 'Type',
    'type_id'        => 'Type of Property',
    'development_id' => 'Development',
    'location_id'    => 'Location',
    'style_id'       => 'Style',
    'amenities'      => 'Amenities',

];