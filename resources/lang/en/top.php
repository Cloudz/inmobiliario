<?php

return [

    //BTNS
    'button_export_clients'           => 'EXPORT CLIENTS',
    'button_export_properties'        => 'EXPORT PROPERTIES',
    'button_export_images_properties' => 'EXPORT IMAGES PROPERTIES',
    'button_import_clients'           => 'IMPORT CLIENTS',
    'button_import_properties'        => 'IMPORT PROPERTIES',
    'button_client'                   => 'CREATE CLIENT',
    'button_agency'                   => 'CREATE AGENCY',
    'button_agent'                    => 'CREATE AGENT',

    //NOTIFICATIONS
    'you_have'   => 'You Have',
    'tasks'      => 'Incomplete Tasks',
    'task'       => 'Incomplete Task',
    'calls'      => 'Calls Reminders',
    'call'       => 'Call Reminder',
    'view_all_a' => 'View All',
    'view_all_b' => 'View All',
    'mails'      => 'Mails Reminders',
    'mail'       => 'Mail Reminder',
    'showings'   => 'Expired Showings',
    'showing'    => 'Expired Showing',
    'messages'   => 'New Messages',
    'message'    => 'New Message',

];