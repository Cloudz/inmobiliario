<?php

return [

    //GLOBAL
    'create_title'   => 'Create Schedule',
    'back_list'      => 'Back to Index of Schedules',
    'create_section' => 'CREATE SCHEDULE',

    //INDEX
    'index_title'     => 'My Schedules',
    'index_section'   => 'SECTION SCHEDULES',
    'create_schedule' => 'Create Schedule',

    'agent'    => 'SALES AGENT',
    'office'   => 'OFFICE',
    'start_at' => 'START AT',
    'end_at'   => 'END AT',
    'actions'  => 'ACTIONS',

    //CREATE
    'select_agent'     => 'Select Agent',
    'select_office'    => 'Select Office',
    'select_start_at'  => 'Start',
    'select_end_at'    => 'End',
    'comments'         => 'Comments',
    'add_schedule'     => 'Add Schedule',
    'enable_edition'   => 'Enable Edition',
    'send_information' => 'Send Information',

];