<?php

return [

    //GLOBAL
    'create_title'   => 'Create Agency',
    'back_list'      => 'Back to Index of Agencies',
    'create_section' => 'CREATE AGENCY',

    //INDEX
    'index_title'   => 'My Agencies',
    'index_section' => 'SECTION AGENCIES',
    'create_agency' => 'Create Agency',
    'create_agent'  => 'Create Agent',
    'clean_search'  => 'Clean Search',
    'search_agency' => 'Search Agency',

    'index_name'    => 'NAME',
    'index_website' => 'WEBSITE',
    'phone'         => 'PHONE',
    'city'          => 'CITY',
    'actions'       => 'ACTIONS',

    //VIEW
    'view_title'       => 'Agency Profile',
    'view_section'     => 'AGENCY PROFILE',
    'title_agents'     => 'Agents',
    'broker'           => 'Broker',
    'agent'            => 'Sales Agent',
    'enable_edition'   => 'Enable Edition',
    'name'             => 'Name',
    'website'          => 'Website',
    'main_phone'       => 'Main Phone',
    'secondary_phone'  => 'Secondary Phone',
    'nationality'      => 'Nationality',
    'select_country'   => 'Select Country',
    'address'          => 'Address',
    'make_comment'     => 'Make Comment',
    'comments'         => 'Comments',
    'send_information' => 'Send Information',
    'add_agency'       => 'Add Agency',

];