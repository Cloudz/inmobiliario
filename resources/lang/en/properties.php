<?php

return [

    //GLOBAL
    'create_title'   => 'Create Property',
    'back_list'      => 'Back to Index of Properties',
    'create_section' => 'CREATE PROPERTY',

    //INDEX
    'index_title'     => 'My Properties',
    'index_section'   => 'SECTION PROPERTIES',
    'create_property' => 'Create Property',
    'clean_search'    => 'Clean Search',
    'search_property' => 'Search Property',

    'index_name' => 'NAME',
    'actions'    => 'ACTIONS',

    //VIEW
    'view_title'       => 'Property Profile',
    'view_section'     => 'PROPERTY PROFILE',
    'address'          => 'Address',
    'make_comment'     => 'Make Comment',
    'comments'         => 'Comments',
    'send_information' => 'Send Information',
    'add_property'     => 'Add Property',

    //IMPORT
    'import_properties' => 'Import Properties',
    'file'              => 'File',

];