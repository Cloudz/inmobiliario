<?php

return [

    //GLOBAL
    'create_title'   => 'Create Agent',
    'back_list'      => 'Back to Index of Agents',
    'create_section' => 'CREATE AGENT - Agency:',

    //INDEX
    'index_title'   => 'My Agents',
    'index_section' => 'SECTION AGENTS',
    'create_agent'  => 'Create Agent',
    'clean_search'  => 'Clean Search',
    'search_agent' => 'Search Agent',

    'index_name' => 'NAME',
    'email'      => 'EMAIL',
    'phone'      => 'PHONE',
    'agency'     => 'AGENCY',
    'actions'    => 'ACTIONS',

    //VIEW
    'single_create'    => 'CREATE AGENT',
    'view_title'       => 'Agent Profile',
    'view_section'     => 'AGENT PROFILE - Agency',
    'enable_edition'   => 'Enable Edition',
    'show_agency'      => 'View Agency',
    'name'             => 'Name',
    'view_agency'      => 'Agency',
    'select_agency'    => 'Select Agency',
    'view_email'       => 'Email',
    'view_phone'       => 'Phone',
    'is_broker'        => 'Is Broker',
    'broker'           => 'Broker',
    'make_comment'     => 'Make Comment',
    'comments'         => 'Comments',
    'send_information' => 'Send Information',
    'add_agent'        => 'Add Agent',

];