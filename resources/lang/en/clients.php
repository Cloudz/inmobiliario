<?php

return [

    //GLOBAL
    'create_title'   => 'Create Client',
    'back_list'      => 'Back to Index of Clients',
    'create_section' => 'ADD CLIENT',
    'previous'       => 'Previous',
    'next'           => 'Next',

    //INDEX
    'index_title'   => 'My Clients',
    'index_section' => 'SECTION CLIENTS',
    'add_client'    => 'Create Client',
    'clean_search'  => 'Clean Search',
    'search_client' => 'Search Client',
    'complete'      => 'Complete',
    'incomplete'    => 'Incomplete',

    'index_name'           => 'NAME',
    'index_last_name'      => 'LAST NAME',
    'email'                => 'EMAIL',
    'mobile'               => 'MOBILE',
    'index_interest'       => 'INTEREST',
    'index_level_interest' => 'LEVEL INTEREST',
    'actions'              => 'ACTIONS',
    'status'               => 'STATUS',

    //STEP ONE / EDIT / VIEW
    'title_step_one'   => 'General Information - STEP 1',
    'create_name'      => 'Name',
    'create_last_name' => 'Last Name',
    'main_email'       => 'Main Email',
    'verify_client'    => 'Verify Client',
    'select_city'      => 'Select City',

    //STEP TWO / EDIT / VIEW
    'title_step_two'   => 'Contact Information',
    'optional'         => '(Optional)',
    'treatment'        => 'Treatment',
    'select_treatment' => 'Select Treatment',
    'secondary_email'  => 'Secondary Email',
    'home_phone'       => 'Home Phone',
    'office_phone'     => 'Office Phone',
    'main_mobile'      => 'Main Mobile',
    'secondary_mobile' => 'Secondary Mobile',

    //STEP THREE / EDIT / VIEW
    'title_step_three'        => 'Marketing Information - STEP 3',
    'contact_date'            => 'First Contact',
    'choose_date'             => 'Choose Date',
    'sex'                     => 'Gender',
    'male'                    => 'Male',
    'female'                  => 'Female',
    'lgbt'                    => 'LGBT',
    'nationality'             => 'Nationality',
    'select_country'          => 'Select Country',
    'state'                   => 'State',
    'select_state'            => 'Select State',
    'city'                    => 'City',
    'street'                  => 'Street',
    'number_int'              => 'Interior Number',
    'number_ext'              => 'Street Number',
    'zipcode'                 => 'Zip Code',
    'title_contact_way'       => 'How did the client contact you? - Contact Way',
    'title_contact_medium'    => 'How did the client contact you? - Contact Medium',
    'choose_option'           => 'Choose option',
    'view_details'            => 'Click on each option to see details',
    'select_main_source'      => 'Select Main Resource',
    'secondary_source'        => 'Secondary Resource',
    'select_secondary_source' => 'Select Secondary Resource',
    'interested'              => 'Interested in',
    'create_level_interest'   => 'Level of interest',
    'select_source'           => 'Select Resource',
    'create'                  => 'Create',
    'create_source'           => 'Create Resource',
    'not_sources'             => 'No Resources',
    'interest_one'            => 'Little Interested',
    'interest_two'            => 'Interested',
    'interest_three'          => 'Very Interested',
    'interest_four'           => 'Urgent',

    //STEP FOUR / EDIT / VIEW
    'end_register'      => 'End of registration',
    'comments'          => 'Comments',
    'send_notification' => 'Send Notification to the Client',
    'custom_message'    => 'Custom Message',
    'add_title'         => 'Add Client',

    //VIEW
    'view_title'            => 'Client Profile',
    'view_section'          => 'CLIENT PROFILE:',
    'key_section'           => 'CLIENT KEY:',
    'view_title_one'        => 'General Information',
    'edit_information'      => 'Edit Marketing Information',
    'title_information'     => 'Current Marketing Information',
    'choose_gender'         => 'Choose Gender',
    'address'               => 'Address',
    'contact_way'           => 'Contact Way',
    'contact_medium'        => 'Contact Medium',
    'main_source'           => 'Main Resource - Contact Way',
    'main_medium'           => 'Main Resource - Contact Medium',
    'view_secondary_source' => 'Secondary Resource - Contact Way',
    'view_secondary_medium' => 'Secondary Resource - Contact Medium',
    'view_interest'         => 'Interest',
    'make_comment'          => 'Make Comment',
    'send_information'      => 'Send Information',
    'enable_edition'        => 'Enable Edition',
    'call'                  => 'Call',
    'mail'                  => 'Mail',
    'showing'               => 'Showing',
    'inbox'                 => 'INBOX',
    'followups'             => 'FOLLOWUPS',

    //RE-ASSIGN CLIENTS
    'assign_client'  => 'Re-assign client',
    'agents'         => 'Agents',
    'select_agent'   => 'Select Agent',
    'agent'          => 'AGENT',

    //SEND MESSAGE
    'send'         => 'Send Message',
    'send_message' => 'Send Message to Agents related to the Client',

    //IMPORT
    'import_clients' => 'Import Clients',
    'file'           => 'File',

];