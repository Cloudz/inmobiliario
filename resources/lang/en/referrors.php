<?php

return [

    //GLOBAL
    'create_title'   => 'Create Referror',
    'back_list'      => 'Back to Index of Referrors',
    'create_section' => 'CREATE REFERROR',

    //INDEX
    'index_title'     => 'My Referrrors',
    'index_section'   => 'SECTION REFERIDORES',
    'create_referror'    => 'Create Referror',
    'clean_search'    => 'Clean Search',
    'search_referror' => 'Search Referror',

    'index_name'        => 'NAME',
    'index_last_name'   => 'LAST NAME',
    'email'             => 'EMAIL',
    'phone'             => 'PHONE',
    'index_occupation'  => 'OCCUPATION',
    'clients_referrors' => 'REFERRED CUSTOMERS',
    'actions'           => 'ACTIONS',

    //CREATE
    'user'              => 'User',
    'select_user'       => 'Select User',

    //VIEW
    'view_title'        => 'Referror Profile',
    'view_section'      => 'REFERROR PROFILE',
    'enable_edition'    => 'Enable Edition',
    'name'              => 'Name',
    'last_name'         => 'Last Name',
    'main_email'        => 'Main Email',
    'secondary_email'   => 'Secondary Email',
    'home_phone'        => 'Home Phone',
    'office_phone'      => 'Office Phone',
    'main_mobile'       => 'Main Mobile',
    'secondary_mobile'  => 'Secondary Mobile',
    'sex'               => 'Sex',
    'male'              => 'Male',
    'female'            => 'Female',
    'lgbt'              => 'LGBT',
    'nationality'       => 'Nationality',
    'select_country'    => 'Select Country',
    'view_occupation'   => 'Occupation',
    'address'           => 'Address',
    'related_customers' => 'Related Customers',
    'comments'          => 'Comments',
    'make_comment'      => 'Make Comment',
    'send_information'  => 'Send Information',
    'add_referror'      => 'Add Referror',

    //RE-ASSIGN CLIENTS
    'agent' => 'AGENT',

];