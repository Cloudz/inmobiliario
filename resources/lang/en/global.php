<?php

return [
    'orderby'        => 'Sort By',
    'loading'        => 'Loading',
    'view'           => 'View',
    'delete'         => 'Delete',
    'search'         => 'Search',
    'deactivate'     => 'Deactivate',
    'activate'       => 'Activate',
    'assist'         => 'Assist',
    'not_found'      => 'No records found',
    'response'       => 'Response',
    'show'           => 'Show',
    'back_dashboard' => 'Back to Dashboard',
    'back'           => 'Back',
];